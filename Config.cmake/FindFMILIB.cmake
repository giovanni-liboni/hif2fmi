cmake_minimum_required (VERSION 2.8.11)

if (FMILIB_USE_SHARED_LIB)
	set (_fmilib_name "fmilib_shared")
	set (_fmilib_type "SHARED")
else ()
	set (_fmilib_name "fmilib")
	set (_fmilib_type "STATIC")
endif ()

find_library (_fmilib_library ${_fmilib_name}
	PATHS $ENV{FMIL_HOME}
	PATH_SUFFIXES "lib")
find_path (FMILIB_INCLUDE_DIRS "fmilib.h"
	PATHS $ENV{FMIL_HOME}
	PATH_SUFFIXES "include")

if (_fmilib_library)
	add_library ("fmilib" ${_fmilib_type} IMPORTED)
	set_target_properties ("fmilib" PROPERTIES
		IMPORTED_CONFIGURATIONS "RELEASE"
		IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
		IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "${CMAKE_DL_LIBS}"
		IMPORTED_LOCATION_RELEASE "${_fmilib_library}"
		INTERFACE_INCLUDE_DIRECTORIES "${FMILIB_INCLUDE_DIRS}"
		)
	set (FMILIB_LIBRARIES "fmilib")
endif ()

unset (_fmilib_name)
unset (_fmilib_type)
unset (_fmilib_library)

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (FMILIB DEFAULT_MSG FMILIB_LIBRARIES FMILIB_INCLUDE_DIRS)

