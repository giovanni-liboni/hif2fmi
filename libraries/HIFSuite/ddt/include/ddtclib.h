#ifndef DDT_C_LIB__H
#define DDT_C_LIB__H

// Library global include
#include <stdint.h>
#include "ddtclib/config.h"

// bool typedef definition for a C compilation
#ifndef __cplusplus
typedef int32_t bool;
#define false 0L
#define true 1L
#endif

#include "ddtclib/hif_multiple_assign.h"
#include "ddtclib/hif_member_assign.h"
#include "ddtclib/hif_mask.h"
#include "ddtclib/hif_aggregate.h"
#include "ddtclib/hif_bitwise_reduction.h"

#endif
