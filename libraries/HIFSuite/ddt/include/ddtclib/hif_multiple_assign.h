#ifndef DDT_C_LIB_MULTIPLE_ASSIGN__H
#define DDT_C_LIB_MULTIPLE_ASSIGN__H

#include "config.h"

#ifdef __cplusplus
#include <systemc>
#endif

#ifdef __cplusplus
extern "C" {
#endif

DDTCLIB_EXPORT
void uintToArrayAssign(
    bool* target,
    uint64_t source,
    uint32_t tleft,
    uint32_t tright,
    uint32_t sright);

DDTCLIB_EXPORT
uint64_t arrayToUintAssign(
    bool* source,
    uint32_t size);

DDTCLIB_EXPORT
int64_t arrayToIntAssign(
    bool* source,
    uint32_t size);

#ifdef __cplusplus
} // extern C
#endif

#ifdef __cplusplus

// ////////////////////////////////
// uintToArrayAssign
// ////////////////////////////////

DDTCLIB_EXPORT
void uintToArrayAssign(
    sc_core::sc_signal< bool >* sTarget,
    uint64_t source,
    uint32_t tleft,
    uint32_t tright,
    uint32_t sright);

DDTCLIB_EXPORT
void uintToArrayAssign(
    sc_core::sc_vector < sc_core::sc_signal< bool > >& sTarget,
    uint64_t source,
    uint32_t tleft,
    uint32_t tright,
    uint32_t sright);


// ////////////////////////////////
// arrayToUintAssign
// ////////////////////////////////

DDTCLIB_EXPORT
uint64_t arrayToUintAssign(
    sc_core::sc_signal< bool >* sSource,
    uint32_t size);

DDTCLIB_EXPORT
uint64_t arrayToUintAssign(
    const sc_core::sc_vector< sc_core::sc_signal< bool > >& sSource,
    uint32_t size);


// ////////////////////////////////
// arrayToIntAssign
// ////////////////////////////////

DDTCLIB_EXPORT
int64_t arrayToIntAssign(
    sc_core::sc_signal< bool >* sSource,
    uint32_t size);

DDTCLIB_EXPORT
int64_t arrayToIntAssign(
    const sc_core::sc_vector< sc_core::sc_signal< bool > >& sSource,
    uint32_t size);


// ////////////////////////////////
// arrayArrayBoolToArrayUintAssign
// ////////////////////////////////

// sig[][] - sig[]
template<typename T, int S>
void arrayArrayBoolToArrayUintAssign(
    sc_core::sc_signal<bool> source[][S],
    sc_core::sc_signal< T >* target,
    uint32_t arraySize,
    uint32_t subArraySize);

// vect< vect<sig> > - sig[]
template<typename T>
void arrayArrayBoolToArrayUintAssign(
    const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal<bool> > >& source,
    sc_core::sc_signal< T >* target,
    uint32_t arraySize,
    uint32_t subArraySize);

// sig[][] - vect<sig>
template<typename T, int S>
void arrayArrayBoolToArrayUintAssign(
    sc_core::sc_signal<bool> source[][S],
    sc_core::sc_vector< sc_core::sc_signal< T > >& target,
    uint32_t arraySize,
    uint32_t subArraySize);

// vect< vect<sig> > - vect<sig>
template<typename T>
void arrayArrayBoolToArrayUintAssign(
    const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal<bool> > >& source,
    sc_core::sc_vector< sc_core::sc_signal< T > >& target,
    uint32_t arraySize,
    uint32_t subArraySize);

// var[][] - var[]
template<typename T, int S>
void arrayArrayBoolToArrayUintAssign(
    bool source[][S],
    T target[],
    uint32_t arraySize,
    uint32_t subArraySize);


// ////////////////////////////////
// arrayArrayBoolToArrayIntAssign
// ////////////////////////////////

// sig[][] - sig[]
template<typename T, int S>
void arrayArrayBoolToArrayIntAssign(
    sc_core::sc_signal<bool> source[][S],
    sc_core::sc_signal< T >* target,
    uint32_t arraySize,
    uint32_t subArraySize);

// vect< vect<sig> > - sig[]
template<typename T>
void arrayArrayBoolToArrayIntAssign(
    const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal<bool> > >& source,
    sc_core::sc_signal< T >* target,
    uint32_t arraySize,
    uint32_t subArraySize);

// sig[][] - vect<sig>
template<typename T, int S>
void arrayArrayBoolToArrayIntAssign(
    sc_core::sc_signal<bool> source[][S],
    sc_core::sc_vector< sc_core::sc_signal< T > >& target,
    uint32_t arraySize,
    uint32_t subArraySize);

// vect< vect<sig> > - vect<sig>
template<typename T>
void arrayArrayBoolToArrayIntAssign(
    const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal<bool> > >& source,
    sc_core::sc_vector< sc_core::sc_signal< T > >& target,
    uint32_t arraySize,
    uint32_t subArraySize);

// var[][] - var[]
template<typename T, int S>
void arrayArrayBoolToArrayIntAssign(
    bool source[][S],
    T target[],
    uint32_t arraySize,
    uint32_t subArraySize);


// ////////////////////////////////
// arrayUintToArrayArrayBoolAssign
// ////////////////////////////////

// sig[] - sig[][]
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    sc_core::sc_signal<T>* source,
    sc_core::sc_signal< bool >* target,
    uint32_t arraySize,
    uint32_t subArraySize);

// sig[] - vect< vect<sig> >
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    sc_core::sc_signal<T>* source,
    sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal< bool > > >& target,
    uint32_t arraySize,
    uint32_t subArraySize);

// vect<sig> - sig[][]
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    const sc_core::sc_vector< sc_core::sc_signal<T> >& source,
    sc_core::sc_signal< bool >* target,
    uint32_t arraySize,
    uint32_t subArraySize);

// vect<sig> - vect< vect<sig> >
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    const sc_core::sc_vector< sc_core::sc_signal<T> >& source,
    sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal< bool > > >& target,
    uint32_t arraySize,
    uint32_t subArraySize);

// var[] - var[][]
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    T source[],
    bool* target,
    uint32_t arraySize,
    uint32_t subArraySize);


/////////////////////////////////////////
// Template-implementation header
/////////////////////////////////////////

#include "hif_multiple_assign.i.h"

#endif // __cplusplus

#endif // DDT_C_LIB_MULTIPLE_ASSIGN__H
