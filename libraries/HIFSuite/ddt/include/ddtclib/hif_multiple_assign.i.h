#ifndef DDT_C_LIB_MULTIPLE_ASSIGN_I_H
#define DDT_C_LIB_MULTIPLE_ASSIGN_I_H

#include "hif_multiple_assign.h"

// ////////////////////////////////
// arrayArrayBoolToArrayUintAssign
// ////////////////////////////////

// sig[][] - sig[]
template<typename T, int S>
void arrayArrayBoolToArrayUintAssign(
    sc_core::sc_signal<bool> source[][S],
    sc_core::sc_signal< T >* target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToUintAssign(source[i], subArraySize));
    }
}

// vect< vect<sig> > - sig[]
template<typename T>
void arrayArrayBoolToArrayUintAssign(
    const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal<bool> > >& source,
    sc_core::sc_signal< T >* target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToUintAssign(source[i], subArraySize));
    }
}

// sig[][] - vect<sig>
template<typename T, int S>
void arrayArrayBoolToArrayUintAssign(
    sc_core::sc_signal<bool> source[][S],
    sc_core::sc_vector< sc_core::sc_signal< T > >& target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToUintAssign(source[i], subArraySize));
    }
}

// vect< vect<sig> > - vect<sig>
template<typename T>
void arrayArrayBoolToArrayUintAssign(
    const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal<bool> > >& source,
    sc_core::sc_vector< sc_core::sc_signal< T > >& target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToUintAssign(source[i], subArraySize));
    }
}

// var[][] - var[]
template<typename T, int S>
void arrayArrayBoolToArrayUintAssign(
    bool source[][S],
    T target[],
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToUintAssign(source[i], subArraySize));
    }
}


// ////////////////////////////////
// arrayArrayBoolToArrayIntAssign
// ////////////////////////////////

// sig[][] - sig[]
template<typename T, int S>
void arrayArrayBoolToArrayIntAssign(
    sc_core::sc_signal<bool> source[][S],
    sc_core::sc_signal< T >* target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToIntAssign(source[i], subArraySize));
    }
}

// vect< vect<sig> > - sig[]
template<typename T>
void arrayArrayBoolToArrayIntAssign(
    const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal<bool> > >& source,
    sc_core::sc_signal< T >* target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToIntAssign(source[i], subArraySize));
    }
}

// sig[][] - vect<sig>
template<typename T, int S>
void arrayArrayBoolToArrayIntAssign(
    sc_core::sc_signal<bool> source[][S],
    sc_core::sc_vector< sc_core::sc_signal< T > >& target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToIntAssign(source[i], subArraySize));
    }
}

// vect< vect<sig> > - vect<sig>
template<typename T>
void arrayArrayBoolToArrayIntAssign(
    const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal<bool> > >& source,
    sc_core::sc_vector< sc_core::sc_signal< T > >& target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToIntAssign(source[i], subArraySize));
    }
}

// var[][] - var[]
template<typename T, int S>
void arrayArrayBoolToArrayIntAssign(
    bool source[][S],
    T target[],
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        target[i] = static_cast<T>(
                        arrayToIntAssign(source[i], subArraySize));
    }
}


// ////////////////////////////////
// arrayUintToArrayArrayBoolAssign
// ////////////////////////////////

// sig[] - sig[][]
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    sc_core::sc_signal<T>* source,
    sc_core::sc_signal< bool >* target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        uintToArrayAssign(&target[i * subArraySize], source[i], subArraySize - 1U, 0U, 0U);
    }
}

// sig[] - vect< vect<sig> >
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    sc_core::sc_signal<T>* source,
    sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal< bool > > >& target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        uintToArrayAssign(target[i], source[i], subArraySize - 1U, 0U, 0U);
    }
}

// vect<sig> - sig[][]
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    const sc_core::sc_vector< sc_core::sc_signal<T> >& source,
    sc_core::sc_signal< bool >* target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        uintToArrayAssign(&target[i * subArraySize], source[i], subArraySize - 1U, 0U, 0U);
    }
}

// vect<sig> - vect< vect<sig> >
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    const sc_core::sc_vector< sc_core::sc_signal<T> >& source,
    sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal< bool > > >& target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        uintToArrayAssign(target[i], source[i], subArraySize - 1U, 0U, 0U);
    }
}

// var[] - var[]
template<typename T>
void arrayUintToArrayArrayBoolAssign(
    T source[],
    bool* target,
    uint32_t arraySize,
    uint32_t subArraySize)
{
    for (uint32_t i = 0U; i < arraySize; ++i)
    {
        uintToArrayAssign(&target[i * subArraySize], source[i], subArraySize - 1U, 0U, 0U);
    }
}

#endif // DDT_C_LIB_MULTIPLE_ASSIGN_I_H
