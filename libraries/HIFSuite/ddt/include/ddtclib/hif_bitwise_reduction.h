#ifndef DDT_C_LIB_BITWISE_REDUCTION__H
#define DDT_C_LIB_BITWISE_REDUCTION__H

#ifdef __cplusplus
#include <systemc>
#endif

#ifdef __cplusplus
extern "C" {
#endif

DDTCLIB_EXPORT
bool andReduce(const uint64_t value, const uint32_t width);

DDTCLIB_EXPORT
bool orReduce(const uint64_t value, const uint32_t width);

DDTCLIB_EXPORT
bool xorReduce(const uint64_t value, const uint32_t width);

#ifdef __cplusplus
} // extern C
#endif

#endif
