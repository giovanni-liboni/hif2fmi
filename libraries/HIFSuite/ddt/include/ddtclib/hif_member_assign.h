#ifndef DDT_C_LIB_MEMBER_ASSIGN__H
#define DDT_C_LIB_MEMBER_ASSIGN__H

#ifdef __cplusplus
extern "C" {
#endif

DDTCLIB_EXPORT
uint64_t memberAssign( uint64_t target, uint32_t position, bool source);

DDTCLIB_EXPORT
uint64_t memberAssign2( uint64_t target, uint32_t position, uint64_t
                        source, uint32_t position2 );

#ifdef __cplusplus
} // extern C
#endif

#endif
