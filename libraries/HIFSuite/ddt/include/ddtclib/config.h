// /////////////////////////////////////////////////////////////
// Copyright (C) 2012
// EDALab S.r.l.
// All rights reserved.
//
// This file is part of ddtclib.
//
// ddtclib is closed non-free software:
// you can use it only with written license permission.
// If you have not a license, please delete this file.
//
// ddtclib is distributed WITHOUT ANY WARRANTY.
// See the License for more details.
//
// You should have received a copy of the License along with
// ddtclib, in a file named LICENSE.txt.
// /////////////////////////////////////////////////////////////

#ifndef DDTCLIB_CONFIG_H_
#define DDTCLIB_CONFIG_H_

#include <algorithm>

#if (defined _MSC_VER)

#if (defined COMPILE_DDTCLIB_LIB)
// Compiling dynamic ddtclib
#define DDTCLIB_EXPORT __declspec(dllexport)
#elif (defined USE_DDTCLIB_LIB)
// Linking dynamic ddtclib
#define DDTCLIB_EXPORT __declspec(dllimport)
#else
// Compiling/using static ddtclib
#define DDTCLIB_EXPORT
#endif

#else // _WIN32
#define DDTCLIB_EXPORT __attribute__ ((visibility ("default")))
#endif

#endif /* CONFIG_HH_ */
