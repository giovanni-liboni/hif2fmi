#ifndef DDT_C_LIB_MASK__H
#define DDT_C_LIB_MASK__H

#include "config.h"

#ifdef __cplusplus
extern "C" {
#endif

DDTCLIB_EXPORT
uint64_t getMask(const uint32_t left, const uint32_t right);

DDTCLIB_EXPORT
uint64_t getMask0(const uint32_t left, const uint32_t right);

DDTCLIB_EXPORT
int64_t getSignedMask(const int64_t value, const uint32_t width);

DDTCLIB_EXPORT
int64_t signExtend(const uint64_t value, const uint32_t width);

#ifdef __cplusplus
}
#endif


#endif
