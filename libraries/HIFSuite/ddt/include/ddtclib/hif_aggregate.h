#ifndef DDT_C_LIB_AGGREGATE__H
#define DDT_C_LIB_AGGREGATE__H

#ifdef __cplusplus
#include <systemc>
#endif

#ifdef __cplusplus
extern "C" {
#endif

DDTCLIB_EXPORT
uint64_t aggregateToUint(bool source, uint32_t size);

#ifdef __cplusplus
} // extern C
#endif

#ifdef __cplusplus

DDTCLIB_EXPORT
uint64_t aggregateToUint(sc_core::sc_signal< bool > sSource, uint32_t size);

#endif


#endif
