var classhif_1_1ReferencedType =
[
    [ "ReferencedType", "classhif_1_1ReferencedType.html#a7a75adfc327c26d83dd30ba81580dfef", null ],
    [ "~ReferencedType", "classhif_1_1ReferencedType.html#a0fb476e052e4046ca6274ee30bf9b4a5", null ],
    [ "_calculateFields", "classhif_1_1ReferencedType.html#aa45c2703e36404819df3cc87b21391f8", null ],
    [ "_getFieldName", "classhif_1_1ReferencedType.html#aa87f3960bfd50acf5515eacc1890811b", null ],
    [ "getInstance", "classhif_1_1ReferencedType.html#af7708b2862d54bd72f0296f169780925", null ],
    [ "setInstance", "classhif_1_1ReferencedType.html#aacc88aff988123bb204bc0bf12f4de4f", null ],
    [ "_instance", "classhif_1_1ReferencedType.html#ac22054c679c7c01b2ab23f0c118ac389", null ]
];