var structhif_1_1CopyOptions =
[
    [ "UserFunction", "structhif_1_1CopyOptions.html#ac7371b79c725c01d87611fb51738cd75", null ],
    [ "CopyOptions", "structhif_1_1CopyOptions.html#a67a43fccad7d00eda57ada8353057a03", null ],
    [ "~CopyOptions", "structhif_1_1CopyOptions.html#a2945dd2eaf66cfb77a6e9e80dfe1754b", null ],
    [ "CopyOptions", "structhif_1_1CopyOptions.html#a966bc425e1f6d0df54381027ea17621e", null ],
    [ "operator=", "structhif_1_1CopyOptions.html#a85db86d089d45e85c09405dcfc7be236", null ],
    [ "copyChildObjects", "structhif_1_1CopyOptions.html#aa47c8ae3384653101638b0f377717a64", null ],
    [ "copyCodeInfos", "structhif_1_1CopyOptions.html#a0f85d11c835cdfad5b836b90fff521a4", null ],
    [ "copyComments", "structhif_1_1CopyOptions.html#a9feda32def1e7ee5d291377dd3719687", null ],
    [ "copyDeclarations", "structhif_1_1CopyOptions.html#adec8af35284493484d5dfb1c666ff1d3", null ],
    [ "copyProperties", "structhif_1_1CopyOptions.html#a01c641eb2610472e7422fd628cc3b70d", null ],
    [ "copySemanticsTypes", "structhif_1_1CopyOptions.html#ac7b9235bf66b6d4a5f8f5603b83d7bab", null ],
    [ "userData", "structhif_1_1CopyOptions.html#a8c497bdeda61dfa7d598e20f9a19e66d", null ],
    [ "userFunction", "structhif_1_1CopyOptions.html#afdab42cf9bd8d485fcb45f56a25bf9ab", null ]
];