var classhif_1_1Aggregate =
[
    [ "Aggregate", "classhif_1_1Aggregate.html#a0eb0a472b0e2e4f3d273a85dc5fd04c7", null ],
    [ "~Aggregate", "classhif_1_1Aggregate.html#a56127a1995fc184c5035fb5e068eee84", null ],
    [ "_calculateFields", "classhif_1_1Aggregate.html#a3da082df5e5fa5aa52ac7b4cd532f36e", null ],
    [ "_getBListName", "classhif_1_1Aggregate.html#ac0275eb82903e4858335b96878048fe9", null ],
    [ "_getFieldName", "classhif_1_1Aggregate.html#ad7b7e96a3fa2af515620895ed4b4bd81", null ],
    [ "acceptVisitor", "classhif_1_1Aggregate.html#a9cb96399be6aa15bebaa1c14a308e6c7", null ],
    [ "getClassId", "classhif_1_1Aggregate.html#a8a1fe347012bc478f66c782d40c00f23", null ],
    [ "getOthers", "classhif_1_1Aggregate.html#a61cc76c0876fdf1b9e3b979c783bf387", null ],
    [ "setOthers", "classhif_1_1Aggregate.html#aa61329c90770861020c863a3a40be5f4", null ],
    [ "alts", "classhif_1_1Aggregate.html#a0f02f13bea089bf41abaa2ed951f5611", null ]
];