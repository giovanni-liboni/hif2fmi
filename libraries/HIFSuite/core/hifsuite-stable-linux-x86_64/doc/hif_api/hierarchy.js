var hierarchy =
[
    [ "hif::__conversion::checkSubclass< PARENT, CHILD >", "structhif_1_1____conversion_1_1checkSubclass.html", null ],
    [ "hif::__conversion::Conversion_t< CHILD, PARENT >", "classhif_1_1____conversion_1_1Conversion__t.html", null ],
    [ "hif::__conversion::Conversion_t< T, T >", "classhif_1_1____conversion_1_1Conversion__t_3_01T_00_01T_01_4.html", null ],
    [ "hif::__conversion::internal_rebind_t< isSub, rebind_t >", "structhif_1_1____conversion_1_1internal__rebind__t.html", null ],
    [ "hif::__conversion::internal_rebind_t< true, rebind_t >", "structhif_1_1____conversion_1_1internal__rebind__t_3_01true_00_01rebind__t_01_4.html", null ],
    [ "hif::analysis::AnalyzeProcessOptions", "structhif_1_1analysis_1_1AnalyzeProcessOptions.html", null ],
    [ "hif::analysis::AnalyzeSpansResult", "structhif_1_1analysis_1_1AnalyzeSpansResult.html", null ],
    [ "hif::analysis::AnalyzeSpansResult::ValueIndex", "classhif_1_1analysis_1_1AnalyzeSpansResult_1_1ValueIndex.html", null ],
    [ "hif::analysis::DelayInfos", "structhif_1_1analysis_1_1DelayInfos.html", null ],
    [ "hif::analysis::DelayProperties", "structhif_1_1analysis_1_1DelayProperties.html", null ],
    [ "hif::analysis::IndexInfo", "structhif_1_1analysis_1_1IndexInfo.html", null ],
    [ "hif::analysis::ProcessInfos", "structhif_1_1analysis_1_1ProcessInfos.html", null ],
    [ "hif::analysis::Types< KEY, VALUE >", "structhif_1_1analysis_1_1Types.html", null ],
    [ "hif::applicationUtils::CommandLineParser", "classhif_1_1applicationUtils_1_1CommandLineParser.html", null ],
    [ "hif::applicationUtils::CommandLineParser::DictionaryComparator", "structhif_1_1applicationUtils_1_1CommandLineParser_1_1DictionaryComparator.html", null ],
    [ "hif::applicationUtils::CommandLineParser::Option", "structhif_1_1applicationUtils_1_1CommandLineParser_1_1Option.html", null ],
    [ "hif::applicationUtils::CommandLineParser::ParserOptions", "structhif_1_1applicationUtils_1_1CommandLineParser_1_1ParserOptions.html", null ],
    [ "hif::applicationUtils::ConfigurationManager", "classhif_1_1applicationUtils_1_1ConfigurationManager.html", null ],
    [ "hif::applicationUtils::ConfigurationManager::KeyValues", "structhif_1_1applicationUtils_1_1ConfigurationManager_1_1KeyValues.html", null ],
    [ "hif::applicationUtils::ConfigurationManager::SectionData", "structhif_1_1applicationUtils_1_1ConfigurationManager_1_1SectionData.html", null ],
    [ "hif::applicationUtils::FileStructure", "classhif_1_1applicationUtils_1_1FileStructure.html", null ],
    [ "hif::applicationUtils::hif_option", "structhif_1_1applicationUtils_1_1hif__option.html", null ],
    [ "hif::applicationUtils::StepFileManager", "classhif_1_1applicationUtils_1_1StepFileManager.html", null ],
    [ "hif::applicationUtils::WarningInfo", "structhif_1_1applicationUtils_1_1WarningInfo.html", null ],
    [ "hif::backends::CHifDirStruct", "classhif_1_1backends_1_1CHifDirStruct.html", null ],
    [ "hif::backends::CNode", "classhif_1_1backends_1_1CNode.html", null ],
    [ "hif::backends::CNodeVisitor", "classhif_1_1backends_1_1CNodeVisitor.html", [
      [ "hif::backends::CApplyVisitor", "classhif_1_1backends_1_1CApplyVisitor.html", null ],
      [ "hif::backends::CCheckDirVisitor", "classhif_1_1backends_1_1CCheckDirVisitor.html", null ],
      [ "hif::backends::CDateVisitor", "classhif_1_1backends_1_1CDateVisitor.html", null ],
      [ "hif::backends::CFindVisitor", "classhif_1_1backends_1_1CFindVisitor.html", null ],
      [ "hif::backends::CPrintVisitor", "classhif_1_1backends_1_1CPrintVisitor.html", null ],
      [ "hif::backends::CUpdateVisitor", "classhif_1_1backends_1_1CUpdateVisitor.html", null ]
    ] ],
    [ "hif::backends::CSession", "classhif_1_1backends_1_1CSession.html", null ],
    [ "hif::backends::Properties", "classhif_1_1backends_1_1Properties.html", null ],
    [ "hif::BListHost", "classhif_1_1BListHost.html", [
      [ "hif::BList< hif::Action >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::AggregateAlt >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::DataDeclaration >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Declaration >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::DesignUnit >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::EnumValue >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Field >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Generate >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::IfAlt >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Instance >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Library >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::LibraryDef >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Object >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Parameter >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::ParameterAssign >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Port >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::PortAssign >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Range >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::RecordValueAlt >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::State >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::StateTable >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::SwitchAlt >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::TPAssign >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Transition >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::Value >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::View >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::ViewReference >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::WhenAlt >", "classhif_1_1BList.html", null ],
      [ "hif::BList< hif::WithAlt >", "classhif_1_1BList.html", null ],
      [ "hif::BList< T >", "classhif_1_1BList.html", null ]
    ] ],
    [ "hif::BListHost::iterator", "classhif_1_1BListHost_1_1iterator.html", [
      [ "hif::BList< T >::iterator", "classhif_1_1BList_1_1iterator.html", null ]
    ] ],
    [ "hif::class_map_helper1_t< MapTo_t >", "structhif_1_1class__map__helper1__t.html", null ],
    [ "hif::class_map_helper2_t< MapTo_t >", "structhif_1_1class__map__helper2__t.html", null ],
    [ "hif::CopyOptions", "structhif_1_1CopyOptions.html", null ],
    [ "hif::DeclarationVisitorOptions", "structhif_1_1DeclarationVisitorOptions.html", null ],
    [ "hif::EqualsOptions", "structhif_1_1EqualsOptions.html", null ],
    [ "hif::features::IFeature", "classhif_1_1features_1_1IFeature.html", [
      [ "hif::features::INamedObject", "classhif_1_1features_1_1INamedObject.html", [
        [ "hif::Break", "classhif_1_1Break.html", null ],
        [ "hif::Continue", "classhif_1_1Continue.html", null ],
        [ "hif::Declaration", "classhif_1_1Declaration.html", [
          [ "hif::DataDeclaration", "classhif_1_1DataDeclaration.html", [
            [ "hif::Alias", "classhif_1_1Alias.html", null ],
            [ "hif::Const", "classhif_1_1Const.html", null ],
            [ "hif::EnumValue", "classhif_1_1EnumValue.html", null ],
            [ "hif::Field", "classhif_1_1Field.html", null ],
            [ "hif::Parameter", "classhif_1_1Parameter.html", null ],
            [ "hif::Port", "classhif_1_1Port.html", null ],
            [ "hif::Signal", "classhif_1_1Signal.html", null ],
            [ "hif::ValueTP", "classhif_1_1ValueTP.html", null ],
            [ "hif::Variable", "classhif_1_1Variable.html", null ]
          ] ],
          [ "hif::Scope", "classhif_1_1Scope.html", [
            [ "hif::BaseContents", "classhif_1_1BaseContents.html", [
              [ "hif::Contents", "classhif_1_1Contents.html", null ],
              [ "hif::Generate", "classhif_1_1Generate.html", [
                [ "hif::ForGenerate", "classhif_1_1ForGenerate.html", null ],
                [ "hif::IfGenerate", "classhif_1_1IfGenerate.html", null ]
              ] ]
            ] ],
            [ "hif::DesignUnit", "classhif_1_1DesignUnit.html", null ],
            [ "hif::Entity", "classhif_1_1Entity.html", null ],
            [ "hif::LibraryDef", "classhif_1_1LibraryDef.html", null ],
            [ "hif::StateTable", "classhif_1_1StateTable.html", null ],
            [ "hif::SubProgram", "classhif_1_1SubProgram.html", [
              [ "hif::Function", "classhif_1_1Function.html", null ],
              [ "hif::Procedure", "classhif_1_1Procedure.html", null ]
            ] ],
            [ "hif::System", "classhif_1_1System.html", null ],
            [ "hif::TypeDeclaration", "classhif_1_1TypeDeclaration.html", [
              [ "hif::TypeDef", "classhif_1_1TypeDef.html", null ],
              [ "hif::TypeTP", "classhif_1_1TypeTP.html", null ]
            ] ],
            [ "hif::View", "classhif_1_1View.html", null ]
          ] ],
          [ "hif::State", "classhif_1_1State.html", null ]
        ] ],
        [ "hif::FieldReference", "classhif_1_1FieldReference.html", null ],
        [ "hif::For", "classhif_1_1For.html", null ],
        [ "hif::FunctionCall", "classhif_1_1FunctionCall.html", null ],
        [ "hif::Identifier", "classhif_1_1Identifier.html", null ],
        [ "hif::Instance", "classhif_1_1Instance.html", null ],
        [ "hif::ProcedureCall", "classhif_1_1ProcedureCall.html", null ],
        [ "hif::RecordValueAlt", "classhif_1_1RecordValueAlt.html", null ],
        [ "hif::ReferencedAssign", "classhif_1_1ReferencedAssign.html", [
          [ "hif::PPAssign", "classhif_1_1PPAssign.html", [
            [ "hif::ParameterAssign", "classhif_1_1ParameterAssign.html", null ],
            [ "hif::PortAssign", "classhif_1_1PortAssign.html", null ]
          ] ],
          [ "hif::TPAssign", "classhif_1_1TPAssign.html", [
            [ "hif::TypeTPAssign", "classhif_1_1TypeTPAssign.html", null ],
            [ "hif::ValueTPAssign", "classhif_1_1ValueTPAssign.html", null ]
          ] ]
        ] ],
        [ "hif::ReferencedType", "classhif_1_1ReferencedType.html", [
          [ "hif::Library", "classhif_1_1Library.html", null ],
          [ "hif::TypeReference", "classhif_1_1TypeReference.html", null ],
          [ "hif::ViewReference", "classhif_1_1ViewReference.html", null ]
        ] ],
        [ "hif::While", "classhif_1_1While.html", null ]
      ] ],
      [ "hif::features::ISymbol", "classhif_1_1features_1_1ISymbol.html", [
        [ "hif::features::TemplateSymbolIf< T >", "classhif_1_1features_1_1TemplateSymbolIf.html", null ],
        [ "hif::features::TemplateSymbolIf< DataDeclaration >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::Identifier", "classhif_1_1Identifier.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< Declaration >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::FieldReference", "classhif_1_1FieldReference.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< Entity >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::Instance", "classhif_1_1Instance.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< Function >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::FunctionCall", "classhif_1_1FunctionCall.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< LibraryDef >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::Library", "classhif_1_1Library.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< Parameter >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::ParameterAssign", "classhif_1_1ParameterAssign.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< Port >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::PortAssign", "classhif_1_1PortAssign.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< SubProgram >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::ProcedureCall", "classhif_1_1ProcedureCall.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< TypeDeclaration >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::TypeReference", "classhif_1_1TypeReference.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< TypeTP >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::TypeTPAssign", "classhif_1_1TypeTPAssign.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< ValueTP >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::ValueTPAssign", "classhif_1_1ValueTPAssign.html", null ]
        ] ],
        [ "hif::features::TemplateSymbolIf< View >", "classhif_1_1features_1_1TemplateSymbolIf.html", [
          [ "hif::ViewReference", "classhif_1_1ViewReference.html", null ]
        ] ]
      ] ],
      [ "hif::features::ITypeSpan", "classhif_1_1features_1_1ITypeSpan.html", [
        [ "hif::Array", "classhif_1_1Array.html", null ],
        [ "hif::Bitvector", "classhif_1_1Bitvector.html", null ],
        [ "hif::Int", "classhif_1_1Int.html", null ],
        [ "hif::Real", "classhif_1_1Real.html", null ],
        [ "hif::Signed", "classhif_1_1Signed.html", null ],
        [ "hif::Unsigned", "classhif_1_1Unsigned.html", null ]
      ] ]
    ] ],
    [ "hif::functor_map_helper1_t< map_to_t >", "structhif_1_1functor__map__helper1__t.html", null ],
    [ "hif::functor_map_helper2_t< map_to_t >", "structhif_1_1functor__map__helper2__t.html", null ],
    [ "hif::HifFactory< T >::ListElement< T >", "classhif_1_1HifFactory_1_1ListElement.html", null ],
    [ "hif::HifNodeRef", "classhif_1_1HifNodeRef.html", null ],
    [ "hif::HifQueryBase", "classhif_1_1HifQueryBase.html", [
      [ "hif::HifTypedQuery< T >", "classhif_1_1HifTypedQuery.html", null ],
      [ "hif::HifUntypedQuery", "classhif_1_1HifUntypedQuery.html", null ]
    ] ],
    [ "hif::HifVisitor", "classhif_1_1HifVisitor.html", [
      [ "hif::AncestorVisitor", "classhif_1_1AncestorVisitor.html", null ],
      [ "hif::GuideVisitor", "classhif_1_1GuideVisitor.html", [
        [ "hif::DeclarationVisitor", "classhif_1_1DeclarationVisitor.html", null ],
        [ "hif::semantics::TypeVisitor", "classhif_1_1semantics_1_1TypeVisitor.html", null ]
      ] ],
      [ "hif::MapVisitor1< FinalRebind >", "classhif_1_1MapVisitor1.html", null ],
      [ "hif::MapVisitor2< FinalRebind, Parent2 >", "classhif_1_1MapVisitor2.html", null ]
    ] ],
    [ "hif::InstanceCounter", "classhif_1_1InstanceCounter.html", null ],
    [ "hif::manipulation::AddUniqueObjectOptions", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html", null ],
    [ "hif::manipulation::DefinitionStyle", "structhif_1_1manipulation_1_1DefinitionStyle.html", null ],
    [ "hif::manipulation::FindTopOptions", "structhif_1_1manipulation_1_1FindTopOptions.html", null ],
    [ "hif::manipulation::FixAssignmentOptions", "structhif_1_1manipulation_1_1FixAssignmentOptions.html", null ],
    [ "hif::manipulation::FixBindingOptions", "structhif_1_1manipulation_1_1FixBindingOptions.html", null ],
    [ "hif::manipulation::FixTemplateOptions", "structhif_1_1manipulation_1_1FixTemplateOptions.html", null ],
    [ "hif::manipulation::FixUnsupportedBitsOptions", "structhif_1_1manipulation_1_1FixUnsupportedBitsOptions.html", null ],
    [ "hif::manipulation::FlattenDesignOptions", "structhif_1_1manipulation_1_1FlattenDesignOptions.html", null ],
    [ "hif::manipulation::InstantiateOptions", "structhif_1_1manipulation_1_1InstantiateOptions.html", null ],
    [ "hif::manipulation::LeftHandSideOptions", "structhif_1_1manipulation_1_1LeftHandSideOptions.html", null ],
    [ "hif::manipulation::MatchedInsertType", "structhif_1_1manipulation_1_1MatchedInsertType.html", null ],
    [ "hif::manipulation::MatchObjectOptions", "structhif_1_1manipulation_1_1MatchObjectOptions.html", null ],
    [ "hif::manipulation::MergeTreesOptions", "structhif_1_1manipulation_1_1MergeTreesOptions.html", null ],
    [ "hif::manipulation::PrefixTreeOptions", "structhif_1_1manipulation_1_1PrefixTreeOptions.html", null ],
    [ "hif::manipulation::PropagationOptions", "structhif_1_1manipulation_1_1PropagationOptions.html", null ],
    [ "hif::manipulation::RemoveUnusedDeclarationOptions", "structhif_1_1manipulation_1_1RemoveUnusedDeclarationOptions.html", null ],
    [ "hif::manipulation::ResolveTempalteOptions", "structhif_1_1manipulation_1_1ResolveTempalteOptions.html", null ],
    [ "hif::manipulation::SimplifiedType< T >", "structhif_1_1manipulation_1_1SimplifiedType.html", null ],
    [ "hif::manipulation::SimplifiedType< Cast >", "structhif_1_1manipulation_1_1SimplifiedType_3_01Cast_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< Expression >", "structhif_1_1manipulation_1_1SimplifiedType_3_01Expression_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< FieldReference >", "structhif_1_1manipulation_1_1SimplifiedType_3_01FieldReference_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< For >", "structhif_1_1manipulation_1_1SimplifiedType_3_01For_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< ForGenerate >", "structhif_1_1manipulation_1_1SimplifiedType_3_01ForGenerate_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< If >", "structhif_1_1manipulation_1_1SimplifiedType_3_01If_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< IfGenerate >", "structhif_1_1manipulation_1_1SimplifiedType_3_01IfGenerate_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< Range >", "structhif_1_1manipulation_1_1SimplifiedType_3_01Range_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< Switch >", "structhif_1_1manipulation_1_1SimplifiedType_3_01Switch_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< Type >", "structhif_1_1manipulation_1_1SimplifiedType_3_01Type_01_4.html", null ],
    [ "hif::manipulation::SimplifiedType< Value >", "structhif_1_1manipulation_1_1SimplifiedType_3_01Value_01_4.html", null ],
    [ "hif::manipulation::SimplifyOptions", "structhif_1_1manipulation_1_1SimplifyOptions.html", null ],
    [ "hif::manipulation::SortMissingKind", "structhif_1_1manipulation_1_1SortMissingKind.html", null ],
    [ "hif::manipulation::SortOptions", "structhif_1_1manipulation_1_1SortOptions.html", null ],
    [ "hif::manipulation::SplitConcatTargetOptions", "structhif_1_1manipulation_1_1SplitConcatTargetOptions.html", null ],
    [ "hif::manipulation::TransformCaseOptions", "structhif_1_1manipulation_1_1TransformCaseOptions.html", null ],
    [ "hif::manipulation::ViewDependencyOptions", "structhif_1_1manipulation_1_1ViewDependencyOptions.html", null ],
    [ "hif::MapVisitor2< FinalRebind, Parent2 >::rebind_t< T2 >", "structhif_1_1MapVisitor2_1_1rebind__t.html", null ],
    [ "hif::method_map_helper1_t< Ta, map_to_t >", "structhif_1_1method__map__helper1__t.html", null ],
    [ "hif::method_map_helper2_t< Ta, Tb, map_to_t >", "structhif_1_1method__map__helper2__t.html", null ],
    [ "hif::MonoVisitor< Child >", "classhif_1_1MonoVisitor.html", [
      [ "hif::BiVisitor< Child >", "classhif_1_1BiVisitor.html", null ]
    ] ],
    [ "hif::name", "classhif_1_1name.html", null ],
    [ "hif::NameTable", "classhif_1_1NameTable.html", null ],
    [ "hif::Object", "classhif_1_1Object.html", [
      [ "hif::Action", "classhif_1_1Action.html", [
        [ "hif::Assign", "classhif_1_1Assign.html", null ],
        [ "hif::Break", "classhif_1_1Break.html", null ],
        [ "hif::Continue", "classhif_1_1Continue.html", null ],
        [ "hif::For", "classhif_1_1For.html", null ],
        [ "hif::If", "classhif_1_1If.html", null ],
        [ "hif::Null", "classhif_1_1Null.html", null ],
        [ "hif::ProcedureCall", "classhif_1_1ProcedureCall.html", null ],
        [ "hif::Return", "classhif_1_1Return.html", null ],
        [ "hif::Switch", "classhif_1_1Switch.html", null ],
        [ "hif::Transition", "classhif_1_1Transition.html", null ],
        [ "hif::ValueStatement", "classhif_1_1ValueStatement.html", null ],
        [ "hif::Wait", "classhif_1_1Wait.html", null ],
        [ "hif::While", "classhif_1_1While.html", null ]
      ] ],
      [ "hif::Alt", "classhif_1_1Alt.html", [
        [ "hif::AggregateAlt", "classhif_1_1AggregateAlt.html", null ],
        [ "hif::IfAlt", "classhif_1_1IfAlt.html", null ],
        [ "hif::RecordValueAlt", "classhif_1_1RecordValueAlt.html", null ],
        [ "hif::SwitchAlt", "classhif_1_1SwitchAlt.html", null ],
        [ "hif::WhenAlt", "classhif_1_1WhenAlt.html", null ],
        [ "hif::WithAlt", "classhif_1_1WithAlt.html", null ]
      ] ],
      [ "hif::Declaration", "classhif_1_1Declaration.html", null ],
      [ "hif::GlobalAction", "classhif_1_1GlobalAction.html", null ],
      [ "hif::Type", "classhif_1_1Type.html", [
        [ "hif::CompositeType", "classhif_1_1CompositeType.html", [
          [ "hif::Array", "classhif_1_1Array.html", null ],
          [ "hif::File", "classhif_1_1File.html", null ],
          [ "hif::Pointer", "classhif_1_1Pointer.html", null ],
          [ "hif::Reference", "classhif_1_1Reference.html", null ]
        ] ],
        [ "hif::ReferencedType", "classhif_1_1ReferencedType.html", null ],
        [ "hif::ScopedType", "classhif_1_1ScopedType.html", [
          [ "hif::Enum", "classhif_1_1Enum.html", null ],
          [ "hif::Record", "classhif_1_1Record.html", null ]
        ] ],
        [ "hif::SimpleType", "classhif_1_1SimpleType.html", [
          [ "hif::Bit", "classhif_1_1Bit.html", null ],
          [ "hif::Bitvector", "classhif_1_1Bitvector.html", null ],
          [ "hif::Bool", "classhif_1_1Bool.html", null ],
          [ "hif::Char", "classhif_1_1Char.html", null ],
          [ "hif::Event", "classhif_1_1Event.html", null ],
          [ "hif::Int", "classhif_1_1Int.html", null ],
          [ "hif::Real", "classhif_1_1Real.html", null ],
          [ "hif::Signed", "classhif_1_1Signed.html", null ],
          [ "hif::String", "classhif_1_1String.html", null ],
          [ "hif::Time", "classhif_1_1Time.html", null ],
          [ "hif::Unsigned", "classhif_1_1Unsigned.html", null ]
        ] ]
      ] ],
      [ "hif::TypedObject", "classhif_1_1TypedObject.html", [
        [ "hif::ReferencedAssign", "classhif_1_1ReferencedAssign.html", null ],
        [ "hif::Value", "classhif_1_1Value.html", [
          [ "hif::Aggregate", "classhif_1_1Aggregate.html", null ],
          [ "hif::Cast", "classhif_1_1Cast.html", null ],
          [ "hif::ConstValue", "classhif_1_1ConstValue.html", [
            [ "hif::BitValue", "classhif_1_1BitValue.html", null ],
            [ "hif::BitvectorValue", "classhif_1_1BitvectorValue.html", null ],
            [ "hif::BoolValue", "classhif_1_1BoolValue.html", null ],
            [ "hif::CharValue", "classhif_1_1CharValue.html", null ],
            [ "hif::IntValue", "classhif_1_1IntValue.html", null ],
            [ "hif::RealValue", "classhif_1_1RealValue.html", null ],
            [ "hif::StringValue", "classhif_1_1StringValue.html", null ],
            [ "hif::TimeValue", "classhif_1_1TimeValue.html", null ]
          ] ],
          [ "hif::Expression", "classhif_1_1Expression.html", null ],
          [ "hif::FunctionCall", "classhif_1_1FunctionCall.html", null ],
          [ "hif::Identifier", "classhif_1_1Identifier.html", null ],
          [ "hif::Instance", "classhif_1_1Instance.html", null ],
          [ "hif::PrefixedReference", "classhif_1_1PrefixedReference.html", [
            [ "hif::FieldReference", "classhif_1_1FieldReference.html", null ],
            [ "hif::Member", "classhif_1_1Member.html", null ],
            [ "hif::Slice", "classhif_1_1Slice.html", null ]
          ] ],
          [ "hif::Range", "classhif_1_1Range.html", null ],
          [ "hif::RecordValue", "classhif_1_1RecordValue.html", null ],
          [ "hif::When", "classhif_1_1When.html", null ],
          [ "hif::With", "classhif_1_1With.html", null ]
        ] ]
      ] ]
    ] ],
    [ "hif::Object::CodeInfo", "structhif_1_1Object_1_1CodeInfo.html", null ],
    [ "hif::PrintHifOptions", "structhif_1_1PrintHifOptions.html", null ],
    [ "hif::ReadHifOptions", "structhif_1_1ReadHifOptions.html", null ],
    [ "hif::semantics::AnalyzeParams", "structhif_1_1semantics_1_1AnalyzeParams.html", null ],
    [ "hif::semantics::CheckOptions", "structhif_1_1semantics_1_1CheckOptions.html", null ],
    [ "hif::semantics::DeclarationOptions", "structhif_1_1semantics_1_1DeclarationOptions.html", [
      [ "hif::semantics::GetCandidatesOptions", "structhif_1_1semantics_1_1GetCandidatesOptions.html", null ],
      [ "hif::semantics::UpdateDeclarationOptions", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html", null ]
    ] ],
    [ "hif::semantics::DeclarationsStack", "classhif_1_1semantics_1_1DeclarationsStack.html", null ],
    [ "hif::semantics::GetReferencesOptions", "structhif_1_1semantics_1_1GetReferencesOptions.html", null ],
    [ "hif::semantics::ILanguageSemantics", "classhif_1_1semantics_1_1ILanguageSemantics.html", [
      [ "hif::semantics::HIFSemantics", "classhif_1_1semantics_1_1HIFSemantics.html", null ],
      [ "hif::semantics::SystemCSemantics", "classhif_1_1semantics_1_1SystemCSemantics.html", null ],
      [ "hif::semantics::VerilogSemantics", "classhif_1_1semantics_1_1VerilogSemantics.html", null ],
      [ "hif::semantics::VHDLSemantics", "classhif_1_1semantics_1_1VHDLSemantics.html", null ]
    ] ],
    [ "hif::semantics::ILanguageSemantics::ExpressionTypeInfo", "structhif_1_1semantics_1_1ILanguageSemantics_1_1ExpressionTypeInfo.html", null ],
    [ "hif::semantics::ILanguageSemantics::SemanticOptions", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html", null ],
    [ "hif::semantics::ILanguageSemantics::ValueSymbol", "structhif_1_1semantics_1_1ILanguageSemantics_1_1ValueSymbol.html", null ],
    [ "hif::semantics::ResetDeclarationsOptions", "structhif_1_1semantics_1_1ResetDeclarationsOptions.html", null ],
    [ "hif::semantics::SemanticAnalysis", "classhif_1_1semantics_1_1SemanticAnalysis.html", null ],
    [ "hif::System::VersionInfo", "structhif_1_1System_1_1VersionInfo.html", null ],
    [ "hif::TerminalPrefixOptions", "structhif_1_1TerminalPrefixOptions.html", null ],
    [ "hif::Trash", "classhif_1_1Trash.html", null ],
    [ "ListElement public ListElementHost", null, [
      [ "hif::HifFactory< T >", "classhif_1_1HifFactory.html", null ]
    ] ],
    [ "ListElementHost", null, [
      [ "hif::HifFactory< T >", "classhif_1_1HifFactory.html", null ]
    ] ],
    [ "ostream", null, [
      [ "hif::backends::IndentedStream", "classhif_1_1backends_1_1IndentedStream.html", null ]
    ] ],
    [ "hif::analysis::Types< DataDeclaration >", "structhif_1_1analysis_1_1Types.html", null ]
];