var classhif_1_1String =
[
    [ "String", "classhif_1_1String.html#a336ae12fe6c92c4de47d150fa59edefd", null ],
    [ "~String", "classhif_1_1String.html#aba97c9dc90496970b80db5c160a79461", null ],
    [ "_calculateFields", "classhif_1_1String.html#a700eca0970310dd2f51a93f2b4265bd1", null ],
    [ "_getFieldName", "classhif_1_1String.html#ac5a749fa382f378b8ee1499efd8ea9e5", null ],
    [ "acceptVisitor", "classhif_1_1String.html#a80ea2920e1147de1e0bb4fd576e4e5de", null ],
    [ "getClassId", "classhif_1_1String.html#a9e6a16bb7bd5264f534992350a2bdbe7", null ],
    [ "getSpanInformation", "classhif_1_1String.html#ae7fb7c3090106a59fbb18122da79085c", null ],
    [ "setSpanInformation", "classhif_1_1String.html#adbf129aa271ebf7aec9b22e61d10c1fd", null ],
    [ "toObject", "classhif_1_1String.html#a599007ed09bf8631f89192ee9977da4b", null ]
];