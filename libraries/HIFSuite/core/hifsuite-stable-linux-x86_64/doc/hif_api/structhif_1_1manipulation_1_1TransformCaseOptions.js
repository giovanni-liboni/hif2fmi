var structhif_1_1manipulation_1_1TransformCaseOptions =
[
    [ "TransformCaseOptions", "structhif_1_1manipulation_1_1TransformCaseOptions.html#a267ef1ff813200edcd1cf6c53db778ff", null ],
    [ "~TransformCaseOptions", "structhif_1_1manipulation_1_1TransformCaseOptions.html#a1ac187dd24d3bde943ca91bf7e06a7c9", null ],
    [ "TransformCaseOptions", "structhif_1_1manipulation_1_1TransformCaseOptions.html#aeca8a107b305af423046f0b538b3d909", null ],
    [ "operator=", "structhif_1_1manipulation_1_1TransformCaseOptions.html#a2833f8f5e5d1aff8b49743fe639f72d7", null ],
    [ "swap", "structhif_1_1manipulation_1_1TransformCaseOptions.html#a257801fb199367bd218c6671125f3d7d", null ],
    [ "createdAssign", "structhif_1_1manipulation_1_1TransformCaseOptions.html#af56464bc9eb8f5c3d53a403af43f6675", null ],
    [ "createdVariable", "structhif_1_1manipulation_1_1TransformCaseOptions.html#a85375fccadb428b3f30f79b4eb74a33b", null ],
    [ "fixCondition", "structhif_1_1manipulation_1_1TransformCaseOptions.html#a11563ba2e5ebdda600360869a14c6350", null ],
    [ "fixSignalOrPortCondition", "structhif_1_1manipulation_1_1TransformCaseOptions.html#a6cda505408d8fc964a2bc114350374b1", null ],
    [ "simplify", "structhif_1_1manipulation_1_1TransformCaseOptions.html#a6d0e7e6f9f2101e156e66d36aaa82c50", null ],
    [ "splitCases", "structhif_1_1manipulation_1_1TransformCaseOptions.html#aee3c06ac1860d6ace560b739c34f21fc", null ]
];