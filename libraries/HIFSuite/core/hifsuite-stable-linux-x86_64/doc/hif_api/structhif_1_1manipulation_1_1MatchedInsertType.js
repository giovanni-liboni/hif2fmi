var structhif_1_1manipulation_1_1MatchedInsertType =
[
    [ "type", "structhif_1_1manipulation_1_1MatchedInsertType.html#a15562662eb293cf822151e3a7090c164", [
      [ "TYPE_DELETE", "structhif_1_1manipulation_1_1MatchedInsertType.html#a15562662eb293cf822151e3a7090c164a4c312c7a9df36da6743eb12053d1f136", null ],
      [ "TYPE_ERROR", "structhif_1_1manipulation_1_1MatchedInsertType.html#a15562662eb293cf822151e3a7090c164a783b9162eb00e0bdf217fab85ba57323", null ],
      [ "TYPE_ONLY_REPLACE", "structhif_1_1manipulation_1_1MatchedInsertType.html#a15562662eb293cf822151e3a7090c164ab6f9228d207948d767b5058138648a87", null ],
      [ "TYPE_EXPAND", "structhif_1_1manipulation_1_1MatchedInsertType.html#a15562662eb293cf822151e3a7090c164a44e2d431e30af3f0d8aa51f0f8e53aaa", null ]
    ] ]
];