var classhif_1_1Declaration =
[
    [ "KeywordList", "classhif_1_1Declaration.html#a20b26b921e981055a521392567d7191f", null ],
    [ "Declaration", "classhif_1_1Declaration.html#a8a31b63ddcc850c8a0758797bb62b145", null ],
    [ "~Declaration", "classhif_1_1Declaration.html#a4a0cb21fc16bf75e03d5d426e7e4425b", null ],
    [ "_calculateFields", "classhif_1_1Declaration.html#a6427c69601890e428d51f661ef7b948d", null ],
    [ "addAdditionalKeyword", "classhif_1_1Declaration.html#a1d61a8a87e0229259fc45a8999188039", null ],
    [ "checkAdditionalKeyword", "classhif_1_1Declaration.html#aaed719495a681cd74d67fa1c3c3f83bf", null ],
    [ "clearAdditionalKeywords", "classhif_1_1Declaration.html#acb2cbf4ddd21d102f053330dee84f2eb", null ],
    [ "getAdditionalKeywordsBeginIterator", "classhif_1_1Declaration.html#a5eec33848c88a209fe8704a32d2fde8c", null ],
    [ "getAdditionalKeywordsEndIterator", "classhif_1_1Declaration.html#ac23e89b7e5aead8849596175889c45c4", null ],
    [ "hasAdditionalKeywords", "classhif_1_1Declaration.html#ac634a1e50b8688da6c05dd5aed54e1b7", null ],
    [ "removeAdditionalKeyword", "classhif_1_1Declaration.html#a821ce944270e5a700e499c4b1d11da04", null ],
    [ "toObject", "classhif_1_1Declaration.html#ab8dfcc9165b6c23317b94e876c8fcfc0", null ],
    [ "_additionalKeywords", "classhif_1_1Declaration.html#a86f3c20f4e46c4add4ce9336e177a524", null ]
];