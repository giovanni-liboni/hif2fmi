var structhif_1_1DeclarationVisitorOptions =
[
    [ "DeclarationVisitorOptions", "structhif_1_1DeclarationVisitorOptions.html#a7b76769652f9a74fa3e04a94aeaaad80", null ],
    [ "~DeclarationVisitorOptions", "structhif_1_1DeclarationVisitorOptions.html#ac8aa9beeb27febd9e34a660a12d7842c", null ],
    [ "DeclarationVisitorOptions", "structhif_1_1DeclarationVisitorOptions.html#a333528a92b1c317c207671f971a1cf84", null ],
    [ "operator=", "structhif_1_1DeclarationVisitorOptions.html#a1b787d93e822f4dc7adaa59ecbf95894", null ],
    [ "swap", "structhif_1_1DeclarationVisitorOptions.html#ada39c986b4f8f01f614a63b627e5603d", null ],
    [ "refsMap", "structhif_1_1DeclarationVisitorOptions.html#a34352617d9a461be7517eed982cea0b4", null ],
    [ "sem", "structhif_1_1DeclarationVisitorOptions.html#af0757e1ba438dc28266e9975dd752a61", null ],
    [ "visitDeclarationsOnce", "structhif_1_1DeclarationVisitorOptions.html#a3cc31b546fad8cb11eb26adf20b32499", null ],
    [ "visitReferencesAfterDeclaration", "structhif_1_1DeclarationVisitorOptions.html#a9311cfe941556f2f0314cf8e29193139", null ],
    [ "visitSymbolsOnce", "structhif_1_1DeclarationVisitorOptions.html#a04c549c9770d3afeacd4fc99cfe77e06", null ]
];