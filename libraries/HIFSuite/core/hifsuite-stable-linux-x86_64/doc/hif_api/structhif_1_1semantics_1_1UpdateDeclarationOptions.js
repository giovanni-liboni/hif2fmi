var structhif_1_1semantics_1_1UpdateDeclarationOptions =
[
    [ "UpdateDeclarationOptions", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html#a33121d24f9d670b2e6c14104e14b78e7", null ],
    [ "~UpdateDeclarationOptions", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html#a4950532c9cb9d78a7e305f78ae9ff3f8", null ],
    [ "UpdateDeclarationOptions", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html#a2df73beaecebf96c2417404c8fc15af5", null ],
    [ "operator=", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html#a190a38aafafc7a602bff130106cb1b40", null ],
    [ "operator=", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html#a0436c5ec36218ba035304aa85da1b22a", null ],
    [ "swap", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html#a45a9eebaee5dd19705b6f269352f41ba", null ],
    [ "swap", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html#a69f315167d8e2de87db1516712be1b88", null ],
    [ "onlyVisible", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html#a39c77d4d39b1bf71bfd3be69e2bb5f3c", null ],
    [ "root", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html#a0453ac911d6b6adc2c87c99ce85ba81a", null ]
];