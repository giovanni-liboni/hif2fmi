var classhif_1_1PrefixedReference =
[
    [ "PrefixedReference", "classhif_1_1PrefixedReference.html#abf91c389534514fa3bfd648d055532e8", null ],
    [ "~PrefixedReference", "classhif_1_1PrefixedReference.html#ab6bea155b776708287f3dfe7f04e5659", null ],
    [ "_calculateFields", "classhif_1_1PrefixedReference.html#ad8eb00965c593d40f34475c8e0a27d9d", null ],
    [ "_getFieldName", "classhif_1_1PrefixedReference.html#a8aa628ae004727584a79aa7a2497fdb4", null ],
    [ "getPrefix", "classhif_1_1PrefixedReference.html#a38c35a83c24738d969cc7ccd7a933de9", null ],
    [ "setPrefix", "classhif_1_1PrefixedReference.html#a9d3870e647639310fc403483c381279d", null ],
    [ "_prefix", "classhif_1_1PrefixedReference.html#a417998f1315d7881ecbfef8160c7032a", null ]
];