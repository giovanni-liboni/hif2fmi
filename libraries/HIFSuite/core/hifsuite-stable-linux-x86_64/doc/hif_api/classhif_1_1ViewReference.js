var classhif_1_1ViewReference =
[
    [ "ViewReference", "classhif_1_1ViewReference.html#a311bc442a83b94020baf8fc932e69b6a", null ],
    [ "~ViewReference", "classhif_1_1ViewReference.html#a6d979d73019a95ef51709ec6612f1e34", null ],
    [ "_calculateFields", "classhif_1_1ViewReference.html#a4e703c9fb427559d37fc6920eeeac587", null ],
    [ "_getBListName", "classhif_1_1ViewReference.html#a646fc430359665529ce2efc5059c0d4b", null ],
    [ "acceptVisitor", "classhif_1_1ViewReference.html#ad1b13de0cfd8d145ca91fd42b643ace7", null ],
    [ "getClassId", "classhif_1_1ViewReference.html#a50c09d8956e8c44402747d10fb3dc758", null ],
    [ "getDesignUnit", "classhif_1_1ViewReference.html#a4b7c83a39392c05260d629ac9b2c2030", null ],
    [ "setDesignUnit", "classhif_1_1ViewReference.html#afc6ee2465a08dd7d39fad9b535a81f1b", null ],
    [ "toObject", "classhif_1_1ViewReference.html#adcba7d4291bcd480cbb8ea73fc1f8cb1", null ],
    [ "templateParameterAssigns", "classhif_1_1ViewReference.html#af3b6ac36ea11caf5e6ff2e328e318a7a", null ]
];