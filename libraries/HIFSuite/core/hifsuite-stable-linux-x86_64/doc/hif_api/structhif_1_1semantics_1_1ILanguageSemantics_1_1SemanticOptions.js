var structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions =
[
    [ "ForConditionType", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a781ecea1e82f5658017004a0c54dea10", [
      [ "RANGE", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a781ecea1e82f5658017004a0c54dea10ab2fec2e14db4515552e41dc708f12cad", null ],
      [ "EXPRESSION", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a781ecea1e82f5658017004a0c54dea10a8e3b11fc2347e6dd4ae8a529af4f3778", null ],
      [ "RANGE_AND_EXPRESSION", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a781ecea1e82f5658017004a0c54dea10af0adafe316bdde4fc9107afbbfb8a7d9", null ]
    ] ],
    [ "SemanticOptions", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a091d974e8d6ac877ea1f4dadf9923ba1", null ],
    [ "~SemanticOptions", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a02a00469fb3bd3d0df170c2b0e46e3fc", null ],
    [ "SemanticOptions", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a731d0f0e57a76c9f92a2d4a8ff0bbfca", null ],
    [ "operator=", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#ad3b654e505fa0a1cada2c60c8b5839bd", null ],
    [ "after_isNoAllowed", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#adc00a965a023c60176a723ad6fcb6cfd", null ],
    [ "case_isOnlyLiteral", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a4d61f703926e2b4018e48655638a8c3f", null ],
    [ "dataDeclaration_initialValue", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#aa3514253a6a80e7942e2549301fc765a", null ],
    [ "designUnit_uniqueView", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a7437bd8a716d0120240082eb18c522ab", null ],
    [ "for_conditionType", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a90475d5b5173231fb97a341026623058", null ],
    [ "for_implictIndex", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a557a731ea386daba3e95e19d90ae17de", null ],
    [ "generates_isNoAllowed", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a139d6662dbc938dcecff956dd8cb7b95", null ],
    [ "globact_isNoAllowed", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a7b7e771d6d00400d8c151660b02977e1", null ],
    [ "int_bitfields", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a8356e59c2a067ee35a8a38df30f29cb5", null ],
    [ "lang_has9logic", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a60d2fc9efd3546b4c07f47220949ebc5", null ],
    [ "lang_hasDontCare", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a8444399ae7b92ad74d11895c0135786d", null ],
    [ "lang_signPortNoBitAccess", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a05844c6d41d5f5fac95afdf1c2a74856", null ],
    [ "lang_sortKind", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a5e12ffbc0e93bb0760501fdf5ac505ef", null ],
    [ "port_inNoInitialValue", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a9ce3b146b68158e2a0b0a743ed13c6cb", null ],
    [ "port_outInitialValue", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#af8d65827b12e9a265adccc4d73c19794", null ],
    [ "scopedType_insideTypedef", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#aeaef7ced716e815093ed2904309c093f", null ],
    [ "valueStatement_isNoAllowed", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#a320505af176720addd5f48fa1d77f40f", null ],
    [ "with_isNoAllowed", "structhif_1_1semantics_1_1ILanguageSemantics_1_1SemanticOptions.html#af6c52cbe477f362dc07307f4e5def908", null ]
];