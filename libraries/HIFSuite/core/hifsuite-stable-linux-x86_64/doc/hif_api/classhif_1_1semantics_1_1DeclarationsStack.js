var classhif_1_1semantics_1_1DeclarationsStack =
[
    [ "DeclarationsMap", "classhif_1_1semantics_1_1DeclarationsStack.html#abba91a1e5ff654844983c2aea0be0c9e", null ],
    [ "Stack", "classhif_1_1semantics_1_1DeclarationsStack.html#a74cb9c712cf5ef54e5df089ae4877a64", null ],
    [ "Symbol", "classhif_1_1semantics_1_1DeclarationsStack.html#a8f9c55a8b5a2f396d0b04df544fc6dc6", null ],
    [ "DeclarationsStack", "classhif_1_1semantics_1_1DeclarationsStack.html#a590d3679cf23410dcf9ce865644f5785", null ],
    [ "~DeclarationsStack", "classhif_1_1semantics_1_1DeclarationsStack.html#aa20f0d99baa7ca3eececac660f558d96", null ],
    [ "pop", "classhif_1_1semantics_1_1DeclarationsStack.html#abffd69c57bdd6549203947f9b55f9fcf", null ],
    [ "push", "classhif_1_1semantics_1_1DeclarationsStack.html#afa595e738dcae7ea4b1eb0f9f37a6283", null ]
];