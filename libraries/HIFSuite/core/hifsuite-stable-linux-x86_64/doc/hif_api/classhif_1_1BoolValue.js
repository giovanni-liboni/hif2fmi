var classhif_1_1BoolValue =
[
    [ "BoolValue", "classhif_1_1BoolValue.html#a176d120de244e1e8dc317acd7f879d13", null ],
    [ "BoolValue", "classhif_1_1BoolValue.html#ad8e79502ea05f317122e0fbaf5fcc128", null ],
    [ "~BoolValue", "classhif_1_1BoolValue.html#aaa16864004e15aed46218305430deede", null ],
    [ "_calculateFields", "classhif_1_1BoolValue.html#a5afad3dbbb9dbc2e367791030ce7b11c", null ],
    [ "acceptVisitor", "classhif_1_1BoolValue.html#a3df074f4f215781ea42a14ff92afb49b", null ],
    [ "getClassId", "classhif_1_1BoolValue.html#a73b2192c6404146b9b0a1da642635ad6", null ],
    [ "getValue", "classhif_1_1BoolValue.html#abe7dc331e3cff081b4cda8f36f3d8cda", null ],
    [ "setValue", "classhif_1_1BoolValue.html#a30d4b72b61d549ed85a1d6d0f35b1c1d", null ]
];