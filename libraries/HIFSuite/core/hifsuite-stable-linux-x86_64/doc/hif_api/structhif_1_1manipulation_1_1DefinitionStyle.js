var structhif_1_1manipulation_1_1DefinitionStyle =
[
    [ "Type", "structhif_1_1manipulation_1_1DefinitionStyle.html#a943347d115e44fb7fd9cbd127d59d62c", [
      [ "HIF", "structhif_1_1manipulation_1_1DefinitionStyle.html#a943347d115e44fb7fd9cbd127d59d62cafaad9d6a3b7ddc63af21fab1d82e1ab6", null ],
      [ "VHDL", "structhif_1_1manipulation_1_1DefinitionStyle.html#a943347d115e44fb7fd9cbd127d59d62ca4fe39639c9dad315c01de729b2e6c546", null ],
      [ "VERILOG", "structhif_1_1manipulation_1_1DefinitionStyle.html#a943347d115e44fb7fd9cbd127d59d62cacec28e348ec3a34489ab982283f84dc9", null ],
      [ "SYSTEMC", "structhif_1_1manipulation_1_1DefinitionStyle.html#a943347d115e44fb7fd9cbd127d59d62ca478329f51e4455b7d60cfb138f263bdc", null ]
    ] ]
];