var namespacehif_1_1features =
[
    [ "IFeature", "classhif_1_1features_1_1IFeature.html", "classhif_1_1features_1_1IFeature" ],
    [ "INamedObject", "classhif_1_1features_1_1INamedObject.html", "classhif_1_1features_1_1INamedObject" ],
    [ "ISymbol", "classhif_1_1features_1_1ISymbol.html", "classhif_1_1features_1_1ISymbol" ],
    [ "ITypeSpan", "classhif_1_1features_1_1ITypeSpan.html", "classhif_1_1features_1_1ITypeSpan" ],
    [ "TemplateSymbolIf", "classhif_1_1features_1_1TemplateSymbolIf.html", "classhif_1_1features_1_1TemplateSymbolIf" ]
];