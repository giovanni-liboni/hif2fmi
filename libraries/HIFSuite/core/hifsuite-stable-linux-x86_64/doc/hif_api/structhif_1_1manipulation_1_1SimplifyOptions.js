var structhif_1_1manipulation_1_1SimplifyOptions =
[
    [ "Behavior", "structhif_1_1manipulation_1_1SimplifyOptions.html#a235f1dbfc6a5e3cfc40e0010fb07c3b4", [
      [ "BEHAVIOR_CONSERVATIVE", "structhif_1_1manipulation_1_1SimplifyOptions.html#a235f1dbfc6a5e3cfc40e0010fb07c3b4ab668f7371d78c6be829793ae0da7af8e", null ],
      [ "BEHAVIOR_NORMAL", "structhif_1_1manipulation_1_1SimplifyOptions.html#a235f1dbfc6a5e3cfc40e0010fb07c3b4a82cb8ca0e306ea4721941f25417f47fe", null ],
      [ "BEHAVIOR_AGGRESSIVE", "structhif_1_1manipulation_1_1SimplifyOptions.html#a235f1dbfc6a5e3cfc40e0010fb07c3b4a1f2880831af052dbaf07e313b0cb46f0", null ]
    ] ],
    [ "SimplifyOptions", "structhif_1_1manipulation_1_1SimplifyOptions.html#a393accb46c6912a9f6db5be07ce55168", null ],
    [ "~SimplifyOptions", "structhif_1_1manipulation_1_1SimplifyOptions.html#a90b84ef3780f813c3a37ef3acb60a9ff", null ],
    [ "SimplifyOptions", "structhif_1_1manipulation_1_1SimplifyOptions.html#ab48b0be7e9196ba1989604ee7f73773d", null ],
    [ "operator=", "structhif_1_1manipulation_1_1SimplifyOptions.html#a10cc582c025494ed48989c0aeb5e82c5", null ],
    [ "behavior", "structhif_1_1manipulation_1_1SimplifyOptions.html#abddbc4bb1cf0e128acd2fda6cf0d4cab", null ],
    [ "context", "structhif_1_1manipulation_1_1SimplifyOptions.html#a42e0f350bcff0c166dfae6045a898b55", null ],
    [ "replace_result", "structhif_1_1manipulation_1_1SimplifyOptions.html#abae67f64f89d7d354a1d7e003a40e2f5", null ],
    [ "root", "structhif_1_1manipulation_1_1SimplifyOptions.html#a3e643f0dded4eeacc958786ed7103f82", null ],
    [ "simplify_constants", "structhif_1_1manipulation_1_1SimplifyOptions.html#aee35fcd64aa83929f4d796555dbdb5e0", null ],
    [ "simplify_declarations", "structhif_1_1manipulation_1_1SimplifyOptions.html#a93bee2faa8b3616ab416fc7ce9fd5624", null ],
    [ "simplify_defines", "structhif_1_1manipulation_1_1SimplifyOptions.html#a860416f1a7fdeb101396877bc289b0e4", null ],
    [ "simplify_generates", "structhif_1_1manipulation_1_1SimplifyOptions.html#adba3483958accd0473648ff33a2fe8b1", null ],
    [ "simplify_parameters", "structhif_1_1manipulation_1_1SimplifyOptions.html#a36179451fd7b77e8cde1b6fa591cfc89", null ],
    [ "simplify_semantics_types", "structhif_1_1manipulation_1_1SimplifyOptions.html#af0774585e8aec3bc98e80690312ee4ed", null ],
    [ "simplify_statements", "structhif_1_1manipulation_1_1SimplifyOptions.html#aa4a463751489dc817a5a229cac3eb316", null ],
    [ "simplify_template_parameters", "structhif_1_1manipulation_1_1SimplifyOptions.html#a5bafe171f28f47583997d64c53618073", null ],
    [ "simplify_typereferences", "structhif_1_1manipulation_1_1SimplifyOptions.html#a1aac6cc62ff58dbbb7a1f5c54987f071", null ]
];