var structhif_1_1manipulation_1_1AddUniqueObjectOptions =
[
    [ "AddUniqueObjectOptions", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html#af8602a9882a082be35fdd11ae7992f2a", null ],
    [ "~AddUniqueObjectOptions", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html#aee69b62c7f15c72dbfdb844e517049a5", null ],
    [ "AddUniqueObjectOptions", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html#ab69463d37936b0632fb56eb8f2c92a88", null ],
    [ "operator=", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html#a9b9fc46aa04c1b418ef3fab3cc3a0466", null ],
    [ "swap", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html#abe7f54771d102b507f11d6caa71faf9b", null ],
    [ "copyIfUnique", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html#a4891895e7bed0765c7d580dbd1161e63", null ],
    [ "deleteIfNotAdded", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html#a7a673f9c6d43e0f82dc0f2ee66dd84d2", null ],
    [ "equalsOptions", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html#ae8156a052979371fa2eae3af8ec2c8bb", null ],
    [ "position", "structhif_1_1manipulation_1_1AddUniqueObjectOptions.html#a3b737a4c049a8648500e9d1fb516037e", null ]
];