var classhif_1_1Variable =
[
    [ "Variable", "classhif_1_1Variable.html#ab9a431390dd46adf94976dda74bf3aa9", null ],
    [ "~Variable", "classhif_1_1Variable.html#ac506406a299dc20b03f5e8c78f607ef9", null ],
    [ "_calculateFields", "classhif_1_1Variable.html#a6b4eca4f738ac6d547a6a725a7668de7", null ],
    [ "acceptVisitor", "classhif_1_1Variable.html#afde659cccb583c1285bcc61211ac6fce", null ],
    [ "getClassId", "classhif_1_1Variable.html#a500220495c61cadf1c1c06c107ced162", null ],
    [ "isInstance", "classhif_1_1Variable.html#a8e4bd842bf0f5c62f65aab0a747bf244", null ],
    [ "isStandard", "classhif_1_1Variable.html#aa174201fa466fc361cb4d46f8960d1e0", null ],
    [ "setInstance", "classhif_1_1Variable.html#a8bff50de41189bf2c233bfc2dbf9e8c0", null ],
    [ "setStandard", "classhif_1_1Variable.html#a70f6a6e9c4a671209829775c8375a0e8", null ]
];