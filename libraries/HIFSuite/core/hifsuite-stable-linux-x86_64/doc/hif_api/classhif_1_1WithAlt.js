var classhif_1_1WithAlt =
[
    [ "WithAlt", "classhif_1_1WithAlt.html#ab764f838170a3f4e5ad6ff9aa6024965", null ],
    [ "~WithAlt", "classhif_1_1WithAlt.html#af836d8eae390f8c7d6995d5333ef02ef", null ],
    [ "_calculateFields", "classhif_1_1WithAlt.html#a2c69dedb75129122bee70655af075bbb", null ],
    [ "_getBListName", "classhif_1_1WithAlt.html#a7d0b79a9b0b4bad8618c9c9f6fb6f201", null ],
    [ "_getFieldName", "classhif_1_1WithAlt.html#a6de62cfa72d93f366cd7ccd482cd9500", null ],
    [ "acceptVisitor", "classhif_1_1WithAlt.html#ac254bab3891cf48ed5ffe2f2abc54028", null ],
    [ "getClassId", "classhif_1_1WithAlt.html#a2701966f84347be467d38ac3e68e8741", null ],
    [ "getValue", "classhif_1_1WithAlt.html#af5b1174cb0ec81b9ff849d948f0f257e", null ],
    [ "setValue", "classhif_1_1WithAlt.html#aac0321aa8fc19be773c2d722aa48f3bf", null ],
    [ "conditions", "classhif_1_1WithAlt.html#a6eb1200f40b36d7826b72814763614a7", null ]
];