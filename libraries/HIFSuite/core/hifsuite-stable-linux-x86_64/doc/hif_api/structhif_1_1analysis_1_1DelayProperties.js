var structhif_1_1analysis_1_1DelayProperties =
[
    [ "Size", "structhif_1_1analysis_1_1DelayProperties.html#af9588dd62c3abc7db7daad0490ca2c2e", null ],
    [ "DelayProperties", "structhif_1_1analysis_1_1DelayProperties.html#a38e4b3afde1005b48260b5f5cab0d8e9", null ],
    [ "~DelayProperties", "structhif_1_1analysis_1_1DelayProperties.html#a8d6e474b72078ba51c0fb88a66f4638f", null ],
    [ "DelayProperties", "structhif_1_1analysis_1_1DelayProperties.html#ae04b4255fe0191f96313224e5a6589c8", null ],
    [ "operator=", "structhif_1_1analysis_1_1DelayProperties.html#ace667c5e1e9d4cf8773ed6b0f1ac3fe1", null ],
    [ "clockCycles", "structhif_1_1analysis_1_1DelayProperties.html#a3872e41fa2be8ac359ec29637805175c", null ],
    [ "deltas", "structhif_1_1analysis_1_1DelayProperties.html#ac700302609968316748574618df592cf", null ],
    [ "halfClock", "structhif_1_1analysis_1_1DelayProperties.html#a45d9e688715cd3bdf078a49fca9df2ed", null ],
    [ "port", "structhif_1_1analysis_1_1DelayProperties.html#aeb55424fa043c9eb648d550614895b54", null ]
];