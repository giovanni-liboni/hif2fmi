var structhif_1_1manipulation_1_1FixTemplateOptions =
[
    [ "FixTemplateOptions", "structhif_1_1manipulation_1_1FixTemplateOptions.html#ae0d3c5446862206fea48b2f14d1c8145", null ],
    [ "~FixTemplateOptions", "structhif_1_1manipulation_1_1FixTemplateOptions.html#a5f74a0262e00b776c95b317fc06354a9", null ],
    [ "FixTemplateOptions", "structhif_1_1manipulation_1_1FixTemplateOptions.html#ac4d3c58468d8dbcc9033b09a353b1559", null ],
    [ "operator=", "structhif_1_1manipulation_1_1FixTemplateOptions.html#abe7df4d764ab00302bcb6bb4d74fc0bf", null ],
    [ "swap", "structhif_1_1manipulation_1_1FixTemplateOptions.html#a7b35bfae347df5be5b0f3b88d9a89212", null ],
    [ "checkSem", "structhif_1_1manipulation_1_1FixTemplateOptions.html#a5730fb869db9bdb2115cad66a0683c96", null ],
    [ "fixStandardDeclarations", "structhif_1_1manipulation_1_1FixTemplateOptions.html#aa740319fe8ef4b25de7ecefd347039be", null ],
    [ "setConstExpr", "structhif_1_1manipulation_1_1FixTemplateOptions.html#a0cf4ddc528b51e4a683a9caef00a746b", null ],
    [ "useHdtLib", "structhif_1_1manipulation_1_1FixTemplateOptions.html#ab3c3ff1b3431cbd879b793b009d6f87d", null ]
];