var structhif_1_1manipulation_1_1FlattenDesignOptions =
[
    [ "FlattenDesignOptions", "structhif_1_1manipulation_1_1FlattenDesignOptions.html#a455e865bd7a9e983b92224bc0a05b0cb", null ],
    [ "~FlattenDesignOptions", "structhif_1_1manipulation_1_1FlattenDesignOptions.html#a1f0785701da959be06ce1d1b4c74013c", null ],
    [ "FlattenDesignOptions", "structhif_1_1manipulation_1_1FlattenDesignOptions.html#a8c53ac37c8572302c4d28d02f2b6b2fa", null ],
    [ "operator=", "structhif_1_1manipulation_1_1FlattenDesignOptions.html#a66dde5dda9cfbb8dcc5a6fe436fef932", null ],
    [ "rootDUs", "structhif_1_1manipulation_1_1FlattenDesignOptions.html#a84d9faa8efcb585ed6b1a0190bd25efc", null ],
    [ "rootInstances", "structhif_1_1manipulation_1_1FlattenDesignOptions.html#a783ad890f6bb06cafe84f00568de6899", null ],
    [ "topLevelName", "structhif_1_1manipulation_1_1FlattenDesignOptions.html#ae684ceabdb5987623714078772b6862b", null ],
    [ "verbose", "structhif_1_1manipulation_1_1FlattenDesignOptions.html#abfe564c125db81f29fc71b8544b9fed6", null ]
];