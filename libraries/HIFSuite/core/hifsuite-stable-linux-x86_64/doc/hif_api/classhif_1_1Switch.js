var classhif_1_1Switch =
[
    [ "AltType", "classhif_1_1Switch.html#abbc16dbd702b54bf38bbd7e515387d7c", null ],
    [ "Switch", "classhif_1_1Switch.html#aceb99c15ab70414dc628d9ca3d415ba8", null ],
    [ "~Switch", "classhif_1_1Switch.html#ab890428f93fdb6bc4fe52b75bca3abec", null ],
    [ "_calculateFields", "classhif_1_1Switch.html#a411d3af97a491d848a2553cf54dfb412", null ],
    [ "_getBListName", "classhif_1_1Switch.html#a07bb5e7a57b49334c04e4cab8ab31b16", null ],
    [ "_getFieldName", "classhif_1_1Switch.html#a96ef90f31a40b607378bea299fba6b37", null ],
    [ "acceptVisitor", "classhif_1_1Switch.html#af0ab20db590debb5d803862af46cb215", null ],
    [ "getCaseSemantics", "classhif_1_1Switch.html#a6d98cef7e143dbe9fc6b51f1fb6de4de", null ],
    [ "getClassId", "classhif_1_1Switch.html#a42bd54354927e2486fa1600c700dc353", null ],
    [ "getCondition", "classhif_1_1Switch.html#ae008d9561ab8e00e14109eb5b9c47859", null ],
    [ "setCaseSemantics", "classhif_1_1Switch.html#a1f9c66ad2ab170aa50e7f1ee966fc5de", null ],
    [ "setCondition", "classhif_1_1Switch.html#a0a4daa0facf130c650e07d83ce9cde8b", null ],
    [ "alts", "classhif_1_1Switch.html#a7d81506a1aa24fe7bb8ed66866183765", null ],
    [ "defaults", "classhif_1_1Switch.html#a231e7d5d150a25b689404766e2799931", null ]
];