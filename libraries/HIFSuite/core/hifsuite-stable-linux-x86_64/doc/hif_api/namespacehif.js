var namespacehif =
[
    [ "__conversion", "namespacehif_1_1____conversion.html", "namespacehif_1_1____conversion" ],
    [ "analysis", "namespacehif_1_1analysis.html", "namespacehif_1_1analysis" ],
    [ "applicationUtils", "namespacehif_1_1applicationUtils.html", "namespacehif_1_1applicationUtils" ],
    [ "backends", "namespacehif_1_1backends.html", "namespacehif_1_1backends" ],
    [ "features", "namespacehif_1_1features.html", "namespacehif_1_1features" ],
    [ "manipulation", "namespacehif_1_1manipulation.html", "namespacehif_1_1manipulation" ],
    [ "semantics", "namespacehif_1_1semantics.html", "namespacehif_1_1semantics" ],
    [ "univerCM", "namespacehif_1_1univerCM.html", null ],
    [ "Action", "classhif_1_1Action.html", "classhif_1_1Action" ],
    [ "Aggregate", "classhif_1_1Aggregate.html", "classhif_1_1Aggregate" ],
    [ "AggregateAlt", "classhif_1_1AggregateAlt.html", "classhif_1_1AggregateAlt" ],
    [ "Alias", "classhif_1_1Alias.html", "classhif_1_1Alias" ],
    [ "Alt", "classhif_1_1Alt.html", "classhif_1_1Alt" ],
    [ "AncestorVisitor", "classhif_1_1AncestorVisitor.html", "classhif_1_1AncestorVisitor" ],
    [ "Array", "classhif_1_1Array.html", "classhif_1_1Array" ],
    [ "Assign", "classhif_1_1Assign.html", "classhif_1_1Assign" ],
    [ "BaseContents", "classhif_1_1BaseContents.html", "classhif_1_1BaseContents" ],
    [ "Bit", "classhif_1_1Bit.html", "classhif_1_1Bit" ],
    [ "BitValue", "classhif_1_1BitValue.html", "classhif_1_1BitValue" ],
    [ "Bitvector", "classhif_1_1Bitvector.html", "classhif_1_1Bitvector" ],
    [ "BitvectorValue", "classhif_1_1BitvectorValue.html", "classhif_1_1BitvectorValue" ],
    [ "BiVisitor", "classhif_1_1BiVisitor.html", "classhif_1_1BiVisitor" ],
    [ "BList", "classhif_1_1BList.html", "classhif_1_1BList" ],
    [ "BListHost", "classhif_1_1BListHost.html", "classhif_1_1BListHost" ],
    [ "Bool", "classhif_1_1Bool.html", "classhif_1_1Bool" ],
    [ "BoolValue", "classhif_1_1BoolValue.html", "classhif_1_1BoolValue" ],
    [ "Break", "classhif_1_1Break.html", "classhif_1_1Break" ],
    [ "Cast", "classhif_1_1Cast.html", "classhif_1_1Cast" ],
    [ "Char", "classhif_1_1Char.html", "classhif_1_1Char" ],
    [ "CharValue", "classhif_1_1CharValue.html", "classhif_1_1CharValue" ],
    [ "class_map_helper1_t", "structhif_1_1class__map__helper1__t.html", null ],
    [ "class_map_helper2_t", "structhif_1_1class__map__helper2__t.html", null ],
    [ "CompositeType", "classhif_1_1CompositeType.html", "classhif_1_1CompositeType" ],
    [ "Const", "classhif_1_1Const.html", "classhif_1_1Const" ],
    [ "ConstValue", "classhif_1_1ConstValue.html", "classhif_1_1ConstValue" ],
    [ "Contents", "classhif_1_1Contents.html", "classhif_1_1Contents" ],
    [ "Continue", "classhif_1_1Continue.html", "classhif_1_1Continue" ],
    [ "CopyOptions", "structhif_1_1CopyOptions.html", "structhif_1_1CopyOptions" ],
    [ "DataDeclaration", "classhif_1_1DataDeclaration.html", "classhif_1_1DataDeclaration" ],
    [ "Declaration", "classhif_1_1Declaration.html", "classhif_1_1Declaration" ],
    [ "DeclarationVisitor", "classhif_1_1DeclarationVisitor.html", "classhif_1_1DeclarationVisitor" ],
    [ "DeclarationVisitorOptions", "structhif_1_1DeclarationVisitorOptions.html", "structhif_1_1DeclarationVisitorOptions" ],
    [ "DesignUnit", "classhif_1_1DesignUnit.html", "classhif_1_1DesignUnit" ],
    [ "Entity", "classhif_1_1Entity.html", "classhif_1_1Entity" ],
    [ "Enum", "classhif_1_1Enum.html", "classhif_1_1Enum" ],
    [ "EnumValue", "classhif_1_1EnumValue.html", "classhif_1_1EnumValue" ],
    [ "EqualsOptions", "structhif_1_1EqualsOptions.html", "structhif_1_1EqualsOptions" ],
    [ "Event", "classhif_1_1Event.html", "classhif_1_1Event" ],
    [ "Expression", "classhif_1_1Expression.html", "classhif_1_1Expression" ],
    [ "Field", "classhif_1_1Field.html", "classhif_1_1Field" ],
    [ "FieldReference", "classhif_1_1FieldReference.html", "classhif_1_1FieldReference" ],
    [ "File", "classhif_1_1File.html", "classhif_1_1File" ],
    [ "For", "classhif_1_1For.html", "classhif_1_1For" ],
    [ "ForGenerate", "classhif_1_1ForGenerate.html", "classhif_1_1ForGenerate" ],
    [ "Function", "classhif_1_1Function.html", "classhif_1_1Function" ],
    [ "FunctionCall", "classhif_1_1FunctionCall.html", "classhif_1_1FunctionCall" ],
    [ "functor_map_helper1_t", "structhif_1_1functor__map__helper1__t.html", null ],
    [ "functor_map_helper2_t", "structhif_1_1functor__map__helper2__t.html", null ],
    [ "Generate", "classhif_1_1Generate.html", "classhif_1_1Generate" ],
    [ "GlobalAction", "classhif_1_1GlobalAction.html", "classhif_1_1GlobalAction" ],
    [ "GuideVisitor", "classhif_1_1GuideVisitor.html", "classhif_1_1GuideVisitor" ],
    [ "HifFactory", "classhif_1_1HifFactory.html", "classhif_1_1HifFactory" ],
    [ "HifNodeRef", "classhif_1_1HifNodeRef.html", "classhif_1_1HifNodeRef" ],
    [ "HifQueryBase", "classhif_1_1HifQueryBase.html", "classhif_1_1HifQueryBase" ],
    [ "HifTypedQuery", "classhif_1_1HifTypedQuery.html", "classhif_1_1HifTypedQuery" ],
    [ "HifUntypedQuery", "classhif_1_1HifUntypedQuery.html", "classhif_1_1HifUntypedQuery" ],
    [ "HifVisitor", "classhif_1_1HifVisitor.html", "classhif_1_1HifVisitor" ],
    [ "Identifier", "classhif_1_1Identifier.html", "classhif_1_1Identifier" ],
    [ "If", "classhif_1_1If.html", "classhif_1_1If" ],
    [ "IfAlt", "classhif_1_1IfAlt.html", "classhif_1_1IfAlt" ],
    [ "IfGenerate", "classhif_1_1IfGenerate.html", "classhif_1_1IfGenerate" ],
    [ "Instance", "classhif_1_1Instance.html", "classhif_1_1Instance" ],
    [ "InstanceCounter", "classhif_1_1InstanceCounter.html", "classhif_1_1InstanceCounter" ],
    [ "Int", "classhif_1_1Int.html", "classhif_1_1Int" ],
    [ "IntValue", "classhif_1_1IntValue.html", "classhif_1_1IntValue" ],
    [ "Library", "classhif_1_1Library.html", "classhif_1_1Library" ],
    [ "LibraryDef", "classhif_1_1LibraryDef.html", "classhif_1_1LibraryDef" ],
    [ "MapVisitor1", "classhif_1_1MapVisitor1.html", "classhif_1_1MapVisitor1" ],
    [ "MapVisitor2", "classhif_1_1MapVisitor2.html", "classhif_1_1MapVisitor2" ],
    [ "Member", "classhif_1_1Member.html", "classhif_1_1Member" ],
    [ "method_map_helper1_t", "structhif_1_1method__map__helper1__t.html", null ],
    [ "method_map_helper2_t", "structhif_1_1method__map__helper2__t.html", null ],
    [ "MonoVisitor", "classhif_1_1MonoVisitor.html", "classhif_1_1MonoVisitor" ],
    [ "name", "classhif_1_1name.html", "classhif_1_1name" ],
    [ "NameTable", "classhif_1_1NameTable.html", "classhif_1_1NameTable" ],
    [ "Null", "classhif_1_1Null.html", "classhif_1_1Null" ],
    [ "Object", "classhif_1_1Object.html", "classhif_1_1Object" ],
    [ "Parameter", "classhif_1_1Parameter.html", "classhif_1_1Parameter" ],
    [ "ParameterAssign", "classhif_1_1ParameterAssign.html", "classhif_1_1ParameterAssign" ],
    [ "Pointer", "classhif_1_1Pointer.html", "classhif_1_1Pointer" ],
    [ "Port", "classhif_1_1Port.html", "classhif_1_1Port" ],
    [ "PortAssign", "classhif_1_1PortAssign.html", "classhif_1_1PortAssign" ],
    [ "PPAssign", "classhif_1_1PPAssign.html", "classhif_1_1PPAssign" ],
    [ "PrefixedReference", "classhif_1_1PrefixedReference.html", "classhif_1_1PrefixedReference" ],
    [ "PrintHifOptions", "structhif_1_1PrintHifOptions.html", "structhif_1_1PrintHifOptions" ],
    [ "Procedure", "classhif_1_1Procedure.html", "classhif_1_1Procedure" ],
    [ "ProcedureCall", "classhif_1_1ProcedureCall.html", "classhif_1_1ProcedureCall" ],
    [ "Range", "classhif_1_1Range.html", "classhif_1_1Range" ],
    [ "ReadHifOptions", "structhif_1_1ReadHifOptions.html", "structhif_1_1ReadHifOptions" ],
    [ "Real", "classhif_1_1Real.html", "classhif_1_1Real" ],
    [ "RealValue", "classhif_1_1RealValue.html", "classhif_1_1RealValue" ],
    [ "Record", "classhif_1_1Record.html", "classhif_1_1Record" ],
    [ "RecordValue", "classhif_1_1RecordValue.html", "classhif_1_1RecordValue" ],
    [ "RecordValueAlt", "classhif_1_1RecordValueAlt.html", "classhif_1_1RecordValueAlt" ],
    [ "Reference", "classhif_1_1Reference.html", "classhif_1_1Reference" ],
    [ "ReferencedAssign", "classhif_1_1ReferencedAssign.html", "classhif_1_1ReferencedAssign" ],
    [ "ReferencedType", "classhif_1_1ReferencedType.html", "classhif_1_1ReferencedType" ],
    [ "Return", "classhif_1_1Return.html", "classhif_1_1Return" ],
    [ "Scope", "classhif_1_1Scope.html", "classhif_1_1Scope" ],
    [ "ScopedType", "classhif_1_1ScopedType.html", "classhif_1_1ScopedType" ],
    [ "Signal", "classhif_1_1Signal.html", "classhif_1_1Signal" ],
    [ "Signed", "classhif_1_1Signed.html", "classhif_1_1Signed" ],
    [ "SimpleType", "classhif_1_1SimpleType.html", "classhif_1_1SimpleType" ],
    [ "Slice", "classhif_1_1Slice.html", "classhif_1_1Slice" ],
    [ "State", "classhif_1_1State.html", "classhif_1_1State" ],
    [ "StateTable", "classhif_1_1StateTable.html", "classhif_1_1StateTable" ],
    [ "String", "classhif_1_1String.html", "classhif_1_1String" ],
    [ "StringValue", "classhif_1_1StringValue.html", "classhif_1_1StringValue" ],
    [ "SubProgram", "classhif_1_1SubProgram.html", "classhif_1_1SubProgram" ],
    [ "Switch", "classhif_1_1Switch.html", "classhif_1_1Switch" ],
    [ "SwitchAlt", "classhif_1_1SwitchAlt.html", "classhif_1_1SwitchAlt" ],
    [ "System", "classhif_1_1System.html", "classhif_1_1System" ],
    [ "TerminalPrefixOptions", "structhif_1_1TerminalPrefixOptions.html", "structhif_1_1TerminalPrefixOptions" ],
    [ "Time", "classhif_1_1Time.html", "classhif_1_1Time" ],
    [ "TimeValue", "classhif_1_1TimeValue.html", "classhif_1_1TimeValue" ],
    [ "TPAssign", "classhif_1_1TPAssign.html", "classhif_1_1TPAssign" ],
    [ "Transition", "classhif_1_1Transition.html", "classhif_1_1Transition" ],
    [ "Trash", "classhif_1_1Trash.html", "classhif_1_1Trash" ],
    [ "Type", "classhif_1_1Type.html", "classhif_1_1Type" ],
    [ "TypeDeclaration", "classhif_1_1TypeDeclaration.html", "classhif_1_1TypeDeclaration" ],
    [ "TypeDef", "classhif_1_1TypeDef.html", "classhif_1_1TypeDef" ],
    [ "TypedObject", "classhif_1_1TypedObject.html", "classhif_1_1TypedObject" ],
    [ "TypeReference", "classhif_1_1TypeReference.html", "classhif_1_1TypeReference" ],
    [ "TypeTP", "classhif_1_1TypeTP.html", "classhif_1_1TypeTP" ],
    [ "TypeTPAssign", "classhif_1_1TypeTPAssign.html", "classhif_1_1TypeTPAssign" ],
    [ "Unsigned", "classhif_1_1Unsigned.html", "classhif_1_1Unsigned" ],
    [ "Value", "classhif_1_1Value.html", "classhif_1_1Value" ],
    [ "ValueStatement", "classhif_1_1ValueStatement.html", "classhif_1_1ValueStatement" ],
    [ "ValueTP", "classhif_1_1ValueTP.html", "classhif_1_1ValueTP" ],
    [ "ValueTPAssign", "classhif_1_1ValueTPAssign.html", "classhif_1_1ValueTPAssign" ],
    [ "Variable", "classhif_1_1Variable.html", "classhif_1_1Variable" ],
    [ "View", "classhif_1_1View.html", "classhif_1_1View" ],
    [ "ViewReference", "classhif_1_1ViewReference.html", "classhif_1_1ViewReference" ],
    [ "Wait", "classhif_1_1Wait.html", "classhif_1_1Wait" ],
    [ "When", "classhif_1_1When.html", "classhif_1_1When" ],
    [ "WhenAlt", "classhif_1_1WhenAlt.html", "classhif_1_1WhenAlt" ],
    [ "While", "classhif_1_1While.html", "classhif_1_1While" ],
    [ "With", "classhif_1_1With.html", "classhif_1_1With" ],
    [ "WithAlt", "classhif_1_1WithAlt.html", "classhif_1_1WithAlt" ]
];