var structhif_1_1analysis_1_1ProcessInfos =
[
    [ "ReferredDeclarations", "structhif_1_1analysis_1_1ProcessInfos.html#aa82b7ed4e57d9ca8b33d68ff87114a3b", null ],
    [ "ProcessKind", "structhif_1_1analysis_1_1ProcessInfos.html#aacace8517fec858f6d55a29aa53dee1b", [
      [ "ASYNCHRONOUS", "structhif_1_1analysis_1_1ProcessInfos.html#aacace8517fec858f6d55a29aa53dee1ba156af8ff0a264ecdeeca67c570a84b53", null ],
      [ "SYNCHRONOUS", "structhif_1_1analysis_1_1ProcessInfos.html#aacace8517fec858f6d55a29aa53dee1ba4605772902c1dedef532124ebbde3998", null ],
      [ "DERIVED_SYNCHRONOUS", "structhif_1_1analysis_1_1ProcessInfos.html#aacace8517fec858f6d55a29aa53dee1baffc1efb6cae60085629aa3842e0caba4", null ],
      [ "MIXED", "structhif_1_1analysis_1_1ProcessInfos.html#aacace8517fec858f6d55a29aa53dee1baca474df41932f8d27d4b3e4d657040e1", null ],
      [ "DERIVED_MIXED", "structhif_1_1analysis_1_1ProcessInfos.html#aacace8517fec858f6d55a29aa53dee1ba36ebd5df69689bdbe174f4c8255c7509", null ]
    ] ],
    [ "ProcessStyle", "structhif_1_1analysis_1_1ProcessInfos.html#a8d6364262d7dd5f14b000b6ae5157bbc", [
      [ "NO_STYLE", "structhif_1_1analysis_1_1ProcessInfos.html#a8d6364262d7dd5f14b000b6ae5157bbca828672e40000cd003a1ad251c73a2d5e", null ],
      [ "STYLE_1", "structhif_1_1analysis_1_1ProcessInfos.html#a8d6364262d7dd5f14b000b6ae5157bbca24dc7d65d70c0d989aa1c82219d9e1e8", null ],
      [ "STYLE_2", "structhif_1_1analysis_1_1ProcessInfos.html#a8d6364262d7dd5f14b000b6ae5157bbca9b1723121549b1a718487e12eb7c3187", null ],
      [ "STYLE_3", "structhif_1_1analysis_1_1ProcessInfos.html#a8d6364262d7dd5f14b000b6ae5157bbcac824150dfc14422f8ca9ae2a4e20e331", null ],
      [ "STYLE_4", "structhif_1_1analysis_1_1ProcessInfos.html#a8d6364262d7dd5f14b000b6ae5157bbca154b8bd963fd1675eabece65b5dedc3b", null ],
      [ "STYLE_5", "structhif_1_1analysis_1_1ProcessInfos.html#a8d6364262d7dd5f14b000b6ae5157bbca1484e9ef56bc92e9b690009272c57fc2", null ],
      [ "STYLE_6", "structhif_1_1analysis_1_1ProcessInfos.html#a8d6364262d7dd5f14b000b6ae5157bbcaa120cf0db775006fcd6759b2148ecc85", null ]
    ] ],
    [ "ResetKind", "structhif_1_1analysis_1_1ProcessInfos.html#a378e923832e68b2cc19ce1342fe6b9bc", [
      [ "NO_RESET", "structhif_1_1analysis_1_1ProcessInfos.html#a378e923832e68b2cc19ce1342fe6b9bcab40b9b918e543b58c42351f9e821f272", null ],
      [ "SYNCHRONOUS_RESET", "structhif_1_1analysis_1_1ProcessInfos.html#a378e923832e68b2cc19ce1342fe6b9bca483325c65b50942bf3b00f34ab37f712", null ],
      [ "ASYNCHRONOUS_RESET", "structhif_1_1analysis_1_1ProcessInfos.html#a378e923832e68b2cc19ce1342fe6b9bca83215720496b05c1f804a7c0a0901e05", null ],
      [ "DERIVED_SYNCHRONOUS_RESET", "structhif_1_1analysis_1_1ProcessInfos.html#a378e923832e68b2cc19ce1342fe6b9bcad2fbddb8e01650bac5d74ed3adbe4a2d", null ]
    ] ],
    [ "ResetPhase", "structhif_1_1analysis_1_1ProcessInfos.html#af364c9333afaf7cd70caa0f6145b6d7c", [
      [ "NO_PHASE", "structhif_1_1analysis_1_1ProcessInfos.html#af364c9333afaf7cd70caa0f6145b6d7ca2ff09a57dc054c2621537ce54938a596", null ],
      [ "HIGH_PHASE", "structhif_1_1analysis_1_1ProcessInfos.html#af364c9333afaf7cd70caa0f6145b6d7ca7af129be0e2e3b5cc87fc4e25fb50599", null ],
      [ "LOW_PHASE", "structhif_1_1analysis_1_1ProcessInfos.html#af364c9333afaf7cd70caa0f6145b6d7ca7b18fa85f1f3524fe08371d10c7b0663", null ]
    ] ],
    [ "WorkingEdge", "structhif_1_1analysis_1_1ProcessInfos.html#ac53eb665f0c03b33edce1564b29c43c1", [
      [ "NO_EDGE", "structhif_1_1analysis_1_1ProcessInfos.html#ac53eb665f0c03b33edce1564b29c43c1a8a400f838845671ff9900f294d1ae7e2", null ],
      [ "RISING_EDGE", "structhif_1_1analysis_1_1ProcessInfos.html#ac53eb665f0c03b33edce1564b29c43c1aec98754162a5024bc13af0a404434c28", null ],
      [ "FALLING_EDGE", "structhif_1_1analysis_1_1ProcessInfos.html#ac53eb665f0c03b33edce1564b29c43c1a40623395e4b022ab005e6060ccfed1a5", null ],
      [ "BOTH_EDGES", "structhif_1_1analysis_1_1ProcessInfos.html#ac53eb665f0c03b33edce1564b29c43c1a747b4201272252690fbd52c3d8bd9b48", null ]
    ] ],
    [ "ProcessInfos", "structhif_1_1analysis_1_1ProcessInfos.html#a3959048680336b4b9c65cafe74325c68", null ],
    [ "~ProcessInfos", "structhif_1_1analysis_1_1ProcessInfos.html#a7a5a9047952865179dcad7187fce0f2a", null ],
    [ "ProcessInfos", "structhif_1_1analysis_1_1ProcessInfos.html#ac5e99d4b21532445090c1cf806c8eaee", null ],
    [ "getSensitivitySize", "structhif_1_1analysis_1_1ProcessInfos.html#a85bc3251db07af3a01ffcb0bdb06080a", null ],
    [ "isInSensitivity", "structhif_1_1analysis_1_1ProcessInfos.html#ae3424e4dd381921e1b659bc23d5d4a5f", null ],
    [ "operator=", "structhif_1_1analysis_1_1ProcessInfos.html#a4aa4205673002eae64ce70de84ad4ef0", null ],
    [ "clock", "structhif_1_1analysis_1_1ProcessInfos.html#af75aade84882e58e007e547e672d9a57", null ],
    [ "fallingSensitivity", "structhif_1_1analysis_1_1ProcessInfos.html#afd8226f9433722bed55b03b7e4c1ebc8", null ],
    [ "inputs", "structhif_1_1analysis_1_1ProcessInfos.html#a8d87f2b79e12077adde8496217af2d40", null ],
    [ "inputVariables", "structhif_1_1analysis_1_1ProcessInfos.html#a57d4fed52040df3b15e76a2ffc92bef0", null ],
    [ "outputs", "structhif_1_1analysis_1_1ProcessInfos.html#a0a0b25864f4d807fa70966da8a405b7d", null ],
    [ "outputVariables", "structhif_1_1analysis_1_1ProcessInfos.html#a013cb55b476c6cc05486b3dea9e7200b", null ],
    [ "processKind", "structhif_1_1analysis_1_1ProcessInfos.html#adb71fb5a9eb5c981df6fac4fa5b7c77d", null ],
    [ "processStyle", "structhif_1_1analysis_1_1ProcessInfos.html#a9e65aa42ac31f29a3dd850fe41e3b018", null ],
    [ "reset", "structhif_1_1analysis_1_1ProcessInfos.html#a3c1deef9993e2989d6f12ee373d42a07", null ],
    [ "resetKind", "structhif_1_1analysis_1_1ProcessInfos.html#aa2abc61144be53fff0b296250787e556", null ],
    [ "resetPhase", "structhif_1_1analysis_1_1ProcessInfos.html#aa55eb920910e560bae3712d285d7aee4", null ],
    [ "risingSensitivity", "structhif_1_1analysis_1_1ProcessInfos.html#a31d0a98465f3a2c96de988e0c25c2826", null ],
    [ "sensitivity", "structhif_1_1analysis_1_1ProcessInfos.html#ab58b1d3ae3e3ef8e4abcc850d255df21", null ],
    [ "workingEdge", "structhif_1_1analysis_1_1ProcessInfos.html#a38251d3a93bd55ec46a36d4e88482b4a", null ]
];