var classhif_1_1TimeValue =
[
    [ "TimeUnit", "classhif_1_1TimeValue.html#aa14f32818a1b78dd551afa4377222435", [
      [ "time_fs", "classhif_1_1TimeValue.html#aa14f32818a1b78dd551afa4377222435aa9ebf51f6c58a63abc90b219a80097af", null ],
      [ "time_ps", "classhif_1_1TimeValue.html#aa14f32818a1b78dd551afa4377222435ac56d0b6da9b4c7963cda49bcec086ad1", null ],
      [ "time_ns", "classhif_1_1TimeValue.html#aa14f32818a1b78dd551afa4377222435ae0f9210ef3554ed930f4bf6b74c76fce", null ],
      [ "time_us", "classhif_1_1TimeValue.html#aa14f32818a1b78dd551afa4377222435abea6f2ef4bdab25991a5177d4f7aaa67", null ],
      [ "time_ms", "classhif_1_1TimeValue.html#aa14f32818a1b78dd551afa4377222435acaea807cf79029300a36861d4752dd97", null ],
      [ "time_sec", "classhif_1_1TimeValue.html#aa14f32818a1b78dd551afa4377222435a76e079a9bbf6494943cb73ada61583fe", null ],
      [ "time_min", "classhif_1_1TimeValue.html#aa14f32818a1b78dd551afa4377222435a1f34faa3ed3435c4e651e87350ef4a30", null ],
      [ "time_hr", "classhif_1_1TimeValue.html#aa14f32818a1b78dd551afa4377222435a5f1763748ba1dfa4620620e0cb837d4e", null ]
    ] ],
    [ "TimeValue", "classhif_1_1TimeValue.html#a941929f3de3725e7216d0417aed826d2", null ],
    [ "~TimeValue", "classhif_1_1TimeValue.html#abeda6c407d224fbe8965b3849a0b3889", null ],
    [ "_calculateFields", "classhif_1_1TimeValue.html#ad951e020de16b5507b16942a0b120e85", null ],
    [ "acceptVisitor", "classhif_1_1TimeValue.html#abc00f65ed6a736d10aba14acaad85aea", null ],
    [ "changeUnit", "classhif_1_1TimeValue.html#a06ad22053ee0f12f6708134e35004a3b", null ],
    [ "getClassId", "classhif_1_1TimeValue.html#ad9da327ff2dd834838ce97339029ac99", null ],
    [ "getUnit", "classhif_1_1TimeValue.html#a6eb38f3d2f7a652bd125d03314c87c87", null ],
    [ "getValue", "classhif_1_1TimeValue.html#aa6241e7bc6514e8e5cb52f2c284acda4", null ],
    [ "operator<", "classhif_1_1TimeValue.html#a20d077ac66a9df38558c15a44791133f", null ],
    [ "setUnit", "classhif_1_1TimeValue.html#a24c1700e47d2275b9b552550bd1e5228", null ],
    [ "setValue", "classhif_1_1TimeValue.html#a743ef2cb359e33ec5f30316f7a37fdab", null ]
];