var classhif_1_1LibraryDef =
[
    [ "LibraryDef", "classhif_1_1LibraryDef.html#ac25461e10aafbb1b764d40b4a945ff78", null ],
    [ "~LibraryDef", "classhif_1_1LibraryDef.html#a47623a3dc7b3e8435a0be6ffb72e33e7", null ],
    [ "_calculateFields", "classhif_1_1LibraryDef.html#a9ea31535d0ba04d28e9fa29a3ddbd8d9", null ],
    [ "_getBListName", "classhif_1_1LibraryDef.html#ac67d039cf7c454f381d4166d5c1ba2ad", null ],
    [ "acceptVisitor", "classhif_1_1LibraryDef.html#a1ba6b085d7f476a0183c96740a6d4ce5", null ],
    [ "getClassId", "classhif_1_1LibraryDef.html#a6a78dc8356ea92d43c214e1c8b850e69", null ],
    [ "getLanguageID", "classhif_1_1LibraryDef.html#a3c462e411c9d6d190820b112b4a5bf86", null ],
    [ "isStandard", "classhif_1_1LibraryDef.html#a001be0d696fea15d2bd72c4cb3ff2a10", null ],
    [ "setLanguageID", "classhif_1_1LibraryDef.html#a44ae7ae9f267c4d2652c9f2d31668aba", null ],
    [ "setStandard", "classhif_1_1LibraryDef.html#a56901d3d28dd7a25989153bece5b836d", null ],
    [ "declarations", "classhif_1_1LibraryDef.html#a19687f57d902edb2a96d7c8ef4ffc272", null ],
    [ "libraries", "classhif_1_1LibraryDef.html#a8ebef69235853d18afdd87e9502f349b", null ]
];