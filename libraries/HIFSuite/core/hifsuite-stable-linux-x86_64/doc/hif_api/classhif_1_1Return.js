var classhif_1_1Return =
[
    [ "DeclarationType", "classhif_1_1Return.html#a75c476c85c6ba8b4d976cc0dccf9e6a7", null ],
    [ "Return", "classhif_1_1Return.html#a2966a8ef8b0437a6d6334d7a7872ecec", null ],
    [ "~Return", "classhif_1_1Return.html#a2b38ee41732a6b5760b71805a6d9ca58", null ],
    [ "_calculateFields", "classhif_1_1Return.html#aa4806f617464e89a5bfa9b9b1c641e8e", null ],
    [ "_getFieldName", "classhif_1_1Return.html#aebdef5dd627708babe5c00194c232356", null ],
    [ "acceptVisitor", "classhif_1_1Return.html#ac2c9b739f797b7d38bee6320c4a4801b", null ],
    [ "getClassId", "classhif_1_1Return.html#ad83dd2489160628f7844dbce2553f1ad", null ],
    [ "getValue", "classhif_1_1Return.html#a44162d878368da50bdfd1446384b905e", null ],
    [ "setValue", "classhif_1_1Return.html#a95e9ec5fa980bfe552beb805188bd822", null ]
];