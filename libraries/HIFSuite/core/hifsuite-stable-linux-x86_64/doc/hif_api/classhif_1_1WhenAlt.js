var classhif_1_1WhenAlt =
[
    [ "WhenAlt", "classhif_1_1WhenAlt.html#aba45b276fd2e5671d67e514bd513d75b", null ],
    [ "~WhenAlt", "classhif_1_1WhenAlt.html#a11f842e584211d4296c4571d8f3a58dd", null ],
    [ "_calculateFields", "classhif_1_1WhenAlt.html#a94abb070288ac4d7fc93cbe26c285d1e", null ],
    [ "_getFieldName", "classhif_1_1WhenAlt.html#a1e95cf42bb8a2531c70d9edd2300c7ed", null ],
    [ "acceptVisitor", "classhif_1_1WhenAlt.html#a5768ecaf582f4a43d945e65a674363ff", null ],
    [ "getClassId", "classhif_1_1WhenAlt.html#ac9902259faec5ffdc51c14d390193b13", null ],
    [ "getCondition", "classhif_1_1WhenAlt.html#a7a5e4eaff0b23f875aae5fccef08fe49", null ],
    [ "getValue", "classhif_1_1WhenAlt.html#ae8db5914100a8e1506296fa39a05a7d9", null ],
    [ "setCondition", "classhif_1_1WhenAlt.html#ab843f38c30c4e3fd2700c10d88e47863", null ],
    [ "setValue", "classhif_1_1WhenAlt.html#a5416bb171544b9de525582fe30af72ef", null ]
];