var classhif_1_1Int =
[
    [ "Int", "classhif_1_1Int.html#a80120a2bc40d18d6209c1cefc47082b7", null ],
    [ "~Int", "classhif_1_1Int.html#a1f370c6ed4b43213eb92f81ccf8eb087", null ],
    [ "_calculateFields", "classhif_1_1Int.html#afc73916b6e81bc5bdbc72b56bdb8ba15", null ],
    [ "_getFieldName", "classhif_1_1Int.html#a1d058ce932adb46be67eb1841947cb09", null ],
    [ "acceptVisitor", "classhif_1_1Int.html#a4c6cd3a80a1069bba306b491d07b4eef", null ],
    [ "getClassId", "classhif_1_1Int.html#aad86c2f8ca5dc4cefdb5ece6a9104539", null ],
    [ "isSigned", "classhif_1_1Int.html#ab25cc15529a41addda18fc30fdad203b", null ],
    [ "setSigned", "classhif_1_1Int.html#a0fe29fb311ea16f5fb69ef0fd92e540e", null ],
    [ "setSpan", "classhif_1_1Int.html#aef104a673194b2ccfe93cc73d726d0fe", null ],
    [ "toObject", "classhif_1_1Int.html#aadfa6ae634c72269838e1be38749a979", null ]
];