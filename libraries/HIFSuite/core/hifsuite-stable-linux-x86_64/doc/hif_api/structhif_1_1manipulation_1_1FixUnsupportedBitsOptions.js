var structhif_1_1manipulation_1_1FixUnsupportedBitsOptions =
[
    [ "FixUnsupportedBitsOptions", "structhif_1_1manipulation_1_1FixUnsupportedBitsOptions.html#afec4860d50bbea05f3526711317b75bb", null ],
    [ "~FixUnsupportedBitsOptions", "structhif_1_1manipulation_1_1FixUnsupportedBitsOptions.html#a59487005c583bda23c3d0eb360832761", null ],
    [ "FixUnsupportedBitsOptions", "structhif_1_1manipulation_1_1FixUnsupportedBitsOptions.html#a69df473a508f471b80609fa48c6f4085", null ],
    [ "operator=", "structhif_1_1manipulation_1_1FixUnsupportedBitsOptions.html#adefee59c4cae03d78e838cbcc45faf1f", null ],
    [ "swap", "structhif_1_1manipulation_1_1FixUnsupportedBitsOptions.html#a61b51717da215c74d4ee4821e1f8be71", null ],
    [ "onlyBinaryBits", "structhif_1_1manipulation_1_1FixUnsupportedBitsOptions.html#a20d5d411c5d222274652b4f83cb9c24b", null ],
    [ "skipInitialValues", "structhif_1_1manipulation_1_1FixUnsupportedBitsOptions.html#ad7ae495e84bcb1c0508b2d5ebef8c9ca", null ],
    [ "xzReplaceValue", "structhif_1_1manipulation_1_1FixUnsupportedBitsOptions.html#a913df2fffaa1b43a2820abfa0cb123b9", null ]
];