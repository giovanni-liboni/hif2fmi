var structhif_1_1manipulation_1_1PrefixTreeOptions =
[
    [ "PrefixTreeOptions", "structhif_1_1manipulation_1_1PrefixTreeOptions.html#ace69812a20ef21dd3b2d3dfb80764a7e", null ],
    [ "~PrefixTreeOptions", "structhif_1_1manipulation_1_1PrefixTreeOptions.html#aa526153c6a9067a55b90919925a0661c", null ],
    [ "PrefixTreeOptions", "structhif_1_1manipulation_1_1PrefixTreeOptions.html#acd3cc821c92b840d13c5312186c974f8", null ],
    [ "operator=", "structhif_1_1manipulation_1_1PrefixTreeOptions.html#a1e3c589717d60acc7615351c6a1ac54e", null ],
    [ "swap", "structhif_1_1manipulation_1_1PrefixTreeOptions.html#a443766dc6f50dc00f843daf933b9a606", null ],
    [ "recursive", "structhif_1_1manipulation_1_1PrefixTreeOptions.html#ace621b2dee7fdb4cb3bfae836cb57e8d", null ],
    [ "skipPrefixingIfSameScope", "structhif_1_1manipulation_1_1PrefixTreeOptions.html#ad8336aaed88103bce2534856cf9864f3", null ],
    [ "strictChecks", "structhif_1_1manipulation_1_1PrefixTreeOptions.html#ae893e357b254b9e40042768623775d73", null ]
];