var structhif_1_1manipulation_1_1MergeTreesOptions =
[
    [ "MergeTreesOptions", "structhif_1_1manipulation_1_1MergeTreesOptions.html#aed46e1111832ae313e7e206d63494c9a", null ],
    [ "~MergeTreesOptions", "structhif_1_1manipulation_1_1MergeTreesOptions.html#a4897cd03d5154827ac2bc423dd9ae747", null ],
    [ "MergeTreesOptions", "structhif_1_1manipulation_1_1MergeTreesOptions.html#a8bc7249a0ed78e765c21b7d36378f386", null ],
    [ "operator=", "structhif_1_1manipulation_1_1MergeTreesOptions.html#a309c43508721bf3c8b7b54c7450fe3fd", null ],
    [ "isIpxact", "structhif_1_1manipulation_1_1MergeTreesOptions.html#ac794378dd9d355e9581fa3af14371546", null ],
    [ "mergeBranches", "structhif_1_1manipulation_1_1MergeTreesOptions.html#a36d944084c1d7108a7c438c0ee262980", null ],
    [ "printInfos", "structhif_1_1manipulation_1_1MergeTreesOptions.html#a5e4e03505cc455d8bdf051f2e96bc3ac", null ]
];