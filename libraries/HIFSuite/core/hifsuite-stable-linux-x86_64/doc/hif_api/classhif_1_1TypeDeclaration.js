var classhif_1_1TypeDeclaration =
[
    [ "TypeDeclaration", "classhif_1_1TypeDeclaration.html#a3e31a236f32582bad1015afa54abff33", null ],
    [ "~TypeDeclaration", "classhif_1_1TypeDeclaration.html#ae7d07653eda69cd840239f33d272c0ad", null ],
    [ "_calculateFields", "classhif_1_1TypeDeclaration.html#a614874a4f2e32c7ceefbf8869c332560", null ],
    [ "_getFieldName", "classhif_1_1TypeDeclaration.html#a0b39f9e4d7bd0fead4e21adec7aaba52", null ],
    [ "getType", "classhif_1_1TypeDeclaration.html#ab3dd3195d6b886828bed2ff484abbdce", null ],
    [ "setType", "classhif_1_1TypeDeclaration.html#a3cb0e980a80dbe35c7e8c301d9d7100f", null ],
    [ "_type", "classhif_1_1TypeDeclaration.html#ac35788548bcc1fcaa1f0bb2a255a2f02", null ]
];