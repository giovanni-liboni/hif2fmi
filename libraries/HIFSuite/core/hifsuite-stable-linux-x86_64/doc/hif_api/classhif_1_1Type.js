var classhif_1_1Type =
[
    [ "TypeVariant", "classhif_1_1Type.html#a7d11764714c25175ce44451051de0b10", [
      [ "NATIVE_TYPE", "classhif_1_1Type.html#a7d11764714c25175ce44451051de0b10a3517dd0f4be1bdd584bfe047088405de", null ],
      [ "VHDL_BITVECTOR_NUMERIC_STD", "classhif_1_1Type.html#a7d11764714c25175ce44451051de0b10a98e2f84a316bf3bc3553bfcbc2378fad", null ],
      [ "SYSTEMC_INT_BITFIELD", "classhif_1_1Type.html#a7d11764714c25175ce44451051de0b10abee5cf7fc2bbe3f1c3f774ed659e3417", null ],
      [ "SYSTEMC_INT_SC_INT", "classhif_1_1Type.html#a7d11764714c25175ce44451051de0b10a1002c920082dd34468ad807ea242814d", null ],
      [ "SYSTEMC_INT_SC_BIGINT", "classhif_1_1Type.html#a7d11764714c25175ce44451051de0b10a30d31180d2be089333f1b0c7caa98d02", null ],
      [ "SYSTEMC_BITVECTOR_PROXY", "classhif_1_1Type.html#a7d11764714c25175ce44451051de0b10a034766aa7a708a29c7c2304f014561d1", null ],
      [ "SYSTEMC_BITVECTOR_BASE", "classhif_1_1Type.html#a7d11764714c25175ce44451051de0b10a500ecb962998377b2eb1da6e8dbe6321", null ],
      [ "SYSTEMC_BIT_BITREF", "classhif_1_1Type.html#a7d11764714c25175ce44451051de0b10ae8e3c3d4bc64b866d7023e46de0740e1", null ]
    ] ],
    [ "Type", "classhif_1_1Type.html#acf95c80d496af82bd5879721e8134739", null ],
    [ "~Type", "classhif_1_1Type.html#a8f71d01b7a5d774bea6692959b6daba1", null ],
    [ "_calculateFields", "classhif_1_1Type.html#aad069062667d336e7efd593ac6a12aac", null ],
    [ "getTypeVariant", "classhif_1_1Type.html#aa15abd343970ec63fdde44b8df57c058", null ],
    [ "setTypeVariant", "classhif_1_1Type.html#acedb110fa849d6553ffe22e04eabf03f", null ],
    [ "_typeVariant", "classhif_1_1Type.html#a522465d3d19d973658675a18d07852dc", null ]
];