var namespacehif_1_1semantics =
[
    [ "AnalyzeParams", "structhif_1_1semantics_1_1AnalyzeParams.html", "structhif_1_1semantics_1_1AnalyzeParams" ],
    [ "CheckOptions", "structhif_1_1semantics_1_1CheckOptions.html", "structhif_1_1semantics_1_1CheckOptions" ],
    [ "DeclarationOptions", "structhif_1_1semantics_1_1DeclarationOptions.html", "structhif_1_1semantics_1_1DeclarationOptions" ],
    [ "DeclarationsStack", "classhif_1_1semantics_1_1DeclarationsStack.html", "classhif_1_1semantics_1_1DeclarationsStack" ],
    [ "GetCandidatesOptions", "structhif_1_1semantics_1_1GetCandidatesOptions.html", "structhif_1_1semantics_1_1GetCandidatesOptions" ],
    [ "GetReferencesOptions", "structhif_1_1semantics_1_1GetReferencesOptions.html", "structhif_1_1semantics_1_1GetReferencesOptions" ],
    [ "HIFSemantics", "classhif_1_1semantics_1_1HIFSemantics.html", "classhif_1_1semantics_1_1HIFSemantics" ],
    [ "ILanguageSemantics", "classhif_1_1semantics_1_1ILanguageSemantics.html", "classhif_1_1semantics_1_1ILanguageSemantics" ],
    [ "ResetDeclarationsOptions", "structhif_1_1semantics_1_1ResetDeclarationsOptions.html", "structhif_1_1semantics_1_1ResetDeclarationsOptions" ],
    [ "SemanticAnalysis", "classhif_1_1semantics_1_1SemanticAnalysis.html", "classhif_1_1semantics_1_1SemanticAnalysis" ],
    [ "SystemCSemantics", "classhif_1_1semantics_1_1SystemCSemantics.html", "classhif_1_1semantics_1_1SystemCSemantics" ],
    [ "TypeVisitor", "classhif_1_1semantics_1_1TypeVisitor.html", "classhif_1_1semantics_1_1TypeVisitor" ],
    [ "UpdateDeclarationOptions", "structhif_1_1semantics_1_1UpdateDeclarationOptions.html", "structhif_1_1semantics_1_1UpdateDeclarationOptions" ],
    [ "VerilogSemantics", "classhif_1_1semantics_1_1VerilogSemantics.html", "classhif_1_1semantics_1_1VerilogSemantics" ],
    [ "VHDLSemantics", "classhif_1_1semantics_1_1VHDLSemantics.html", "classhif_1_1semantics_1_1VHDLSemantics" ]
];