var classhif_1_1features_1_1INamedObject =
[
    [ "INamedObject", "classhif_1_1features_1_1INamedObject.html#a3d058c74ff88ecd9f785fc4c5b84e1c3", null ],
    [ "~INamedObject", "classhif_1_1features_1_1INamedObject.html#a46be5f77d78b46d5de5cbe41337113b6", null ],
    [ "INamedObject", "classhif_1_1features_1_1INamedObject.html#a8ad6c1aa9a320a0b3fad68bfa6f0e7fe", null ],
    [ "getName", "classhif_1_1features_1_1INamedObject.html#ae9c853e1d417a644baf8da674019a7b8", null ],
    [ "matchName", "classhif_1_1features_1_1INamedObject.html#a8683e9ad8e7b7fa375d62da565ffcd60", null ],
    [ "operator=", "classhif_1_1features_1_1INamedObject.html#a9641c3d6c99d9ae1781852a54aebfd17", null ],
    [ "setName", "classhif_1_1features_1_1INamedObject.html#a4001bdbda039d737e153ac675dc0f480", null ]
];