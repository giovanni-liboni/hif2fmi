var structhif_1_1semantics_1_1DeclarationOptions =
[
    [ "DeclarationOptions", "structhif_1_1semantics_1_1DeclarationOptions.html#a1f40a43f3266f6cb16bfc7f54095cf52", null ],
    [ "~DeclarationOptions", "structhif_1_1semantics_1_1DeclarationOptions.html#a6cb1dba3b69d48d03acbaf28738e491a", null ],
    [ "DeclarationOptions", "structhif_1_1semantics_1_1DeclarationOptions.html#a2ab53c92dc6444ac9d0a82fdcf3d36a3", null ],
    [ "operator=", "structhif_1_1semantics_1_1DeclarationOptions.html#a44058a8624bd85bc6a783d49173260cc", null ],
    [ "swap", "structhif_1_1semantics_1_1DeclarationOptions.html#a9282bb9a4184183747b49765c723c91f", null ],
    [ "dontSearch", "structhif_1_1semantics_1_1DeclarationOptions.html#a05ea25013afc5dea8832cfb370da81d1", null ],
    [ "error", "structhif_1_1semantics_1_1DeclarationOptions.html#a90548e8fe10173985f54d6b4c8fcdde9", null ],
    [ "forceRefresh", "structhif_1_1semantics_1_1DeclarationOptions.html#a986c8c8fcbaa3fd9b3cec9cb87c54bc3", null ],
    [ "location", "structhif_1_1semantics_1_1DeclarationOptions.html#a062b8af066dc54c2055264ce10cc5306", null ],
    [ "looseTypeChecks", "structhif_1_1semantics_1_1DeclarationOptions.html#a351dfb0b40919d9196d13c8e2fb73c74", null ],
    [ "onlyVisible", "structhif_1_1semantics_1_1DeclarationOptions.html#a57c5ce4418f1edeaeedd45fc8c9aaef9", null ]
];