var structhif_1_1manipulation_1_1FixAssignmentOptions =
[
    [ "FixAssignmentOptions", "structhif_1_1manipulation_1_1FixAssignmentOptions.html#a2d1b025c113a3f3cee45c5e0b75157fa", null ],
    [ "~FixAssignmentOptions", "structhif_1_1manipulation_1_1FixAssignmentOptions.html#aeeb1c19ae67c45b0e31786ce0746d522", null ],
    [ "FixAssignmentOptions", "structhif_1_1manipulation_1_1FixAssignmentOptions.html#a75b75c6cc3337b99d1d0a1e63eeab40b", null ],
    [ "hasSomethingToFix", "structhif_1_1manipulation_1_1FixAssignmentOptions.html#a872b161d529837ea2de2990c910e9762", null ],
    [ "operator=", "structhif_1_1manipulation_1_1FixAssignmentOptions.html#aea4a07c07aea8f9fcf1fc7383132c7f1", null ],
    [ "swap", "structhif_1_1manipulation_1_1FixAssignmentOptions.html#a2aeb84ce4c878b09f559a42bdf113405", null ],
    [ "fixAssigns", "structhif_1_1manipulation_1_1FixAssignmentOptions.html#aad7929edf4eb2ded22dc08865b3400a6", null ],
    [ "fixDataDeclarationsValue", "structhif_1_1manipulation_1_1FixAssignmentOptions.html#a0c51c9a1165e38e6c9b4896b9cc1ad97", null ],
    [ "fixParameterAssigns", "structhif_1_1manipulation_1_1FixAssignmentOptions.html#a8500bdb7817dfc8be5d1fb8742fce84a", null ]
];