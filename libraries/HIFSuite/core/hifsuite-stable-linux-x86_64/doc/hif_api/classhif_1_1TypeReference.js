var classhif_1_1TypeReference =
[
    [ "TypeReference", "classhif_1_1TypeReference.html#a224429c3ee73341707d668a3fd28d536", null ],
    [ "~TypeReference", "classhif_1_1TypeReference.html#a1b9fb2a224ae310ff34ac4b90638f6f4", null ],
    [ "_calculateFields", "classhif_1_1TypeReference.html#a8f9e346464da2511d1199e232ac30b23", null ],
    [ "_getBListName", "classhif_1_1TypeReference.html#a8aa90df711eb4c06597f6569cfbc2a2a", null ],
    [ "acceptVisitor", "classhif_1_1TypeReference.html#ae19d4e98129af9b1dc1f4a1252ed5276", null ],
    [ "getClassId", "classhif_1_1TypeReference.html#a711823605d5a34637e2b18c4b290bcf3", null ],
    [ "toObject", "classhif_1_1TypeReference.html#a2b614361b40b425172d20dd24e3c1454", null ],
    [ "ranges", "classhif_1_1TypeReference.html#a638661e967ed84caa02f39fb79062936", null ],
    [ "templateParameterAssigns", "classhif_1_1TypeReference.html#afbae9653fa8cd1564182f2aa7f4016ce", null ]
];