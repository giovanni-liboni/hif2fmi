var classhif_1_1TypeDef =
[
    [ "TypeDef", "classhif_1_1TypeDef.html#a989ad484276bbba0be88c94242e8f59b", null ],
    [ "~TypeDef", "classhif_1_1TypeDef.html#a3eb4201f32d6d585cc009b1e95812008", null ],
    [ "_calculateFields", "classhif_1_1TypeDef.html#a9a4bbb5901ca165fa3bd348c289cdeb9", null ],
    [ "_getBListName", "classhif_1_1TypeDef.html#a66e3f9ed9bd48c291f933a4b111a269a", null ],
    [ "_getFieldName", "classhif_1_1TypeDef.html#a634d58b977acbc57905df0d45551d0f9", null ],
    [ "acceptVisitor", "classhif_1_1TypeDef.html#a5b50f59ad7f23e753d278c781e531a79", null ],
    [ "getClassId", "classhif_1_1TypeDef.html#a3f9435e841261a82a293d08e8d41b9de", null ],
    [ "getRange", "classhif_1_1TypeDef.html#aebc65682bc9374abf07141c1b395977a", null ],
    [ "isExternal", "classhif_1_1TypeDef.html#a6ba5733a08f7ee61ca7a713ec70da1ea", null ],
    [ "isOpaque", "classhif_1_1TypeDef.html#a7ee0655d83ebe6a77813af1c885bd64a", null ],
    [ "isStandard", "classhif_1_1TypeDef.html#ad3be82dc3277c628d779ce9c92b51945", null ],
    [ "setExternal", "classhif_1_1TypeDef.html#ac6dedc90f2b001bb30c7ffa401d4322d", null ],
    [ "setOpaque", "classhif_1_1TypeDef.html#ab19794b6c9cdac2679adb8e888e84966", null ],
    [ "setRange", "classhif_1_1TypeDef.html#ae18582f910453412044a4a7c954b45df", null ],
    [ "setStandard", "classhif_1_1TypeDef.html#af943475d483f26a0b8424e8daf253aae", null ],
    [ "templateParameters", "classhif_1_1TypeDef.html#aea0882d34d838c1d74d70a930bb044a0", null ]
];