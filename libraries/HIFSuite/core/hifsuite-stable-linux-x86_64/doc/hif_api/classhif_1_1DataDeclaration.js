var classhif_1_1DataDeclaration =
[
    [ "DataDeclaration", "classhif_1_1DataDeclaration.html#aea90bb6183029571347b8395ac189a6b", null ],
    [ "~DataDeclaration", "classhif_1_1DataDeclaration.html#aefa2ddbb2c910b209cf86441bdc1b83c", null ],
    [ "_calculateFields", "classhif_1_1DataDeclaration.html#ad3b0a6936d7bfe0a79a26422b22c8e87", null ],
    [ "_getFieldName", "classhif_1_1DataDeclaration.html#a27ed2323379588cd2171d63020e37c71", null ],
    [ "getRange", "classhif_1_1DataDeclaration.html#a8404db3ddda310854ec39e6dd061c3b0", null ],
    [ "getType", "classhif_1_1DataDeclaration.html#aab95d43b99b9aa3f6380417d3644c28d", null ],
    [ "getValue", "classhif_1_1DataDeclaration.html#adebde53aabbb9c773e26e6bc9452095a", null ],
    [ "setRange", "classhif_1_1DataDeclaration.html#afc2705613c7220b42a34091e68d1612a", null ],
    [ "setType", "classhif_1_1DataDeclaration.html#af0fe5ea42cdac721aabae6de65a5921e", null ],
    [ "setValue", "classhif_1_1DataDeclaration.html#a683134b52bc566b2c962a33b36b68f08", null ],
    [ "_range", "classhif_1_1DataDeclaration.html#a8944efc2d2354c1a585f6de6d48487f3", null ],
    [ "_type", "classhif_1_1DataDeclaration.html#a68b60e679a43cdbca10c3013e682e29e", null ],
    [ "_value", "classhif_1_1DataDeclaration.html#a7ef53c46950f7b4188cce4132bd7dd18", null ]
];