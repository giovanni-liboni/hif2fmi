var classhif_1_1HifTypedQuery =
[
    [ "Results", "classhif_1_1HifTypedQuery.html#ad3ad29284e4d8ea3e14da9c5a0bd6841", null ],
    [ "HifTypedQuery", "classhif_1_1HifTypedQuery.html#a65040f8a0635c15112fc2faa05f6fbf1", null ],
    [ "~HifTypedQuery", "classhif_1_1HifTypedQuery.html#a62a0ae82da088863bb31f92751b4a8de", null ],
    [ "getNextQueryType", "classhif_1_1HifTypedQuery.html#a0b63e4c32bab287f1a1dc92834bf8e67", null ],
    [ "isSameType", "classhif_1_1HifTypedQuery.html#a963a114ce6fc42fd592758470e6c0e14", null ],
    [ "setNextQueryType", "classhif_1_1HifTypedQuery.html#a6258aa0fdeba07f8d128094c0bf6388e", null ]
];