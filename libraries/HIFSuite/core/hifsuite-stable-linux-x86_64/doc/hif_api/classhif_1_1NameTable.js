var classhif_1_1NameTable =
[
    [ "ForbiddenNames", "classhif_1_1NameTable.html#aa5c5f3267c9c35e4e19a88f976818580", null ],
    [ "NameMap", "classhif_1_1NameTable.html#a82d092bf709a1ba6e220b338eb148023", null ],
    [ "any", "classhif_1_1NameTable.html#ae6b3f07cde9025fcb83f64c817686faf", null ],
    [ "getFreshName", "classhif_1_1NameTable.html#a06feee8d12b7a3f925f4d8f99e3aba3f", null ],
    [ "getFreshName", "classhif_1_1NameTable.html#a94937ff128000823fdec5f0c7e3eaa6b", null ],
    [ "getName", "classhif_1_1NameTable.html#aff4a993d83449508dde35652ec619a29", null ],
    [ "getOrCreateName", "classhif_1_1NameTable.html#a9be5de0622e1ddae3b6640bf6e32a2e6", null ],
    [ "getOrCreateName", "classhif_1_1NameTable.html#ab8760ac966d778930389017dddddba39", null ],
    [ "hifConstructor", "classhif_1_1NameTable.html#aab342a059958682f634bbe9f4c623e84", null ],
    [ "hifDestructor", "classhif_1_1NameTable.html#a23ffd6f712fd66b71a26a557e82903f0", null ],
    [ "hifEmptyStringName", "classhif_1_1NameTable.html#a365eaf08c312bc6d46fb62480dbf7bab", null ],
    [ "hifGlobals", "classhif_1_1NameTable.html#af024a436d0723232c9e0f45fe92b5100", null ],
    [ "hifStringNames", "classhif_1_1NameTable.html#a7e55ac14b308c9b43795561261db1e00", null ],
    [ "none", "classhif_1_1NameTable.html#a04a635539c4cd160051cbd0005adf34c", null ],
    [ "printNameTable", "classhif_1_1NameTable.html#a2d412ec308ec59ad8469148f212b1aa6", null ],
    [ "resetTmpInfos", "classhif_1_1NameTable.html#a7a6b40bb2e4f90a68b43a37d0389550c", null ],
    [ "setForbiddenListFromFile", "classhif_1_1NameTable.html#adfb6196e1c256780451054581b61083a", null ]
];