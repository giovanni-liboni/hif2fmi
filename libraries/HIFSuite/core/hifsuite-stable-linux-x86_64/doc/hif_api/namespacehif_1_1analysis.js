var namespacehif_1_1analysis =
[
    [ "AnalyzeProcessOptions", "structhif_1_1analysis_1_1AnalyzeProcessOptions.html", "structhif_1_1analysis_1_1AnalyzeProcessOptions" ],
    [ "AnalyzeSpansResult", "structhif_1_1analysis_1_1AnalyzeSpansResult.html", "structhif_1_1analysis_1_1AnalyzeSpansResult" ],
    [ "DelayInfos", "structhif_1_1analysis_1_1DelayInfos.html", "structhif_1_1analysis_1_1DelayInfos" ],
    [ "DelayProperties", "structhif_1_1analysis_1_1DelayProperties.html", "structhif_1_1analysis_1_1DelayProperties" ],
    [ "IndexInfo", "structhif_1_1analysis_1_1IndexInfo.html", "structhif_1_1analysis_1_1IndexInfo" ],
    [ "ProcessInfos", "structhif_1_1analysis_1_1ProcessInfos.html", "structhif_1_1analysis_1_1ProcessInfos" ],
    [ "Types", "structhif_1_1analysis_1_1Types.html", "structhif_1_1analysis_1_1Types" ]
];