var structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlLv_3_01size_01_4_01_4 =
[
    [ "ValueType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlLv_3_01size_01_4_01_4.html#adda0624881ad69398e66061a1638ebc4", null ],
    [ "VectorType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlLv_3_01size_01_4_01_4.html#a8e9fb3675bdbf78c0508b69e5e6bb4fe", null ],
    [ "VectorTypeReturn", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlLv_3_01size_01_4_01_4.html#a24856eb14d47ebbd9fa74908bfb372c4", null ],
    [ "VectorSize", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlLv_3_01size_01_4_01_4.html#a28a51b61c1246ad0f2e2ec50acfa5db9", [
      [ "SIZE", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlLv_3_01size_01_4_01_4.html#a28a51b61c1246ad0f2e2ec50acfa5db9aac3c8bef49d10ab80673669e45a016cc", null ]
    ] ]
];