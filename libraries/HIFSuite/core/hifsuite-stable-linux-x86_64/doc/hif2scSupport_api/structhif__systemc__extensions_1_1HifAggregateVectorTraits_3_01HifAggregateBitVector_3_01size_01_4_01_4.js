var structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateBitVector_3_01size_01_4_01_4 =
[
    [ "ValueType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateBitVector_3_01size_01_4_01_4.html#aa1d5b00f76caa4b6f4c703ed36c1472d", null ],
    [ "VectorType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateBitVector_3_01size_01_4_01_4.html#a7e42874f705ceb04166756fa94a74b7d", null ],
    [ "VectorTypeReturn", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateBitVector_3_01size_01_4_01_4.html#a9cace84cac66149ba1c93e08234724ad", null ],
    [ "VectorSize", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateBitVector_3_01size_01_4_01_4.html#aec8128c2693b2eb74ff986d839a6a8af", [
      [ "SIZE", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateBitVector_3_01size_01_4_01_4.html#aec8128c2693b2eb74ff986d839a6a8afa123ec8ba56f8980256183df02d711f73", null ]
    ] ]
];