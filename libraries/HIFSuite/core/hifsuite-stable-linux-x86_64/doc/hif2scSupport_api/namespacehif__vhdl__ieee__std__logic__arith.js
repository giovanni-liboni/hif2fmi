var namespacehif__vhdl__ieee__std__logic__arith =
[
    [ "RetType", "structhif__vhdl__ieee__std__logic__arith_1_1RetType.html", "structhif__vhdl__ieee__std__logic__arith_1_1RetType" ],
    [ "RetType< size, hdtlib::hl_bv_t >", "structhif__vhdl__ieee__std__logic__arith_1_1RetType_3_01size_00_01hdtlib_1_1hl__bv__t_01_4.html", "structhif__vhdl__ieee__std__logic__arith_1_1RetType_3_01size_00_01hdtlib_1_1hl__bv__t_01_4" ],
    [ "RetType< size, hdtlib::hl_lv_t >", "structhif__vhdl__ieee__std__logic__arith_1_1RetType_3_01size_00_01hdtlib_1_1hl__lv__t_01_4.html", "structhif__vhdl__ieee__std__logic__arith_1_1RetType_3_01size_00_01hdtlib_1_1hl__lv__t_01_4" ]
];