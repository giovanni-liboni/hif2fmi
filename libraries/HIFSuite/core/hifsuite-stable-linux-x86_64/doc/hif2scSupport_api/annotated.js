var annotated =
[
    [ "hif_systemc_extensions", "namespacehif__systemc__extensions.html", "namespacehif__systemc__extensions" ],
    [ "hif_verilog_standard", "namespacehif__verilog__standard.html", "namespacehif__verilog__standard" ],
    [ "hif_vhdl_ieee_math_complex", "namespacehif__vhdl__ieee__math__complex.html", null ],
    [ "hif_vhdl_ieee_math_real", "namespacehif__vhdl__ieee__math__real.html", null ],
    [ "hif_vhdl_ieee_numeric_bit", "namespacehif__vhdl__ieee__numeric__bit.html", null ],
    [ "hif_vhdl_ieee_numeric_std", "namespacehif__vhdl__ieee__numeric__std.html", null ],
    [ "hif_vhdl_ieee_std_logic_1164", "namespacehif__vhdl__ieee__std__logic__1164.html", null ],
    [ "hif_vhdl_ieee_std_logic_arith", "namespacehif__vhdl__ieee__std__logic__arith.html", "namespacehif__vhdl__ieee__std__logic__arith" ],
    [ "hif_vhdl_ieee_std_logic_arith_ex", "namespacehif__vhdl__ieee__std__logic__arith__ex.html", null ],
    [ "hif_vhdl_ieee_std_logic_misc", "namespacehif__vhdl__ieee__std__logic__misc.html", null ],
    [ "hif_vhdl_ieee_std_logic_signed", "namespacehif__vhdl__ieee__std__logic__signed.html", null ],
    [ "hif_vhdl_ieee_std_logic_textio", "namespacehif__vhdl__ieee__std__logic__textio.html", null ],
    [ "hif_vhdl_ieee_std_logic_unsigned", "namespacehif__vhdl__ieee__std__logic__unsigned.html", null ],
    [ "hif_vhdl_standard", "namespacehif__vhdl__standard.html", null ],
    [ "hif_vhdl_std_textio", "namespacehif__vhdl__std__textio.html", null ]
];