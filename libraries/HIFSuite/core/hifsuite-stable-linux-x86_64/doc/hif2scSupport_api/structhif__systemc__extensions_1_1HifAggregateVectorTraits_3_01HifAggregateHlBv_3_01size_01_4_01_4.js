var structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlBv_3_01size_01_4_01_4 =
[
    [ "ValueType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlBv_3_01size_01_4_01_4.html#a3ab11300d02cfac25375d90c75f6de1a", null ],
    [ "VectorType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlBv_3_01size_01_4_01_4.html#ad120b79649e4df05c868f7adbdffde37", null ],
    [ "VectorTypeReturn", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlBv_3_01size_01_4_01_4.html#a153ae19b605d801c8dff3a0b05fd9e55", null ],
    [ "VectorSize", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlBv_3_01size_01_4_01_4.html#a1675b4a948ca670be9b0a2740bbd4c76", [
      [ "SIZE", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateHlBv_3_01size_01_4_01_4.html#a1675b4a948ca670be9b0a2740bbd4c76aa9081135a0bb620db365e83529c9384b", null ]
    ] ]
];