var structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateLogicVector_3_01size_01_4_01_4 =
[
    [ "ValueType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateLogicVector_3_01size_01_4_01_4.html#a9ec17209af5d494c48f7ad977008ea5f", null ],
    [ "VectorType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateLogicVector_3_01size_01_4_01_4.html#aa9fd4b061317e8f82f2d3be22f87388d", null ],
    [ "VectorTypeReturn", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateLogicVector_3_01size_01_4_01_4.html#a208d0a6178b06bb4170b41ef255cfa91", null ],
    [ "VectorSize", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateLogicVector_3_01size_01_4_01_4.html#af3b1e289ce13d13da756fc45a4ac89b4", [
      [ "SIZE", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateLogicVector_3_01size_01_4_01_4.html#af3b1e289ce13d13da756fc45a4ac89b4ae738f001e32f876bd9e2623374fc937b", null ]
    ] ]
];