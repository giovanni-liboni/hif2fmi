var structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateArray_3_01T_00_01size_01_4_01_4 =
[
    [ "ValueType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateArray_3_01T_00_01size_01_4_01_4.html#a2f58615c2fcf8a01766e9ed910e8ace0", null ],
    [ "VectorType", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateArray_3_01T_00_01size_01_4_01_4.html#a8ee58a804d6ef23bb593ae4acd88db30", null ],
    [ "VectorTypeReturn", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateArray_3_01T_00_01size_01_4_01_4.html#aece84d9ce2976f2ef6468c774f48d296", null ],
    [ "VectorSize", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateArray_3_01T_00_01size_01_4_01_4.html#a506c948fa1920031ca40242269adce72", [
      [ "SIZE", "structhif__systemc__extensions_1_1HifAggregateVectorTraits_3_01HifAggregateArray_3_01T_00_01size_01_4_01_4.html#a506c948fa1920031ca40242269adce72ab65f6c07c99bdafddeaf1df434d3a744", null ]
    ] ]
];