/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// Class to represent the list of names in System description.
/// Can be used for efficient handling of the names used in the design.
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_LIBRARY_NAME_TABLE_H
#define HIF_LIBRARY_NAME_TABLE_H

/// PROJECT INCLUDES
///
#include "applicationUtils/portability.hh"

// SYSTEM INCLUDES
//
#include <string>
#include <ctime>
#include <list>
#include <set>
#include <map>


namespace hif {


class NameTable;

///
/// name
///
///	@brief This class is a class for identifiers.
///
class HIF_EXPORT name
{
    friend class NameTable;
public:

    ///
    /// @brief Function that return the string corresponding to the identifier _string.
    ///
    /// @return a char * corresponding to the identifier
    ///
    char * getString() const;

    ///
    /// @brief Function that return the string corresponding to the identifier _string.
    ///
    /// @return a char * corresponding to the identifier
    ///
    char * c_str() const;

    ///
    /// @brief Function that check if a name is create without set any string value.
    ///	If true is set to default value.
    /// Default value for uninitialized is "(no name)".
    ///
    /// @return -1 if name is set to default value "(no name)", 0 otherwise.
    ///
    bool isDefaultValue() const;

    ///
    /// @brief Stores temporary information in the identifier.
    /// The temporary information is of type unsigned int.
    ///	This is used mainly for binary file saving.
    ///
    /// @param ti information that we want save.
    ///
    void setTmpInfo(const unsigned int ti);

    ///
    ///	@brief Return temporary information in the identifier.
    ///
    /// @return value that represents tmp info saved into _tmp_info field
    ///
    unsigned int getTmpInfo() const;

protected:

    /// @brief Constructor
    ///
    /// Create a new name starting from string representation.
    ///
    /// @param str the string value of name that we want add to list.
    ///
    name( const char* str);

    /// Destructor
    ///
    ~name();

private:

    std::string _string;
    unsigned int _tmp_info;

    /// @brief Copy constructor
    ///
    /// Disabled copy constructor.
    ///
    name (const name & );

    /// @brief Assignment operator
    ///
    /// Disabled assigment operator.
    ///
    name & operator = (const name & );


};



/// NameTable
///
///	@brief This class is a table containing all names.
/// This class contains all names (identifiers) used in Hif system descriptions.
/// As names are unique in the table, they can be
///	compared using == and != (and comparison costs a pointer
///	comparison.
///
/// Design patters:
/// Singleton
///
class HIF_EXPORT NameTable
{
public:
	typedef std::map<std::string, name*> NameMap;
	typedef std::set<std::string> ForbiddenNames;
	
    /// Singleton stuff.
    ///
    /// @brief Function thats return an instance to NameTable class.
    ///
    static NameTable* getInstance ();

    ///
    /// @brief Sets the List of reserved names, that should not be converted in uppercase or lowercase
    ///
    /// @param file_name the filename (full_path) that contains reserved names. It should be formatted as follow:
    ///		one name for lines (whitout spaces)
    /// @param append boolean that indicates if append.
    /// @returns a boolean indicating if the file was correctly loaded into nametable.
    ///
    bool setForbiddenListFromFile (std::string file_name, bool append = false);

    ///
    /// @brief Return the special "none" name.
    ///
    /// @return return m_none value.
    ///
    name* none () const;

    ///
    /// @brief Return the special "any" name.
    ///
    /// @return return m_any value.
    ///
    name* any () const;

    ///
    /// @brief Return the special "hif_string_names" name.
    ///
    /// @return return hif_string_names.
    ///
    name* hifStringNames ();

    /// @brief Returns the special name to signal a empty string.
    ///
    /// @return The string.
    ///
    name* hifEmptyStringName();

    /// @brief Returns the special name to global HIF library.
    ///
    /// @return The string.
    ///
    name* hifGlobals();

    /// @brief Returns the special name "__hif_constructor" name.
    ///
    /// @return The string.
    ///
    name* hifConstructor();

    /// @brief Returns the special name "__hif_destructor" name.
    ///
    /// @return The string.
    ///
    name* hifDestructor();

    ///
    /// @brief Return the name associated to a given string.
    ///
    /// @param s the string whose name is sought.
    /// @return The name if it is in the table, NULL otherwise.
    ///
    name* getName (const char * s);

    /// @brief Return the name associated to a given string.
    /// This one creates the name if it is not in the table yet.
    ///
    /// @param s the string whose name is sought.
    /// @return The name.
    ///
    name* getOrCreateName (const char * s);

    /// @brief Return the name associated to a given string.
    /// This one creates the name if it is not in the table yet.
    ///
    /// @param s the string whose name is sought.
    /// @param index The index to concat at the end of @p s.
    /// @return The name.
    ///
    name* getOrCreateName (const std::string & s, const int index);

    ///
    /// @brief Reset the temporary info of all names to 0.
    ///
    void resetTmpInfos ();

    ///
    /// @brief Return a fresh name.
    /// A name is fresh if it does not occur in the file.
    /// This function returns a name containing exactly the prefix if
    /// - a prefix is given
    /// - prefix is not already present
    /// .
    /// Otherwise it generates a fresh name using the prefix string as prefix.
    ///
    /// @param prefix [optional] the string prefix to be used to generate a fresh name
    /// @return the fresh name.
    ///
    name* getFreshName (const char * prefix = "");

    /// @brief Return a fresh name.
    /// A name is fresh if it does not occur in the file.
    ///
    /// @param n The old name.
    /// @param suffix [optional] the string prefix to be used to generate a fresh name
    /// @return The fresh name.
    ///
    name* getFreshName (name * n, const char * suffix = "");

    ///
    /// @brief Print the NameTable content
    ///
    void printNameTable ();

private:

    NameMap m_name_map;
	ForbiddenNames fobbidden_name_list;

    /// @brief Default Constructor
    ///
    NameTable ();

    /// @brief Copy constructor
    ///
    /// Disabled copy constructor.
    ///
    NameTable (NameTable &);

    /// @brief Assignment operator
    ///
    /// Disabled assignment operator.
    ///
    NameTable & operator = ( NameTable & );

    /// @brief Default Destructor
    ///
    ~NameTable ();

    ///
    /// @brief Function that generate a random string.
    ///
    /// @return a random string generated by function
    ///
    std::string generateRndStr ();

    ///
    /// Function that generate a random char
    ///
    /// @return a random char generated by function
    ///
    char generateRndChr ();

};

typedef name* Name;

#define NAME_NONE NameTable::getInstance()->none()
#define NAME_ANY NameTable::getInstance()->any()

HIF_EXPORT char* Name2Str(Name n);
HIF_EXPORT Name Str2Name(const char* s);
HIF_EXPORT Name Str2Name(const std::string s);

} // namespace hif

#endif
