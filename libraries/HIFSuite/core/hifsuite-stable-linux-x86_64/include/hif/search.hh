///
/// @file
/// Copyright (C) 2008
///
/// COPYRIGHT
///

#ifndef HIF_SEARCH_HH_
#define HIF_SEARCH_HH_

#include "applicationUtils/portability.hh"
#include "classes/classes.hh"
#include "../hifCore.hh"

namespace hif {

///	@name Search function.
///	Those functions are useful to search one or more objects
///	in a subtree or list of subtrees of Hif descriptions.
///
/// @{

class HIF_EXPORT HifQueryBase
{
public:
    typedef unsigned int Depth;
    typedef bool (*CollectObjectMethod)(Object*, const HifQueryBase *);
    
    /// @brief The maximum search depth.
    Depth depth;
    /// @brief The name to be searched.
    Name name;
    /// @brief A set of objects to avoid. This is useful when the research must
    /// not consider nested objects, independently from the depth.
    /// @warning At the moment it is not supported yet.
    std::set< ClassId > classToAvoid;
    /// @brief If set, object is collected only if the method returns true.
    CollectObjectMethod collectObjectMethod;
    /// @brief If set search inside method calls declarations;
    bool checkInsideCallsDeclarations;
    /// @brief If set, search stops at first match.
    bool onlyFirstMatch;
    /// @brief If set, skips standard scopes. Default is false.
    bool skipStandardScopes;
    /// @brief The semantics.
    hif::semantics::ILanguageSemantics * sem;
    
    virtual
    bool isSameType(Object *) const = 0;
    
protected:
    
    HifQueryBase();
    virtual ~HifQueryBase();
    
private:
    
    HifQueryBase(const HifQueryBase &);
    HifQueryBase & operator = (const HifQueryBase &);
};


/// @tparam T The type to be searched.
template< class T >
class HifTypedQuery:
        public HifQueryBase
{
public:
    typedef std::list<T *> Results;

    HifTypedQuery();
    virtual ~HifTypedQuery();
    
    virtual
    bool isSameType(Object * o) const;
    
    HifQueryBase * getNextQueryType() const;
    
    template<typename P>
    void setNextQueryType(HifTypedQuery<P> * value);
    
private:
    
    HifQueryBase * nextQueryType;
    
    HifTypedQuery(const HifTypedQuery &);
    HifTypedQuery & operator = (const HifTypedQuery &);
};

template< class T>
HifTypedQuery<T>::HifTypedQuery():
    HifQueryBase(),
    nextQueryType(NULL)
{
    // ntd
}

template< class T>
HifTypedQuery<T>::~HifTypedQuery()
{
    // ntd
}

template< class T>
bool HifTypedQuery<T>::isSameType(Object * o) const
{
    if (nextQueryType != NULL && nextQueryType->isSameType(o)) return true;
    return (dynamic_cast<T*>(o) != NULL);
}

template< class T>
HifQueryBase * HifTypedQuery<T>::getNextQueryType() const
{
    return nextQueryType;
}

template< class T>
template<typename P>
void HifTypedQuery<T>::setNextQueryType(HifTypedQuery<P> * value)
{
    nextQueryType = value;
}

class HIF_EXPORT HifUntypedQuery:
        public HifQueryBase
{
public:
    typedef std::list<Object*> Results;

    HifUntypedQuery();
    virtual ~HifUntypedQuery();
    
    virtual
    bool isSameType(Object * o) const;
    
private:
    
    HifUntypedQuery(const HifUntypedQuery &);
    HifUntypedQuery & operator = (const HifUntypedQuery &);
};


/// @brief Finds the objects which match specific criteria.
///	This function will go down the Hif tree starting from a given object
/// until it reaches the leaves of the Hif tree (or the max depth, if this
/// criterion is set).
///
/// @param result The list where to store the found objects.
///	@param root The search starting point.
/// @param query An HifQueryBase child object with search parameters already set.
///
HIF_EXPORT
void search(std::list<Object*> & result, Object* root, const HifQueryBase & query);

/// @brief Finds the objects which match specific criteria.
/// This function will go down the Hif tree starting from a given object
/// until it reaches the leaves of the Hif tree (or the max depth, if this
/// criterion is set).
///
/// @param result The list where to store the found objects.
/// @param root The search starting point.
/// @param query An HifQueryBase child object with search parameters already set.
///
template< typename T >
void search(std::list<T*> & result, Object* root, const HifQueryBase & query)
{
    std::list<Object*> * tmp = reinterpret_cast<std::list<Object*>* >(&result);
    search(*tmp, root, query);
}

/// @brief Finds the objects which match specific criteria.
/// This function will go down the Hif tree starting from a given object
/// until it reaches the leaves of the Hif tree (or the max depth, if this
/// criterion is set).
///
/// @param result The list where to store the found objects.
/// @param root The list of root objects to be used as search starting point.
/// @param query An HifQueryBase child object with search parameters already set.
///
template <typename T1, typename T2>
void search(std::list<T1*> & result, BList<T2> & root, const HifQueryBase & query)
{
    for(typename BList<T2>::iterator iter = root.begin();
        iter != root.end(); ++iter)
    {
        search(result, *iter, query);
    }
}

/// @}

}


#endif /* HIF_SEARCH_HH_ */
