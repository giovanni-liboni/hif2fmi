#ifndef HIF_APPLICATIONUTILS_STEPFILEMANAGER_HH
#define HIF_APPLICATIONUTILS_STEPFILEMANAGER_HH

#include "portability.hh"
#include "../hifIOUtils.hh"

namespace hif {  namespace applicationUtils {

class HIF_EXPORT StepFileManager
{
public:
    StepFileManager();
    virtual ~StepFileManager();

    hif::PrintHifOptions getPrintOpt() const;
    void setPrintOpt(const hif::PrintHifOptions & opt);

    std::string getPrefix() const;
    void setPrefix(const std::string & prefix);

    std::string getSuffix() const;
    void setSuffix(const std::string & suffix);

    int getStepNumber() const;
    void setStepNumber(int stepNumber);

    StepFileManager * getParentManager() const;
    void setParentManager(StepFileManager * parentManager);

    bool getPrint() const;
    void setPrint(bool print);

    void printStep(System * s, const std::string & stepName);
    void startStep(const std::string & stepName);
    void endStep(System * s);
    const std::string & getCurrentStepName() const;
    std::string getCurrentStepInfo() const;

    void setAutoStepFile(const std::string & autoStepFile);
    const std::string & getAutoStepFile() const;
    bool checkStepName();

private:
    hif::PrintHifOptions _opt;
    std::string _prefix;
    std::string _suffix;
    std::string _currentStep;
    int _stepNumber;
    std::string _autoStepFile;
    int _autoStepNumber;
    int _currentAutoStepNumber;
    StepFileManager * _parentManager;
    bool _print;

    StepFileManager (const StepFileManager &);
    StepFileManager & operator = (const StepFileManager &);
};

}} // hif::applicationUtils

#endif // HIF_APPLICATIONUTILS__STEPFILEMANAGER_HH
