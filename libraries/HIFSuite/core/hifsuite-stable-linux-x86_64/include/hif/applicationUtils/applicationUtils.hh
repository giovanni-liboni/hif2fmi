#ifndef HIF_APPLICATIONUTILS_HH
#define HIF_APPLICATIONUTILS_HH

#include "CommandLineParser.hh"
#include "ConfigurationManager.hh"
#include "dumpVersion.hh"
#include "FileStructure.hh"
#include "StepFileManager.hh"
//#include "hifGetOpt.hh"
#include "licenseManager.hh"
//#include "lmxsupport.hh"
#include "Log.hh"
#include "portability.hh"

namespace hif {

/// @brief Wraps utilities to write potable applications.
namespace applicationUtils { }

} // hif

#endif
