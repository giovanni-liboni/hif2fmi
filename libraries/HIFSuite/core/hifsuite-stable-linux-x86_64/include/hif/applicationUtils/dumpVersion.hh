#ifndef HIF_DUMP_VERSION_HH_
#define HIF_DUMP_VERSION_HH_

#include <string>
#include "portability.hh"

namespace hif { namespace applicationUtils {

/// @brief Returns the current HIF tag.
HIF_EXPORT
const std::string & getHIFSuiteVersion();

/// @brief Returns the current HIF revno.
HIF_EXPORT
const std::string & getHIFSuiteRevision();

/// @brief prints a banner with current HIFSuite infos.
HIF_EXPORT
void dumpVersion(std::string const& tool_name);

}} // namespace hif::applicationUtils

#endif // _dumpVersion_hh_
