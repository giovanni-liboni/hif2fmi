#ifndef HIF_APPLICATIONUTILS_FILESTRUCTURE_HH
#define HIF_APPLICATIONUTILS_FILESTRUCTURE_HH

//
// Header files
//

///
/// SYSTEM INCLUDES
#include <cstdio>
#include <ctime>
#include <vector>
#include <string>

///
/// LOCAL INCLUDES
#include "portability.hh"
#include "Log.hh"

namespace hif { namespace applicationUtils {

/// FileStructure
///
/// @brief Files management API, this class provide a
/// common, platform independent access to system file names.
///
class HIF_EXPORT FileStructure
{
private :

	///
	/// @brief abtract name (list of fields) corresponding to a path
	std::vector<std::string> abstractName;

	///
	/// @brief Start point of an absolute path
	std::string prefix;

	///
	/// @brief Separator between fields inside an abstract name
	std::string separator;

	///
	/// @brief Flag to identify if abstractName is an absolute path or not
	bool absolutePath;

public :

	///
	/// @brief Default System Separator (Linux '/', Windows '\')
	///
	static const char* const  D_SEPARATOR;

	///
	/// @brief Default System Prefix (Linux '/', Windows 'C:\')
	///
	static const char* const  D_PREFIX;

	///
	/// @brief Integer code for binary write access
	///
	static const char* const  MODE_BIN_WRITE;

	///
	/// @brief Integer code for ASCII write access
	///
	static const char* const  MODE_TXT_WRITE;

	///
	/// @brief Integer code for binary append access
	///
	static const char* const  MODE_BIN_APPEND;

	///
	/// @brief Integer code for ASCII append access
	///
	static const char* const  MODE_TXT_APPEND;

	///
	/// @brief Integer code for binary read access
	///
	static const char* const  MODE_BIN_READ;

	///
	/// @brief Integer code for ASCII read access
	///
	static const char* const  MODE_TXT_READ;

	///
	/// @brief Default location for support files (e.g. files with .hif extension)
	///
	static const char* const DEFAULT_HIF_PATH;

	///
	/// @name Constructors.
	///
	/// @{

	///
	/// Default Constructor
	///
	/// @brief Create a file structure without AbstractName (without file name).
	///
	FileStructure ();

	///
	/// Constructor
	///
	/// @brief Create a file structure based on the PathName.
	///
	/// @param path_name file o directory path
	///
	FileStructure (const std::string& path_name);

    /// @brief Create a file structure based on the following
    /// path name: Parent DefaultSeparator Child.
	///
	FileStructure (const FileStructure& parent, const std::string& child);

	///
    /// @brief Create a file structure based on the following
	/// 	path name : parent DefaultSeparator child.
	FileStructure (const std::string& parent, const std::string& child);

	///
    /// @brief Create a file structure based on the following
	/// 	vector of std::string (path names).
	///
	FileStructure (const std::vector<std::string>& path_name);

	///
	/// @brief Destructor
	///
	~FileStructure ();

	/// @}
	///

	///
	/// @name General FileStructure support functions.
	///	Functions to elaborate a general FileStructure (both of file and directory)
	///
	/// @{

	///
	/// @brief Return true if the file structure is readable.
	///
	/// @return true if the file structure is readable.
	///
	bool canRead () const;

	///
	/// @brief Return true if the file structure is writable.
	///
	/// @return true if the file structure is writable.
	///
	bool canWrite () const;

	///
	/// @brief Compare two FileStructure
	///
    /// @return -1 if the PathName.length > this.length
    /// Return -2 if the this.length > PathName.length
    /// Return 0  if the PathName != this
    /// Return 1  if the PathName == this
	///
	int  compareTo (const FileStructure& path_name) const;

	///
	/// @brief Return true if the file structure exists.
	///
	/// @return true if file structure exists, false otherwise.
	///
	bool exists() const;

	///
	/// @brief Return the absolute path of the file structure.
	///
	/// @return absolute path FileStructure of current file structure.
	///
	FileStructure getAbsoluteFile() const;

	///
	/// @brief Return the absolute path of the file structure.
	///
	/// @return string representation of path of current file structure.
	///
	std::string getAbsolutePath() const;

	///
	/// @brief Return the string of the file structure.
	///
	/// @return string representation of current file structure.
	///
	std::string getName() const;

	///
	/// @brief Return the parent file structure of the file structure.
	///
	/// @return FileStructure (file) representation of parent current file structure.
	///
	FileStructure getParentFile() const;

	///
	/// @brief Add a child file structure to the current file structure.
	///
	/// @param string_e string representation of path to file that is added to current file structure
	///
	void addChild(const std::string& string_e);

	///
	/// @brief Return the local path of the file structure.
	///
	/// @return local path of the file structure.
	///
	std::string getPath() const;

	///
	/// @brief Return the local path of the file structure.
	///
	/// @return local path of the file structure.
	///
	std::vector<std::string> getAbstractName() const;

	///
	/// @brief Return true if the file structure is an absolute path.
	///
	/// @return true if the file structure is an absolute path.
	///
	bool isAbsolute() const;

	///
	/// @brief Return true if the file structure is a symbolic link.
	///
	/// @return true is actual FileStructure is a link
	///
	bool isLink() const;

	///
	/// @brief Return true if the file structure is a hidden file.
	///
	/// @return true if actual FileStructure is a hidden file
	///
	bool isHidden() const;

	///
	/// @brief Return the last modified date of the file structure.
	///
	/// @return time_t of the last modified date of the file structure
	///
	time_t lastModified() const;

	///
	/// @brief Return the last access date of the file structure.
	///
	/// @return time_t of the last access date of the file structure
	///
	time_t lastAccess() const;

	///
	/// @brief Return the creation date of the file structure.
	///
	/// @return time_t of the creation date of the file structure.
	///
	time_t lastAttributesChange() const;

	///
    /// @brief Return the length of the file structure (number of characters
	/// 	of the absolute path).
	///
	/// @return long representation of the length of the file structure
	///
	long length() const;

	///
	/// @brief Return the size in blocks of the file structure.
	///
	/// @return long representation of the size in blocks of the file structure
	///
	long size() const;

	///
	/// @brief Return the number of element of the file structure.
	///
	/// @return number of element of the file structure.
	///
	int depth() const;

	///
    /// @brief Return a list of paths name contained into the directory
	/// corresponded to the file structure.
	///
	/// @return a list of paths name contained into the directory corresponded to the file structure.
	///
	std::vector<std::string> list() const;

	///
    /// @brief Return a list of paths name filtered by the filter string
	/// contained into the directory corresponded to the file structure.
	///
	/// @param filter string filter for filtered list of paths
	/// @return Return a vector list of paths name filtered by the filter string
	///
	std::vector<std::string> list (const std::string& filter ) const;

	///
	/// @brief Create a symbolic link to the dest file structure.
	///
	/// @param dest destination FileStructure/
	/// @return true on success, false otherwise.
	///
	bool symbolicLink (FileStructure& dest);

	///
	/// @brief Change to the directory corresponded to the file structure.
	///
	/// @return true on success, false otherwise.
	///
	bool chdir();

	///
	/// @brief Remove the file structure whichever it is a file or a directory.
	///
	/// @return true on success, false otherwise.
	///
	bool remove();

	///
	/// @brief Rename the directory or the file into the Dest file structure.
	///
	/// @return true on success, false otherwise.
	///
	bool renameTo (FileStructure dest);

	///
	/// @brief Copy all the directory or the file into the Dest file structure.
	///
	/// @return true on success, false otherwise.
	///
	bool copyTo (FileStructure dest);

	///
	/// @brief Rename only the file structure variable without physical action.
	///
	/// @return true on success, false otherwise.
	///
	bool renameFile(const FileStructure& source);

	///
	/// @brief Return the string of the file structure.
	///
	/// @return string representation of actual FileStructure.
	///
	std::string toString() const;

	///
	/// @brief Eval separators in a string
	///
	/// @param path string with $SEP$ and $PRE$ as platform independent separator.
	/// @return evaluate separators in the path parameter and return it.
	///
	static std::string eval (const std::string& path);

	/// @}
	///

	///
	/// @name Directory support functions.
	///	Functions to elaborate a directory FileStructure
	///
	/// @{

	///
	/// @brief Return the parent directory of the file structure.
	///
	/// @return string representation of directory name of current file structure.
	///
	std::string getParent() const;

	///
	/// @brief Return the child directory or file of the file structure.
	///
	/// @return child directory or file of the file structure.
	///
	std::string getChild() const;

	///
	/// @brief Return true if the file structure is a directory.
	///
	/// @return true if FileStructure is a directory
	///
	bool isDirectory() const;

	///
    /// @brief Create the directory (if it didn't exists) corresponded to the
	/// child abstract name of the file structure.
	///
	/// @return true on success, false otherwise.
	///
	bool make_dir();

	///
    /// @brief Create directories (if they didn't exist) corresponded to the
	/// file structure.
	///
	/// @return true on success, false otherwise.
	///
	bool make_dirs();

	///
    /// @brief Remove the child abstract name of the file structure even
	/// it is full.
	///
	/// @return true on success, false otherwise.
	///
	bool rmdir();

	///
    /// @brief Remove all directories represented by the file structure even
	/// they are full.
	///
	/// @return true on success, false otherwise.
	bool rmdirs();

	/// @}
	///

	///
	/// @name File support functions.
	///	Functions to elaborate file FileStructure
	///
	/// @{

	///
	/// @brief Return true if the file structure is a file.
	///
	/// @return true if actual FileStructure is a file
	///
	bool isFile() const;

	///
	/// @brief Set file to executable mode. Return chmod status.
	///
	/// @return the chmod status
	///
	int setToExe() const;

	///
    /// @brief Return a list of files structure contained into the directory
	/// corresponded to the file structure.
	///
	/// @return list of files structure contained into the directory
	///
	std::vector<FileStructure> listFiles() const;

	///
    /// @brief Return a list of files structure filtered by the filter string
	/// contained into the directory corresponded to the file structure.
	///
	/// @param filter string filter for filtered list of files
	/// @return vector list of FileStructure filtered by the filter string
	///
	std::vector<FileStructure> listFiles(const std::string& filter) const;

	///
    /// @brief Create or open a file in a mode. There are different modes:
    /// MODE_BIN_WRITE : binary writing.
    /// MODE_TXT_WRITE : ASCII  writing.
    /// MODE_BIN_READ  : binary reading.
	/// MODE_TXT_READ  : ASCII  reading.
	///
	/// @param mode modes to open file
	/// @return FILE* to file opened.
	///
	FILE* openfile (char* mode);

	///
	/// @brief Remove the file represented by the file structure.
	///
	/// @return true on success, false otherwise.
	///
	bool rmfile();

	///
	/// @brief Remove the file represented by the file structure (without error msg.
	///
	/// @return true on success, false otherwise.
	///
	bool rmfile_weak();

	/// @}
	///
};

}}
#endif
