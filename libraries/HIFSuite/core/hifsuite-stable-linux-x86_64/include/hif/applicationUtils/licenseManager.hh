#ifndef LICENSE_MANAGER_HH
#define LICENSE_MANAGER_HH

#ifdef HIF_LICENSE_MANAGER

#include "lmxcpp.h"
#include "lmxsupport.hh"

#define HIF_LICENSE_CHECK( FEATURE_TO_TEST ) \
  { \
    CLmxCpp LMX; \
    LMX_STATUS LmxStat; \
    LMX_FEATURE_INFO FI; \
    char* feature = const_cast<char*>( FEATURE_TO_TEST ); \
    if ( (LmxStat = LMX.Init()) != LMX_SUCCESS ) \
    { \
      printf("Unable to initialize!\n"); \
      printf("%s\n", LMX.GetErrorMessage()); \
      return 1; \
    } \
    /* Enable the callback functions with license server failure behavior. */ \
    LMX.SetOption(LMX_OPT_HEARTBEAT_CHECKOUT_FAILURE_FUNCTION, (LMX_OPTION) UserCheckoutFailureRoutine); \
    LMX.SetOption(LMX_OPT_HEARTBEAT_CHECKOUT_SUCCESS_FUNCTION, (LMX_OPTION) UserCheckoutSuccessRoutine); \
    LMX.SetOption(LMX_OPT_HEARTBEAT_RETRY_FEATURE_FUNCTION, (LMX_OPTION) UserRetryRoutine); \
    LMX.SetOption(LMX_OPT_HEARTBEAT_EXIT_FUNCTION, (LMX_OPTION) UserExitRoutine); \
    LMX.SetOption(LMX_OPT_AUTOMATIC_SERVER_DISCOVERY, (LMX_OPTION) 1); \
    if ((LmxStat = LMX.Checkout(feature, 1, 0, 1)) != LMX_SUCCESS) \
    { \
      printf("Unable to checkout:\n"); \
      printf("%s\n", LMX.GetErrorMessage()); \
      return 1; \
    } \
    if (LMX.GetFeatureInfo(feature, &FI) != LMX_SUCCESS) \
    { \
      printf("Unable to get feature info:\n"); \
      printf("%s\n", LMX.GetErrorMessage()); \
      return 1; \
    } \
  }
#else

#define HIF_LICENSE_CHECK( FEATURE_TO_TEST )

#endif

#endif
