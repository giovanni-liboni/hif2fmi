#ifndef HIF_UTILS_COMMANDLINEPARSER_HH
#define HIF_UTILS_COMMANDLINEPARSER_HH

#include <vector>
#include <string>
#include <map>
#include "portability.hh"
#include "hifGetOpt.hh"

namespace hif { namespace applicationUtils {

/// @brief Base class for parsing a tool command line.
/// Can be derived to customize the behavior.
class HIF_EXPORT CommandLineParser
{
public:

    /// @name Traits.
    /// @{

    typedef std::size_t Size;
    typedef std::vector<std::string> Chunks;
    typedef std::vector<hif_option> OptionsList;
    typedef std::vector<std::string> Files;

    /// @brief The parser options.
    struct HIF_EXPORT ParserOptions
    {
        /// @brief When true, prints the command line before parsing.
        /// Default is true.
        bool printCommandLine;

        /// @brief When true, sorts the list of input files.
        /// Default is true.
        bool sortInputFiles;

        ParserOptions();
        ~ParserOptions();
        ParserOptions(const ParserOptions & other);
        ParserOptions & operator =(ParserOptions other);
        void swap(ParserOptions & other);
    };

	struct HIF_EXPORT Option
    {
        bool hasArgument;
        bool isActive;
        std::string value;
        std::string description;

        Option();
        ~Option();
        Option(const Option & o);
        Option & operator = (const Option & o);
    };

	struct HIF_EXPORT DictionaryComparator
    {
        typedef char first_argument_type;
        typedef char second_argument_type;
        typedef bool result_type;

        DictionaryComparator();
        ~DictionaryComparator();
        DictionaryComparator(const DictionaryComparator &);
        DictionaryComparator & operator = (const DictionaryComparator &);

        result_type operator() (
            const first_argument_type& x, const second_argument_type& y) const;
    };

    typedef std::map<char, Option, DictionaryComparator> Options;
    typedef std::map<std::string, char> Long2ShortNames;

    /// @}

    CommandLineParser();
    virtual ~CommandLineParser();

    /// @name Main methods.
    /// @{

    /// @brief Adds a command line option.
    ///
    /// @param shortName The option short name.
    /// @param longName The option long name.
    /// @param hasArgument True if an argument is required.
    /// @param isActive True if the option is enabled.
    /// @param description The option synopsis description.
    /// @param defaultValue (Optional) The option default value.
    ///
    void addOption(const char shortName,
                   const std::string & longName,
                   const bool hasArgument,
                   const bool isActive,
                   const std::string & description,
                   const std::string & defaultValue = "");

    /// @brief Sets the parsing options.
    /// @param opt the options.
    void setParserOptions(const ParserOptions & opt);

    /// @brief Returns the current parsing options.
    /// @return The options.
    const ParserOptions & getParserOptions() const;


    /// @brief Actual command line parsing method.
    ////
    /// @param argc The main() argc parameter.
    /// @param argv The main() argv parameter.
    ///
    void parse(int argc, char * argv[]);

    /// @brief Add tool infos for banner and help.
    ///
    /// @param toolName The tool name.
    /// @param copyright The copiright notice.
    /// @param toolDescription A brief tool description.
    /// @param synopsys The tool synopsys.
    /// @param notes Optional footnotes.
    ///
    void addToolInfos(const std::string & toolName,
                      const std::string & copyright,
                      const std::string & toolDescription,
                      const std::string & synopsys,
                      const std::string & notes);

    /// @brief Gets an option value.
    ///
    /// @param c The option.
    /// @return The associated value.
    ///
    const std::string & getOption(const char c) const;

    /// @brief Gets an option value.
    ///
    /// @param s The option.
    /// @return The associated value.
    ///
    const std::string & getOption(const std::string & s) const;

    /// @brief Gets an option as boolean value.
    ///
    /// @param c The option.
    /// @return True if passed by command line.
    ///
    bool isOptionFlagSet(const char c) const;

    /// @brief Gets an option as boolean value.
    ///
    /// @param s The option.
    /// @return True if passed by command line.
    ///
    bool isOptionFlagSet(const std::string & s) const;


    /// @brief Checks if given option is active in current version.
    ///
    /// @param c The option.
    /// @return True if active.
    ///
    bool isActiveOption(const char c) const;

    /// @brief Checks if given option is active in current version.
    ///
    /// @param s The option.
    /// @return True if active.
    ///
    bool isActiveOption(const std::string & s) const;

    /// @}

    /// @name Standard options.
    /// @{

    void addHelp();
    void addVersion();
    void addVerbose();
    void addOutputFile();
    void addOutputDirectory();
    void addAutostep();
    void addPrintOnly();
    void addWriteParsing();
    void addParseOnly();
    void addConfigFile(const bool generateStub = false);
    void addOptimization();
    void addTopLevel();

    /// @}

    /// @name Support methods.
    /// @{

    Files & getFiles();
    void printHelp() const;
    void printVersion() const;
    bool isVerbose() const;
    const std::string & getOutputFile() const;
    const std::string & getOutputDirectory() const;
    bool isAutostep() const;
    bool isPrintOnly() const;
    bool isWriteParsing() const;
    bool isParseOnly() const;
    const std::string & getConfigFile() const;
    bool isOptimized() const;
    const std::string & getTopLevel() const;
    const std::string & getGenerateConfigFile() const;

    /// @}

protected:

    /// @name Internal methods.
    /// @{

    /// @brief Format a string as a line of the help.
    ///
    /// @param s The string.
    /// @return The formatted string.
    ///
    std::string _makeLine(const std::string & s = "") const;

    /// @brief Formats the parameters as a synopsys description of the help.
    ///
    /// @param shortName The command short name.
    /// @param longName The command long name.
    /// @param description The command description.
    /// @param margin Additional margin when wrapping. Default is 0.
    /// @return The formatted entry string.
    ///
    std::string _formatLine(const char shortName, const std::string & longName,
                            const std::string & description,
                            const Size margin = 0) const;

    /// @brief Splits a string into chunks for the help.
    ///
    /// @param chunks The filled shunk list.
    /// @param s The string to be split.
    /// @param maxSize THe maximum size.
    /// @param margin Additional margin when wrapping. Default is 0.
    ///
    void _makeChunks(Chunks & chunks, std::string s, const Size maxSize,
                     const Size margin = 0) const;

    /// @brief Pads given string to fix exactely maxLineSize.
    ///
    /// @param s The string.
    /// @param maxLineSize The line size.
    /// @return The padded string.
    ///
    std::string _padString(const std::string & s, const Size maxLineSize) const;

    /// @}

    OptionsList _optionsList;
    Options _options;
    Long2ShortNames _long2short;
    std::string _format;
    Files _files;
    Size _maxLineSize;
    const std::string _indentation;
    std::string _toolName;
    std::string _toolDescription;
    std::string _copyright;
    std::string _synopsys;
    std::string _notes;

    ParserOptions _parserOptions;

private:

    CommandLineParser(const CommandLineParser &);
    CommandLineParser * operator = (const CommandLineParser &);
};

} } // hif::applicationUtils

#endif // HIF_UTILS__COMMANDLINEPARSER_HH
