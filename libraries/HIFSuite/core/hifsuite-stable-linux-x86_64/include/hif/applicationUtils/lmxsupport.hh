// =====================================================================================
// 
//       Filename:  lmxsupport.hh
// 
//    Description:  Support functions to llvm license manager
// 
//        Version:  1.0
//        Created:  27/07/2010 15:22:43
//       Revision:  none
//       Compiler:  g++
// 
//         Author:  Alessandro Venturelli (), alessandro.venturelli@univr.it
//        Company:  
// 
// =====================================================================================

#ifndef HIF_LIBRARY_LMXSUPPORT_H
#define HIF_LIBRARY_LMXSUPPORT_H

// LICENSE SUPPORT FUNCTIONS

void LMX_CALLBACK UserConnectionLostRoutine(void *pVendorData, const char *szHost, int nPort, int nFailedHeartbeats);

void LMX_CALLBACK UserCheckoutFailureRoutine(void *pVendorData, const char *szFeatureName, int nUsedLicCount, LMX_STATUS LmxStat);

void LMX_CALLBACK UserCheckoutSuccessRoutine(void *pVendorData, const char *szFeatureName, int nUsedLicCount);

void LMX_CALLBACK UserRetryRoutine(void *pVendorData, const char *szFeatureName, int nUsedLicCount);

void LMX_CALLBACK UserExitRoutine(void *pVendorData);


#endif
