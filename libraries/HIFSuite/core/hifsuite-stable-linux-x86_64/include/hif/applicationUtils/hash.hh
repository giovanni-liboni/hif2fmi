#ifndef HIF_HASH_HH
#define HIF_HASH_HH

#include <cstdlib>
#include <fstream>
#include <stdint.h>
#include <string.h>
#include "portability.hh"
#include "Log.hh"

namespace hif { namespace applicationUtils {

#if ((defined EDALAB_LICENSE_KIND) && (EDALAB_LICENSE_KIND == 1))

/// @brief Computes the SHA-512 hash value for the given data.
/// @param data The data for which the hash value is to be computed.
/// @param length The length of data to be hashed.
/// @param hash Pointer to the output hash value.
HIF_EXPORT
void computeSHA512Hash(const uint8_t * data, const unsigned int length, uint8_t * hash);

/// @brief Computes the 512-bit MD6 hash value for the given data.
/// @param data The data for which the hash value is to be computed.
/// @param length The length of data to be hashed.
/// @param hash Pointer to the output hash value.
HIF_EXPORT
void computeMD6Hash(const uint8_t * data, const unsigned int length, uint8_t * hash);

/// @brief Retrieves the content of a given file as an array of bytes.
/// @param filename The name of the file of which the content is to retrieved.
/// @param contents Pointer where the file content will be stored (as array of bytes).
/// @return The length of the contents array if the operation is successful, 0 otherwise.
HIF_EXPORT
unsigned int retrieveFileContent(const std::string & filename, uint8_t * & contents);

#endif

}} // namespace hif::applicationUtils

#endif // HIF_HASH_HH
