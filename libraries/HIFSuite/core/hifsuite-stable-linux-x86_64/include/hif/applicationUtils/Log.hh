/// @file Log.hh
/// Copyright (C) 2013
/// EDALab S.r.l.
///
/// This file is part of HIF library
/// Class that provide a common method to log output of libraries and tools.
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0

#ifndef HIF_LOGGER_H
#define HIF_LOGGER_H

#include <sstream>
#include <string>
#include "portability.hh"

#include "../classes/classes.hh"


namespace hif {
namespace semantics {
class ILanguageSemantics;
} }


namespace hif { namespace applicationUtils {

typedef std::list<Object*> WarningList;
typedef std::set<Object*> WarningSet;
typedef std::set<std::string> WarningStringSet;

struct HIF_EXPORT WarningInfo
{
    WarningInfo();
    explicit WarningInfo(Object * o);
    ~WarningInfo();
    WarningInfo (const WarningInfo & o);
    WarningInfo & operator = (WarningInfo o);
    void swap(WarningInfo & o);
    bool operator < (const WarningInfo & o) const;

    Name name;
    Object::CodeInfo codeInfo;
    std::string description;
};

typedef std::list<WarningInfo> WarningInfoList;
typedef std::set<WarningInfo> WarningInfoSet;


/// @brief Gets the application name (i.e., a specific front-end/back-end).
/// @return The name of application set (if any).
HIF_EXPORT
std::string& getApplicationName();

/// @brief Gets the component name (i.e., a part of the set front-end/back-end).
/// @return The name of component set (if any).
HIF_EXPORT
std::string& getComponentName();

/// @brief Initializes the log messages for a component, setting the application
/// name and component name. Current values are stored.
HIF_EXPORT
void initializeLogHeader( const std::string appName, const std::string compName );

/// @brief Restores the previous values for application and component name, if present.
HIF_EXPORT
void restoreLogHeader();

/// @brief Prints information message.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param message is the actual message to print.
HIF_EXPORT
void _hif_internal_messageInfo(const std::string file, const unsigned int line,
    const std::string message);

/// @brief Prints warning message.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param message is the actual message to print.
/// @param involvedObject is the object related to the message (mainly if the
/// message concern an error). It includes also information about source
/// file name and line number originally related to the object.
/// @param sem The reference semantics.
HIF_EXPORT
void _hif_internal_messageWarning(const std::string file,
    const unsigned int line, const std::string message,
    hif::Object * involvedObject = NULL,
    hif::semantics::ILanguageSemantics * sem = NULL);

/// @brief Collects a unique warning message. This kind of messages are
/// collected and not printed, until a call to printUniqueWarnings is raised.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param message is the actual message to print.
HIF_EXPORT
void _hif_internal_raiseUniqueWarning(const std::string file,
    const unsigned int line, const std::string message);

/// @brief Prints all the unique warning messages collected until now.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param message is the optional message to print.
HIF_EXPORT
void _hif_internal_printUniqueWarnings(const std::string file,
    const unsigned int line, const std::string message);

/// @brief Prints an error message, which causes exit.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param message is the actual message to print.
/// @param involvedObject is the object related to the message (mainly if the
/// message concern an error). It includes also information about source
/// file name and line number originally related to the object.
/// @param sem The reference semantics.
HIF_EXPORT
void _hif_internal_messageError(const std::string file, const unsigned int line,
    const std::string message, hif::Object * involvedObject = NULL,
    hif::semantics::ILanguageSemantics * sem = NULL);

/// @brief Prints a debug message.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param message is the actual message to print.
/// @param involvedObject is the object related to the message (mainly if the
/// message concern an error). It includes also information about source
/// file name and line number originally related to the object.
/// @param sem The reference semantics.
/// @param dontPrintCondition indicates a boolean condition to suppress the print.
HIF_EXPORT
void _hif_internal_messageDebug(const std::string file, const unsigned int line,
    const std::string message, hif::Object * involvedObject = NULL,
    hif::semantics::ILanguageSemantics * sem = NULL,
    const bool dontPrintCondition = true);

/// @brief Prints a debug message that also validates a boolean
/// condition.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param message is the actual message to print.
/// @param involvedObject is the object related to the message (mainly if the
/// message concern an error). It includes also information about source
/// file name and line number originally related to the object.
/// @param sem The reference semantics.
/// @param assertCondition indicates a boolean condition to be validated.
HIF_EXPORT
void _hif_internal_messageAssert(const std::string file,
    const unsigned int line, const std::string message,
    hif::Object * involvedObject = NULL,
    hif::semantics::ILanguageSemantics * sem = NULL,
    const bool assertCondition = true);

/// @brief Prints a list of warnings.
/// The message is common for all passed objects.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param condition Warnings are printed only if condition holds.
/// @param message is the actual message to print.
/// @param objList The list of involved objects.
HIF_EXPORT
void _hif_internal_messageWarningList(const std::string file,
    const unsigned int line, const bool condition,
    const std::string message,
    WarningList & objList);

/// @brief Prints a list of warnings.
/// The message is common for all passed objects.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param condition Warnings are printed only if condition holds.
/// @param message is the actual message to print.
/// @param objSet The set of involved objects.
HIF_EXPORT
void _hif_internal_messageWarningList(const std::string file,
    const unsigned int line, const bool condition,
    const std::string message,
    WarningSet & objSet);

/// @brief Prints a list of warnings.
/// The message is common for all passed objects.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param condition Warnings are printed only if condition holds.
/// @param message is the actual message to print.
/// @param objSet The set of involved objects.
HIF_EXPORT
void _hif_internal_messageWarningList(const std::string file,
    const unsigned int line, const bool condition,
    const std::string message,
    WarningStringSet & objSet);

/// @brief Prints a list of warnings.
/// The message is common for all passed objects.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param condition Warnings are printed only if condition holds.
/// @param message is the actual message to print.
/// @param objList The list of involved objects.
HIF_EXPORT
void _hif_internal_messageWarningList(const std::string file,
    const unsigned int line, const bool condition,
    const std::string message,
    WarningInfoList & objList);

/// @brief Prints a list of warnings.
/// The message is common for all passed objects.
/// @param file is expanded by macro to the raising point of the message.
/// @param line is expanded by macro to the raising point of the message.
/// @param condition Warnings are printed only if condition holds.
/// @param message is the actual message to print.
/// @param objSet The set of involved objects.
HIF_EXPORT
void _hif_internal_messageWarningList(const std::string file,
    const unsigned int line, const bool condition,
    const std::string message,
    WarningInfoSet & objSet);

/// @brief Sets the verbose printing flag.
///
/// @param isVerbose The value.
///
HIF_EXPORT
void setVerboseLog(const bool isVerbose);

/// @brief Gets the verbose printing flag.
///
/// @return The value.
///
HIF_EXPORT
bool isVerboseLog();

/// @name These macros are required to get __FILE__ and __LINE__ correct value
//@{
#define messageInfo( message ) \
    hif::applicationUtils::_hif_internal_messageInfo( __FILE__, __LINE__, (message) )

#define messageWarning( message, involvedObject, semantics ) \
    hif::applicationUtils::_hif_internal_messageWarning( __FILE__, __LINE__, (message), (involvedObject), \
        (semantics) )

#define raiseUniqueWarning( message ) \
    hif::applicationUtils::_hif_internal_raiseUniqueWarning( __FILE__, __LINE__, (message) )

#define printUniqueWarnings( message ) \
    hif::applicationUtils::_hif_internal_printUniqueWarnings( __FILE__, __LINE__, (message) )

#define messageError( message, involvedObject, semantics ) \
    hif::applicationUtils::_hif_internal_messageError( __FILE__, __LINE__, (message), (involvedObject), \
        (semantics) )

#define messageAssert( assertCondition, message, involvedObject, semantics ) \
    hif::applicationUtils::_hif_internal_messageAssert( __FILE__, __LINE__, (message), (involvedObject), \
        (semantics), (assertCondition) )

#define messageWarningList(cond, message, objList) \
    hif::applicationUtils::_hif_internal_messageWarningList( __FILE__, __LINE__, (cond), (message), (objList))

#ifdef NDEBUG

#define messageDebug(...)

#define messageDebugError(...)

#define messageDebugIfFails(...)

#define messageDebugAssert(...)

#define messageDebugIfEnabled(...)

#else

HIF_EXPORT
extern bool hifLogDebugIsActive;

#define messageDebug( message, involvedObject, semantics ) \
    hif::applicationUtils::_hif_internal_messageDebug( __FILE__, __LINE__, (message), (involvedObject), \
        (semantics), false )

#define messageDebugError(...) messageError(__VA_ARGS__)

#define messageDebugIfFails( condition, message, involvedObject, semantics ) \
    hif::applicationUtils::_hif_internal_messageDebug( __FILE__, __LINE__, (message), (involvedObject), \
        (semantics), (condition) )

#define messageDebugAssert( assertCondition, message, involvedObject, semantics ) \
    hif::applicationUtils::_hif_internal_messageAssert( __FILE__, __LINE__, (message), (involvedObject), \
        (semantics), (assertCondition) )

#define messageDebugIfEnabled(message, involvedObject, semantics) \
    hif::applicationUtils::_hif_internal_messageDebug( __FILE__, __LINE__, (message), (involvedObject), \
        (semantics), !hif::hifLogDebugIsActive)

#endif

//@}

}} // Hif namespace::applicationUtils

#endif // HIF_LOGGER_H
