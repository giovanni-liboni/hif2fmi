/// @file
/// Portability support stuff.
///
/// @author Copyright (C) 2009 EDALab S.r.l.
///

#ifndef HIF_PORTABILITY_HH
#define HIF_PORTABILITY_HH

#include <cstdio>
#include <string>
#include <sys/stat.h>

#if (defined _MSC_VER)


#define HIF_DEPRECATED(msg) __declspec(deprecated(msg))
// dll-interface export problem
#pragma warning(disable:4251)
// no suitable template instantation
#pragma warning(disable:4661)

#if (defined COMPILE_HIF_LIB)
// Compiling dynamic hif
#define HIF_EXPORT __declspec(dllexport)
#elif (defined USE_HIF_LIB)
// Linking dynamic libs
#define HIF_EXPORT __declspec(dllimport)
#else
// Compiling or linking static libs
#define HIF_EXPORT
#endif

#else

// Linux
#include <dirent.h>
#include <sys/types.h>

#define HIF_EXPORT __attribute__ ((visibility ("default")))
#define HIF_DEPRECATED(msg) __attribute__ ((deprecated))
#define HIF_DIAGNOSTIC_PUSH_POP (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 5))
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

#endif

namespace hif { namespace applicationUtils {

    /// @name Common portability methods.
    /// @{
    HIF_EXPORT
    int hif_strcasecmp(const char * s1, const char * s2);
    HIF_EXPORT
    int hif_strncasecmp(const char * s1, const char * s2, const size_t size);
    HIF_EXPORT
    long long hif_strtoll(const char * s1, char ** s2, int base);
    HIF_EXPORT
    char * hif_getcwd(char * buf, size_t size);
    HIF_EXPORT
    int hif_chmod(const char * path, int m);
    HIF_EXPORT
    int hif_chdir(const char * path);
    HIF_EXPORT
    int hif_rmdir(const char * path);
    HIF_EXPORT
    int hif_mkdir(const char * path, int mode);
    HIF_EXPORT
    char * hif_strdup(const char * s);
    HIF_EXPORT
    int hif_fileno(FILE * f);
    HIF_EXPORT
    int hif_isatty(int fd);
    HIF_EXPORT
    int hif_isdir(unsigned int mode);
    HIF_EXPORT
    int hif_islink(unsigned int mode);
    HIF_EXPORT
    int hif_getfilesize(struct stat & s);
    HIF_EXPORT
    int hif_symlink(const char * s1, const char * s2);
	HIF_EXPORT
    double hif_round(const double d);
	HIF_EXPORT
    double hif_log2(const double d);
	HIF_EXPORT
	FILE * hif_fmemopen( const char * buffer, int size, const char * mode, const char * path );
	HIF_EXPORT
    FILE * hif_fdopen(const int fd, const char * const mode);
    HIF_EXPORT
    std::string hif_getCurrentTimeAsString();
    HIF_EXPORT
    std::string hif_getCurrentDateAsString();
	/// @}

    /// @name Common portability constants.
    /// @{

    HIF_EXPORT
    extern const int PERMISSION_RWX_USR;
    HIF_EXPORT
    extern const int PERMISSION_R_USR;
    HIF_EXPORT
    extern const int PERMISSION_W_USR;
    HIF_EXPORT
    extern const int PERMISSION_X_USR;
    HIF_EXPORT
    extern const int PERMISSION_RWX_GRP;
    HIF_EXPORT
    extern const int PERMISSION_R_GRP;
    HIF_EXPORT
    extern const int PERMISSION_W_GRP;
    HIF_EXPORT
    extern const int PERMISSION_X_GRP;
    HIF_EXPORT
    extern const int PERMISSION_RWX_OTH;
    HIF_EXPORT
    extern const int PERMISSION_R_OTH;
    HIF_EXPORT
    extern const int PERMISSION_W_OTH;
    HIF_EXPORT
    extern const int PERMISSION_X_OTH;

    /// @}

}} // end namespace hif::applicationUtils

// Helper method to instantiate a class over all HIF objects.
#define HIF_INSTANTIATE_CLASS(C) \
template class HIF_EXPORT C <Action>; \
template class HIF_EXPORT C <Aggregate>; \
template class HIF_EXPORT C <AggregateAlt>; \
template class HIF_EXPORT C <Alias>; \
template class HIF_EXPORT C <Alt>; \
template class HIF_EXPORT C <Array>; \
template class HIF_EXPORT C <Assign>; \
template class HIF_EXPORT C <BaseContents>; \
template class HIF_EXPORT C <Bit>; \
template class HIF_EXPORT C <BitValue>; \
template class HIF_EXPORT C <Bitvector>; \
template class HIF_EXPORT C <BitvectorValue>; \
template class HIF_EXPORT C <Bool>; \
template class HIF_EXPORT C <BoolValue>; \
template class HIF_EXPORT C <Break>; \
template class HIF_EXPORT C <Cast>; \
template class HIF_EXPORT C <Char>; \
template class HIF_EXPORT C <CharValue>; \
template class HIF_EXPORT C <CompositeType>; \
template class HIF_EXPORT C <Const>; \
template class HIF_EXPORT C <ConstValue>; \
template class HIF_EXPORT C <Contents>; \
template class HIF_EXPORT C <Continue>; \
template class HIF_EXPORT C <DataDeclaration>; \
template class HIF_EXPORT C <Declaration>; \
template class HIF_EXPORT C <DesignUnit>; \
template class HIF_EXPORT C <Entity>; \
template class HIF_EXPORT C <Enum>; \
template class HIF_EXPORT C <EnumValue>; \
template class HIF_EXPORT C <Event>; \
template class HIF_EXPORT C <Expression>; \
template class HIF_EXPORT C <Field>; \
template class HIF_EXPORT C <FieldReference>; \
template class HIF_EXPORT C <File>; \
template class HIF_EXPORT C <For>; \
template class HIF_EXPORT C <ForGenerate>; \
template class HIF_EXPORT C <Function>; \
template class HIF_EXPORT C <FunctionCall>; \
template class HIF_EXPORT C <Generate>; \
template class HIF_EXPORT C <GlobalAction>; \
template class HIF_EXPORT C <Identifier>; \
template class HIF_EXPORT C <If>; \
template class HIF_EXPORT C <IfAlt>; \
template class HIF_EXPORT C <IfGenerate>; \
template class HIF_EXPORT C <Instance>; \
template class HIF_EXPORT C <Int>; \
template class HIF_EXPORT C <IntValue>; \
template class HIF_EXPORT C <Library>; \
template class HIF_EXPORT C <LibraryDef>; \
template class HIF_EXPORT C <Member>; \
template class HIF_EXPORT C <Null>; \
template class HIF_EXPORT C <Transition>; \
template class HIF_EXPORT C <Object>; \
template class HIF_EXPORT C <PPAssign>; \
template class HIF_EXPORT C <Parameter>; \
template class HIF_EXPORT C <ParameterAssign>; \
template class HIF_EXPORT C <Pointer>; \
template class HIF_EXPORT C <Port>; \
template class HIF_EXPORT C <PortAssign>; \
template class HIF_EXPORT C <PrefixedReference>; \
template class HIF_EXPORT C <Procedure>; \
template class HIF_EXPORT C <ProcedureCall>; \
template class HIF_EXPORT C <Range>; \
template class HIF_EXPORT C <Real>; \
template class HIF_EXPORT C <RealValue>; \
template class HIF_EXPORT C <Record>; \
template class HIF_EXPORT C <RecordValue>; \
template class HIF_EXPORT C <RecordValueAlt>; \
template class HIF_EXPORT C <Reference>; \
template class HIF_EXPORT C <ReferencedAssign>; \
template class HIF_EXPORT C <ReferencedType>; \
template class HIF_EXPORT C <Return>; \
template class HIF_EXPORT C <Scope>; \
template class HIF_EXPORT C <ScopedType>; \
template class HIF_EXPORT C <Signal>; \
template class HIF_EXPORT C <Signed>; \
template class HIF_EXPORT C <SimpleType>; \
template class HIF_EXPORT C <Slice>; \
template class HIF_EXPORT C <State>; \
template class HIF_EXPORT C <StateTable>; \
template class HIF_EXPORT C <String>; \
template class HIF_EXPORT C <SubProgram>; \
template class HIF_EXPORT C <Switch>; \
template class HIF_EXPORT C <SwitchAlt>; \
template class HIF_EXPORT C <System>; \
template class HIF_EXPORT C <TPAssign>; \
template class HIF_EXPORT C <StringValue>; \
template class HIF_EXPORT C <Time>; \
template class HIF_EXPORT C <TimeValue>; \
template class HIF_EXPORT C <Type>; \
template class HIF_EXPORT C <TypeDeclaration>; \
template class HIF_EXPORT C <TypeDef>; \
template class HIF_EXPORT C <TypeReference>; \
template class HIF_EXPORT C <TypeTP>; \
template class HIF_EXPORT C <TypeTPAssign>; \
template class HIF_EXPORT C <TypedObject>; \
template class HIF_EXPORT C <Unsigned>; \
template class HIF_EXPORT C <Value>; \
template class HIF_EXPORT C <ValueStatement>; \
template class HIF_EXPORT C <ValueTP>; \
template class HIF_EXPORT C <ValueTPAssign>; \
template class HIF_EXPORT C <Variable>; \
template class HIF_EXPORT C <View>; \
template class HIF_EXPORT C <ViewReference>; \
template class HIF_EXPORT C <Wait>; \
template class HIF_EXPORT C <When>; \
template class HIF_EXPORT C <WhenAlt>; \
template class HIF_EXPORT C <While>; \
template class HIF_EXPORT C <With>; \
template class HIF_EXPORT C <WithAlt>

// Helper method to instantiate a method over all HIF objects.
// Requires to define macro HIF_FOO(T) as the method to be instantiated.
#define HIF_INSTANTIATE_METHOD() \
template HIF_EXPORT HIF_FOO(Object); \
template HIF_EXPORT HIF_FOO(Action); \
template HIF_EXPORT HIF_FOO(Aggregate); \
template HIF_EXPORT HIF_FOO(AggregateAlt); \
template HIF_EXPORT HIF_FOO(Alias); \
template HIF_EXPORT HIF_FOO(Alt); \
template HIF_EXPORT HIF_FOO(Array); \
template HIF_EXPORT HIF_FOO(Assign); \
template HIF_EXPORT HIF_FOO(BaseContents); \
template HIF_EXPORT HIF_FOO(Bit); \
template HIF_EXPORT HIF_FOO(BitValue); \
template HIF_EXPORT HIF_FOO(Bitvector); \
template HIF_EXPORT HIF_FOO(BitvectorValue); \
template HIF_EXPORT HIF_FOO(Bool); \
template HIF_EXPORT HIF_FOO(BoolValue); \
template HIF_EXPORT HIF_FOO(Break); \
template HIF_EXPORT HIF_FOO(Cast); \
template HIF_EXPORT HIF_FOO(Char); \
template HIF_EXPORT HIF_FOO(CharValue); \
template HIF_EXPORT HIF_FOO(CompositeType); \
template HIF_EXPORT HIF_FOO(Const); \
template HIF_EXPORT HIF_FOO(ConstValue); \
template HIF_EXPORT HIF_FOO(Contents); \
template HIF_EXPORT HIF_FOO(Continue); \
template HIF_EXPORT HIF_FOO(DataDeclaration); \
template HIF_EXPORT HIF_FOO(Declaration); \
template HIF_EXPORT HIF_FOO(DesignUnit); \
template HIF_EXPORT HIF_FOO(Entity); \
template HIF_EXPORT HIF_FOO(Enum); \
template HIF_EXPORT HIF_FOO(EnumValue); \
template HIF_EXPORT HIF_FOO(Event); \
template HIF_EXPORT HIF_FOO(Expression); \
template HIF_EXPORT HIF_FOO(Field); \
template HIF_EXPORT HIF_FOO(FieldReference); \
template HIF_EXPORT HIF_FOO(File); \
template HIF_EXPORT HIF_FOO(For); \
template HIF_EXPORT HIF_FOO(ForGenerate); \
template HIF_EXPORT HIF_FOO(Function); \
template HIF_EXPORT HIF_FOO(FunctionCall); \
template HIF_EXPORT HIF_FOO(Generate); \
template HIF_EXPORT HIF_FOO(GlobalAction); \
template HIF_EXPORT HIF_FOO(Identifier); \
template HIF_EXPORT HIF_FOO(If); \
template HIF_EXPORT HIF_FOO(IfAlt); \
template HIF_EXPORT HIF_FOO(IfGenerate); \
template HIF_EXPORT HIF_FOO(Instance); \
template HIF_EXPORT HIF_FOO(Int); \
template HIF_EXPORT HIF_FOO(IntValue); \
template HIF_EXPORT HIF_FOO(Library); \
template HIF_EXPORT HIF_FOO(LibraryDef); \
template HIF_EXPORT HIF_FOO(Member); \
template HIF_EXPORT HIF_FOO(Null); \
template HIF_EXPORT HIF_FOO(Transition); \
template HIF_EXPORT HIF_FOO(PPAssign); \
template HIF_EXPORT HIF_FOO(Parameter); \
template HIF_EXPORT HIF_FOO(ParameterAssign); \
template HIF_EXPORT HIF_FOO(Pointer); \
template HIF_EXPORT HIF_FOO(Port); \
template HIF_EXPORT HIF_FOO(PortAssign); \
template HIF_EXPORT HIF_FOO(PrefixedReference); \
template HIF_EXPORT HIF_FOO(Procedure); \
template HIF_EXPORT HIF_FOO(ProcedureCall); \
template HIF_EXPORT HIF_FOO(Range); \
template HIF_EXPORT HIF_FOO(Real); \
template HIF_EXPORT HIF_FOO(RealValue); \
template HIF_EXPORT HIF_FOO(Record); \
template HIF_EXPORT HIF_FOO(RecordValue); \
template HIF_EXPORT HIF_FOO(RecordValueAlt); \
template HIF_EXPORT HIF_FOO(Reference); \
template HIF_EXPORT HIF_FOO(ReferencedAssign); \
template HIF_EXPORT HIF_FOO(ReferencedType); \
template HIF_EXPORT HIF_FOO(Return); \
template HIF_EXPORT HIF_FOO(Scope); \
template HIF_EXPORT HIF_FOO(ScopedType); \
template HIF_EXPORT HIF_FOO(Signal); \
template HIF_EXPORT HIF_FOO(Signed); \
template HIF_EXPORT HIF_FOO(SimpleType); \
template HIF_EXPORT HIF_FOO(Slice); \
template HIF_EXPORT HIF_FOO(State); \
template HIF_EXPORT HIF_FOO(StateTable); \
template HIF_EXPORT HIF_FOO(String); \
template HIF_EXPORT HIF_FOO(SubProgram); \
template HIF_EXPORT HIF_FOO(Switch); \
template HIF_EXPORT HIF_FOO(SwitchAlt); \
template HIF_EXPORT HIF_FOO(System); \
template HIF_EXPORT HIF_FOO(TPAssign); \
template HIF_EXPORT HIF_FOO(StringValue); \
template HIF_EXPORT HIF_FOO(Time); \
template HIF_EXPORT HIF_FOO(TimeValue); \
template HIF_EXPORT HIF_FOO(Type); \
template HIF_EXPORT HIF_FOO(TypeDeclaration); \
template HIF_EXPORT HIF_FOO(TypeDef); \
template HIF_EXPORT HIF_FOO(TypeReference); \
template HIF_EXPORT HIF_FOO(TypeTP); \
template HIF_EXPORT HIF_FOO(TypeTPAssign); \
template HIF_EXPORT HIF_FOO(TypedObject); \
template HIF_EXPORT HIF_FOO(Unsigned); \
template HIF_EXPORT HIF_FOO(Value); \
template HIF_EXPORT HIF_FOO(ValueStatement); \
template HIF_EXPORT HIF_FOO(ValueTP); \
template HIF_EXPORT HIF_FOO(ValueTPAssign); \
template HIF_EXPORT HIF_FOO(Variable); \
template HIF_EXPORT HIF_FOO(View); \
template HIF_EXPORT HIF_FOO(ViewReference); \
template HIF_EXPORT HIF_FOO(Wait); \
template HIF_EXPORT HIF_FOO(When); \
template HIF_EXPORT HIF_FOO(WhenAlt); \
template HIF_EXPORT HIF_FOO(While); \
template HIF_EXPORT HIF_FOO(With); \
template HIF_EXPORT HIF_FOO(WithAlt)

#define HIF_INSTANTIATE_SYMBOLS() \
    template HIF_EXPORT HIF_FOO(FieldReference); \
    template HIF_EXPORT HIF_FOO(FunctionCall); \
    template HIF_EXPORT HIF_FOO(Identifier); \
    template HIF_EXPORT HIF_FOO(Instance); \
    template HIF_EXPORT HIF_FOO(Library); \
    template HIF_EXPORT HIF_FOO(ParameterAssign); \
    template HIF_EXPORT HIF_FOO(PortAssign); \
    template HIF_EXPORT HIF_FOO(ProcedureCall); \
    template HIF_EXPORT HIF_FOO(TypeTPAssign); \
    template HIF_EXPORT HIF_FOO(TypeReference); \
    template HIF_EXPORT HIF_FOO(ValueTPAssign); \
    template HIF_EXPORT HIF_FOO(ViewReference)

#endif
