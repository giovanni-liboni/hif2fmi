#ifndef HIF_MANIPULATION_CONFIGURATIONPARSER_HH
#define HIF_MANIPULATION_CONFIGURATIONPARSER_HH

#include <map>
#include <string>
#include <vector>
#include <list>

#include "portability.hh"

namespace hif { namespace applicationUtils {

/// @brief Parser for configuraton files.
/// Configuration files have the following format:
/// @code
/// # Comment spanning till the end of the line.
/// [section of variables]
/// ;directive = value1 ... valueN
/// variable = value1 ... valueN
/// @endcode
/// This class is able to both read and write such files.
///
class HIF_EXPORT ConfigurationManager
{
public:

    /// @name Traits.
    /// @{

    /// @brief Base string type.
    typedef std::string String;

    /// @brief Base identifier type.
    typedef String Identifier;

    /// @brief Section type.
    typedef Identifier Section;

    /// @brief directive type.
    typedef Identifier Directive;

    /// @brief Variable type.
    typedef Identifier Variable;

    /// @brief Comment type.
    typedef Identifier Comment;

    /// @brief Variable value type.
    typedef String Value;

    /// @brief Values list.
    typedef std::vector<Value> Values;

    /// @brief List for comments.
    typedef std::vector<Comment> Comments;

    /// @brief Stores data for keywords.
    struct KeyValues
    {
        KeyValues();
        ~KeyValues();
        KeyValues(const KeyValues & other);
        KeyValues & operator =(KeyValues other);
        void swap(KeyValues & other);

        Values values;
        Comments comments;
    };

    /// @brief Map for directives.
    typedef std::map<Directive, KeyValues> Directives;

    /// @brief Map for variables.
    typedef std::map<Variable, KeyValues> Variables;


    struct SectionData
    {
        SectionData();
        ~SectionData();
        SectionData(const SectionData & other);
        SectionData & operator = (SectionData other);
        void swap(SectionData & other);

        Directives directives;
        Variables variables;
        Comments comments;
    };

    /// @brief Map for sections.
    typedef std::map<Section, SectionData> Sections;

    /// @brief List for ordering sections.
    typedef std::list<Section> OrderedSections;
    /// @}

    /// @name Basic stuff.
    /// @{
    ConfigurationManager();
    virtual ~ConfigurationManager();
    ConfigurationManager(const ConfigurationManager & other);
    ConfigurationManager & operator = (ConfigurationManager other);
    void swap(ConfigurationManager & other);
    /// @}

    /// @name Accessors.
    /// @{

    /// @brief Parses the given configuration file.
    /// When called multiple times, possible matcing variables are overridden.
    ///
    /// @param file The name of the file.
    /// @return <tt>false</tt> on error.
    ///
    bool parse(const String & file);

    /// @brief Generates an empty section.
    /// @param section The new section name.
    void addSection(const Section & section);
    /// @brief Adds a value.
    ///
    /// @param section The section into which insert the value.
    /// @param id The associated id.
    /// @param value The value to be added.
    /// @param isDirective True if the value refers to a directive. Default is false.
    ///
    void addValue(const Section & section,
                  const Identifier & id, const Value & value,
                  const bool isDirective = false);

    /// @brief Adds values.
    ///
    /// @param section The section into which insert the value.
    /// @param id The associated id.
    /// @param values The value to be added.
    /// @param isDirective True if the value refers to a directive. Default is false.
    ///
    void addValues(const Section & section, const Identifier & id,
                   const Values & values, const bool isDirective = false);

    /// @brief Sets a value.
    ///
    /// @param section The section into which insert the value.
    /// @param id The associated id.
    /// @param value The value to be added.
    /// @param isDirective True if the value refers to a directive. Default is false.
    ///
    void setValue(const Section & section,
                  const Identifier & id, const Value & value,
                  const bool isDirective = false);

    /// @brief Sets values.
    ///
    /// @param section The section into which insert the value.
    /// @param id The associated id.
    /// @param values The value to be added.
    /// @param isDirective True if the value refers to a directive. Default is false.
    ///
    void setValues(const Section & section, const Identifier & id,
                   const Values & values, const bool isDirective = false);

    /// @brief Gets a value.
    /// Returns the last associated value.
    ///
    /// @param section The section from which get the value.
    /// @param id The associated id.
    /// @param isDirective True if the value refers to a directive. Default is false.
    /// @throw std::out_of_range in case of errors in accessing section or variable.
    /// @return The value.
    ///
    Value & getValue(const Section & section, const Identifier & id,
                     const bool isDirective = false);

    /// @brief Gets all values associated with a variable.
    ///
    /// @param section The section from which get the values.
    /// @param id The associated id.
    /// @param isDirective True if the value refers to a directive. Default is false.
    /// @return The values.
    /// @throw std::out_of_range in case of errors in accessing section or variable.
    ///
    Values & getValues(const Section & section, const Identifier & id,
                       const bool isDirective = false);

    /// @brief Gets a value.
    /// Returns the last associated value.
    ///
    /// @param section The section from which get the value.
    /// @param id The associated id.
    /// @param isDirective True if the value refers to a directive. Default is false.
    /// @return The value.
    /// @throw std::out_of_range in case of errors in accessing section or variable.
    ///
    const Value & getValue(const Section & section, const Identifier & id,
                           const bool isDirective = false) const;

    /// @brief Gets all values associated with a variable.
    ///
    /// @param section The section from which get the values.
    /// @param id The associated id.
    /// @param isDirective True if the value refers to a directive. Default is false.
    /// @return The values.
    ///
    const Values & getValues(const Section & section, const Identifier & id,
                             const bool isDirective = false) const;


    /// @brief Checks if the section is defined.
    ///
    /// @param section The section name to check.
    /// @return True if the section exists.
    ///
    bool hasSection(const Section & section) const;

    /// @brief Checks if the variable is defined.
    ///
    /// @param section The section name to check.
    /// @param dir The directive to be checked.
    /// @return True if the section and variable exists.
    ///
    bool hasDirective(const Section & section, const Directive & dir) const;

    /// @brief Checks if the variable is defined.
    ///
    /// @param section The section name to check.
    /// @param var The variable to be checked.
    /// @return True if the section and variable exists.
    ///
    bool hasVariable(const Section & section, const Variable & var) const;

    /// @brief Deletes the given section and all associated variables and values.
    ///
    /// @param section The section to be deleted.
    ///
    void eraseSection(const Section & section);

    /// @brief Deletes the given variable values.
    ///
    /// @param section The related section.
    /// @param dir The directive to be deleted.
    ///
    void eraseDirective(const Section & section, const Directive & dir);

    /// @brief Deletes the given variable values.
    ///
    /// @param section The related section.
    /// @param var The variable to be deleted.
    ///
    void eraseVariable(const Section & section, const Variable & var);

    /// @brief Writes the current configuration.
    ///
    /// @param file The output file.
    /// @return <tt>false</tt> in case of error.
    ///
    bool writeFile(const String & file) const;
    /// @brief Add a comment to at begin of file.
    /// @param comment The comment.
    void addComment(const Comment & comment);

    /// @brief Add a comment to a section.
    ///
    /// @param section The section.
    /// @param comment The comment.
    ///
    void addComment(const Section & section, const Comment & comment);

    /// @brief Add a comment to a variable or directive.
    ///
    /// @param section The section.
    /// @param identifier The identifier.
    /// @param comment The comment.
    /// @param isDirective True if the identifier is a directive. Default is false.
    ///
    void addComment(const Section & section,
                    const Identifier & identifier,
                    const Comment & comment,
                    const bool isDirective = false);

    /// @}

protected:

    /// @name Validity checks.
    /// @{

    /// @brief Checks values validity after reading of files.
    ///
    /// @return <tt>false</tt> in case of error.
    ///
    virtual bool _validateInput();

    /// @brief Checks values validity before writing of files.
    ///
    /// @return <tt>false</tt> in case of error.
    ///
    virtual bool _validateOutput() const;

    /// @}


    /// @name Parsing support methods.
    /// @{

    bool _parseLine(const std::string & line);
    bool _parseComment(const std::string & s);
    bool _parseSection(const std::string & s);
    bool _parseDirective(const std::string & s);
    bool _parseVariable(const std::string & s);
    void _parseValues(const Identifier & key, const std::string & s, const bool isDirective);

    /// @}

    /// @brief The sections.
    Sections _sections;

    /// @brief The current section.
    String _currentSection;

    /// @brief list for ordering sections
    OrderedSections _orderedSections;
};

} } // hif::applicationUtils

#endif // HIF_MANIPULATION_CONFIGURATIONPARSER_HH
