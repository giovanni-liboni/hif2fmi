#ifndef HIF_HIF_HH
#define HIF_HIF_HH

// ///////////////////////////////////////////////////////////////////
// HIF local includes
// ///////////////////////////////////////////////////////////////////

#include "hifEnums.hh"
#include "AncestorVisitor.hh"
#include "BiVisitor.hh"
#include "NameTable.hh"
#include "search.hh"
#include "HifVisitor.hh"
#include "GuideVisitor.hh"
#include "DeclarationVisitor.hh"
#include "HifFactory.hh"
#include "MapVisitor.hh"
#include "trash.hh"
#include "HifNodeRef.hh"

// ///////////////////////////////////////////////////////////////////
// Files grouping many methods.
// ///////////////////////////////////////////////////////////////////

#include "hifIOUtils.hh"

// ///////////////////////////////////////////////////////////////////
// Subdirs includes
// ///////////////////////////////////////////////////////////////////

#include "applicationUtils/applicationUtils.hh"
#include "backends/backends.hh"
#include "classes/classes.hh"
#include "hifUtils/hifUtils.hh"
#include "features/features.hh"
#include "manipulation/manipulation.hh"
#include "semantics/semantics.hh"
#include "analysis/analysis.hh"
#include "univerCM/univerCM.hh"

/// @brief Wraps all HIF library.
namespace hif {

} // hif

#endif

