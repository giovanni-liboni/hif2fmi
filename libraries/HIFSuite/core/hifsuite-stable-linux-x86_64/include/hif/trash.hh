#ifndef HIF_MANIPULATION_TRASH_HH
#define HIF_MANIPULATION_TRASH_HH

#include <set>
#include "classes/classes.hh"

namespace hif {

/// @brief Trash.
class HIF_EXPORT Trash
{
public:
    typedef std::set<Object *> TrashHolder;

    Trash();
    ~Trash();

    /// @brief Deletes all object inside the trash, possibly removing them
    /// from the tree.
    /// @param where The current point inside the tree. Used to avoid to delete
    ///        a parrent of current object.
    void clear(Object * where = NULL);

    /// @brief Add given object in trash.
    /// @param object The object.
    void insert(Object * object);

    /// @brief Add given list objects in trash. All list elements are removed.
    /// @param list The list
    void insert(BList<Object> & list);

    /// @brief Add given list objects in trash. All list elements are removed.
    /// @param list The list
    template< typename T >
    void insert(BList<T> & list);

    /// @brief Check whether given object is in trash.
    /// @param o The object.
    bool contains(Object * o) const;

private:

    TrashHolder _holder;

    Trash(const Trash & other);
    Trash & operator =(const Trash & other);
};




} // end Hif

#endif // HIF_MANIPULATION_TRASH_HH

