#ifndef HIF_TYPE_SPAN_IF_HH
#define HIF_TYPE_SPAN_IF_HH

#include <string>
#include "IFeature.hh"
#include "../applicationUtils/portability.hh"

namespace hif {

class Object;
class Range;

namespace features {

/// @brief Non-template base class.
/// Interface for symbols, including their declaration type and other
/// common features.
class HIF_EXPORT ITypeSpan: public IFeature
{
public :

    virtual
    ~ITypeSpan() = 0;

    /// @brief Returns the span of the type.
    /// @return The span of the type.
    Range* getSpan() const;

    /// @brief Sets the span of the type.
    /// @param r The span of the type to be set.
    /// @return The old span of the type if it is different
    /// from the new one, NULL otherwise.
    virtual Range* setSpan(Range* r) = 0;

protected:

    ITypeSpan();

    ITypeSpan(const ITypeSpan& other);

    ITypeSpan& operator=(const ITypeSpan& other);

    /// The span of the type.
    Range* _span;
};

}} // namespace hif::features

#endif /* TYPE_SPAN_IF_HH */
