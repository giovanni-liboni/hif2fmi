#ifndef HIF_SYMBOL_IF_HH
#define HIF_SYMBOL_IF_HH

#include <string>
#include "IFeature.hh"
#include "../applicationUtils/portability.hh"

namespace hif {

class Object;

namespace semantics {
HIF_EXPORT
void setDeclaration(Object * o, Object * decl);
} // namespace semantics

namespace features {

/// @brief Non-template base class.
/// Interface for symbols, including their declaration type and other
/// common features.
class HIF_EXPORT ISymbol: public IFeature
{
public :

    virtual
    ~ISymbol() = 0;

    /// @brief Given an object, check whether it matches the DeclarationType.
    virtual
    bool matchDeclarationType(Object * o) = 0;

protected:

    ISymbol();

    ISymbol(const ISymbol& other);

    ISymbol& operator=(const ISymbol& other);

    /// @brief Set the declaration of the corresponding symbol.
    /// @param d pointer to a DeclarationType that contains the
    /// corresponding symbol declaration.
    virtual
    void setDeclaration(Object * d) = 0;

    friend
    void hif::semantics::setDeclaration(Object * o, Object * decl);
};


/// @brief Interface for symbols, including their declaration type and other
/// common features.
template <class T>
class TemplateSymbolIf :
    public ISymbol
{
public:

    typedef T DeclarationType;

    virtual
    ~TemplateSymbolIf() = 0;

    virtual
    bool matchDeclarationType(Object * o);

    /// @brief Get the declaration of the corresponding symbol.
    /// @return Pointer to a DeclarationType that contains the
    /// corresponding symbol declaration.
    DeclarationType* GetDeclaration();

protected:

    TemplateSymbolIf();

    TemplateSymbolIf(const TemplateSymbolIf<T>& other);

    TemplateSymbolIf<T>& operator=(const TemplateSymbolIf<T>& other);

    virtual void setDeclaration(Object* d);

    /// @brief The declaration.
    DeclarationType * _declaration;
};

}} // namespace hif::features

#endif /* SYMBOL_IF_HH */
