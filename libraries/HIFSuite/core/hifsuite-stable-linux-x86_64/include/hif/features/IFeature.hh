#ifndef HIF_FEATURES_IFEATURE_HH
#define HIF_FEATURES_IFEATURE_HH

#include "../applicationUtils/portability.hh"
#include "../classes/Object.hh"

namespace hif { namespace features {

/// @brief Objects with this feature have a Name.
class HIF_EXPORT IFeature
{
public:

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject() = 0;

    /// @brief The destructor
    virtual ~IFeature() = 0;

protected:
    IFeature();
    IFeature(const IFeature & other);
    IFeature & operator = (const IFeature & other);
};

} } // hif::features

#endif
