#ifndef NAMEDOBJECT_HH
#define NAMEDOBJECT_HH

#include "IFeature.hh"
#include "../applicationUtils/portability.hh"
#include "../NameTable.hh"

namespace hif { namespace features {

/// @brief Objects with this feature have a Name.
class HIF_EXPORT INamedObject: public IFeature
{
public:

    /// @name Accessors.
    /// @{

    /// @brief Sets the name.
    ///
    /// @param n the name to be set.
    ///
    void setName(Name n);

    /// @brief Gets the name.
    ///
    /// @return The name.
    ///
    Name getName() const;

    /// @brief Checks whether given name is equals to the object name.
    /// @param nameToMatch The name to check.
    /// @return <tt>True</tt> if names are equals.
    bool matchName(const std::string & nameToMatch) const;

    /// @}

protected:

    INamedObject();
    virtual ~INamedObject() = 0;
    INamedObject(const INamedObject & other);
    INamedObject & operator = (const INamedObject & other);

private:

    /// @brief The stored name.
    hif::Name _name;
};

} } // hif::features

#endif // NAMEDOBJECT_HH
