#ifndef HIF_FEATURES_HH
#define HIF_FEATURES_HH

#include "ISymbol.hh"
#include "INamedObject.hh"
#include "ITypeSpan.hh"

namespace hif {

/// @brief Wraps common characteristics of HIF objects.
namespace features { }

} // hif

#endif
