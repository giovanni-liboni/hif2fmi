#ifndef HIF_BACKENDS_BACKENDS_HH
#define HIF_BACKENDS_BACKENDS_HH

#include "CHifDirStruct.hh"
#include "IndentedStream.hh"
#include "NodeVisitor.hh"
#include "Properties.hh"
#include "Session.hh"
#include "backendManipulations.hh"

namespace hif {

/// @brief Wraps utilities to write HIF backends.
namespace backends { }

} // hif

#endif
