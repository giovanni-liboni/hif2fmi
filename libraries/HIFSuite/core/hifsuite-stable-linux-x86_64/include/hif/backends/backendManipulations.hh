#ifndef HIF_BACKEND_MANIPULATIONS_HH
#define HIF_BACKEND_MANIPULATIONS_HH

#include "../classes/classes.hh"

namespace hif {
namespace backends {

/// @brief Given a method call, assures that all its actual parameters are
/// assignable to the declaration formal parameters.
/// @param call The method call.
/// @param sem The reference semantics.
/// @param shiftToZero True if actual parameters must be shifted to zero.
/// @param skip The number of parameters to not check (starting from the method
///             first parameter).
HIF_EXPORT
void makeParametersAssignable(ProcedureCall * call, hif::semantics::ILanguageSemantics* sem,
    const bool shiftToZero,
    const unsigned int skip = 0 );

/// @brief Given a method call, assures that all its actual parameters are
/// assignable to the declaration formal parameters.
/// @param call The method call.
/// @param sem The reference semantics.
/// @param shiftToZero True if actual parameters must be shifted to zero.
/// @param skip The number of parameters to not check (starting from the method
///             first parameter).
HIF_EXPORT
void makeParametersAssignable(FunctionCall * call, hif::semantics::ILanguageSemantics* sem,
    const bool shiftToZero,
    const unsigned int skip = 0 );

/// @brief Given a value, assures that its original type is preserved
/// for fully substitutability.
///
/// @param v The value to be checked.
/// @param valueType The current value type.
/// @param originalType The type to be preserved.
/// @return True if a cast has been added.
///
HIF_EXPORT
bool addEventualCast(Value * v, Type * valueType, Type * originalType);

/// @brief Introduces a library and the matching LibraryDef into given scope and system.
/// - The scope is used as starting object to get the actual scope into which add the library.
/// - The given name of the library must be complete. E.g. "hif_systemc_sc_core".
///
/// @param libName The library name.
/// @param scope The scope into which add the library. If NULL library is not added.
/// @param system The system into which add the LibraryDef.
/// @param sem The reference semantics.
/// @param standard If <tt>true</tt> the inclusion is created as standard.
/// @return True if the library has been added.
///
HIF_EXPORT
bool addHifLibrary(const std::string & libName, Object * scope, System * system,
    hif::semantics::ILanguageSemantics* sem, const bool standard = false);

/// @brief Calculate the include path from given <tt>where</tt> scope to
/// <tt>toInclude</tt> scope.
/// @param where The starting scope.
/// @param toInclude The scope to be reached.
/// @param cppHeaderExtension The c++ header extension.
/// @param sem The semantics.
/// @todo Added language scope strings and separators.
///
HIF_EXPORT
std::string calculateIncludePath(Scope * where,
                                 Scope * toInclude,
                                 const std::string & cppHeaderExtension,
                                 hif::semantics::ILanguageSemantics * sem);

}} // hif::backends

#include "backendManipulations.i.hh"

#endif /* HIF_BACKEND_MANIPULATIONS_HH */
