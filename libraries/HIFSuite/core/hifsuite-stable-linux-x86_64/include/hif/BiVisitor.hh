#ifndef HIF_BIVISITOR_HH
#define HIF_BIVISITOR_HH

#include "classes/forwards.hh"

namespace hif {

template<class Child>
class MonoVisitor
{
public:
    MonoVisitor();
    virtual ~MonoVisitor() = 0;

    void callMap(Object * o1);

private:

    MonoVisitor (const MonoVisitor<Child> &);
    MonoVisitor<Child> & operator = (const MonoVisitor<Child> &);
};



template<class Child>
class BiVisitor:
        public MonoVisitor< Child >
{
public:

    using MonoVisitor<Child>::callMap;

    BiVisitor();
    virtual ~BiVisitor() = 0;

    void callMap(Object * o1, Object * o2);

private:

    template<typename T>
    void rebind(T * o1, Object * o2);

    BiVisitor (const BiVisitor<Child> &);
    BiVisitor<Child> & operator = (const BiVisitor<Child> &);
};

}

#include "BiVisitor.i.hh"

#endif

