#ifndef HIF_HIFNODEREF_HH
#define HIF_HIFNODEREF_HH

#include "classes/classes.hh"

namespace hif {

/// @brief Wrapper used to store a temporary reference to a node in the tree.
/// The passed object is removed from the tree.
/// Example code:
/// @code
/// HifNodeRef ref(o);
/// ref.replace(factory.xxx(xxx, o, xxx));
/// @endcode
class HIF_EXPORT HifNodeRef
{
public:

    /// @brief Constructor.
    /// @param o the object from which take the reference.
    explicit HifNodeRef(Object * o);

    /// @brief Destructor.
    ~HifNodeRef();

    /// @brief Method to replace the reference with another object.
    bool replace(Object * o);

private:
    /// @brief The placeholder.
    Bit ref;

    HifNodeRef(const HifNodeRef &);
    HifNodeRef & operator =(const HifNodeRef &);
};

} // hif

#endif // HIFNODEREF_HH
