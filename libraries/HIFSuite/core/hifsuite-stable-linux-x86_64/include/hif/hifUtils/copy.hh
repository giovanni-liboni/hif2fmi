#ifndef HIF_COPY_HH
#define HIF_COPY_HH

#include "../classes/classes.hh"

namespace hif {

/// @name Copy stuff.
///
/// <warning>The copy function does not copy the parent object.
///
///	@{

struct HIF_EXPORT CopyOptions
{
    typedef
    Object* (*UserFunction)(Object*, Object*, void*);

    /// @brief Copy the semantic type of TypedObjects. Default is false.
    bool copySemanticsTypes;

    /// @brief Aliases the eventual declaration pointer. Default is true.
    bool copyDeclarations;

    /// @brief Performs a deep copy. Default is true.
    bool copyChildObjects;

    /// @brief Copy properties. Default is true.
    bool copyProperties;

    /// @brief Copy code info. Default is true.
    bool copyCodeInfos;

    /// @brief Copy comments. Default is true.
    bool copyComments;

    /// @brief Eventual user function to be applied to objects during the copy. Default is NULL.
    UserFunction userFunction;

    /// @brief Eventual user data to be passed to the user function. Default is NULL.
    void * userData;

    CopyOptions();
    ~CopyOptions();

    CopyOptions( const CopyOptions & o );
    CopyOptions & operator =( const CopyOptions & o );
};


/// @brief Copies a Hif object.
///
/// @param obj Object to be copied.
/// @param opt The copy options.
/// @return The copy of the object.
///
HIF_EXPORT
Object* copy(const Object* obj, const CopyOptions & opt = CopyOptions() );


/// @brief Copies a Hif object.
///
/// @param obj Object to be copied.
/// @param opt The copy options.
/// @return The copy of the object.
///
template <typename T>
T*
copy( const T* obj, const CopyOptions & opt = CopyOptions() )
{
    return static_cast <T*>( copy(static_cast <const Object*>(obj), opt ) );
}


/// @brief Copies a Hif object list.
///
/// @param src List to be copied.
/// @param dest Copy of the list.
/// @param opt The copy options.
///
HIF_EXPORT
void copy( const BList<Object>& src,
        BList<Object>& dest,
        const CopyOptions & opt = CopyOptions() );


/// @brief Copies a Hif object list.
///
/// @param src List to be copied.
/// @param dest Copy of the list.
/// @param opt The copy options.
///
template <class T>
void copy( const BList<T>& src,
        BList<T>& dest,
        const CopyOptions & opt = CopyOptions() )
{
    const BList< Object > * s = reinterpret_cast< const BList< Object > * >( &src );
    BList< Object > * d = reinterpret_cast< BList< Object > * >( &dest );
    copy( *s, *d, opt );
}


///
/// @}

} // end hif

#endif
