#ifndef HIF_APPLY_HH
#define HIF_APPLY_HH

#include "../classes/classes.hh"

namespace hif {

///	@name Applies a function to all objects of a subtree.
///	Those functions are useful to apply a given function
///	on all objects contained in a subtree or list of subtrees of a
///	Hif description.
///
///	The function must take two parameters (an <tt>Object</tt> and
///	a <tt> void *</tt>) and return a <tt>bool</tt>.
///
/// @{

typedef bool (*ApplyMethod)(Object*, void*);

///
/// @brief Applies a function in all objects of a subtree.
/// This function first invokes the <tt>f</tt> function with parameters
/// <tt>o</tt> and <tt>data</tt>.
/// If the value returned by <tt>f</tt> is true, <tt>f</tt> will be applied to
/// all children of <tt>o</tt>.
/// If the return value is <tt>false</tt>, function <tt>f</tt> will not be applied to the subtree.
///
/// @param o The top object of the subtree.
/// @param f The function to be invoked for all objects.
/// @param data The data to be used as second parameter when invoking <tt>f</tt>.
///
HIF_EXPORT
void apply( Object* o, ApplyMethod f, void * data = NULL );

///
/// @brief Applies a function in all subtrees of a BList.
/// This function invokes <tt>apply</tt> on all elements of <tt>l</tt>
/// using <tt>f</tt> and <tt>data</tt> as parameters.
///
/// @param l The list of subtrees.
/// @param f The function to be invoked for all objects.
/// @param data The data to be used as second parameter when invoking <tt>f</tt>.
///
template <class T>
void apply( BList<T>& l, ApplyMethod f, void * data = NULL )
{
    typename BList<T>::iterator i;
    Object * o;

    for (i = l.begin(); i != l.end();)
    {
        o = *i;
        ++i;
        apply(o, f, data);
    }
}


/// @}
///

} // end hif

#endif
