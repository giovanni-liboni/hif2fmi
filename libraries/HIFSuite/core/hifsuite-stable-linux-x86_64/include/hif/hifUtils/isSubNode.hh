#ifndef HIF_ISSUBNODE_HH
#define HIF_ISSUBNODE_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Checks whether the passed object is in the subtree of @p parent.
/// If @p obj is @p parent, it returns <tt>true</tt>.
/// @param obj The passed object.
/// @param parent The parent tree.
/// @param matchStarting If <tt>true</tt> returns true also if @p obj is @p parent.
/// Default is <tt>true</tt>.
/// @return <tt>true</tt> if the passed object is in the sub tree of parent.
///
HIF_EXPORT
bool isSubNode(Object * obj, Object * parent, const bool matchStarting = true);

/// @brief Checks whether the passed object is in the subtree of @p parentList.
/// @param obj The passed object.
/// @param parentList The parent list.
/// @param matchStarting If <tt>true</tt> returns true also if @p obj is @p parent.
/// Default is <tt>true</tt>.
/// @return <tt>true</tt> if the passed object is in the sub tree of parentList.
///
HIF_EXPORT
bool isSubNode(Object * obj, BList<Object> & parentList, const bool matchStarting = true);

/// @brief Checks whether the passed object is in the subtree of @p parentList.
/// @param obj The passed object.
/// @param parentList The parent list.
/// @param matchStarting If <tt>true</tt> returns true also if @p obj is @p parent.
/// Default is <tt>true</tt>.
/// @return <tt>true</tt> if the passed object is in the sub tree of parentList.
///
template <typename T>
bool isSubNode(Object * obj, BList<T> & parentList, const bool matchStarting = true);

} // end hif

#endif
