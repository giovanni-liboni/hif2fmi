#ifndef HIF_OBJECTPROPERTYUTILS_HH
#define HIF_OBJECTPROPERTYUTILS_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Change the name of a generic object.
/// @param obj The object on which to operate.
/// @param n The new name to set.
///
HIF_EXPORT
void objectSetName( Object * obj, Name n );


/// @brief Returns the name of a generic object.
/// @param obj The object on which to operate.
/// @return The name of the object if it has a <tt>name</tt> field, NULL otherwise.
///
HIF_EXPORT
Name objectGetName( Object * obj );


/// @brief Compares the object name with a string passed as parameter.
/// It is useful when you do not want to add a name in the name table.
/// @param obj The object whose name is to be compared.
/// @param nameToMatch The name to match.
/// @return <tt>true</tt> if the names match, <tt>false</tt> otherwise.
HIF_EXPORT
bool objectMatchName( Object * obj, const std::string & nameToMatch );


/// @brief Returns the <tt>type</tt> field of an object (if any).
/// In case object is a type, the object itself is returned.
/// All types are returned by aliasing.
/// @param obj The object on which to operate.
/// @return The <tt>type</tt> field of the object if any, NULL otherwise.
///
HIF_EXPORT
Type * objectGetType( Object * obj );


/// @brief Sets the <tt>type</tt> of an object if any.
///
/// @param obj The object on which the type is to be set.
/// @param t The type to be set.
/// @return The old type.
///
HIF_EXPORT
Type * objectSetType( Object * obj, Type * t );

/// @brief Returns the list of template-parameter assignments (if any).
///
/// @param obj The object on which to operate.
/// @return The template-parameter assignments  list, or NULL if the object does not have a template assignments list.
///
HIF_EXPORT
BList< TPAssign > * objectGetTemplateAssignList( Object * obj );


/// @brief Return the list of template parameters (if any).
///
/// @param obj The object on which to operate.
/// @return The template parameters list, or NULL if object does not have a template parameters list.
///
HIF_EXPORT
BList< Declaration > * objectGetTemplateParameterList( Object * obj );


/// @brief Returns the list of parameters (if any).
///
/// @param obj The object on which to operate.
/// @return The parameters list, or NULL if object does not have a parameters list.
///
HIF_EXPORT
BList< Parameter > * objectGetParameterList( Object * obj );


/// @brief Returns the list of declarations (if any).
///
/// @param obj The object on which to operate.
/// @return The declarations list, or NULL if object does not have a declarations list.
///
HIF_EXPORT
BList< Declaration > * objectGetDeclarationList( Object * obj );


/// @brief Returns the list of libraries (if any).
///
/// @param obj The object on which to operate.
/// @return The libraries list, or NULL if object does not have a libraries list.
///
HIF_EXPORT
BList< Library > * objectGetLibraryList( Object * obj );


/// @brief Returns the specified language in the object if present,
/// otherwise the language specified in the nearest parent having one.
/// @param obj The object on which to operate.
/// @return The specified language in the object, or in the nearest parent having one.
///
HIF_EXPORT
hif::LanguageID objectGetLanguage( Object * obj );


/// @brief Sets the language ID of an object if any.
///
/// @param obj The object on which the language ID is to be set.
/// @param id The language ID to set.
/// @param recursive If recoursive in parents.
///
HIF_EXPORT
void objectSetLanguage(Object * obj, const hif::LanguageID id,
                       const bool recursive = false);

/// @brief Sets an object <tt>instance</tt> field.
/// @note If instance can be set and <tt>obj</tt> already has an instance,
/// the old intance is deleted.
/// @param obj The object on which to operate.
/// @param instance The new instance to be set.
/// @return <tt>true</tt> if instance is set, <tt>false</tt> otherwise.
HIF_EXPORT
bool objectSetInstance( Object * obj, Object * instance );


/// @brief Gets an object <tt>instance</tt> field.
/// @param obj The object on which to operate.
/// @return The <tt>instance</tt> field of the object.
HIF_EXPORT
Object * objectGetInstance( Object * obj );


/// @brief Returns whether an object is an item in a sensitivity list of a state table.
/// @param obj The object to be checked.
/// @param checkAll Check all objects with sensitivity (e.g. Wait)
/// @return <tt>true</tt> if the object belongs to a sensitivity list, <tt>false</tt> otherwise.
HIF_EXPORT
bool objectIsInSensitivityList(Object* obj, const bool checkAll = false);


/// @brief Returns the nearest parent sensitivity of an object.
/// @param obj The object to be checked.
/// @param checkAll Check all objects with sensitivity (e.g. Wait)
/// @return The sensitivity list if the object belongs to a sensitivity list, NULL otherwise.
HIF_EXPORT
BList<Value> * objectGetSensitivityList(Object* obj, const bool checkAll = false);


/// @brief Returns true of given object is a process.
/// In case of StateTables and Assigns without parent, returns true.
///
/// @param o The object to be checked.
/// @return True if given object is a process.
///
HIF_EXPORT
bool objectIsProcess(Object* o);


/// @brief Returns true if given object is standard.
/// In case of has no standard flag it returns false.
///
/// @param o The object to be checked.
/// @return True if given object is a standard.
///
HIF_EXPORT
bool objectIsStandard(Object* o);

/// @brief Sets the given object standard flag.
/// In case of has no standard flag it does nothing.
///
/// @param o The object to be modified.
/// @param isStandard The new value.
///
HIF_EXPORT
void objectSetStandard(Object* o, const bool isStandard);

/// @brief Returns true if given object is external.
/// In case of has no external flag it returns false.
///
/// @param o The object to be checked.
/// @return True if given object is external.
///
HIF_EXPORT
bool objectIsExternal(Object* o);

/// @brief Sets the given object external flag.
/// In case of has no external flag it does nothing.
///
/// @param o The object to be modified.
/// @param isExternal The new value.
///
HIF_EXPORT
void objectSetExternal(Object* o, const bool isExternal);


/// @brief Checks whether given object is a NULL pointer.
///
/// @param o The object to be checked.
/// @return True when it is a NULL pointer representation.
///
HIF_EXPORT
bool objectIsNUllPointer(Object* o);


/// @brief Return the object port direction field.
/// If no direction field is present, dir_none is returned.
/// @param o The object.
/// @return The direction.
HIF_EXPORT
hif::PortDirection objectGetPortDirection(Object * o);

/// @brief Set the object port direction field.
/// If no direction field is present, does nothing.
/// @param o The object.
/// @param dir The direction.
HIF_EXPORT
void objectSetPortDirection(Object * o, const hif::PortDirection dir);


} // end hif

#endif
