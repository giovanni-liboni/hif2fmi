#ifndef HIF_HIFUTILS_HH_
#define HIF_HIFUTILS_HH_

#include "../applicationUtils/portability.hh"

#include "apply.hh"
#include "equals.hh"
#include "compare.hh"
#include "copy.hh"
#include "getNearestParent.hh"
#include "getNearestScope.hh"
#include "getParentSkippingObjects.hh"
#include "getChildSkippingObjects.hh"
#include "terminalPrefixUtils.hh"
#include "terminalInstanceUtils.hh"
#include "isSubNode.hh"
#include "isInTree.hh"
#include "getNearestCommonParent.hh"
#include "objectPropertyUtils.hh"
#include "declarationPropertyUtils.hh"
#include "objectGetKey.hh"
#include "operatorUtils.hh"
#include "rangePropertyUtils.hh"
#include "typePropertyUtils.hh"

namespace hif { namespace semantics {

class ILanguageSemantics;

} }

#endif /* HIFUTILS_HH_ */
