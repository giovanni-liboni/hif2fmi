#ifndef HIF_GETPARENTSKIPPINGOBJECTS_HH
#define HIF_GETPARENTSKIPPINGOBJECTS_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Returns the first parent object different from a cast.
/// @param o The starting object.
/// @return The first parent object different from cast.
///
HIF_EXPORT
Object * getParentSkippingCasts( Value * o );

/// @brief Returns the first parent object different from @p T.
/// @param o The starting object.
/// @return The first parent object different from @p T.
///
template< typename T >
Object * getParentSkippingClass(Object * o);

} // end hif

#endif
