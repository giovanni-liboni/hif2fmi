#ifndef HIF_GETCHILDSKIPPINGCASTS_HH
#define HIF_GETCHILDSKIPPINGCASTS_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Returns the first child object different from a cast.
/// Note: The function navigates into cast value node (not the type).
/// @param o The starting object.
/// @return The first child object different from cast.
///
HIF_EXPORT
Value * getChildSkippingCasts( Value * o );

} // end hif

#endif
