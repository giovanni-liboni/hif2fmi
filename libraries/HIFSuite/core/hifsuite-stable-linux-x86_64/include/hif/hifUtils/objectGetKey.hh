#ifndef HIF_OBJECTGETKEY_HH
#define HIF_OBJECTGETKEY_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Given a object returns a key string of the object.
///
/// @param obj The object.
/// @return The key string of the object.
///
HIF_EXPORT
std::string objectGetKey(Object * obj);

} // end hif

#endif
