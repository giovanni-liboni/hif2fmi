#ifndef HIF_GETNEARESTCOMMONPARENT_HH
#define HIF_GETNEARESTCOMMONPARENT_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Returns the nearest common parent.
/// @param obj1 The first Object.
/// @param obj2 The second Object.
/// @return The nearest common parent if present, NULL otherwise.
///
HIF_EXPORT
Object * getNearestCommonParent( Object * obj1, Object * obj2 );

} // end hif

#endif
