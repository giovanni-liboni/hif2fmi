#ifndef HIF_ISINTREE_HH
#define HIF_ISINTREE_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Checks whether it is possible to reach the System object starting
/// from the passed object.
/// @return <tt>true</tt> if System is reachable.
///
HIF_EXPORT
bool isInTree(Object * obj);

} // end hif

#endif
