#ifndef HIF_GETNEARESTSCOPE_HH
#define HIF_GETNEARESTSCOPE_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Finds the nearest scope of the object with selected features.
/// This function will go up the Hif tree starting from a given object
/// until it reaches a Scope object matching the given features.
///
/// @param o The starting point of the function search.
/// @param needDeclarationList Find a scope which has a list of declarations.
/// @param needLibraryList Find a scope which has a list of libraries.
/// @param needTemplates Find a scope which has a list of templates.
/// @return The wanted Hif object or NULL if it has not been found.
///
HIF_EXPORT
Scope* getNearestScope(Object* o, const bool needDeclarationList,
    const bool needLibraryList, const bool needTemplates);

} // end hif

#endif
