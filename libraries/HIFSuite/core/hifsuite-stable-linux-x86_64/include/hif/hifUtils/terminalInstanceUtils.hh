#ifndef HIF_TERMINALINSTANCEUTILS_HH
#define HIF_TERMINALINSTANCEUTILS_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief This function returns the instance of the given object.
/// This function operates on Hif objects having an <tt>instance</tt> field.
/// For example with my_namespace::my_class::my_static_const, return my_namespace.
/// @param obj The object from which the search will start.
/// @return The terminal instance if found, NULL otherwise.
///
HIF_EXPORT
Object * getTerminalInstance( Object * obj );

} // end hif

#endif
