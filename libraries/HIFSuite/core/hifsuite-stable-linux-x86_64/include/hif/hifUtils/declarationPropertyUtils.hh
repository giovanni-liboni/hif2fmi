#ifndef HIF_DECLARATIONPROPERTYUTILS_HH
#define HIF_DECLARATIONPROPERTYUTILS_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Return <tt>true</tt> if the given declaration is an instance.
/// @param obj The declaration to be checked.
/// @param scope The starting scope, if any.
/// @return <tt>true</tt> if the given declaration is an instance.
///
HIF_EXPORT
bool declarationIsInstance(Declaration * obj, Object * scope);

/// @brief Check whether the given declaration is part of a standard component.
/// @param decl The declaration to check.
/// @param dontCheckStandardViews Do not check standard flag on views.
/// @return <tt>true</tt> If the given declaration is part of a standard component.
HIF_EXPORT
bool declarationIsPartOfStandard(Declaration* decl,
                                 const bool dontCheckStandardViews = false);

/// @brief Check whether the declaration of given symbol is part of a standard component.
/// @param symbol The symbol to check.
/// @param dontCheckStandardViews Do not check standard flag on views.
/// @return <tt>true</tt> If the given symbol is part of a standard component.
HIF_EXPORT
bool declarationIsPartOfStandard(Object* symb,
                                 hif::semantics::ILanguageSemantics * sem,
                                 const bool dontCheckStandardViews = false);

} // end hif

#endif
