#ifndef HIF_RANGEPROPERTYUTILS_HH
#define HIF_RANGEPROPERTYUTILS_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Returns the upper bound of a range, depending on its direction.
///
/// @param r The range of which the upper bound is to be computed.
/// @return The upper bound of @p r.
///
HIF_EXPORT
Value * rangeGetMaxBound(Range * r);

/// @brief Returns the lower bound of a range, depending on its direction.
///
/// @param r The range of which the lower bound is to be computed.
/// @return The lower bound of @p r.
///
HIF_EXPORT
Value * rangeGetMinBound(Range * r);

/// @brief Sets the upper bound of a range, depending on its direction.
///
/// @param r The range of which the upper bound is to be computed.
/// @param v The value to set.
/// @return The old upper bound of @p r.
///
HIF_EXPORT
Value * rangeSetMaxBound(Range * r, Value * v);

/// @brief Sets the lower bound of a range, depending on its direction.
///
/// @param r The range of which the lower bound is to be computed.
/// @param v The value to set.
/// @return The old lower bound of @p r.
///
HIF_EXPORT
Value * rangeSetMinBound(Range * r, Value * v);

/// @brief Returns whether a given range @p r must be considered as a true span
/// or range or as a generic value.
/// For example, if @p r is the value inside a switch alternative, the function
/// returns <tt>true</tt>.
///
/// @param r The range to be checked.
/// @return <tt>true</tt> if @p r must be considered as a generic value.
///
HIF_EXPORT
bool rangeIsValue(Range * r);

} // end hif

#endif
