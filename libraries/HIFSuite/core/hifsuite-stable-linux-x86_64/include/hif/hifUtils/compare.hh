#ifndef HIF_COMPARE_HH
#define HIF_COMPARE_HH

#include "../classes/classes.hh"

namespace hif {

/// @brief Compares two objects according to their type and their children nodes.
/// It provides a total ordering on Hif objects, which is useful for sorting methods
/// employed by the <tt>simplify</tt> function.
/// @return -1 if @p obj1 < @p obj2, 0 if @p obj1 == @p obj2, 1 if @p obj1 > @p obj2.
///
HIF_EXPORT
int compare(Object * obj1, Object * obj2);

} // end hif

#endif
