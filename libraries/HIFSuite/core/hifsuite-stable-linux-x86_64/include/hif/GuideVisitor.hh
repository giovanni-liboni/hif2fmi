// guide_visitor.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//


///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// Utility class that define a base visit for an object subtree.
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_GUIDE_VISITOR_H
#define HIF_GUIDE_VISITOR_H

/// LOCAL INCLUDES
///
#include "HifVisitor.hh"
#include "applicationUtils/portability.hh"

namespace hif {

/// GuideVisitor
///
/// @brief Class that define a base strategy to visit
/// an object and it's subtree.
///

class HIF_EXPORT GuideVisitor:
        public HifVisitor
{

public:

    ///
    /// @brief Constructor
    ///
    /// @param rv default return value for a visit of an object.
    ///
    GuideVisitor(int rv = 0);

    /// Virtual Destructor
    ///
    virtual ~GuideVisitor();

    virtual int visitAggregate(Aggregate & o);
    virtual int visitAggregateAlt(AggregateAlt & o);
    virtual int visitAlias(Alias & o);
    virtual int visitArray(Array & o);
    virtual int visitAssign(Assign & o);
    virtual int visitBit(Bit & o);
    virtual int visitBitValue(BitValue & o);
    virtual int visitBitvector(Bitvector &o);
    virtual int visitBitvectorValue(BitvectorValue &o);
    virtual int visitBool(Bool & o);
    virtual int visitBoolValue(BoolValue & o);
    virtual int visitBreak(Break & o);
    virtual int visitCast(Cast & o);
    virtual int visitChar(Char & o);
    virtual int visitCharValue(CharValue & o);
    virtual int visitConst(Const & o);
    virtual int visitContents(Contents & o);
    virtual int visitContinue(Continue & o);
    virtual int visitDesignUnit(DesignUnit & o);
    virtual int visitEnum(Enum & o);
    virtual int visitEnumValue(EnumValue & o);
    virtual int visitEvent(Event & o);
    virtual int visitExpression(Expression & o);
    virtual int visitFunctionCall(FunctionCall & o);
    virtual int visitField(Field & o);
    virtual int visitFieldReference(FieldReference & o);
    virtual int visitFile(File & o);
    virtual int visitFor(For & o);
    virtual int visitForGenerate(ForGenerate & o);
    virtual int visitFunction(Function & o);
    virtual int visitGlobalAction(GlobalAction & o);
    virtual int visitEntity(Entity & o);
    virtual int visitIdentifier(Identifier & o);
    virtual int visitIf(If & o);
    virtual int visitIfAlt(IfAlt & o);
    virtual int visitIfGenerate(IfGenerate & o);
    virtual int visitInstance(Instance & o);
    virtual int visitInt(Int & o);
    virtual int visitIntValue(IntValue & o);
    virtual int visitLibraryDef(LibraryDef & o);
    virtual int visitLibrary(Library & o);
    virtual int visitMember(Member & o);
    virtual int visitNull(Null & o);
    virtual int visitTransition(Transition & o);
    virtual int visitParameterAssign(ParameterAssign & o);
    virtual int visitParameter(Parameter & o);
    virtual int visitProcedureCall(ProcedureCall & o);
    virtual int visitPointer(Pointer & o);
    virtual int visitPortAssign(PortAssign & o);
    virtual int visitPort(Port & o);
    virtual int visitProcedure(Procedure & o);
    virtual int visitRange(Range & o);
    virtual int visitReal(Real & o);
    virtual int visitRealValue(RealValue & o);
    virtual int visitRecord(Record & o);
    virtual int visitRecordValue(RecordValue & o);
    virtual int visitRecordValueAlt(RecordValueAlt & o);
    virtual int visitReference(Reference & o);
    virtual int visitReturn(Return & o);
    virtual int visitSignal(Signal & o);
    virtual int visitSigned(Signed & o);
    virtual int visitSlice(Slice & o);
    virtual int visitState(State & o);
    virtual int visitString(String & o);
    virtual int visitStateTable(StateTable & o);
    virtual int visitSystem(System & o);
    virtual int visitSwitchAlt(SwitchAlt & o);
    virtual int visitSwitch(Switch & o);
    virtual int visitStringValue(StringValue & o);
    virtual int visitTime(Time & o);
    virtual int visitTimeValue(TimeValue & o);
    virtual int visitTypeDef(TypeDef & o);
    virtual int visitTypeReference(TypeReference & o);
    virtual int visitTypeTPAssign(TypeTPAssign & o);
    virtual int visitTypeTP(TypeTP & o);
    virtual int visitUnsigned(Unsigned & o);
    virtual int visitValueStatement(ValueStatement & o);
    virtual int visitValueTPAssign(ValueTPAssign & o);
    virtual int visitValueTP(ValueTP & o);
    virtual int visitVariable(Variable & o);
    virtual int visitView(View & o);
    virtual int visitViewReference(ViewReference & o);
    virtual int visitWait(Wait & o);
    virtual int visitWhen(When & o);
    virtual int visitWhenAlt(WhenAlt & o);
    virtual int visitWhile(While & o);
    virtual int visitWith(With & o);
    virtual int visitWithAlt(WithAlt & o);

private:

    ///
    /// @brief default return value for a visit of an object
    ///
    int _rv;
};

} // namespace hif

#endif
