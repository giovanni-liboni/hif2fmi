///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_RECORD_VALUE_HXX
#define HIF_RECORD_VALUE_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Value.hh"

namespace hif {

///	@brief Record value constant.
///
/// This class represents constant values of record types.
///
/// @see Record, RecordValueAlt

class HIF_EXPORT RecordValue: public Value
{

public:

    /// @brief List of  RecordValueAlt to describe the values of the fields
    /// in the constant.
    BList <RecordValueAlt> alts;

    /// @brief Constructor.
    RecordValue();

    /// @brief Destructor.
    virtual ~RecordValue();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

};

} // namespace hif

#endif // HIF_RECORD_VALUE_HXX
