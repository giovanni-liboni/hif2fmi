///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_TIME_HXX
#define HIF_TIME_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "SimpleType.hh"

namespace hif {

///	@brief Time type.
///
/// This class represents the time type.

class HIF_EXPORT Time: public SimpleType
{

public:

    /// @brief Constructor.
    Time();

    /// @brief Destructor.
    virtual ~Time();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();
};

} // namespace hif

#endif // HIF_TIME_HXX
