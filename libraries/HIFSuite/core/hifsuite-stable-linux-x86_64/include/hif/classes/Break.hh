// Break

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_EXITOBJECT_HXX
#define HIF_EXITOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "../features/INamedObject.hh"
#include "Action.hh"

namespace hif {

/// @brief Break statement.
///
/// This class represents a break statement (i.e., exit from a loop).
/// The name of the loop to exit from can be specified.
/// If no loop name is provided, the exit will be from the current loop.

class HIF_EXPORT Break:
    public Action,
    public features::INamedObject
{
public:

    /// @brief Constructor.
    /// The default loop name is <tt>NAME_NONE</tt>.
    Break();

    /// @brief Destructor.
    virtual ~Break();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief The name of the loop to exit from.
    Name _name;

    // K: disabled
    Break(const Break &);
    Break & operator =(const Break &);
};

} // namespace hif

#endif

