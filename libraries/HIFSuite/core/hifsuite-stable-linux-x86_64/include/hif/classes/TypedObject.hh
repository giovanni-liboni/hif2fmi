// Value.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_TYPEDOBJECT_HXX
#define HIF_TYPEDOBJECT_HXX

/// LOCAL INCLUDES
///
#include "../applicationUtils/portability.hh"
#include "Object.hh"

namespace hif {

/// @brief Base class for objects having a semantic type.
///
/// This class is a base class for objects which can have a semantic type.

class HIF_EXPORT TypedObject: public Object
{

public:

    /// @brief Constructor.
    TypedObject();

    /// @brief Destructor.
    virtual ~TypedObject();

    /// @brief Returns the semantic type.
    /// @return The semantic type.
    virtual Type* getSemanticType() const;

    /// @brief Sets the semantic type.
    /// @param t The semantic type to be set.
    /// @return The old semantic type.
    virtual Type * setSemanticType(Type* t);

protected:

    /// @brief The semantic type.
    Type * _semanticsType;

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled.
    TypedObject(const TypedObject &);
    TypedObject & operator =(const TypedObject &);

};

} // namespace hif

#endif

