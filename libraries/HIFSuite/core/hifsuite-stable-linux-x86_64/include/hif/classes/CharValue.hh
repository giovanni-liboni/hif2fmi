// BitValue.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_CHARVALOBJECT_HXX
#define HIF_CHARVALOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "ConstValue.hh"

namespace hif {

/// @brief Char value.
///
/// This class represents a value of type char (i.e., single character).
/// By default this value is assigned to '0'.

class HIF_EXPORT CharValue: public ConstValue
{

public:

    /// @brief Constructor.
    CharValue();

    /// @brief Constructor.
    /// @param c The char value to be assigned. Default is 0.
    CharValue(const char c);

    /// @brief Destructor.
    virtual ~CharValue();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the char value.
    /// @return The char value.
    char getValue() const;

    /// @brief Sets the char value.
    /// @param c The char value to be set.
    void setValue(const char c);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief The actual char value.
    char _value;
};

} // namespace hif

#endif

