// Identifier

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_NAMEOBJECT_HXX
#define HIF_NAMEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Value.hh"
#include "../features/ISymbol.hh"
#include "../features/INamedObject.hh"

namespace hif {

///	@brief Identifier.
///
/// This class represents the occurrence of an identifier.
/// An identifier is characterized by the corresponding Name in the NameTable.
///
/// @see NameTable

class HIF_EXPORT Identifier:
    public Value,
    public features::TemplateSymbolIf<DataDeclaration>,
    public features::INamedObject
{

public:

    /// @brief Default constructor.
    Identifier();

    /// @brief Destructor.
    virtual ~Identifier();

    /// @brief Constructor.
    /// @param i The name to be assigned.
    Identifier(Name i);

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled
    Identifier(const Identifier &);
    Identifier & operator =(const Identifier &);
};

} // namespace hif

#endif

