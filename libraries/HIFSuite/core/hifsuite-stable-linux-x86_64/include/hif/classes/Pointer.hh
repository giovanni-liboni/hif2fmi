// Pointer.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef POINTEROBJECT_HXX_
#define POINTEROBJECT_HXX_

/// LOCAL INCLUDES
///
#include "../applicationUtils/portability.hh"
#include "CompositeType.hh"

namespace hif {

/// @brief Pointer.
///
/// This class represents a pointer. It is used only in C++ descriptions,
/// as pointers are not typically featured in HDLs.

class HIF_EXPORT Pointer: public CompositeType
{

public:

    /// @brief Constructor.
    Pointer();

    /// @brief Destructor.
    virtual ~Pointer();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled
    Pointer(const Pointer &);
    Pointer & operator =(const Pointer &);
};

} // namespace hif

#endif /*POINTERVALOBJECT_HXX_*/
