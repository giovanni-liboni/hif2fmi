// Copyright (C) 2009
//
// EDALab S.r.l.
//
// COPYRIGHT
//
// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
// @version 1.0

#ifndef HIF_STRINGOBJECT_HXX
#define HIF_STRINGOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "SimpleType.hh"
#include "../features/ITypeSpan.hh"

namespace hif {

/// @brief String type.
///
/// This class represents the string type.

class HIF_EXPORT String:
        public SimpleType
{

public:

    /// @brief Constructor. 
    String();

    /// @brief Destructor.
    virtual ~String();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Sets the span information of the string type.
    /// @param r The span of the string type to be set.
    /// @return The old span of the string type if it is different
    /// from the new one, NULL otherwise.
    Range* setSpanInformation(Range* r);

    /// @brief Returns the span information of the type.
    /// @return The span of the type.
    Range* getSpanInformation() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:
    /// @brief The span information.
    Range * _spanInformation;

    // K: disabled
    String(const String &);
    String & operator =(const String &);
};

} // namespace hif

#endif
