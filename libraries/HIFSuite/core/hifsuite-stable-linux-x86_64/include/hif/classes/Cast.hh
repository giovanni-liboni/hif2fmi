// Cast.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_CASTOBJECT_HXX
#define HIF_CASTOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "Value.hh"

namespace hif {

/// @brief Explicit cast.
///
/// This class represents an explicit cast.
/// It contains the operand to be cast, and the type to which the operand
/// to be cast.

class HIF_EXPORT Cast: public Value
{

public:

    /// @brief Constructor.
    Cast();

    /// @brief Destructor.
    virtual ~Cast();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the type to which the operand is to be cast.
    /// @return The type to which the operand is to be cast.
    Type* getType() const;

    /// @brief Sets the type to which the operand is to be cast.
    /// @param t The type to which the operand is to be cast.
    /// @return The old type of the cast if it is different from the new one,
    /// NULL otherwise.
    Type* setType(Type* t);

    /// @brief Returns the operand to be cast.
    /// @return The operand to be cast.
    Value* getValue() const;

    /// @brief Sets the operand to be cast.
    /// @param v The operand to be cast.
    /// @return The old operand of the cast operand if it is different from the
    /// new one, NULL otherwise.
    Value* setValue(Value* v);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:
    // K: disabled
    Cast(const Cast &);
    Cast & operator =(const Cast &);

    /// @brief The type to which the operand is to be cast.
    Type * _type;

    /// @brief The operand to be cast.
    Value * _value;

};

} // namespace hif

#endif

