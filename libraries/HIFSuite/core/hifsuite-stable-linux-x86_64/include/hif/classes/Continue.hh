// Continue.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_NEXTOBJECT_HXX
#define HIF_NEXTOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "../features/INamedObject.hh"
#include "Action.hh"

namespace hif {

/// @brief Continue statement.
///
/// This class represents a continue statement (i.e., move to the next
/// iteration in a loop).
/// The name of the enclosing loop can be specified.

class HIF_EXPORT Continue:
    public Action,
    public features::INamedObject
{

public:

    /// @brief Constructor.
    /// The default loop name is <tt>NAME_NONE</tt>.
    Continue();

    /// @brief Destructor.
    virtual ~Continue();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled
    Continue(const Continue &);
    Continue & operator =(const Continue &);
};

} // namespace hif

#endif

