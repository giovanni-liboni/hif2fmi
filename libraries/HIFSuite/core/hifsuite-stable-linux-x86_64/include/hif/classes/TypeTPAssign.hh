// TypeTPAssign.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_TYPETPASSIGNOBJECT_HXX
#define HIF_TYPETPASSIGNOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "TPAssign.hh"
#include "../features/ISymbol.hh"

namespace hif {

/// @brief Type template parameter assignment.
///
/// This class represents a type template parameter assignment (i.e., a type
/// template argument/actual parameter).
///
/// @see TypeTP

class HIF_EXPORT TypeTPAssign :
    public TPAssign,
    public features::TemplateSymbolIf<TypeTP>
{

public:

    /// @brief Constructor.
    TypeTPAssign();

    /// @brief Destructor.
    virtual ~TypeTPAssign();

    /// @brief Returns the type assigned to the type template paramter.
    /// @return The type assigned to the type template parameter.
    Type* getType() const;

    /// @brief Sets the type assigned to the type template parameter.
    /// @param n The type to be assigned to the type template parameter.
    /// @return The type previously assigned to the type template parameter.
    Type* setType(Type* n);

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    ///@brief The type assigned to the type template parameter.
    Type* _type;

    // K: disabled
    TypeTPAssign(const TypeTPAssign &);
    TypeTPAssign & operator =(const TypeTPAssign &);
};

} // namespace hif

#endif
