// Scope.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_SCOPEOBJECT_H
#define HIF_SCOPEOBJECT_H

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "Declaration.hh"

// SYSTEM INCLUDES
//
#include <list>

namespace hif {

/// @brief Abstract class for scopes.
///
/// This class is an abstract class for scopes.

class HIF_EXPORT Scope: public Declaration
{

public:

    /// @brief Constructor.
    Scope();

    /// @brief Destructor.
    virtual ~Scope();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K; disabled
    Scope(const Scope &);
    Scope & operator =(const Scope &);

};

} // namespace hif

#endif

