// DataDeclaration.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_DATADECLOBJECT_HXX
#define HIF_DATADECLOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Declaration.hh"

namespace hif {

///	@brief Abstract class for data declarations.
///
/// This class is an abstract class for data declarations.
/// A data declaration consists of a name, a type, an initial value,
/// and optionally a range constraint.

class HIF_EXPORT DataDeclaration:
        public Declaration
{

public:

    /// @brief Constructor.
    DataDeclaration();

    /// @brief Destructor.
    virtual ~DataDeclaration();

    /// @brief Returns the type of the data declaration.
    /// @return The type of the data declaration.
    Type* getType() const;

    /// @brief Sets the type of the data declaration.
    /// @param t The type of the data declaration to be set.
    /// @return The old type of the data declaration if it is different
    /// from the new one, NULL otherwise.
    Type * setType(Type* t);

    /// @brief Returns the initial value of the declaration.
    /// @return The initial value of the declaration.
    Value * getValue() const;

    /// @brief Sets the initial value of the data declaration.
    /// @param v The initial value of the data declaration to be set.
    /// @return The old initial value of the data declaration if it is
    /// different from the new one, NULL otherwise.
    Value* setValue(Value * v);

    /// @brief Returns the range of the data declaration.
    /// @return The range of the data declaration.
    Range* getRange() const;

    /// @brief Sets the range constraint of the data declaration. 
    /// @param r The range constraint of the data declaration to be set.
    /// @return The old range constraint of the data declaration if it is
    /// different from the new one, NULL otherwise.
    Range* setRange(Range * r);

protected:

    /// @brief The type of the data declaration.
    Type * _type;

    /// @brief The initial value of the data declaration.
    Value* _value;

    /// @brief The range constraint of the data declaration.
    Range* _range;

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    // K: disabled.
    DataDeclaration(const DataDeclaration &);
    DataDeclaration & operator =(const DataDeclaration &);

};

} // namespace hif

#endif

