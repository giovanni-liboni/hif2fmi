// IfGenerate

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///
#ifndef HIF_IFGENERATEOBJECT_HXX
#define HIF_IFGENERATEOBJECT_HXX

/// LOCAL INCLUDES
///
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Generate.hh"

namespace hif {

///	@brief If generate construct (conditional generator).
///
/// This class represents the VHDL <tt>if generate</tt> construct.
/// It allows to conditionally generate a number of design elements only if
/// a given static condition is met.
///
/// Note: it refers to IEEE Std. 1076-2003.
///
/// @see Generate

class HIF_EXPORT IfGenerate: public Generate
{

public:

    /// @brief Constructor.
    /// The generating condition is set to NULL.
    IfGenerate();

    /// @brief Destructor.
    virtual ~IfGenerate();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the generating condition.
    /// @return The generating condition.
    Value* getCondition() const;

    /// @brief Sets the generating condition.
    /// @param c The generating condition to be set.
    /// @return The old generating condition if it is different
    /// from the new one, NULL otherwise.
    Value* setCondition(Value* c);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    /// @brief The generating condition.
    Value* _condition;

    // K: disabled
    IfGenerate(const IfGenerate &);
    IfGenerate & operator =(const IfGenerate &);
};

} // namespace hif

#endif

