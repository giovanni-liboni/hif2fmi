// Char.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_CHAROBJECT_HXX
#define HIF_CHAROBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "SimpleType.hh"

namespace hif {

///	@brief Char type.
///
/// This class represents the char (i.e., single character) type. 

class HIF_EXPORT Char: public SimpleType
{
public:

    /// @brief Constructor.
    Char();

    /// @brief Destructor.
    virtual ~Char();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

};

} // namespace hif

#endif

