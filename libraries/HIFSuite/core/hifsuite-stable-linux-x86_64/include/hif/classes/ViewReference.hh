// ViewReference.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_VIEWREFOBJECT_HXX
#define HIF_VIEWREFOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "ReferencedType.hh"
#include "../features/ISymbol.hh"

namespace hif {

/// @brief Reference to a view of a design unit.
///
/// This class represents a reference to a view of a design unit.
/// A view is referenced by its name and by the name of the design unit
/// where the view is defined.
///
/// @see View, DesignUnit

class HIF_EXPORT ViewReference :
    public ReferencedType,
    public features::TemplateSymbolIf<View>
{

public:

    /// @brief The list of all template parameters assignments needed to instance the
    /// referenced view.
    ///
    BList <TPAssign> templateParameterAssigns;

    /// @brief Constructor.
    /// The names of the view and the design unit are initialized to NAME_NONE.
    ViewReference();

    /// @brief Destructor.
    virtual ~ViewReference();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the name of the design unit of the referenced view.
    /// @return The name of the design unit of the referenced view.
    Name getDesignUnit() const;

    /// @brief Sets the name of the design unit of the referenced view.
    /// @warning @p n must be different from NULL.
    /// @warning This method only changes the name of the design unit in the
    /// calling view reference. It does not modify the name in the design unit
    /// itself or in any other view reference.
    void setDesignUnit(Name n);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    /// @brief The name of the design unit of the referenced view.
    Name _unitname;

    // K: disabled
    ViewReference(const ViewReference &);
    ViewReference & operator =(const ViewReference &);
};

} // namespace hif

#endif

