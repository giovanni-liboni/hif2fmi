// Action.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_ACTIONOBJECT_HXX
#define HIF_ACTIONOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "Object.hh"

namespace hif {

///	@brief Abstract class for action statements.
///
/// This class is an abstract class for action statements, which typically
/// describe code in processes or subroutines.

class HIF_EXPORT Action: public Object
{

public:

    /// @brief Constructor.
    Action();

    /// @brief Destructor.
    virtual ~Action();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

};

} // namespace hif

#endif

