// Unsigned.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_UNSIGNEDOBJECT_HXX
#define HIF_UNSIGNEDOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "SimpleType.hh"
#include "../features/ITypeSpan.hh"

namespace hif {

/// @brief Unsigned numeric type.
///
/// This class represents an unsigned numeric type.

class HIF_EXPORT Unsigned: public SimpleType, public hif::features::ITypeSpan{

public:

    /// @brief Constructor.
    Unsigned();

    /// @brief Destructor.
    virtual ~Unsigned();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Sets the span of the unsigned type.
    /// @param r The new span of the unsigned type.
    /// @return The old span of the unsigned type if it is different
    /// from the new one, NULL otherwise.
    Range* setSpan(Range* r);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    // K: disabled
    Unsigned(const Unsigned &);
    Unsigned & operator =(const Unsigned &);
};

} // namespace hif

#endif

