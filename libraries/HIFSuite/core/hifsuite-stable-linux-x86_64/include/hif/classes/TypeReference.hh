// TypeReference.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_TYPEREFOBJECT_HXX
#define HIF_TYPEREFOBJECT_HXX

// HIF INCLUDES
//
#include "forwards.hh"
#include "ReferencedType.hh"
#include "../applicationUtils/portability.hh"
#include "../features/ISymbol.hh"
#include "BList.hh"

namespace hif {

/// @brief Reference to a user-defined type.
///
/// This class represents a reference to a user-defined type
/// (i.e., a TypeDef object).
/// A list of ranges can be optionally specified in order to restrict the
/// possible values of the referenced type.

class HIF_EXPORT TypeReference :
    public ReferencedType,
    public features::TemplateSymbolIf<TypeDeclaration>
{

public:

    /// @brief List of template arguments (i.e., assignments to template formal parameters).
    BList <TPAssign> templateParameterAssigns;

    /// @brief List of the optional range restrictions on the referenced type.
    BList <Range> ranges;

    /// @brief Constructor.
    TypeReference();

    /// @brief Destructor.
    virtual ~TypeReference();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    // K: disabled
    TypeReference(const TypeReference &);
    TypeReference & operator =(const TypeReference &);
};

} // namespace hif

#endif

