///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_RECORD_VALUE_ALT_HXX
#define HIF_RECORD_VALUE_ALT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "../features/INamedObject.hh"
#include "Alt.hh"

namespace hif {

/// @brief Alternative for a RecordValue.
//
/// This class represents an alternative for a RecordValue.
/// It consists of the name of the field to which it refers to
/// and the corresponding value.
///
/// @see RecordValue

class HIF_EXPORT RecordValueAlt:
    public Alt,
    public features::INamedObject
{

public:

    /// @brief Constructor.
    RecordValueAlt();

    /// @brief Destructor.
    virtual ~RecordValueAlt();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the value of the alternative.
    /// @return The value of the alternative.
    Value* getValue() const;

    /// @brief Sets the value of the alternative.
    /// @param v The value of the alternative to be set.
    Value* setValue(Value* v);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    /// @brief The value of the alternative.
    Value * _value;

    // K: disabled.
    RecordValueAlt(const RecordValueAlt &);
    RecordValueAlt & operator =(const RecordValueAlt &);
};

} // namespace hif

#endif // HIF_RECORD_VALUE_ALT_HXX
