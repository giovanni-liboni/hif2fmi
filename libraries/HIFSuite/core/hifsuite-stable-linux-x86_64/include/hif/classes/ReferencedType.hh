///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_REFERENCEDTYPEOBJECT_HXX
#define HIF_REFERENCEDTYPEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "../features/INamedObject.hh"
#include "Type.hh"

namespace hif {

/// @brief Referenced type.
///
/// This class represents a referenced type (i.e., a type which actually
/// is a reference to another entity).
///
/// @see Library, TypeReference, ViewReference

class HIF_EXPORT ReferencedType:
    public Type,
    public features::INamedObject{
public:

    /// @brief Constructor.
    ReferencedType();

    /// @brief Destructor.
    virtual ~ReferencedType();

    /// @brief Sets the reference to the definition of the type here referred.
    /// @param type The reference to the definition of the type here referred.
    /// @return The old reference to the definition of the type here referred.
    ReferencedType * setInstance(ReferencedType * type);

    /// @brief Returns the reference to the definition of the type here referred.
    /// @return The reference to the definition of the type here referred.
    ReferencedType * getInstance() const;


protected:

    /// @brief The reference to the definition of the type here referred.
    ReferencedType * _instance;

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    // K: disabled
    ReferencedType(const ReferencedType &);
    ReferencedType & operator =(const ReferencedType &);

};

} // namespace hif

#endif

