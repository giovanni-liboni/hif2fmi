// Array.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_ARRAYOBJECT_HXX
#define HIF_ARRAYOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "CompositeType.hh"
#include "../features/ITypeSpan.hh"

namespace hif {

///	@brief Array type.
///
/// This class represents an array type.
/// It specifies the type of the array elements and the span of the array.
/// The span indicates the range of values to be used as indices to access
/// the array elements.
/// The <tt>signed</tt> attribute is used to determine whether the array
/// has to be interpreted with signed arithmetic (e.g., in VHDL).

class HIF_EXPORT Array:
        public CompositeType,
        public hif::features::ITypeSpan
{

public:

    /// @brief Constructor.
    Array();

    /// @brief Destructor.
    virtual ~Array();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Sets the span of the array.
    /// @param t The span of the array to be set.
    /// @return The old span of the array if it is different from the new one, 
    /// NULL otherwise.
    Range* setSpan(Range* t);

    /// @brief Sets the <tt>signed</tt> attribute.
    /// @param sign The <tt>signed</tt> attribute.
    void setSigned(const bool sign);

    /// @brief Returns the <tt>signed</tt> attribute.
    /// @return The <tt>signed</tt> attribute.
    bool isSigned() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:
    // K: disabled
    Array(const Array &);
    Array & operator =(const Array &);

    /// @brief Flag to store the <tt>signed</tt> attribute.
    bool _isSigned;

};

} // namespace hif

#endif

