// BaseContents.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_BASECONTENTSOBJECT_HXX
#define HIF_BASECONTENTSOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Scope.hh"
#include "BList.hh"

namespace hif {

///	@brief Abstract class for content classes (i.e., Contents and Generate).
///
/// @see: Contents, Generate.

class HIF_EXPORT BaseContents: public Scope
{

public:

    /// @brief List of declarations.
    /// @see Declaration.
    BList <Declaration> declarations;

    /// @brief List of state tables (i.e., processes).
    /// @see StateTable
    BList <StateTable> stateTables;

    /// @brief List of component instances.
    /// @see Instance
    BList <Instance> instances;

    /// @brief List of generate constructs.
    /// @see Generate
    BList <Generate> generates;

    /// @brief Constructor.
    BaseContents();

    /// @brief Destructor.
    virtual ~BaseContents();

    /// @brief Sets concurrent actions in the content.
    /// @param g The new concurrent actions to be set.
    /// @return The old concurrent actions if they differ from
    /// the new ones, NULL otherwise.
    GlobalAction * setGlobalAction(GlobalAction *g);

    /// @brief Return concurrent actions in the content.
    /// @return The concurrent actions in the content.
    GlobalAction * getGlobalAction() const;

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    /// @brief The concurrent actions in the content.
    GlobalAction * _globalAction;

    // K: disabled.
    BaseContents(const BaseContents &);
    BaseContents & operator =(const BaseContents &);
};

} // namespace hif

#endif

