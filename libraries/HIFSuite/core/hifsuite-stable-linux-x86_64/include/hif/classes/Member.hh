// Member.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_MEMBEROBJECT
#define HIF_MEMBEROBJECT

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "PrefixedReference.hh"

namespace hif {

///	@brief Single element selection in an array.
///
/// This class represents the access to a single selected element in an array
/// or a vector.
/// It consists of a prefix (i.e., the array or vector on which selection is
/// performed) and a list of indices (to handle multidimensional arrays).

class HIF_EXPORT Member: public PrefixedReference
{

public:

    /// @brief Constructor.
    Member();

    /// @brief Destructor.
    virtual ~Member();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Return the index of the member.
    /// @return The index of the member.
    Value * getIndex() const;

    /// @brief Sets the index of the member.
    /// @param v The index of the member to be set.
    /// @return The old index of the member, or NULL if none.
    Value * setIndex(Value * v);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    /// @brief The index of the member.
    Value * _index;

    // K: disabled
    Member(const Member &);
    Member & operator =(const Member &);
};

} // namespace hif

#endif

