// Real.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_REALOBJECT_HXX
#define HIF_REALOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "SimpleType.hh"
#include "../features/ITypeSpan.hh"

namespace hif {

/// @brief Real data type.
///
/// This class represents the real data type.
/// The real data type can be restricted to a given range. The physical
/// implementation of a real type cannot be specified.

class HIF_EXPORT Real:
        public SimpleType,
        public hif::features::ITypeSpan
{

public:

    /// @brief Constructor.
    Real();

    /// @brief Destructor.
    virtual ~Real();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Sets the range constraint on the real data type.
    /// @param r The range constraint to be set on the real data type.
    /// @return The previous range constraint, or NULL if none.
    Range * setSpan(Range * r);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    // K: disabled
    Real(const Real &);
    Real & operator =(const Real &);
};

} // namespace hif

#endif
