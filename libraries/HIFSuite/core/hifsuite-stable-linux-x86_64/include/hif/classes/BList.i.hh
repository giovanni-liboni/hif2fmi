#ifndef HIF_BLIST_I_HH
#define HIF_BLIST_I_HH

#include "BList.hh"

namespace hif {

template <class T>
template <typename T1>
BList <T>::operator BList<T1> &()
{
    return this->toOtherBList<T1>();
}

template <class T>
template <typename Comparator>
bool BList <T>::sort(Comparator & c)
{
    return BListHost::sort(c);
}

template <class T>
template <typename T1>
BList<T1> & BList<T>::toOtherBList()
{
    // Sanity checks:
    T * t = NULL;
    T1 * t1 = NULL;
    t1 = static_cast <T1*>(t);
    t = static_cast <T*>(t1);
    // Conversion:
    union U
    {
        BList<T> * l1;
        BList<T1> * l2;
    };
    U u;
    u.l1 = this;
    return *(u.l2);
}

template <class T>
template <typename T1>
const BList<T1> & BList<T>::toOtherBList() const
{
    // Sanity checks:
    T * t = NULL;
    T1 * t1 = NULL;
    t1 = static_cast <T1*>(t);
    t = static_cast <T*>(t1);
    // Conversion:
    union U
    {
        const BList<T> * l1;
        const BList<T1> * l2;
    };
    U u;
    u.l1 = this;
    return *(u.l2);
}

} // Hif

#endif
