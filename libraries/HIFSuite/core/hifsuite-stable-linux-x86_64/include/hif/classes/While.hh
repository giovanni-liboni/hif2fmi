// While.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_WHILEOBJECT_HXX
#define HIF_WHILEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Action.hh"
#include "../features/INamedObject.hh"

namespace hif {

/// @brief While loop.
///
/// This class represents a while loop.

class HIF_EXPORT While:
        public Action,
        public features::INamedObject
{

public:

    /// @brief List of actions that form the loop body.
    BList <Action> actions;

    /// @brief Constructor.
    While();

    /// @brief Destructor.
    virtual ~While();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the while loop condition.
    /// @return The while loop condition.
    Value * getCondition() const;

    /// @brief Sets the while loop condition.
    /// @param b The while loop condition to be set.
    /// @return The old while loop condition.
    Value * setCondition(Value * b);

    /// @brief Returns the <tt>doWhile</tt> attribute.
    /// @return The <tt>doWhile</tt> attribute.
    bool isDoWhile() const;

    /// @brief Sets the <tt>doWhile</tt> attribute.
    /// @param doWhile The <tt>doWhile</tt> attribute.
    void setDoWhile(const bool doWhile);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    /// @brief The while loop condition.
    Value * _condition;

    /// @brief Identify if costruct is a while or a do-while.
    bool _doWhile;

    // K: disabled
    While(const While &);
    While & operator =(const While &);
};

} // namespace hif

#endif

