// Aggregate.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_AGGREGATEOBJECT_HXX
#define HIF_AGGREGATEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "AggregateAlt.hh"
#include "Value.hh"

namespace hif {

///	@brief Composite type constant value.
///
/// Aggregates are used to define constant values for array or record
/// types. They contain a list of AggregateAlt to describe the specified
/// values of the elements or fields, and a default value for all
/// non-specified elements or fields.
///
/// @see AggregateAlt

class HIF_EXPORT Aggregate: public Value
{

public:

    /// @brief List of AggregateAlt to describe the specified values of the
    /// elements or fields of the constant.
    BList <AggregateAlt> alts;

    /// @brief Constructor.
    Aggregate();

    /// @brief Destructor.
    virtual ~Aggregate();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the default value of the aggregate.
    /// @return Pointer to the aggregate default value.
    Value * getOthers() const;

    /// @brief Sets the default value of the aggregate (for all non-specified elements or fields).
    /// @param v The default value to be set.
    /// @return The old default value if it is different from
    /// the new one, NULL otherwise.
    Value* setOthers(Value* v);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    /// @brief Default value for non-specified elements or fields.
    Value * _others;

    // K: disabled.
    Aggregate(const Aggregate &);
    Aggregate & operator =(const Aggregate &);

};

} // namespace hif

#endif

