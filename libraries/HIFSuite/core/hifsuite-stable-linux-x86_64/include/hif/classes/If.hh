// Aggregate.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_CASEOBJECT_HXX
#define HIF_CASEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Action.hh"

namespace hif {

/// @brief If statement.
///
/// This class represents an if statement. It consists of a list of possible
/// alternatives (IfAlt), corresponding to the <tt>if</tt> and <tt>else if</tt>
/// branches of the statement, and of a default list of actions corresponding
/// to the <tt>else</tt> branch.
///
/// @see IfAlt

class HIF_EXPORT If: public Action
{
public:

    /// @brief The alt type.
    typedef IfAlt AltType;

    /// @brief List of alternatives representing the different branches.
    BList <IfAlt> alts;

    /// @brief Default list of actions, to be executed when no condition
    /// in alternatives is matched.
    BList <Action> defaults;

    /// @brief Constructor.
    If();

    /// @brief Destructor.
    virtual ~If();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;
};

} // namespace hif

#endif

