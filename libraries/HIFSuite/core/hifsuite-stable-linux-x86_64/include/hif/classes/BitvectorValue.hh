// BitvectorValue.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_BITVECTORVALOBJECT_HXX
#define HIF_BITVECTORVALOBJECT_HXX

#include <vector>
#include <cstdlib>

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "ConstValue.hh"
#include "../hifEnums.hh"

namespace hif {

/// @brief Bit vector value.
//
/// This class represents a value of bit vector type.
/// By default this value is assigned to '0'.

class HIF_EXPORT BitvectorValue: public ConstValue
{
public:

    /// @brief Constructor.
    BitvectorValue();

    /// @brief Constructor.
    /// @param sValue String representing the bit vector value. Default is "0".
    BitvectorValue(const std::string & sValue);

    /// @brief Destructor.
    virtual ~BitvectorValue();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the bit vector value.
    /// @return The bit vector value.
    std::string getValue() const;

    /// @brief Sets the bit vector value.
    /// @param value String representing the bit vector value to be set.
    void setValue(const std::string & value);

    /// @brief Returns true if contains only 0 or 1.
    /// @return true if contains only 0 or 1.
    bool is01() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief Disabled copy constructor.
    BitvectorValue(const BitvectorValue&);

    /// @brief Disabled assignment operator.
    BitvectorValue& operator =(const BitvectorValue&);

    /// @brief Ensures that a string representing a bit vector value contains
    /// only legal values for bits.
    /// @return <tt>true</tt> if the check is successful, <tt>false</tt>
    /// if an error occurred.
    bool _handleValue(const std::string & val);

    /// @brief The bit vector value.
    std::vector<BitConstant> _value;
};

} // namespace hif

#endif

