// ParameterAssign.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_PARAMASSOBJECT_HXX
#define HIF_PARAMASSOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "PPAssign.hh"
#include "../features/ISymbol.hh"

namespace hif {

///	@brief Parameter assignment.
///
/// This class represents a parameter assignment (i.e., an argument/actual
/// parameter).
///
/// @see PPAssign, Parameter

class HIF_EXPORT ParameterAssign :
    public PPAssign,
    public features::TemplateSymbolIf<Parameter>
{

public:

    /// @brief Constructor.
    ParameterAssign();

    /// @brief Destructor.
    virtual ~ParameterAssign();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled.
    ParameterAssign(const ParameterAssign &);
    ParameterAssign & operator =(const ParameterAssign &);
};

} // namespace hif

#endif

