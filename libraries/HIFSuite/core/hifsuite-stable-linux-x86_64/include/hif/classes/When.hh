// When.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_WHENOBJECT_HXX
#define HIF_WHENOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Value.hh"

namespace hif {

/// @brief Conditional expression.
///
/// This class represents a conditional expression (e.g., the 
/// <tt>when</tt> construct in VHDL, or the ternary operator in C/C++).
/// The value to which a conditional expression evaluates depends on a list
/// of alternatives (WhenAlt), each one containing a condition and an
/// associated value.
/// When one of the condition is matched, the expression takes on the 
/// corresponding value. If no conditions are matched, and a default value
/// is provided, the expression takes on the default value.
///
/// @see WhenAlt

class HIF_EXPORT When: public Value
{
public:

    /// @brief The alt type.
    typedef WhenAlt AltType;

    /// The list of alternatives.
    BList <WhenAlt> alts;

    /// @brief Constructor.
    When();

    /// @brief Destructor.
    virtual ~When();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the default value of the conditional expression.
    /// @return The default value of the conditional expression.
    /// If a default value do not exist 0 is returned.
    Value * getDefault() const;

    /// @brief Sets the default value of the conditional expression.
    /// @param v The default value to be set.
    /// @return The previous default value of the conditional expression.
    Value * setDefault(Value * v);

    /// @brief Returns logic ternary property.
    /// @return logic ternary property.
    bool isLogicTernary() const;

    /// @brief Sets logic ternary property.
    /// @param logicTernary The value to set.
    void setLogicTernary(const bool logicTernary);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    /// @brief The default value of the conditional expression.
    Value * _defaultvalue;

    /// @brief Identify whether the When condition must be treated as logic condition
    /// as in Verilog semantics.
    bool _logicTernary;

    // K: disabled
    When(const When &);
    When & operator =(const When &);
};

} // namespace hif

#endif

