// PPAssign

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_PPASSIGNOBJECT_HXX
#define HIF_PPASSIGNOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "ReferencedAssign.hh"
#include "../hifEnums.hh"

namespace hif {

/// @brief Abstract class for parameter or port assignments. 
///
/// This class is an abstract class for parameter assignments
/// (i.e., arguments) or port assignments (i.e., port bindings).
///
/// @see ParameterAssign, PortAssign

class HIF_EXPORT PPAssign: public ReferencedAssign
{

public:

    /// @brief Constructor.
    PPAssign();

    /// @brief Destructor.
    virtual ~PPAssign();

    /// @brief Returns the direction of the parameter or port assignment.
    /// @return The direction of the parameter or port assignment.
    PortDirection getDirection() const;

    /// @brief Sets the direction of the parameter or port assignment.
    /// @param d The direction of the parameter or port assignment to be set.
    void setDirection(const PortDirection d);

    /// @brief Returns the value of the parameter or port assignment.
    /// It represents the argument passed as parameter or the value bound
    /// to a port.
    /// @return The value of the parameter or port assignment.
    Value* getValue() const;

    /// @brief Sets the value of the parameter or port assignment.
    /// @param v The value of the parameter or port assignment to be set.
    /// @return The old value of the parameter or port assignment if it is
    /// different from the new one, NULL otherwise.
    Value* setValue(Value * v);

protected:

    /// @brief The value of the parameter or port assignment.
    Value * _value;

    /// @brief The direction of the parameter or port assignment.
    PortDirection _direction;

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    // K: disabled.
    PPAssign(const PPAssign &);
    PPAssign & operator =(const PPAssign &);
};

} // namespace hif

#endif

