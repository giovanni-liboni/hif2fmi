// Reference.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef REFERENCEOBJECT_HXX_
#define REFERENCEOBJECT_HXX_

/// LOCAL INCLUDES
///
#include "../applicationUtils/portability.hh"
#include "CompositeType.hh"

namespace hif {

/// @brief Reference (in C++).
///
/// This class represents a C++ reference.

class HIF_EXPORT Reference: public CompositeType
{

public:

    /// @brief Constructor.
    Reference();

    /// @brief Destructor.
    virtual ~Reference();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled
    Reference(const Reference &);
    Reference & operator =(const Reference &);
};

} // namespace hif

#endif /*REFERENCEOBJECT_HXX_*/
