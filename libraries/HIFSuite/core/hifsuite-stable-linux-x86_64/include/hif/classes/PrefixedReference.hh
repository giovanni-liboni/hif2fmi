// PrefixedReference.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_PREFIXEDREFERENCE_HXX
#define HIF_PREFIXEDREFERENCE_HXX

// LOCAL INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "Value.hh"

namespace hif {

///	@brief Base class for prefixed references.
///
/// This class is a base class for prefixed references (i.e., accesses
/// to elements of arrays or records).
///
/// @see FieldReference, Member, Slice

class HIF_EXPORT PrefixedReference: public Value
{

public:

    /// @brief Constructor.
    PrefixedReference();

    /// @brief Destructor.
    virtual ~PrefixedReference();

    /// @brief Returns the prefix.
    /// @return The prefix.
    Value* getPrefix() const;

    /// @brief Sets the prefix.
    /// @param v The prefix to be set.
    /// @return The old prefix if it is different from the new one,
    /// NULL otherwise.
    Value* setPrefix(Value* v);

protected:

    /// @brief The prefix.
    Value* _prefix;

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    // K: disabled.
    PrefixedReference(const PrefixedReference &);
    PrefixedReference & operator =(const PrefixedReference &);

};

} // namespace hif

#endif

