// ParameterAssign.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_PARAMOBJECT_HXX
#define HIF_PARAMOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "DataDeclaration.hh"
#include "../hifEnums.hh"

namespace hif {

///	@brief Parameter declaration.
///
/// This class represents the declaration of a parameter in a subprogram.

class HIF_EXPORT Parameter: public DataDeclaration
{

public:

    /// @brief Constructor.
    /// The type is set to NULL and the direction to <tt>dir_none</tt>.
    Parameter();

    /// @brief Destructor.
    virtual ~Parameter();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the direction of the paramter.
    /// @return The direction of the paramter.
    PortDirection getDirection() const;

    /// @brief Sets the direction of the parameter.
    /// @param d The direction of the parameter to be set.
    void setDirection(PortDirection d);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief The direction of the parameter.
    PortDirection _direction;

    // K: disabled.
    Parameter(const Parameter &);
    Parameter & operator =(const Parameter &);
};

} // namespace hif

#endif

