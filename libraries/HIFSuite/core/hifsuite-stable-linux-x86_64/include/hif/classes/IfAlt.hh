// IfAlt.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_CASEALTOBJECT_HXX
#define HIF_CASEALTOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Alt.hh"

namespace hif {

/// @brief If statement alternative.
/// 
/// This class represents an alternative for an if statement (i.e., a branch
/// of an if statement, except the <tt>else</tt> one).
/// It consists of a boolean condition and a list of actions to be executed
/// when the condition is true.
///
/// @see If

class HIF_EXPORT IfAlt: public Alt
{

public:

    /// @brief List of actions to be executed if the condition is matched.
    BList <Action> actions;

    /// @brief Constructor.
    IfAlt();

    /// @brief Destructor.
    virtual ~IfAlt();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the condition of the if alternative.
    /// @return The condition of the if alternative.
    Value* getCondition() const;

    /// @brief Sets the condition of the if alternative.
    /// @param b The condition of the if alternative to be set.
    /// @return The old condition of the if alternative if it is different
    /// from the new one, NULL otherwise.
    Value* setCondition(Value* b);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:
    // K: disabled
    IfAlt(const IfAlt &);
    IfAlt & operator =(const IfAlt &);

    /// @brief Condition of the if alternative.
    Value * _condition;

};

} // namespace hif

#endif

