// BitValue.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_BOOLVALOBJECT_HXX
#define HIF_BOOLVALOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "ConstValue.hh"

namespace hif {

/// @brief Boolean value.
///
/// This class represents a boolean value.
/// By default this value is assigned to <tt>false</tt>.

class HIF_EXPORT BoolValue: public ConstValue
{
public:

    /// @brief Constructor.
    BoolValue();

    /// @brief Constructor.
    /// @param b The boolean value to be assigned. Default is <tt>false</tt>.
    explicit BoolValue(const bool b);

    /// @brief Destructor.
    virtual ~BoolValue();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the boolean value.
    /// @return The boolean value.
    bool getValue() const;

    /// @brief Sets the boolean value.
    /// @param b The boolean value to be set.
    void setValue(const bool b);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief The actual boolean value.
    bool _value;

};

} // namespace hif

#endif

