// Field

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_FIELDOBJECT_HXX
#define HIF_FIELDOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "DataDeclaration.hh"

namespace hif {

/// @brief Field declaration (in a Record).
///
/// This class represents the declaration of field within the declaration
/// of a data structure (a Record).
///
/// @see Record

class HIF_EXPORT Field: public DataDeclaration
{

public:

    /// @brief Constructor.
    Field();

    /// @brief Destructor.
    virtual ~Field();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns the direction of the field when representing a port.
    /// @return The direction.
    PortDirection getDirection() const;

    /// @brief Sets the direction of the field when representing a port.
    /// @param d The direction to set.
    void setDirection(const PortDirection d);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    ///	@brief The direction of the field reference when representing a port.
    PortDirection _direction;

    // K: disabled
    Field(const Field &);
    Field & operator =(const Field &);

};

} // namespace hif

#endif

