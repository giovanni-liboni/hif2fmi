///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_CLASSES_EVENT_H
#define HIF_CLASSES_EVENT_H

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "SimpleType.hh"

namespace hif {

///	@brief Event type.
///
/// This class represents the event type.

class HIF_EXPORT Event: public SimpleType
{

public:

    /// @brief Constructor.
    Event();

    /// @brief Destructor.
    virtual ~Event();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();
};

} // namespace hif

#endif // HIF_CLASSES_EVENT_H
