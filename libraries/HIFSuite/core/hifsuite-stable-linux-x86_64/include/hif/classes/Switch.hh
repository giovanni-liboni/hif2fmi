// Switch.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_SWITCHOBJECT_HXX
#define HIF_SWITCHOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Action.hh"

namespace hif {

/// @brief Switch statement.
///
/// This class represents a switch statement.
/// It consists of the switch value, a list of possible case alternatives
/// (SwitchAlt) and an optional default list of actions.
///
/// @see SwitchAlt

class HIF_EXPORT Switch:
        public Action
{

public:

    /// @brief The alts type.
    typedef SwitchAlt AltType;

    /// @brief The list of alternatives representing the different cases.
    BList <SwitchAlt> alts;

    /// @brief The default list of actions, to be executed when no case
    /// in the alternatives is matched.
    BList <Action> defaults;

    /// @brief Constructor.
    Switch();

    /// @brief Destructor.
    virtual ~Switch();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the switch value on which case selection is performed.
    /// @return The switch value.
    Value * getCondition() const;

    /// @brief Sets the switch value on which case selection is performed.
    /// @param v The switch value to be set.
    /// @return The old switch value, or NULL if none.
    Value * setCondition(Value * v);

    /// @brief Returns the case semantics used in current Switch.
    /// @return The case semantics.
    CaseSemantics getCaseSemantics() const;

    /// @brief Sets the case semantics used in current Switch.
    /// @param c The case semantics to be set.
    void setCaseSemantics(const CaseSemantics c);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    /// @brief The switch value on which case selection is performed.
    Value * _condition;

    /// @brief The case semantics.
    CaseSemantics _caseSemantics;

    // K: disabled
    Switch(const Switch &);
    Switch & operator =(const Switch &);
};

} // namespace hif

#endif

