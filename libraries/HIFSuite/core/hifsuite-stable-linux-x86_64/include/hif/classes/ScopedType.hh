#ifndef HIF_SCOPEDTYPE_HXX
#define HIF_SCOPEDTYPE_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Type.hh"

namespace hif {

/// @brief Abstract class for types containing declarations.
///
/// This class is an abstract class for types which contain declarations.
///
/// @see Enum, Record

class HIF_EXPORT ScopedType: public Type
{
public:

    /// @brief Returns the <tt>constexpr</tt> attribute.
    /// @return The <tt>constexpr</tt> attribute.
    bool isConstexpr();

    /// @brief Sets the <tt>constexpr</tt> attribute.
    /// @param v The <tt>constexpr</tt> attribute to be set.
    void setConstexpr(const bool v);

    /// @brief Destructor.
    virtual ~ScopedType();

protected:

    /// @brief Constructor.
    ScopedType();

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief Flag to store the <tt>constexpr</tt> attribute.
    bool _isConstexpr;
};

} // namespace hif

#endif
