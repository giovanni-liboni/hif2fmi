//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef VALUESTATEMENT_H
#define VALUESTATEMENT_H

#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Action.hh"

namespace hif {

///	@brief Call to a procedure.
///
/// This class represents a call to a procedure.
///
/// @see Procedure

class HIF_EXPORT ValueStatement:
    public Action
{

public:
    /// @brief Constructor.
    ValueStatement();

    /// @brief Destructor.
    virtual ~ValueStatement();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the statement value.
    /// @return The statement value.
    Value * getValue() const;

    /// @brief Sets the statement value.
    /// @param n The statement value to be set.
    /// @return The previous statement value, or NULL if none.
    Value * setValue(Value * n);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    ///@brief The calling object.
    Value * _value;

    // K: disabled
    ValueStatement(const ValueStatement &);
    ValueStatement & operator =(const ValueStatement &);
};

} // namespace hif

#endif // VALUESTATEMENT_H
