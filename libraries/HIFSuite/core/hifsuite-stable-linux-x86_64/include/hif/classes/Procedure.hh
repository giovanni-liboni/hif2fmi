// Procedure

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_SUBPROGRAMOBJECT_H
#define HIF_SUBPROGRAMOBJECT_H

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "SubProgram.hh"

namespace hif {

///	@brief Procedure declaration.
///
/// This class represents the declaration of a procedure.

class HIF_EXPORT Procedure: public SubProgram
{

public:

    /// @brief Constructor.
    Procedure();

    /// @brief Destructor.
    virtual ~Procedure();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled
    Procedure(const Procedure &);
    Procedure & operator =(const Procedure &);
};

} // namespace hif

#endif

