// RealValue.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_REALVALOBJECT_HXX
#define HIF_REALVALOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "ConstValue.hh"

namespace hif {

/// @brief Real value.
///
/// This class represents a value of real type.
/// The underlying data type used to store the value is <tt>double</tt>.

class HIF_EXPORT RealValue: public ConstValue
{

public:

    /// @brief Constructor.
    RealValue();

    /// @brief Constructor.
    /// @param d The real value to be assigned. Default is 0.0.
    RealValue(const double d);

    /// @brief Destructor.
    virtual ~RealValue();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the real value.
    /// @return The real value.
    double getValue() const;

    /// @brief Sets the real value.
    /// @param d The real value to be set.
    void setValue(const double d);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief The actual real value (stored as a <tt>double</tt>).
    double _value;

};

} // namespace hif

#endif

