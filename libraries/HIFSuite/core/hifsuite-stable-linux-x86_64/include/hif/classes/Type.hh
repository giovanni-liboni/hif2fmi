// SubProgram.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_TYPEOBJECT_HXX
#define HIF_TYPEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "Object.hh"

namespace hif {

/// @brief Abstract class for types.
///
/// This class is an abstract class for types.

class HIF_EXPORT Type: public Object
{

public:

    /// @brief The possibile variants of types in all semantics.
    enum TypeVariant
    {
        NATIVE_TYPE,                /// <@brief Default: no variant is in use.
        VHDL_BITVECTOR_NUMERIC_STD, /// <@brief Signed type defined in std_logic_signed
        SYSTEMC_INT_BITFIELD,       /// <@brief c++ bitfield
        SYSTEMC_INT_SC_INT,         /// <@brief sc_int, sc_uint
        SYSTEMC_INT_SC_BIGINT,      /// <@brief sc_bigint, sc_bigunit
        SYSTEMC_BITVECTOR_PROXY,    /// <@brief sc_proxy_lv, sc_proxy_bv
        SYSTEMC_BITVECTOR_BASE,     /// <@brief sc_lv_base, sc_bv_base
        SYSTEMC_BIT_BITREF          /// <@brief sc_bitref_l, sc_bitref_b
    };


    /// @brief Constructor.
    Type();

    /// @brief Destructor.
    virtual ~Type();

    /// @brief Returns the <tt>typeVariant</tt> attribute.
    /// @return The <tt>typeVariant</tt> attribute.
    ///
    TypeVariant getTypeVariant() const;

    /// @brief Sets the <tt>typeVariant</tt> attribute.
    /// @param tv The <tt>typeVariant</tt> attribute to be set.
    void setTypeVariant(const TypeVariant tv);

    /// @brief Return string representation of given type variant.
    /// @param t The type variant.
    /// @return The string representation of given type variant.
    static
    std::string typeVariantToString(const TypeVariant t);

    /// @brief Return the type variant for given string.
    /// @param s The string.
    /// @return The type variant for given string.
    static
    TypeVariant typeVariantFromString(const std::string & s);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Stores the <tt>typeVariant</tt> attribute.
    /// Default value is NATIVE_TYPE.
    TypeVariant _typeVariant;

};

} // namespace hif

#endif

