//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// Utility class for symbol and names used in design.
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_DECLOBJECT_H
#define HIF_DECLOBJECT_H

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "../features/INamedObject.hh"
#include "Object.hh"
#include <vector>

namespace hif {

/// @brief Abstract class for declarations.
class HIF_EXPORT Declaration:
        public Object,
        public features::INamedObject
{
public:

    typedef std::vector<std::string> KeywordList;

    /// @brief Constructor.
    Declaration();

    /// @brief Destructor.
    virtual ~Declaration();

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

    /// @name Additional keywords handling methods.
    //@{

    /// @brief Adds a keyword to object list.
    /// @param kw The name of the keyword to be added.
    void addAdditionalKeyword(const std::string & kw);

    /// @brief Removes a keyword from object list.
    /// @param kw The name of the keyword to be removed.
    void removeAdditionalKeyword(const std::string & kw);

    /// @brief Check if has some additional keyword
    /// @return <tt>true</tt> if there is at least one additional keyword
    bool hasAdditionalKeywords();

    /// @brief Check whether the list contains the keyword @p kw.
    /// @param kw The name of the keyword to be checked.
    /// @return <tt>true</tt> if the list contains the keyword @p kw,
    /// <tt>false</tt> otherwise.
    bool checkAdditionalKeyword(const std::string & kw);

    /// @brief Clears all the keywords from the Declaration
    void clearAdditionalKeywords();

    KeywordList::iterator getAdditionalKeywordsBeginIterator();
    KeywordList::iterator getAdditionalKeywordsEndIterator();

    //@}

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief List of additional keywords (if any)
    KeywordList * _additionalKeywords;

private:

    // K: disabled.
    Declaration(const Declaration&);
    Declaration & operator =(const Declaration &);
};

} // namespace hif

#endif
