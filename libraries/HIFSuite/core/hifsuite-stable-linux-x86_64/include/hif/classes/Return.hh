// Return.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_RETURNOBJECT_HXX
#define HIF_RETURNOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Action.hh"

namespace hif {

/// @brief Return statement.
///
/// This class represents a return statement, which terminates the execution
/// of a subprogram. In case of a function, a return value must be specified.
/// In case of a procedure, a return value must not be specified.

class HIF_EXPORT Return: public Action
{

public:

    /// @brief Returned type by member GetDeclaration
    typedef Function DeclarationType;

    /// @brief Constructor.
    Return();

    /// @brief Destructor.
    virtual ~Return();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the returned value.
    /// @return The returned value.
    Value * getValue() const;

    /// @brief Sets the returned value.
    /// @param n The returned value to be set.
    /// @return The previous returned value, or NULL if none.
    Value * setValue(Value * n);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    /// @brief The returned value.
    Value * _value;

    // K: disabled
    Return(const Return &);
    Return & operator =(const Return &);
};

} // namespace hif

#endif

