//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_TRANSITIONOBJECT_HXX
#define HIF_TRANSITIONOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Action.hh"
#include "../NameTable.hh"

namespace hif {

///	@brief Next state.
///
/// This action specifies the next state and terminates the transition.
///

class HIF_EXPORT Transition: public Action
{
public:

    /// @name Traits.
    //@{

    /// @brief The edge priority. See the formal model description.
    typedef unsigned long int priority_t;

    //@}

    /// @brief Constructor
    ///
    /// Create a new Transition.
    ///
    Transition();

    /// @brief Destructor
    ///
    virtual ~Transition();

    /// @brief Returns a string representing the class name.
    ///
    /// @return The string.
    ///
    ClassId getClassId() const;

    /// @brief Function to change the name of next state.
    ///
    /// @param n Name representing next state's new name.
    ///
    void setName(Name n);

    /// @brief Function to get the name of next state.
    ///
    /// @return Name representing next state's name.
    ///
    Name getName() const;

    /// @brief Function to change the name of prev state.
    ///
    /// @param n Name representing prev state's new name.
    ///
    void setPrevName(Name n);

    /// @brief Function to get the name of prev state.
    ///
    /// @return Name representing prev state's name.
    ///
    Name getPrevName() const;

    /// @brief Function to visit the current object.
    ///
    /// @param vis visitor of type HifVisitor.
    ///
    /// @return Integer representing visit result. Default 0.
    ///
    virtual int acceptVisitor(HifVisitor & vis);

    /// @name Computational model related methods and variables.
    //@{

    /// @brief Sets the edge priority.
    void setPriority(const priority_t p) throw ();

    /// @brief Sets the edge priority.
    priority_t getPriority() const throw ();

    /// @brief Composes all enablingList expressions into a single expression.
    /// The resulting expression will be the only object into the enablingList.
    void enablingListToExpression();

    /// @brief List of conditions on events (labels).
    /// The labels are considered in AND.
    BList <Value> enablingLabelList;

    /// @brief Function to set the value of the enablingLabelOrMode flag.
    ///
    /// @param flag boolean value of the enablingLabelOrMode flag.
    ///
    void setEnablingOrCondition(const bool flag);

    /// @brief Function to get the value of the enablingLabelOrMode flag.
    ///
    /// @return boolean value of the enablingLabelOrMode flag.
    ///
    bool getEnablingOrCondition() const;

    /// @brief List of conditions on variables (enabling function).
    /// The expressions are considered in AND.
    ///
    BList <Value> enablingList;

    /// @brief List of labels (events) to be fired when the edge is traversed (update label function).
    ///
    BList <Value> updateLabelList;

    /// @brief List of actions to be performed when the edge is traversed (update function).
    /// The actions are considered sequential.
    ///
    BList <Action> updateList;

    //@}

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    /// @brief Reference to the next state.
    ///
    Name _name;

    /// @brief Reference to the previous state.
    Name _prevName;

    /// @brief This edge priority. Zero means no priority. One means maximum priority.
    priority_t _priority;

    /// @brief Flag to indicate if the enabling labels have to be considered in
    /// in OR. Default value is false, to consider enabling labels in AND.
    ///
    bool _enablingLabelOrMode;

    // K: disabled
    Transition(const Transition &);
    Transition & operator =(const Transition &);

};

} // namespace hif

#endif

