// Enum.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_ENUMOBJECT_HXX
#define HIF_ENUMOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "ScopedType.hh"

namespace hif {

///	@brief Enumeration type.
///
/// This class represents an enumeration type.
/// The enumeration contains a list of values (EnumValue), to which a
/// progressive integer number starting from 0 is associated.
///
/// @see EnumValue

class HIF_EXPORT Enum: public ScopedType
{

public:

    /// @brief List of values of the enumeration.
    BList <EnumValue> values;

    /// @brief Constructor.
    Enum();

    /// @brief Destructor.
    virtual ~Enum();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;
};

} // namespace hif

#endif

