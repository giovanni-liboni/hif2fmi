// Range.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_RANGEOBJECT_HXX
#define HIF_RANGEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "Value.hh"
#include "../hifEnums.hh"

namespace hif {

/// @brief Range (i.e., a directed discrete interval).
///
/// This class represents a range, which is a directed discrete interval.

class HIF_EXPORT Range: public Value
{

public:

    /// @brief Default constructor.
    Range();

    /// @brief Default constructor.
    Range(Value* lbound, Value* rbound, const RangeDirection dir);

    /// @brief Constructor.
    /// @param lbound The left integer bound of the range.
    /// @param rbound The right integer bound of the range.
    Range(const long long int lbound, const long long int rbound);

    /// @brief Destructor.
    virtual ~Range();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the direction of the range.
    /// @return The direction of the range.
    RangeDirection getDirection() const;

    /// @brief Sets the direction of the range.
    /// @param d The direction of the range to be set.
    void setDirection(const RangeDirection d);

    /// @brief Returns the left bound of the range.
    /// @return The left bound of the range.
    Value * getLeftBound() const;

    /// @brief Sets the left bound of the range.
    /// @param b The left bound of the range to be set.
    /// @return The old left bound of the range, or NULL if none.
    Value * setLeftBound(Value * b);

    /// @brief Returns the right bound of the range.
    /// @return The right bound of the range.
    Value * getRightBound() const;

    /// @brief Sets the right bound of the range.
    /// @param b The right bound of the range to be set.
    /// @return The old right bound of the range, or NULL if none.
    Value * setRightBound(Value * b);

    /// @brief Returns the type of the range.
    /// @return The type of the range.
    Type * getType() const;

    /// @brief Sets the type of the range.
    /// @param t The type of the range to be set.
    /// @return The old type of the range, or NULL if none.
    Type * setType(Type * t);

    /// @brief Returns the semantic type of the range.
    /// @warning Should not be invoked.
    /// @return The semantic type of the range.
    virtual Type* getSemanticType() const;

    /// @brief Sets the semantic type of the range.
    /// @warning Raises an error when invoked.
    /// @param t The semantic type of the range to be set.
    /// @return The old semantic type of the range, or NULL if none.
    virtual Type * setSemanticType(Type* t);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Swap the bounds and the direction.
    void swapBounds();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    /// @brief The direction of the range.
    RangeDirection _direction;

    /// @brief The left bound of the range.
    Value * _leftBound;

    /// @brief The right bound of the range.
    Value * _rightBound;

    /// @brief The type of range (used for typed ranges).
    Type * _type;

    // K: disabled
    Range(const Range &);
    Range & operator =(const Range &);
};

} // namespace hif

#endif

