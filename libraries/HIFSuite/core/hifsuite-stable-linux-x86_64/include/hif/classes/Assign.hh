// Assign.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_ASSIGNOBJECT_HXX
#define HIF_ASSIGNOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Action.hh"

namespace hif {

///	@brief Assignment statement.
///
/// This class represents an assignment statement.
/// It describes assignments to variables, signals or ports.

class HIF_EXPORT Assign: public Action
{

public:

    /// @brief Constructor.
    Assign();

    /// @brief Destructor.
    virtual ~Assign();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the the right-hand side of the assignment.
    /// @return The right-hand side of the assignment.
    Value* getRightHandSide() const;

    /// @brief Sets the right-hand side of the assignment.
    /// @param v The new right-hand side of the assignment.
    /// @return The old right-hand side of the assignment if it is different
    /// from the new one, NULL otherwise.
    Value* setRightHandSide(Value* v);

    /// @brief Returns the left-hand side of the assignment.
    /// @return The left-hand side of the assignment.
    Value* getLeftHandSide() const;

    /// @brief Sets the left-hand side of the assignment.
    /// @param v The new left-hand side of the assignment.
    /// @return The old left-hand side of the assignment if it is different
    /// from the new one, NULL otherwise.
    Value* setLeftHandSide(Value* v);

    /// @brief Returns the delay after which the assignment will be performed.
    /// @return The delay of the assignment, or <tt>NULL</tt> in case the
    /// assignment is not delayed.
    Value* getDelay() const;

    /// @brief Sets the delay after which the assignment will be performed.
    /// @param tv The delay.
    /// @return The old delay of the assignment if it is different from the
    /// new one, NULL otherwise.
    Value* setDelay(Value* tv);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    // K: disabled
    Assign(const Assign &);
    Assign & operator =(const Assign &);

    /// @brief The source (i.e., the right-hand side) of the assignment.
    Value * _leftHandSide;

    /// @brief The target (i.e., the left-hand side) of the assignment.
    Value * _rightHandSide;

    /// @brief The assignment delay.
    Value * _delay;
};

} // namespace hif

#endif

