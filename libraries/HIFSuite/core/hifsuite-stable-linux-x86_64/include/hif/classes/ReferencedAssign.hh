// Value.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_REFERENCEDOBJECT_HXX
#define HIF_REFERENCEDOBJECT_HXX

/// LOCAL INCLUDES
///
#include "../applicationUtils/portability.hh"
#include "../features/INamedObject.hh"
#include "TypedObject.hh"

namespace hif {

/// @brief Base class for name-based assignments of value to parameters.
///
/// This class is a base class for name-based assignments of value to
/// parameters (i.e., assignments of values to parameters referenced by name).
///
/// @see PPAssign, TPAssign

class HIF_EXPORT ReferencedAssign:
    public TypedObject,
    public features::INamedObject{

public:

    /// @brief Constructor.
    ReferencedAssign();

    /// @brief Destructor.
    virtual ~ReferencedAssign();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled.
    ReferencedAssign(const ReferencedAssign &);
    ReferencedAssign & operator =(const ReferencedAssign &);

};

} // namespace hif

#endif

