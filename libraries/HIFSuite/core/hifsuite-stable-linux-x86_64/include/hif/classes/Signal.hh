// Signal.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_SIGNALOBJECT_HXX
#define HIF_SIGNALOBJECT_HXX

/// LOCAL INCLUDES
///
#include "../applicationUtils/portability.hh"
#include "DataDeclaration.hh"

namespace hif {

/// @brief Signal declaration.
///
/// This class represents the declaration of a RTL signal.
/// It consists of the name of the signal, its type and its initial value.

class HIF_EXPORT Signal: public DataDeclaration
{

public:

    /// @brief Constructor.
    Signal();

    /// @brief Destructor.
    virtual ~Signal();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns whether this is a standard declaration.
    /// @return <tt>true</tt> if this is a standard declaration, <tt>false</tt> otherwise.
    bool isStandard() const;

    /// @brief Sets whether this is a standard declaration.
    /// @param standard <tt>true</tt> if this is a standard declaration, <tt>false</tt> otherwise.
    void setStandard(const bool standard);

    /// @brief Returns whether this is a wrapper for an object with
    /// similar features, which does not have a matching data model.
    /// @return <tt>true</tt> if this is a wrapper, <tt>false</tt> otherwise.
    bool isWrapper() const;

    /// @brief Sets whether this is actually a wrapper for an object with
    /// similar features, which does not have a matching data model.
    /// @param wrapper <tt>true</tt> if this is a wrapper, <tt>false</tt> otherwise.
    void setWrapper(const bool wrapper);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief Distinguishes between a normal declaration (i.e., part of design)
    /// and a standard one (i.e., part of the language).
    bool _isStandard;

    /// @brief Tells whether this is actually a wrapper for an object with
    /// similar features, which does not have a matching data model (e.g.,
    /// Verilog-AMS natures or disciplines).
    bool _isWrapper;

    // K: disabled
    Signal(const Signal &);
    Signal & operator =(const Signal &);

};

} // namespace hif

#endif

