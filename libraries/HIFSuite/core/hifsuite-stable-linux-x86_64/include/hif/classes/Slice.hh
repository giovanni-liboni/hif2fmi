// Slice.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_SLICEOBJECT_HXX
#define HIF_SLICEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "PrefixedReference.hh"

namespace hif {

/// @brief Slice of a vector.
///
/// This class represents a slice of a vector (i.e., a contiguous portion
/// of a given vector identified by a corresponding span).

class HIF_EXPORT Slice: public PrefixedReference
{

public:

    /// @brief Constructor.
    Slice();

    /// @brief Destructor.
    virtual ~Slice();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Return the span of the slice.
    /// @return The span of the slice.
    Range * getSpan() const;

    /// @brief Sets the span of the slice.
    /// @param r The span of the slice to be set.
    /// @return The old span of the slice, or NULL if none.
    Range * setSpan(Range * r);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    /// @brief The span of the slice.
    Range * _span;

    // K: disabled
    Slice(const Slice &);
    Slice & operator =(const Slice &);
};

} // namespace hif

#endif
