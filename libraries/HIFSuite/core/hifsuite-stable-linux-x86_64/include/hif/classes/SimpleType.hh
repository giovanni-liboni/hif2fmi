// SimpleType.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_SIMPLETYPEOBJECT_HXX
#define HIF_SIMPLETYPEOBJECT_HXX

// HIF INCLUDES
//
#include "Type.hh"
#include "../applicationUtils/portability.hh"

namespace hif {

/// @brief Abstract class for simple types.
///
/// This class is an abstract class for simple types (i.e., their definition
/// does not involve other types).

class HIF_EXPORT SimpleType: public Type
{

public:

    /// @brief Constructor.
    SimpleType();

    /// @brief Destructor.
    virtual ~SimpleType();

    /// @brief Returns the <tt>constexpr</tt> attribute.
    /// @return The <tt>constexpr</tt> attribute.
    ///
    bool isConstexpr() const;

    /// @brief Sets the <tt>constexpr</tt> attribute.
    /// @param flag The <tt>constexpr</tt> attribute to be set.
    void setConstexpr(const bool flag);

protected:

    /// @brief Flag to store the <tt>constexpr</tt>.
    /// Default value is false.
    bool _isConstexpr;

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled
    SimpleType(const SimpleType &);
    SimpleType & operator =(const SimpleType &);

};

} // namespace hif

#endif

