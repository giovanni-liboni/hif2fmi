// TypeTP.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_TYPETPOBJECT_HXX
#define HIF_TYPETPOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "TypeDeclaration.hh"

namespace hif {

/// @brief Type template parameter.
///
/// This class represents the declaration of a type template parameter
/// (i.e., a template parameter the arguments of which are types).

class HIF_EXPORT TypeTP: public TypeDeclaration
{

public:

    /// @brief Constructor.
    TypeTP();

    /// @brief Destructor.
    virtual ~TypeTP();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();
};

} // namespace hif

#endif

