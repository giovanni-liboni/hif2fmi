///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_FILE_HXX
#define HIF_FILE_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "CompositeType.hh"

namespace hif {

///	@brief File type.
///
/// This class represents a file type.

class HIF_EXPORT File: public CompositeType
{

public:

    /// @brief Constructor.
    File();

    /// @brief Destructor.
    virtual ~File();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled.
    File(const File &);
    File & operator =(const File &);
};

} // namespace hif

#endif // HIF_FILE_HXX
