// ValueTP.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_VALUETPOBJECT_HXX
#define HIF_VALUETPOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "DataDeclaration.hh"

namespace hif {

/// @brief Value template parameter.
///
/// This class represents the declaration of a value tempalte parameter
/// (i.e., a template parameter the arguments of which are values).

class HIF_EXPORT ValueTP: public DataDeclaration
{

public:

    /// @brief Constructor.
    ValueTP();

    /// @brief Destructor.
    virtual ~ValueTP();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns whether this is a value TP that must be resolved
    /// at compile time.
    /// @return <tt>true</tt> if this is a compile time constant, <tt>false</tt> otherwise.
    bool isCompileTimeConstant() const;

    /// @brief Sets whether this is a value TP that must be resolved
    /// at compile time.
    /// @param compileTimeConstant The value to set.
    void setCompileTimeConstant(const bool compileTimeConstant);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Distinguishes between a value TP that must be resolved at compile
    /// time or not.
    bool _isCompileTimeConstant;
};

} // namespace hif

#endif

