// Bit.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_BITOBJECT_HXX
#define HIF_BITOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "SimpleType.hh"

namespace hif {

///	@brief Bit type.
///
/// This class represents a bit type.
/// The <tt>logic</tt> attribute is used to distinguish between logic type
/// (4-value logic) and bit type (2-value logic).
/// The <tt>resolved</tt> attribute is used to distinguish between resolved
/// and unresolved bit types.

class HIF_EXPORT Bit: public SimpleType
{

public:

    /// @brief Constructor.
    Bit();

    /// @brief Destructor.
    virtual ~Bit();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the <tt>logic</tt> attribute.
    /// @return The <tt>logic</tt> attribute.
    bool isLogic() const;

    /// @brief Sets the <tt>logic</tt> attribute.
    /// @param logic The <tt>logic</tt> attribute to be set.
    void setLogic(const bool logic);

    /// @brief Sets the <tt>resolved</tt> attribute.
    /// @return The <tt>resolved</tt> attribute.
    bool isResolved() const;

    /// @brief Set the <tt>resolved</tt> attribute.
    /// @param resolved The <tt>resolved</tt> attribute to be set.
    void setResolved(const bool resolved);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief Flag to store the <tt>logic</tt> attribute.
    bool _isLogic;

    /// @brief Flag to store the <tt>resolved</tt> attribute.
    bool _isResolved;

    // K: disabled
    Bit(const Bit &);
    Bit & operator =(const Bit &);

};

} // namespace hif

#endif

