// LibraryDef.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_LIBRARYDEFOBJECT_HXX
#define HIF_LIBRARYDEFOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Scope.hh"
#include "../hifEnums.hh"

namespace hif {

/// @brief Library definition.
///
/// This class represents a library definition.
/// A library definition could represent a VHDL <tt>package</tt>, or it can
/// be used to define C++ classes to be used by manipulation tools.

class HIF_EXPORT LibraryDef: public Scope
{

public:

    /// @brief List of libraries used by the library definition.
    BList <Library> libraries;

    /// @brief List of declarations in the library definition.
    BList <Declaration> declarations;

    /// @brief Constructor.
    LibraryDef();

    /// @brief Destructor.
    virtual ~LibraryDef();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the language ID of the library definition.
    /// @return The language ID of the library definition.
    hif::LanguageID getLanguageID() const;

    /// @brief Sets the language ID of the library definition.
    /// @param languageID The language ID of the library definition to be set.
    /// @return The language ID previously associated to the library definition.
    hif::LanguageID setLanguageID(const hif::LanguageID languageID);

    /// @brief Returns whether this is the definition of a standard library.
    /// @return <tt>true</tt> if this is the definition of a standard library.
    bool isStandard() const;

    /// @brief Sets whether this is the definition of a standard library.
    /// @param standard <tt>true</tt> if this is the definition of a standard library, <tt>false</tt> otherwise.
    void setStandard(const bool standard);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    /// @brief The implementation language of this library definition. This field
    /// is set by front-end or manipulation tools, and it is used by back-end
    /// tools to determine how the library definition is represented after translation.
    hif::LanguageID _languageID;

    /// @brief Distinguishes between the definition of a standard library 
    /// (e.g., TLM for SystemC or IEEE.numeric_std for VHDL) from a library
    /// definition which is part of a design.
    bool _isStandard;

};

} // namespace hif

#endif

