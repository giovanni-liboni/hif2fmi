// SimpleType.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_ALTOBJECT_HH
#define HIF_ALTOBJECT_HH

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "Object.hh"
#include "BList.hh"

namespace hif {

/// @brief Abstract class for alternative objects.
///
/// This class is an abstract class for alternative objects, which are
/// used by classes that feature selection among a variety of cases
/// (e.g., Aggregate, If, Switch, When, With).

class HIF_EXPORT Alt: public Object
{

public:

    /// @brief Constructor.
    Alt();

    /// @brief Destructor.
    virtual ~Alt();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();
};

} // namespace hif

#endif // HIF_ALTOBJECT_HH

