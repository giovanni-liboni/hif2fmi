// Function

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_FUNCTIONOBJECT_HXX
#define HIF_FUNCTIONOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "SubProgram.hh"

namespace hif {

///	@brief Function declaration.
///
/// This class represents the declaration of a function.

class HIF_EXPORT Function: public SubProgram
{

public:

    /// @brief Constructor.
    /// The return type of the function is set to NULL.
    Function();

    /// @brief Destructor.
    virtual ~Function();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the return type of the function.
    /// @return The return type of the function.
    Type * getType() const;

    /// @brief Sets the return type of the function.
    /// @param t The return type of the function to be set.
    /// @return The old return type of the function if it is different from
    /// the new one, NULL otherwise.
    Type* setType(Type * t);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    /// @brief The return type of the function.
    Type* _type;

    // K: disabled
    Function(const Function &);
    Function & operator =(const Function &);
};

} // namespace hif

#endif

