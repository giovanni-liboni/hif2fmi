// AggregateAlt.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_AGGREGATEALTOBJECT_HXX
#define HIF_AGGREGATEALTOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Alt.hh"
#include "BList.hh"

namespace hif {

/// @brief Alternative for an Aggregate.
///
///	This class represents an alternative for an Aggregate.
/// It consists of a list of indices and a corresponding value.
///
/// @see Aggregate

class HIF_EXPORT AggregateAlt: public Alt
{

public:

    /// @brief The list of indices for which the alternative defines the value.
    BList <Value> indices;

    /// @brief Constructor.
    AggregateAlt();

    /// @brief Destructor.
    virtual ~AggregateAlt();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the value of the aggregate alternative.
    /// @return The value of the aggregate alternative.
    Value* getValue() const;

    /// @brief Sets the value of the aggregate alternative.
    /// @param v The value to be set for the aggregate alternative.
    /// @return old The old value of the aggregate alternative if it is
    /// different from the new one, NULL otherwise.
    Value* setValue(Value* v);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;

private:

    /// @brief The value of the aggregate alternative.
    Value* _value;

    // K: disabled.
    AggregateAlt(const AggregateAlt &);
    AggregateAlt & operator =(const AggregateAlt &);

};

} // namespace hif

#endif

