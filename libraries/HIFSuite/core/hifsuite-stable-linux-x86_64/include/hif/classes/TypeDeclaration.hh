// TypeDeclaration.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_TYPEDECLOBJECT_HXX
#define HIF_TYPEDECLOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "Scope.hh"

namespace hif {

/// @brief Type declaration.
///
/// This class is a base class for type declarations (i.e., type definitions
/// and type template parameters).
///
/// @see TypeDef, TypeTP

class HIF_EXPORT TypeDeclaration: public Scope
{

public:

    /// @brief Constructor.
    TypeDeclaration();

    /// @brief Destructor.
    virtual ~TypeDeclaration();

    /// @brief Returns the type declared.
    /// @return The type declared.
    Type * getType() const;

    /// @brief Sets the type declared.
    /// @param t The type to be set.
    /// @return The old type declared.
    Type * setType(Type * t);

protected:

    /// @brief The type declared.
    Type * _type;

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    // K: disabled
    TypeDeclaration(const TypeDeclaration &);
    TypeDeclaration & operator =(const TypeDeclaration &);
};

} // namespace hif

#endif
