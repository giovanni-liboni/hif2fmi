// Value.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_VALUEOBJECT_HXX
#define HIF_VALUEOBJECT_HXX

/// LOCAL INCLUDES
///
#include "../applicationUtils/portability.hh"
#include "TypedObject.hh"

namespace hif {

/// @brief Abstract class for values.
///
/// This class is an abstract class for values.

class HIF_EXPORT Value: public TypedObject
{

public:

    /// @brief Constructor.
    Value();

    /// @brief Destructor.
    virtual ~Value();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled.
    Value(const Value &);
    Value & operator =(const Value &);

};

} // namespace hif

#endif

