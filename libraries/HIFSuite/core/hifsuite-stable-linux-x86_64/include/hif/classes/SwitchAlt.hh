// SwitchAlt.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_SWITCHALTOBJECT_HXX
#define HIF_SWITCHALTOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Alt.hh"

namespace hif {

/// @brief Switch statement alternative.
///
/// This class represents an alternative for a switch statement
/// (i.e., a switch case).
/// It consists of a list of conditions and a list of actions to be executed
/// when one of the conditions is met.
///
/// @see Switch

class HIF_EXPORT SwitchAlt: public Alt
{
public:
    /// @brief List of conditions.
    BList <Value> conditions;

    /// @brief List of actions to be executed if one of the conditions is met. 
    BList <Action> actions;

    /// @brief Constructor.
    SwitchAlt();

    /// @brief Destructor.
    virtual ~SwitchAlt();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;
};

} // namespace hif

#endif
