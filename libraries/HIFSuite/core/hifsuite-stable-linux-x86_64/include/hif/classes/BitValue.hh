// BitValue.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_BITVALOBJECT_HXX
#define HIF_BITVALOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "ConstValue.hh"

namespace hif {

/// @brief Bit value.
///
/// This class represents a value of bit type.
/// By default this value is assigned to '0'.

class HIF_EXPORT BitValue: public ConstValue
{
public:

    /// @brief Constructor.
    BitValue();

    /// @brief Constructor.
    /// @param b The bit value to be assigned. Default is bit_zero.
    BitValue(const BitConstant b);

    /// @brief Destructor.
    virtual ~BitValue();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the bit value.
    /// @return The bit value.
    BitConstant getValue() const;

    /// @brief Sets the bit value.
    /// @param b The bit value to be set.
    void setValue(const BitConstant b);

    /// @brief Sets the bit value.
    /// @param b Character representing the new bit value.
    void setValue(const char b);

    /// @brief Returns the bit value as a string.
    /// @return The bit value as a string.
    std::string toString() const;

    /// @brief Returns true if contains 0 or 1.
    /// @return true if contains 0 or 1.
    bool is01() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief The actual bit value.
    BitConstant _value;

};

} // namespace hif

#endif

