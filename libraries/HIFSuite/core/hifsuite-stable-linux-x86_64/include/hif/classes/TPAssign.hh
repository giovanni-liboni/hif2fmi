// TPAssign.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_TPASSIGNOBJECT_HXX
#define HIF_TPASSIGNOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "ReferencedAssign.hh"

namespace hif {

/// @brief Template parameter assignment.
///
/// This class represents a template parameter assignment (i.e., a template
/// argument/actual parameter).
///
/// @see ReferencedAssign, TypeTP, ValueTP

class HIF_EXPORT TPAssign: public ReferencedAssign
{
public:

    /// @brief Constructor.
    TPAssign();

    /// @brief Destructor.
    virtual ~TPAssign();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    TPAssign(const TPAssign &);
    TPAssign & operator =(const TPAssign &);
};

} // namespace hif

#endif
