// DesignUnit.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_DUOBJECT_HXX
#define HIF_DUOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Scope.hh"

namespace hif {

/// @brief Design unit.
/// 
/// This class represents a design unit (i.e., a design unit in VHDL, a module
/// in Verilog and SystemC, a class C++).
/// It is possible to define several views (i.e., implementations of the design
/// unit) for a design unit in a HIF description.
///
/// @see View

class HIF_EXPORT DesignUnit: public Scope
{

public:

    /// @brief List of views of the design unit.
    BList <View> views;

    /// @brief Constructor.
    /// The name of the design unit is set to <tt>NAME_NONE</tt>.
    DesignUnit();

    /// @brief Destructor.
    virtual ~DesignUnit();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;
};

} // namespace hif

#endif

