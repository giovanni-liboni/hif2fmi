// FieldReference

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_FIELDREFOBJECT_HXX
#define HIF_FIELDREFOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "PrefixedReference.hh"
#include "../features/ISymbol.hh"
#include "../features/INamedObject.hh"

namespace hif {

/// @brief Field reference (i.e., access to a field).
///
/// This class represents a field reference, which is an access to a field
/// of a data structure (a Record) or of a class variable (in C++).
///
/// @see Field 

class HIF_EXPORT FieldReference:
    public PrefixedReference,
    public features::TemplateSymbolIf<Declaration>,
    public features::INamedObject{

public:

    /// @brief Constructor.
    FieldReference();

    /// @brief Destructor.
    virtual ~FieldReference();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Checks whether given declaration is fine for a FieldReference.
    /// In fact, FieldReference DeclarationType is too generic, due to
    /// DataDeclaration and TypeDef.
    /// E.g. we exclude SubPrograms.
    /// @param d The declaration to be checked.
    /// @return True if given declaration is not fine for a FieldReference.
    static
    bool isNotAllowedDeclaration(Declaration * d);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    // K: disabled.
    FieldReference(const FieldReference &);
    FieldReference & operator =(const FieldReference &);
};

} // namespace hif

#endif
