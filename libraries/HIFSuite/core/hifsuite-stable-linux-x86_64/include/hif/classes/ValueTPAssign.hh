// ValueTPAssign.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_VALUETPASSIGNOBJECT_HXX
#define HIF_VALUETPASSIGNOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "TPAssign.hh"
#include "../features/ISymbol.hh"

namespace hif {

/// @brief Value template parameter assignment.
///
/// This class represents a value template parameter assignment (i.e., a
/// value template argument/actual parameter).
///
/// @see ValueTP

class HIF_EXPORT ValueTPAssign :
    public TPAssign,
    public features::TemplateSymbolIf<ValueTP>
{

public:

    /// @brief Constructor.
    ValueTPAssign();

    /// @brief Destructor.
    virtual ~ValueTPAssign();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Sets the value assigned to the value template parameter.
    /// @param v The value to be assigned to the value template parameter.
    /// @return The value previously assigned to the value template parameter.
    Value* setValue(Value* v);

    /// @brief Returns the value assigned to the value template paramter.
    /// @return The value of the template parameter.
    Value* getValue() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

    /// @brief Returns this object as hif::Object.
    /// @return This object as hif::Object.
    virtual Object * toObject();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    /// @brief The value assigned to the value template parameter.
    Value* _value;

    // K: disabled
    ValueTPAssign(const ValueTPAssign &);
    ValueTPAssign & operator =(const ValueTPAssign &);
};

} // namespace hif

#endif

