// Generate

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_GENERATEOBJECT_HXX
#define HIF_GENERATEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "BaseContents.hh"

namespace hif {

/// @brief Base class for generators.
///
/// This class is the base class for conditional generators (IfGenerate) and
/// iterative generators (ForGenerate).
///
/// @see IfGenerate, ForGenerate

class HIF_EXPORT Generate: public BaseContents
{

public:

    /// @brief Constructor.
    Generate();

    /// @brief Destructor.
    virtual ~Generate();

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

};

} // namespace hif

#endif

