///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_FORWARDS_HH
#define HIF_FORWARDS_HH

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"

namespace hif {

class HIF_EXPORT Action;
class HIF_EXPORT Aggregate;
class HIF_EXPORT AggregateAlt;
class HIF_EXPORT Alias;
class HIF_EXPORT Alt;
class HIF_EXPORT Array;
class HIF_EXPORT Assign;
template <class T> class HIF_EXPORT BList;
class HIF_EXPORT BListHost;
class HIF_EXPORT BaseContents;
class HIF_EXPORT Bit;
class HIF_EXPORT BitValue;
class HIF_EXPORT Bitvector;
class HIF_EXPORT BitvectorValue;
class HIF_EXPORT Bool;
class HIF_EXPORT BoolValue;
class HIF_EXPORT Break;
class HIF_EXPORT Cast;
class HIF_EXPORT Char;
class HIF_EXPORT CharValue;
class HIF_EXPORT CompositeType;
class HIF_EXPORT Const;
class HIF_EXPORT ConstValue;
class HIF_EXPORT Contents;
class HIF_EXPORT Continue;
class HIF_EXPORT DataDeclaration;
class HIF_EXPORT Declaration;
class HIF_EXPORT DesignUnit;
class HIF_EXPORT Entity;
class HIF_EXPORT Enum;
class HIF_EXPORT EnumValue;
class HIF_EXPORT Event;
class HIF_EXPORT Expression;
class HIF_EXPORT Field;
class HIF_EXPORT FieldReference;
class HIF_EXPORT File;
class HIF_EXPORT For;
class HIF_EXPORT ForGenerate;
class HIF_EXPORT Function;
class HIF_EXPORT FunctionCall;
class HIF_EXPORT Generate;
class HIF_EXPORT GlobalAction;
class HIF_EXPORT Identifier;
class HIF_EXPORT If;
class HIF_EXPORT IfAlt;
class HIF_EXPORT IfGenerate;
class HIF_EXPORT Instance;
class HIF_EXPORT Int;
class HIF_EXPORT IntValue;
class HIF_EXPORT Library;
class HIF_EXPORT LibraryDef;
class HIF_EXPORT Member;
class HIF_EXPORT Null;
class HIF_EXPORT Transition;
class HIF_EXPORT Object;
class HIF_EXPORT PPAssign;
class HIF_EXPORT Parameter;
class HIF_EXPORT ParameterAssign;
class HIF_EXPORT Pointer;
class HIF_EXPORT Port;
class HIF_EXPORT PortAssign;
class HIF_EXPORT PrefixedReference;
class HIF_EXPORT Procedure;
class HIF_EXPORT ProcedureCall;
class HIF_EXPORT Range;
class HIF_EXPORT Real;
class HIF_EXPORT RealValue;
class HIF_EXPORT Record;
class HIF_EXPORT RecordValue;
class HIF_EXPORT RecordValueAlt;
class HIF_EXPORT Reference;
class HIF_EXPORT ReferencedAssign;
class HIF_EXPORT ReferencedType;
class HIF_EXPORT Return;
class HIF_EXPORT Scope;
class HIF_EXPORT ScopedType;
class HIF_EXPORT Signal;
class HIF_EXPORT Signed;
class HIF_EXPORT SimpleType;
class HIF_EXPORT Slice;
class HIF_EXPORT State;
class HIF_EXPORT StateTable;
class HIF_EXPORT String;
class HIF_EXPORT SubProgram;
class HIF_EXPORT Switch;
class HIF_EXPORT SwitchAlt;
class HIF_EXPORT System;
class HIF_EXPORT TPAssign;
class HIF_EXPORT StringValue;
class HIF_EXPORT Time;
class HIF_EXPORT TimeValue;
class HIF_EXPORT Type;
class HIF_EXPORT TypeDeclaration;
class HIF_EXPORT TypeDef;
class HIF_EXPORT TypeReference;
class HIF_EXPORT TypeTP;
class HIF_EXPORT TypeTPAssign;
class HIF_EXPORT TypedObject;
class HIF_EXPORT Unsigned;
class HIF_EXPORT Value;
class HIF_EXPORT ValueStatement;
class HIF_EXPORT ValueTP;
class HIF_EXPORT ValueTPAssign;
class HIF_EXPORT Variable;
class HIF_EXPORT View;
class HIF_EXPORT ViewReference;
class HIF_EXPORT Wait;
class HIF_EXPORT When;
class HIF_EXPORT WhenAlt;
class HIF_EXPORT While;
class HIF_EXPORT With;
class HIF_EXPORT WithAlt;
} // namespace hif

#endif

