///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_CONST_VALUE_OBJECT_HXX
#define HIF_CONST_VALUE_OBJECT_HXX

#include "Value.hh"

namespace hif {

/// @brief Base class for constant values.
///
/// This class represents a base class for constant values.

class HIF_EXPORT ConstValue: public Value
{
public:

    /// Destructor.
    virtual ~ConstValue() = 0;

    /// @brief Sets the syntactic type of the constant value.
    /// @param t The syntactic type of the constant value.
    Type * setType(Type *t);

    /// @brief Returns the syntactic type of the constant value.
    /// @return The syntactic type of the constant value.
    Type * getType() const;

protected:

    /// Disabled constructor.
    ConstValue();

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given child w.r.t. this.
    virtual std::string _getFieldName(const Object * child) const;

private:

    /// The syntactic type of the constant value.
    Type * _type;

    /// @name Disabled.
    //@{

    ConstValue(ConstValue &);

    ConstValue & operator =(ConstValue &);

    //@}

};

} // namespace hif

#endif
