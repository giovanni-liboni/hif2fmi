// Entity.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_IFACEOBJECT_HXX
#define HIF_IFACEOBJECT_HXX

// HIF INCLUDES
//
#include "../applicationUtils/portability.hh"
#include "forwards.hh"
#include "Scope.hh"

namespace hif {

/// @brief Entity of a view.
///
/// This class represents the entity of a view (i.e., the definition of its
/// interface in terms of RTL ports and parameters).

class HIF_EXPORT Entity: public Scope
{
public:

    /// @brief List of parameters defined in the interface.
    BList <Parameter> parameters;

    /// @brief List of of ports defined in the interface.
    BList <Port> ports;

    /// @brief Constructor.
    Entity();

    /// @brief Destructor.
    virtual ~Entity();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

    /// @brief Returns the name of given BList w.r.t. this.
    virtual std::string _getBListName(const BList<Object> & list) const;
};

} // namespace hif

#endif

