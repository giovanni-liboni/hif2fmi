// IntValue.hh

//
// Copyright (C) 2009
// EDALab S.r.l.

// This file is part of HIF library
//
// COPYRIGHT
//
//

///
/// @file
/// Copyright (C) 2009
///
/// EDALab S.r.l.
///
/// COPYRIGHT
///
/// @author <a href="mailto:support.hifsuite@edalab.it">HIF Support Team</a>
/// @version 1.0
///

#ifndef HIF_INTVALOBJECT_HXX
#define HIF_INTVALOBJECT_HXX

/// LOCAL INCLUDES
///
#include "../applicationUtils/portability.hh"
#include "ConstValue.hh"

namespace hif {

/// @brief Integer value.
///
/// This class represents a value of integer type.
/// By default this value is assigned to 0.

class HIF_EXPORT IntValue: public ConstValue
{

public:

    /// @brief Constructor.
    IntValue();

    /// @brief Constructor.
    /// @param v The integer value to be assigned. Default is 0.
    IntValue(const long long int v);

    /// @brief Destructor.
    virtual ~IntValue();

    /// @brief Returns a string representing the class name.
    /// @return The string representing the class name.
    ClassId getClassId() const;

    /// @brief Returns the integer value.
    /// @return The integer value.
    long long int getValue() const;

    /// @brief Sets the integer value.
    /// @param a The integer value to be set.
    void setValue(const long long int a);

    /// @brief Accepts a visitor to visit the current object.
    /// @param vis The visitor.
    /// @return Integer representing the result of the visit. Default value is 0.
    virtual int acceptVisitor(HifVisitor & vis);

protected:

    /// @brief Fills the internal fields and blists lists.
    virtual void _calculateFields();

private:

    /// @brief The actual integer value.
    long long int _value;

};

} // namespace hif

#endif

