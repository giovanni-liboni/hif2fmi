#ifndef HIF_MANIPULATION_COLLECTASSIGNTARGETSYMBOLS_HH
#define HIF_MANIPULATION_COLLECTASSIGNTARGETSYMBOLS_HH

#include "../classes/classes.hh"
#include "assignUtils.hh"

namespace hif { namespace manipulation {

/// @brief Returns all symbols which are targets of a given assignment.
/// For example: <tt>(a[i], b) = c</tt> will collect <tt>a</tt> and <tt>b</tt>.
///
/// @param obj The assignment for which symbols are to be collected.
/// @param opt The LeftHandSideOptions used in check.
/// @return The list of collected symbols.
///
HIF_EXPORT
std::list<Value *> collectLeftHandSideSymbols(
        Assign * obj,
        const LeftHandSideOptions & opt = LeftHandSideOptions());

}} // end hif::manipulation

#endif
