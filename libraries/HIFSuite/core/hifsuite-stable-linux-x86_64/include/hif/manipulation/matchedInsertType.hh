#ifndef HIF_MATCHED_INSERT_TYPE
#define HIF_MATCHED_INSERT_TYPE

#include "../applicationUtils/portability.hh"

namespace hif { namespace manipulation {

struct HIF_EXPORT MatchedInsertType
{
    enum type
    {
        TYPE_DELETE,         ///< @brief Deletes potentially existing objects.
        TYPE_ERROR,          ///< @brief Raise error if an object already exists.
        TYPE_ONLY_REPLACE,   ///< @brief Replaces without deleting potentially existing objects.
        TYPE_EXPAND          ///< @brief If the object is inside a BList, insert in the desired position and shift following elements in the list.
    };
};

}}

#endif // HIF_MATCHED_INSERT_TYPE
