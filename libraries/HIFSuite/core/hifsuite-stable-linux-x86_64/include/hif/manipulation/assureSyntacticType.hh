#ifndef HIF_MANIPULATION_ASSURESYNTACTICTYPE_HH
#define HIF_MANIPULATION_ASSURESYNTACTICTYPE_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Checks whether the passed value needs a syntactic type with
/// respect to its parent.
/// @param v The value to be checked.
/// @return <tt>true</tt> if @p v needs a syntactic type, or it does
/// not have a parent.
HIF_EXPORT
bool needSyntacticType(Value * v);


/// @brief Checks whether the passed value is a ConstValue, and in that
/// case it sets the syntactic type, with this priority:
/// * if already present, do nothing;
/// * if passed as @p suggestedType, set it;
/// * otherwise, retrieve the type from semantics @p sem.
/// @param v The value to be checked.
/// @param sem The reference semantics.
/// @param suggestedType The type to set, if specified.
/// Otherwise, retrieve it from semantics @p sem.
/// @return The checked value.
///
HIF_EXPORT
Value * assureSyntacticType(Value * v,
                            hif::semantics::ILanguageSemantics * sem,
                            Type * suggestedType = NULL);

}} // end hif::manipulation

#endif
