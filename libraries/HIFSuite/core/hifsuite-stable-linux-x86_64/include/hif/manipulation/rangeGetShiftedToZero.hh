#ifndef HIF_MANIPULATION_RANGEGETSHIFTEDTOZERO_HH
#define HIF_MANIPULATION_RANGEGETSHIFTEDTOZERO_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Shifts a span to zero by preserving the direction.
///
/// @param range The input span.
/// @param refLang The reference semantics.
/// @return The span shifted to zero.
///
HIF_EXPORT
Range * rangeGetShiftedToZero(
        Range * range,
        hif::semantics::ILanguageSemantics * refLang);

}} // end Hif::Manipulation

#endif
