#ifndef HIF_MANIPULATION_TRANSFORMIFTOWHEN_HH
#define HIF_MANIPULATION_TRANSFORMIFTOWHEN_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Transforms an If object into a corresponding When object.
/// @warning It currently works only if branches contain single return statements.
/// @param ifo The starting if object.
/// @param sem The reference semantics.
/// @return The corresponding When, or NULL if unable to translate.
///
HIF_EXPORT
When * transformIfToWhen( If * ifo,
                          hif::semantics::ILanguageSemantics* sem );

}} // end hif::manipulation

#endif
