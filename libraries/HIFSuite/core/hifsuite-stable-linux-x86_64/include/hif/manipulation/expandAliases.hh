#ifndef HIF_MANIPULATION_EXPANDALIASES_HH
#define HIF_MANIPULATION_EXPANDALIASES_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// Starting from @p root, expands all aliases by replacing them with
/// their corresponding values.
/// @param root The starting object from which to start the code manipulation.
/// @param refLang The reference semantics.
///
HIF_EXPORT
void expandAliases(Object * root, hif::semantics::ILanguageSemantics * refLang);

}} // end hif::manipulation

#endif
