#ifndef HIF_MANIPULATION_FINDTOPLEVELMODULE_HH
#define HIF_MANIPULATION_FINDTOPLEVELMODULE_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

typedef std::set<View*> ViewSet;

/// @brief findTopLevelModule() options.
struct HIF_EXPORT FindTopOptions
{
    /// @brief If <tt>true</tt>, print extra messages.
    /// Default is <tt>false</tt>
    bool verbose;
    /// @brief The name of the top-level design unit. If method founds only
    /// one top level name with different name, is rised an error.
    std::string topLevelName; // TODO use getHerarchical name.
    /// @brief The map storing pointers to sub-module views, which can be use to
    /// optimize performance if findViewDependencies() has already been called.
    std::map<hif::View*, std::set<hif::View*> >* smm;
    /// @brief The map storing pointers to parent-module views, which can be use to
    /// optimize performance if findViewDependencies() has already been called.
    std::map<hif::View*, std::set<hif::View*> >* pmm;
    /// @brief If <tt>true</tt> and are found more than one top-level design unit,
    /// method tries to determinate it using heuristics.
    /// Default is <tt>false</tt>
    bool useHeuristics;
    /// If <tt>true</tt> and are found more than one top-level design unit,
    /// is raised an error.
    /// Default is <tt>false</tt>
    bool checkAtMostOne;
    /// If <tt>true</tt> and there are not found any top-level design unit,
    /// is raised an error.
    /// Default is <tt>false</tt>
    bool checkAtLeastOne;

    FindTopOptions();
    ~FindTopOptions();

    FindTopOptions(const FindTopOptions &);
    FindTopOptions & operator =(const FindTopOptions &);
};


/// @brief Visit the given system description to determine the top-level views
/// w.r.t given options.
///
/// @param root The system object.
/// @param sem The reference semantics.
/// @param opt The options.
/// @return The top-level view, if found. Otherwise return NULL.
///
HIF_EXPORT
ViewSet findTopLevelModules(hif::System* root,
                            hif::semantics::ILanguageSemantics * sem,
                            const FindTopOptions & opt = FindTopOptions());

/// @brief Visit the given system description to determine the top-level view
///  (the one actually used by the top-level design unit) w.r.t given options.
///
/// @param root The system object.
/// @param sem The reference semantics.
/// @param opt The options.
/// @return The top-level view, if found. Otherwise return NULL.
///
HIF_EXPORT
View * findTopLevelModule(hif::System* root,
                          hif::semantics::ILanguageSemantics * sem,
                          const FindTopOptions & opt = FindTopOptions());

}} // end hif::manipulation

#endif
