#ifndef HIF_MANIPULATION_SYSTEMCMANIPULATION_HH
#define HIF_MANIPULATION_SYSTEMCMANIPULATION_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Starting from @p root, replaces all references to VHDL attribute
/// <tt>last_value</tt> with corresponding code for SystemC.
/// @warning This function MUST operate on trees having HIF semantics.
///
/// @param root The root node.
/// @param replaceRisingFallingEdge <tt>true</tt> to replace references
/// to <tt>last_value</tt> within rising and falling edges. Default is <tt>true</tt>.
/// @return <tt>true</tt> if at least one <tt>last_value</tt> reference was found.
///
HIF_EXPORT
bool mapLastValueToSystemC(Object * root,
                           const bool replaceRisingFallingEdge = true);

/// @brief Creates and adds the SystemC module constructor, accepting the mandatory
/// module instance name.
/// @param view The module.
/// @param sem The semantics.
/// @return <tt>true</tt> if added.
HIF_EXPORT
bool addModuleConstructor(View * view, hif::semantics::ILanguageSemantics * sem);

}} // end hif::manipulation

#endif
