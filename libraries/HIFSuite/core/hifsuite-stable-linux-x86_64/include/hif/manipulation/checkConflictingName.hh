#ifndef HIF_MANIPULATION_CHECKCONFLICTINGNAME_HH
#define HIF_MANIPULATION_CHECKCONFLICTINGNAME_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Checks whether the given name matches a declaration into given scope.
/// @param n The name to be checked.
/// @param scope The scope where to perform the check.
/// @return <tt>true</tt> if the name is conflicting, <tt>false</tt> otherwise.
///
HIF_EXPORT
bool checkConflictingName(Name n, Scope * scope);

}} // end hif::manipulation

#endif
