///
/// @file
/// Copyright (C) 2008
///
/// COPYRIGHT
///

#ifndef HIF_MANIPULATION_HH_
#define HIF_MANIPULATION_HH_

// ///////////////////////////////////////////////////////////////////
// Main manipulation methods.
// ///////////////////////////////////////////////////////////////////

#include "renameConflictingDeclarations.hh"
#include "moveDeclaration.hh"
#include "mergeTrees.hh"
#include "renameInScope.hh"
#include "moveToScope.hh"
#include "mapToNative.hh"
#include "aggregateUtils.hh"
#include "transformSwitchToIf.hh"
#include "transformIfToWhen.hh"
#include "transformWhenToIf.hh"
#include "bindOpenPortAssigns.hh"
#include "fixInstanceBindings.hh"
#include "propagateSymbols.hh"
#include "removeUnusedDeclarations.hh"
#include "resolveHierarchicalSymbol.hh"
#include "buildHierarchicalSymbol.hh"
#include "instanceUtils.hh"
#include "simplify.hh"
#include "transformConstant.hh"
#include "transformGlobalActions.hh"
#include "sortParameters.hh"
#include "assignUtils.hh"
#include "collectLeftHandSideSymbols.hh"
#include "fixNestedDeclarations.hh"
#include "matchObject.hh"
#include "matchedInsert.hh"
#include "matchPatternInTree.hh"
#include "matchedGet.hh"
#include "matchTrees.hh"
#include "addDeclarationInContext.hh"
#include "addUniqueObject.hh"
#include "replaceOccurrencesInScope.hh"
#include "findViewDependencies.hh"
#include "findScopeDependencies.hh"
#include "findTopLevelModule.hh"
#include "expandAliases.hh"
#include "expandGenerates.hh"
#include "checkConflictingName.hh"
#include "rebaseTypeSpan.hh"
#include "fixRangesDirection.hh"
#include "fixMultipleSignalPortAssigns.hh"
#include "fixTemplateParameters.hh"
#include "resolveTemplates.hh"
#include "sort.hh"
#include "assureSyntacticType.hh"
#include "narrowToCardinality.hh"
#include "splitConcatTargets.hh"
#include "instanceUtils.hh"
#include "renameForbiddenNames.hh"
#include "transformSpanToRange.hh"
#include "transformRangeToSpan.hh"
#include "rangeGetIncremented.hh"
#include "rangeGetShiftedToZero.hh"
#include "explicitCallsParameters.hh"
#include "flattenDesign.hh"
#include "fixAssignments.hh"
#include "prefixTree.hh"
#include "createConcatFromSpans.hh"
#include "fixUnsupportedBits.hh"
#include "removeStandardMethods.hh"

// ///////////////////////////////////////////////////////////////////
// Semantics specific methods.
// ///////////////////////////////////////////////////////////////////

#include "systemCManipulation.hh"


namespace hif {

/// @brief Wraps methods to manipulate trees.
namespace manipulation { }

} // hif

#endif /* HIF_MANIPULATION_HH_ */
