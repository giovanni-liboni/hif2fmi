#ifndef HIF_MANIPULATION_TRANSFORMSWITCHTOIF_HH
#define HIF_MANIPULATION_TRANSFORMSWITCHTOIF_HH

#include "../classes/classes.hh"
#include "../semantics/HIFSemantics.hh"

namespace hif { namespace manipulation {

struct HIF_EXPORT TransformCaseOptions
{
    TransformCaseOptions();
    ~TransformCaseOptions();
    TransformCaseOptions(const TransformCaseOptions & other);
    TransformCaseOptions & operator =(TransformCaseOptions other);
    void swap(TransformCaseOptions & other);

    /// @brief True when condition is moved into a support variable. Default is false.
    bool fixCondition;
    /// @brief True when condition referred to signal or port must introduce support variable. Default is false.
    /// NOTE: works only when fixCondition holds.
    bool fixSignalOrPortCondition;
    /// @brief True when cases with multiple choices must be splitted into different
    /// if alts. Default is false.
    bool splitCases;
    /// @brief True when result object must be simplified. Default is true.
    bool simplify;
    /// @brief The potential created support condition variable.
    Variable * createdVariable;
    /// @brief The potential created support condition variable assign.
    Assign * createdAssign;
};

/// @brief Transforms a Switch object into a corresponding If object.
/// It is safe with respect to side effects on the switch condition.
/// @warning The Switch object must be attached to a Hif tree.
///
/// @param s The starting switch.
/// @param opt The transform case options.
/// @param sem The reference semantics.
/// @return The corresponding If.
///
HIF_EXPORT
If * transformSwitchToIf(
        Switch * s,
        const TransformCaseOptions & opt = TransformCaseOptions(),
        hif::semantics::ILanguageSemantics* sem = hif::semantics::HIFSemantics::getInstance());

/// @brief Transforms a With object into a corresponding When object.
/// It is safe with respect to side effects on the with condition.
/// @warning The With object must be attached to a Hif tree.
///
/// @param w The starting with.
/// @param opt The transform case options.
/// @param sem The reference semantics.
/// @return The corresponding When.
///
HIF_EXPORT
When * transformWithToWhen(
        With * w,
        const TransformCaseOptions & opt = TransformCaseOptions(),
        hif::semantics::ILanguageSemantics* sem = hif::semantics::HIFSemantics::getInstance());

}} // end hif::manipulation

#endif
