#ifndef HIF_MANIPULATION_INSTANCEUTILS_HH
#define HIF_MANIPULATION_INSTANCEUTILS_HH

#include "../classes/classes.hh"
#include "../semantics/HIFSemantics.hh"

namespace hif { namespace manipulation {

///
/// @name Instantiation-stuff: methods to instantiate objects.
/// @warning All returned objects are by aliasing. Thus, if you need to manipulate them,
/// you must make a copy.
/// @{

/// @brief Flushes the cache used to optimize performance with instantiations.
HIF_EXPORT
void flushInstanceCache();


struct HIF_EXPORT InstantiateOptions
{
    InstantiateOptions();
    ~InstantiateOptions();

    InstantiateOptions (const InstantiateOptions &);
    InstantiateOptions & operator = (const InstantiateOptions &);

    /// @brief Identify if replace the result. Default is false;
    bool replace;

    /// @brief Identify if instantiate only the signature. Works only with
    /// function calls, procedure call, valueTPAssigns and parameter assigns.
    /// If true intantiated object is not added to the cache. Default is false.
    bool onlySignature;

    /// @brief If set identify eventual candidate for method call.
    SubProgram * candidate;
};


/// @brief Returns the instantiated viewref view, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param instance The view reference to instantiated.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated view.
///
HIF_EXPORT
View * instantiate(ViewReference* instance,
                    hif::semantics::ILanguageSemantics* ref_sem =
                    hif::semantics::HIFSemantics::getInstance(),
                    const InstantiateOptions & opt = InstantiateOptions() );


/// @brief Returns the instantiated call signature, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param originalCall The call to analyze.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated signature.
///
HIF_EXPORT
SubProgram * instantiate(ProcedureCall* originalCall,
                          hif::semantics::ILanguageSemantics* ref_sem =
                          hif::semantics::HIFSemantics::getInstance(),
                          const InstantiateOptions & opt = InstantiateOptions());


/// @brief Returns the instantiated call signature, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param originalCall The call to analyze.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated signature.
///
HIF_EXPORT
Function * instantiate(FunctionCall* originalCall,
                        hif::semantics::ILanguageSemantics* ref_sem =
                        hif::semantics::HIFSemantics::getInstance(),
                        const InstantiateOptions & opt = InstantiateOptions());


/// @brief Returns the instantiated typeref typedef, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param instance The type reference to be instantiated.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated typedef.
///
HIF_EXPORT
TypeReference::DeclarationType * instantiate(TypeReference* instance,
                                              hif::semantics::ILanguageSemantics* ref_sem =
                                              hif::semantics::HIFSemantics::getInstance(),
                                              const InstantiateOptions & opt = InstantiateOptions() );


/// @brief Returns the instantiated portassign port, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param instance The port assignment to be instantiated.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated typedef.
///
HIF_EXPORT
Port * instantiate(PortAssign* instance,
                    hif::semantics::ILanguageSemantics* ref_sem =
                    hif::semantics::HIFSemantics::getInstance(),
                    const InstantiateOptions & opt = InstantiateOptions() );


/// @brief Returns the instantiated parameter assign parameter, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param instance The parameter assign to be instantiated.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated parameter.
///
HIF_EXPORT
Parameter * instantiate(ParameterAssign* instance,
                     hif::semantics::ILanguageSemantics* ref_sem =
                     hif::semantics::HIFSemantics::getInstance(),
                     const InstantiateOptions & opt = InstantiateOptions() );


/// @brief Returns the instantiated fieldref view, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param instance The field reference to be instantiated.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated view.
///
HIF_EXPORT
FieldReference::DeclarationType * instantiate(FieldReference* instance,
                                               hif::semantics::ILanguageSemantics* ref_sem =
                                               hif::semantics::HIFSemantics::getInstance(),
                                               const InstantiateOptions & opt = InstantiateOptions() );


/// @brief Returns the instantiated instance entity, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param instance The instance to be instantiated.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated entity.
///
HIF_EXPORT
Instance::DeclarationType * instantiate(Instance* instance,
                                         hif::semantics::ILanguageSemantics* ref_sem =
                                         hif::semantics::HIFSemantics::getInstance(),
                                         const InstantiateOptions & opt = InstantiateOptions() );


/// @brief Returns the instantiated ValueTP, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param instance The ValueTPAssign to be instantiated.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated ValueTP.
///
HIF_EXPORT
ValueTPAssign::DeclarationType * instantiate(ValueTPAssign* instance,
                                              hif::semantics::ILanguageSemantics* ref_sem =
                                              hif::semantics::HIFSemantics::getInstance(),
                                              const InstantiateOptions & opt = InstantiateOptions());


/// @brief Returns the instantiated TypeTP, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param instance The TypeTPAssign to be instantiated.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated TypeTP.
///
HIF_EXPORT
TypeTPAssign::DeclarationType * instantiate(TypeTPAssign* instance,
                                             hif::semantics::ILanguageSemantics* ref_sem =
                                             hif::semantics::HIFSemantics::getInstance(),
                                             const InstantiateOptions & opt = InstantiateOptions() );


/// @brief Returns the instantiated ReferencedType, w.r.t. the given
/// language semantics.
/// It replaces the template parameters with their actual instantiation.
///
/// @param instance The ReferencedType to be instantiated.
/// @param ref_sem The semantics to be used.
/// @param opt The options.
/// @return The instantiated ReferencedType.
///
HIF_EXPORT
Scope * instantiate(ReferencedType* instance,
                     hif::semantics::ILanguageSemantics* ref_sem =
                     hif::semantics::HIFSemantics::getInstance(),
                     const InstantiateOptions & opt = InstantiateOptions() );


/// @brief Checks whether a given object is stored in the instance cache.
/// @param obj The object to look for.
/// @return <tt>true</tt> if the object is stored in cache, <tt>false</tt> otherwise.
HIF_EXPORT
bool isInCache( Object * obj );


/// @brief Adds given declaration in a trash which will be cleared by flushInstanceCache().
/// @param o The declaration to insert.
HIF_EXPORT
void addInCache(Declaration * o);

///
/// @}
///


}} // end hif::manipulation

#endif
