#ifndef HIF_MANIPULATION_EXPANDGENERATES_HH
#define HIF_MANIPULATION_EXPANDGENERATES_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// Starting from @p root, expands all generate constructs.
/// @param root The starting object from which to start the code manipulation.
/// @param refLang The reference semantics.
/// @return <tt>true</tt> if at least one expansion has been performed.
///
HIF_EXPORT
bool expandGenerates(Object * root, hif::semantics::ILanguageSemantics * refLang);

}} // end hif::manipulation

#endif
