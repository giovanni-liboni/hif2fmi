#ifndef HIF_MANIPULATION_PREFIX_TREE_HH
#define HIF_MANIPULATION_PREFIX_TREE_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

struct HIF_EXPORT PrefixTreeOptions
{
    /// @brief If <tt>false</tt> the fix is performed only for root obeject.
    /// Default is <tt>true</tt>.
    bool recursive;

    /// @brief If <tt>false</tt> the fix is performed only if possible without
    /// raise any error for example if a symbol declaration is not found.
    /// Default is <tt>true</tt>.
    bool strictChecks;

    /// @brief If <tt>false</tt> the symbols are prefixed also with their
    /// parent scopes.
    /// Default is <tt>true</tt>.
    bool skipPrefixingIfSameScope;

    PrefixTreeOptions();
    ~PrefixTreeOptions();
    PrefixTreeOptions(const PrefixTreeOptions & o);
    PrefixTreeOptions & operator = (PrefixTreeOptions o);
    void swap(PrefixTreeOptions & o);
};

/// @brief Starting from given <tt>root</tt> perform a prefix of all matched symbols
/// according with <tt>recursive</tt> flag.
/// For example when is matched a ViewReference which declaration is contained
/// inside a LibraryDef, this methods add a the corresponding library instance to the
/// ViewReference.
/// @param root The root object.
/// @param sem The semantics
/// @param opt The PrefixTreeOptions.
HIF_EXPORT
void prefixTree(Object * root,
                hif::semantics::ILanguageSemantics * sem,
                const PrefixTreeOptions & opt = PrefixTreeOptions());

}} // end hif::manipulation

#endif
