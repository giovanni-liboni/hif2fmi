#ifndef HIF_MANIPULATION_NARROWTOCARDINALITY_HH
#define HIF_MANIPULATION_NARROWTOCARDINALITY_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Given a value, gets the subvalue which matches the given cardinality.
/// For instance: <tt>sc_lv<9>b[10]</tt> with <tt>v = b[5][3]</tt> and
/// <tt>c = 1</tt>, returns <tt>b[5]</tt>.
///
/// @param v The value.
/// @param c The cardinality.
/// @param sem The semantics.
/// @param considerOnlyBits <tt>true</tt> if only bit types are to be considered,
///        <tt>false</tt> otherwise
/// @return The subvalue or NULL in case of error.
///
HIF_EXPORT
Value * narrowToCardinality(Value * v,
                            const unsigned int c,
                            hif::semantics::ILanguageSemantics * sem,
                            const bool considerOnlyBits = true);

}} // end hif::manipulation

#endif
