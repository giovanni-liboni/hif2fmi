#ifndef HIF_MANIPULATION_FIXCLOCKEVENT_HH
#define HIF_MANIPULATION_FIXCLOCKEVENT_HH

#include "hifCore.hh"
#include "../classes/classes.hh"
#include "hif/semantics/semantics.hh"

namespace hif { namespace manipulation {


/// @brief Remove 'event and rising_edge calls.
/// @param root The starting object.
/// @param sem Pointer to the reference semantics.
/// @param boolClock Consider clock to the always a boolean.
/// @return <tt>true</tt> if at least one fix has been performed.
HIF_EXPORT
bool fixClockEvent(Object * root,
                   hif::semantics::ILanguageSemantics * sem =
        hif::semantics::HIFSemantics::getInstance(),
                   const bool boolClock = false);


}} // hif::manipulation

#endif // HIF_MANIPULATION_FIXCLOCKEVENT_HH
