#ifndef HIF_MANIPULATION_RENAMEFORBIDDENNAMES_HH
#define HIF_MANIPULATION_RENAMEFORBIDDENNAMES_HH

#include "../classes/classes.hh"
#include "../semantics/ILanguageSemantics.hh"

namespace hif { namespace manipulation {

/// @brief Checks whether a name conflicts with a reserved keyword according
/// to semantics @p sem. If a conflict is found, renaming is performed.
///
/// @param root The subtree in which names are to be checked.
/// @param sem The reference semantics.
///
HIF_EXPORT
void renameForbiddenNames(Object * root, hif::semantics::ILanguageSemantics * sem);

}} // end Hif::Manipulation

#endif
