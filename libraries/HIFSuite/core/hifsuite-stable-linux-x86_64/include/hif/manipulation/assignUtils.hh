#ifndef HIF_MANIPULATION_TARGETUTILS_HH
#define HIF_MANIPULATION_TARGETUTILS_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {


struct HIF_EXPORT LeftHandSideOptions
{
    LeftHandSideOptions();
    ~LeftHandSideOptions();
    LeftHandSideOptions (const LeftHandSideOptions &o);
    LeftHandSideOptions & operator = (const LeftHandSideOptions &o);

    /// @brief The semantics to be used.
    hif::semantics::ILanguageSemantics * sem;

    /// @brief If <tt>True</tt> consider accesses records as targets. Default is false.
    bool considerRecord;
};


/// @brief Returns whether a given object is the target of an assignment
/// (i.e., is contained in the left-hand side of the assignment).
///
/// @param obj The object to be checked.
/// @param opt The options.
/// @return <tt>true</tt> if @p obj is the target of an assignment,
/// <tt>false</tt> otherwise.
///
HIF_EXPORT
bool isInLeftHandSide(
        Object * obj,
        const LeftHandSideOptions & opt = LeftHandSideOptions());


/// @brief Returns the target of an assignment (i.e., the left-hand side
/// of the assignment).
/// @deprecated
///
/// @param obj The object to be checked.
/// @return The target of an assignment.
///
HIF_EXPORT HIF_DEPRECATED("Useless.")
Value * getLeftHandSide(Value * obj);


}} // end hif::manipulation

#endif
