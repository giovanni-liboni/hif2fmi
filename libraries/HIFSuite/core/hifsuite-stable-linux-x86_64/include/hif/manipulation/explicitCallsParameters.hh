#ifndef HIF_MANIPULATION_EXPLICITCALLSPARAMETERS_HH
#define HIF_MANIPULATION_EXPLICITCALLSPARAMETERS_HH

#include "../classes/classes.hh"
#include "../semantics/referencesUtils.hh"
#include "sortParameters.hh"

namespace hif { namespace manipulation {

/// @brief Explicts all parameters of given subprograms refs.
/// @param subprograms The list of subprograms to be fixed.
/// @param refMap The references map.
/// @param sem The semantics.
/// @param kind The explicit kind (@see sortParameters). Default is ALL.
void explicitCallsParameters(
        std::list<SubProgram *> & subprograms,
        hif::semantics::ReferencesMap & refMap,
        hif::semantics::ILanguageSemantics * sem,
        const SortMissingKind::type kind = SortMissingKind::ALL);

/// @brief Explicts all parameters of given subprograms refs.
/// @param subprograms The set of subprograms to be fixed.
/// @param refMap The references map.
/// @param sem The semantics.
/// @param kind The explicit kind (@see sortParameters). Default is ALL.
void explicitCallsParameters(
        std::set<SubProgram *> & subprograms,
        hif::semantics::ReferencesMap & refMap,
        hif::semantics::ILanguageSemantics * sem,
        const SortMissingKind::type kind = SortMissingKind::ALL);

}} // end hif::manipulation

#endif
