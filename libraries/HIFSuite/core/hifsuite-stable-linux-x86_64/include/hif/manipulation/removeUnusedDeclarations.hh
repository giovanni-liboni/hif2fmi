#ifndef HIF_MANIPULATION_REMOVEUNUSEDDECLARATIONS_HH
#define HIF_MANIPULATION_REMOVEUNUSEDDECLARATIONS_HH

#include "../classes/classes.hh"
#include "../semantics/HIFSemantics.hh"

namespace hif { namespace manipulation {

struct HIF_EXPORT RemoveUnusedDeclarationOptions
{
    /// @name Traits
    /// @{

    typedef std::set<Declaration*> DeclarationSet;
    typedef std::set<Object*> ObjectSet;
    typedef std::map< Declaration*, ObjectSet > ReferencesMap;

    /// @}


    /// @name Configuration options.
    /// @{

    /// @brief Skip standard libraryDefs and views. Default false.
    bool skipStandardDefs;
    /// @brief If true removes all unused Declarations. Default true
    bool removeAll;
    /// @brief Removes unused Constants. Default false.
    bool removeConstants;
    /// @brief Removes unused Params. Default false.
    bool removeParams;
    /// @brief Removes unused SubPrograms. Default false.
    bool removeSubPrograms;
    /// @brief Removes unused TypeDefs. Default false.
    bool removeTypeDefs;
    /// @brief Removes unused TypeTPs. Default false.
    bool removeTypeTPs;
    /// @brief Removes unused ValueTPs. Default false.
    bool removeValueTPs;
    /// @brief Removes unused Variables. Default false.
    bool removeVariables;
    /// @brief Removes unused Views. Default false.
    bool removeView;
    /// @brief Removes unused Aliases. Default false.
    bool removeAlias;
    /// @brief Removes unused EnumValues. Default false.
    bool removeEnumValue;
    /// @brief Removes unused Fields. Default false.
    bool removeField;
    /// @brief Removes unused LibraryDefs. Default false.
    bool removeLibraryDef;
    /// @brief Removes unused Ports. Default false.
    bool removePort;
    /// @brief Removes unused Signals. Default false.
    bool removeSignal;
    /// @brief Removes unused StateTables. Default false.
    bool removeStateTable;


    /// @}


    /// @name Optional infos.
    /// @{

    /// @brief Reference to the top module of design.
    View* top;

    /// @brief Optional list of declarations to checks.
    DeclarationSet * declarationsTarget;

    /// @brief Optional map of all references.
    ReferencesMap * allReferencesMap;

    /// @}

    RemoveUnusedDeclarationOptions();
    ~RemoveUnusedDeclarationOptions();

    RemoveUnusedDeclarationOptions(const RemoveUnusedDeclarationOptions &);
    RemoveUnusedDeclarationOptions & operator = (const RemoveUnusedDeclarationOptions &);
};

/// @brief Removes unused declarations from given system <tt>root</tt> w.r.t.
/// given <tt>options</tt>. Method works by analyzing of getAllReferences:
/// declaration is removed only if it has not got any reference in the tree.
///
/// @param root The system root.
/// @param sem The reference semantics.
/// @param options The method options.
/// @return <tt>false</tt> in case of errors, <tt>true</tt> in case of success.
///
HIF_EXPORT
bool removeUnusedDeclarations(System * root,
    hif::semantics::ILanguageSemantics * sem = hif::semantics::HIFSemantics::getInstance(),
        const RemoveUnusedDeclarationOptions & options = RemoveUnusedDeclarationOptions());

}} // end hif::manipulation

#endif
