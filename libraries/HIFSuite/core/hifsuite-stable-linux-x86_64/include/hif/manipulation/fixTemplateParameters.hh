#ifndef HIF_MANIPULATION_FIXTEMPLATEPARAMETERS_HH
#define HIF_MANIPULATION_FIXTEMPLATEPARAMETERS_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

struct HIF_EXPORT FixTemplateOptions
{
    FixTemplateOptions();
    ~FixTemplateOptions();
    FixTemplateOptions(const FixTemplateOptions & o);
    FixTemplateOptions & operator =(FixTemplateOptions o);
    void swap(FixTemplateOptions & o);

    /// @brief useHdtlib If using hdtlib types. Default is false.
    bool useHdtLib;

    /// @brief checkSem The semantics with perform checks.
    hif::semantics::ILanguageSemantics * checkSem;

    /// @brief <tt>True</tt> to set PROPERTY_CONSTEXPR. Default is false.
    bool setConstExpr;

    /// @brief Fix standard library declarations changing their signature.
    /// Should be enabled only in backends. Default is <tt>false</tt>.
    bool fixStandardDeclarations;
};


/// @brief Performs fixes related to template parameters in multiple steps.
/// @param o The system object.
/// @param sem The semantics with witch work.
/// @param opt The options.
HIF_EXPORT
void fixTemplateParameters( hif::System * o,
        hif::semantics::ILanguageSemantics * sem,
        const FixTemplateOptions & opt = FixTemplateOptions() );

}} // end hif::manipulation

#endif

