#ifndef HIF_MANIPULATION_FINDSCOPEDEPENDENCIES_HH
#define HIF_MANIPULATION_FINDSCOPEDEPENDENCIES_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Visits the given object @p root to determine scope dependencies
/// between objects in its subtree. Only DesignUnit, LibraryDef and System
/// are considered.
/// @param root The root of the tree (usually the system object).
/// @param scopeRelations The map storing pointers to scope objects in
/// relation with the list of objects representing their scope to @p root.
///
HIF_EXPORT
void findScopeDependencies(hif::System* root,
                           std::map<hif::Scope*, std::list<hif::Scope*> >& scopeRelations);

}} // end hif::manipulation

#endif
