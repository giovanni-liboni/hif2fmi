#ifndef HIF_MANIPULATION_SORTPARAMETERS_HH
#define HIF_MANIPULATION_SORTPARAMETERS_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

class ILanguageSemantics;

} }

namespace hif { namespace manipulation {


/// @name Parameters sorting and missing parameters adding stuff.
/// @{

/// @brief Tries to get the implicitly assigned object to a template parameter.
///
/// @param tp The template parameter declaration.
/// @param formalParameterType The reference tree for tp references.
/// @param actualParameterType The tree to be matched.
/// @param sem The semantics.
/// @param hasCandidate True if no error must be raised.
/// @return The matched object or NULL.
///
Object * getImplicitTemplate(
        Declaration * tp,
        Object * formalParameterType,
        Object * actualParameterType,
        hif::semantics::ILanguageSemantics * sem,
        const bool hasCandidate
        );


struct HIF_EXPORT SortMissingKind
{
    enum type
    {
        /// @brief Do not add missing parameters.
        NOTHING,
        /// @brief Add all missing parameters.
        ALL,
        /// @brief Add all missing parameters till the last assigned parameter. After that, all parameters will take default values.
        LIMITED
    };
};


/// @brief Sorts actual parameters according to formal names.
/// Parameters sorting is needed since passing of arguments can occur by
/// positional or named binding.
///
/// @param actualParams The list of actual parameters.
/// @param formalParams The list of formal parameters.
/// @param set_formal_names If <tt>true</tt>, sets the corresponding name of
/// formal parameters in actual parameters.
/// @param missingType How to add missing parameters.
/// @param refSem The reference semantics.
/// @param hasCandidate If <tt>true</tt>, relaxes some checks.
/// @return <tt>false</tt> if some actual parameters are suspected to be implicit
/// (thus, they should take default value) but making them explicit fails,
/// <tt>true</tt> if everything goes well.
///
HIF_EXPORT
bool sortParameters( BList< ParameterAssign > & actualParams,
                     BList< Parameter > & formalParams,
                     const bool set_formal_names,
                     const SortMissingKind::type missingType,
                     hif::semantics::ILanguageSemantics * refSem,
                     const bool hasCandidate = false );


/// @brief Sorts actual template parameters according to formal names.
/// Template parameters sorting is needed since passing of arguments can occur by
/// positional or named binding.
///
/// @param actualParams The list of actual template parameters.
/// @param formalParams The list of formal template parameters.
/// @param set_formal_names If <tt>true</tt>, sets the corresponding name of
/// formal template parameters in actual template parameters.
/// @param missingType How to add missing template parameters.
/// @param refSem The reference semantics.
/// @param hasCandidates If <tt>true</tt>, relaxes some checks.
/// @return <tt>false</tt> if some actual template parameters are suspected to be implicit
/// (thus, they should take default value) but making them explicit fails,
/// <tt>true</tt> if everything goes well.
///
HIF_EXPORT
bool sortParameters( BList< TPAssign > & actualParams,
                     BList< Declaration > & formalParams,
                     const bool set_formal_names,
                     const SortMissingKind::type missingType,
                     hif::semantics::ILanguageSemantics * refSem,
                     const bool hasCandidates = false );


/// @brief Sorts port bindings according to port names.
/// Port bindings sorting is needed since they could occur by positional
/// or named binding.
///
/// @param actualParams The list of port bindings.
/// @param formalParams The list of port names.
/// @param set_formal_names If <tt>true</tt>, set the corresponding port
/// name in port bindings.
/// @param missingType How to add missing parameters.
/// @param refSem The reference semantics.
/// @return <tt>false</tt> if some port bindings are suspected to be implicit
/// (thus, they should take default value) but making them explicit fails,
/// <tt>true</tt> if everything goes well.
///
HIF_EXPORT
bool sortParameters( BList< PortAssign > & actualParams,
                     BList< Port > & formalParams,
                     const bool set_formal_names,
                     const SortMissingKind::type missingType,
                     hif::semantics::ILanguageSemantics * refSem );

///
/// @}
///


}} // end hif::manipulation

#endif

