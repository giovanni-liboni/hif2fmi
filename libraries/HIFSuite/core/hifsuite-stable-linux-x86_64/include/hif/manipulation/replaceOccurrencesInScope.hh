#ifndef HIF_MANIPULATION_REPLACEOCCURRENCESINSCOPE_HH
#define HIF_MANIPULATION_REPLACEOCCURRENCESINSCOPE_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Finds all the references of @p decl in the sub-tree starting from @p root,
/// and replaces them with a copy of @p to.
///
/// @param decl The declaration the references of which are to be found.
/// @param to The object to perform replacement with.
/// @param refLang The reference semantics.
/// @param root The sub-tree from which to start the search.
///
HIF_EXPORT
void replaceOccurrencesInScope(Declaration * decl, Object* to,
                               hif::semantics::ILanguageSemantics * refLang,
                               Object* root );

}} // end hif::manipulation

#endif
