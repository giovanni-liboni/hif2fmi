#ifndef HIF_MANIPULATION_TRANSFORMGLOBALACTIONS_HH
#define HIF_MANIPULATION_TRANSFORMGLOBALACTIONS_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Transforms global actions into corresponding processes (state tables).
///
/// @param o The object from which to start the manipulation.
/// @param sem The reference semantics.
/// @param list The list of generated state tables.
/// @param addVariablesInSensitivity If true, add also variables inside sensitivity
///        list (Default: false).
///
HIF_EXPORT
void transformGlobalActions(Object * o,
    std::set<StateTable *> &list,
    semantics::ILanguageSemantics * sem,
    const bool addVariablesInSensitivity = false);


}} // end hif::manipulation

#endif
