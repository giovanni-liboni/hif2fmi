#ifndef HIF_MANIPULATION_RENAMECONFLICTINGDECLARATIONS_HH
#define HIF_MANIPULATION_RENAMECONFLICTINGDECLARATIONS_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Renames declarations inside design units, to avoid name clashes.
///
/// @param root The tree to be fixed.
/// @param sem The semantics.
/// @param refMap Optional map of all references.
/// @param suffix Optional suffix for the renaming.
/// @return True if at least one renaming has been performed.
///
HIF_EXPORT
bool renameConflictingDeclarations(Object * root,
         hif::semantics::ILanguageSemantics * sem,
         std::map<Declaration *, std::set<Object*> > * refMap = NULL,
         const std::string & suffix = "");

}} // end hif::manipulation

#endif

