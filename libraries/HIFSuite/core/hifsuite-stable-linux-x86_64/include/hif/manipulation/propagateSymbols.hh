#ifndef HIF_MANIPULATION_PROPAGATESYMBOLS_HH
#define HIF_MANIPULATION_PROPAGATESYMBOLS_HH

#include "../classes/classes.hh"
#include "../semantics/HIFSemantics.hh"

namespace hif { namespace manipulation {

struct HIF_EXPORT PropagationOptions
{
    /// @name Traits
    /// @{

    typedef std::list<Object*> DeclarationTargets;
    typedef std::set<Object*> ObjectSet;
    typedef std::set<hif::View*> ViewSet;
    typedef std::map<hif::View*, ViewSet > ViewMap;

    /// @}


    /// @name Configuration options.
    /// @{

    /// @brief Propagate all signals of specified modules. Default is false.
    bool allSignals;
    /// @brief Propagate all ports of specified modules. Default is false.
    bool allPorts;
    /// @brief Propagate also all variable creating support signals. Default is false.
    bool allVariables;
    /// @brief Propagate also input and inout ports. Default is false.
    bool inputPorts;
    /// @brief If true, propagate also types which are not RTL. Default is false.
    /// E.g. File, Pointer.
    bool includeNotRtlTypes;
    /// @brief If true arrays symbols are not propagates. Default is false.
    bool skipArrays;
//    /// @brief If true symbols of type array are not splitted. Default is false.
//    bool doNotSplitArrays;

    /// @}


    /// @name Data to be propagated.
    /// @{

    /// @brief List of declarations that must be propagated.
    DeclarationTargets declarationTargets;

    /// @}


    /// @name Optional infos.
    /// @{

    /// @brief Optional system.
    System * system;
    /// @brief Tracks View dependencies from parent to children.
    ViewMap * smm;
    /// @brief Tracks View dependencies from child to parents.
    ViewMap * pmm;

    /// @}

    PropagationOptions();
    ~PropagationOptions();

    PropagationOptions(const PropagationOptions &);
    PropagationOptions & operator = (const PropagationOptions &);
};

/// @brief Propagates ports and signals of submodules till the top level.
/// @param options Configuration options.
/// @param sem The semantics to be used.
/// @return <tt>false</tt> in case of errors, <tt>true</tt> in case of success.
///
HIF_EXPORT
bool propagateSymbols(PropagationOptions & options,
                          hif::semantics::ILanguageSemantics * sem =
                          hif::semantics::HIFSemantics::getInstance() );

}} // end hif::manipulation

#endif
