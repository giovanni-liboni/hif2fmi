#ifndef HIF_MANIPULATION_MATCHEDGET_HH
#define HIF_MANIPULATION_MATCHEDGET_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Gets the object (from the newParent) in the same position of @p oldObj
/// in the @p oldParent. @p oldParent must be NULL or the direct parent of @p oldObj.
/// @param newParent The new parent.
/// @param oldObj The object to be matched.
/// @param oldParent Optional old parent. Can be NULL.
/// @return The matched object, or NULL.
HIF_EXPORT
Object * matchedGet( Object * newParent, Object * oldObj,  Object * oldParent = NULL);

}} // end hif::manipulation

#endif
