#ifndef HIF_MANIPULATION_ADDDECLARATIONINCONTEXT_HH
#define HIF_MANIPULATION_ADDDECLARATIONINCONTEXT_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Add given @p newDecl in the same context of given @p context declaration.
/// @param newDecl The new declaration to add.
/// @param context The context.
/// @param before If <tt>true</tt> add declaration before, after otherwise.
/// @return <tt>true</tt> if added, <tt>false</tt> otherwise.
HIF_EXPORT
bool addDeclarationInContext(Declaration * newDecl,
                             Declaration * context,
                             const bool before = false);

}} // end hif::manipulation

#endif
