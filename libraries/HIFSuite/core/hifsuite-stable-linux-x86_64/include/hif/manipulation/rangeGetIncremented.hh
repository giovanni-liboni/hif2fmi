#ifndef HIF_MANIPULATION_RANGEGETINCREMENTED_HH
#define HIF_MANIPULATION_RANGEGETINCREMENTED_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Given a span @p range, returns a new span with the upper bound
/// incremented by one.
/// For example, if the span is (0 to 10), this function returns the span
/// (0 to 11).
///
/// @param range The input span.
/// @param refLang The reference semantics.
/// @param increment The increment. Default is 1.
/// @return The new span with the upper bound incremented by one.
///
HIF_EXPORT
Range * rangeGetIncremented(
        Range * range,
        hif::semantics::ILanguageSemantics * refLang,
        long long int increment = 1);

}} // end Hif::Manipulation

#endif
