#ifndef HIF_MANIPULATION_RESOLVETEMPLATES_HH
#define HIF_MANIPULATION_RESOLVETEMPLATES_HH

#include "../classes/classes.hh"
#include "../semantics/HIFSemantics.hh"

namespace hif { namespace manipulation {

struct HIF_EXPORT ResolveTempalteOptions
{
    ResolveTempalteOptions();
    ~ResolveTempalteOptions();
    ResolveTempalteOptions(const ResolveTempalteOptions & other);
    ResolveTempalteOptions & operator =(ResolveTempalteOptions other);
    void swap(ResolveTempalteOptions & other);

    /// Pointer to the top level Hif View.
    View * top;
    /// Remove unreferenced tempalte declarations. Default is false.
    bool removeUnreferenced;
    /// Forces to have constvalues as span bounds. Default is false
    bool constvalueBounds;
    /// Forces to remove instanciated views. Default is false
    bool removeInstantiatedViews;
    /// Forces to remove instanciated subprograms. Default is false
    bool removeInstantiatedSubPrograms;
    /// Forces to remove instanciated typedefs. Default is false
    bool removeInstantiatedTypeDefs;
};

/// @brief Resolve the template declarations.
///
/// @param system Pointer to the Hif System object.
/// @param sem Pointer to the reference semantics.
HIF_EXPORT
bool resolveTemplates(hif::System * system,
    hif::semantics::ILanguageSemantics * sem =
    hif::semantics::HIFSemantics::getInstance(),
    const ResolveTempalteOptions & opt = ResolveTempalteOptions());

}} // end hif::manipulation

#endif
