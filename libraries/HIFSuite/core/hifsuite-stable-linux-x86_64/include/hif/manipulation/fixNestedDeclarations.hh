#ifndef HIF_MANIPULATION_FIXNESTEDDECLARATIONS_HH
#define HIF_MANIPULATION_FIXNESTEDDECLARATIONS_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Fix declarations nested in other declaration. This is not allowed
/// is SystemC.
/// @param o The system object.
/// @param sem The semantics.
HIF_EXPORT
void fixNestedDeclarations( hif::System * o,
    hif::semantics::ILanguageSemantics * sem );

}} // end hif::manipulation

#endif
