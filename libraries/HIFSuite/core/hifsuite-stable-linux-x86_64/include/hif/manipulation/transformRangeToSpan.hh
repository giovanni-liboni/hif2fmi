#ifndef HIF_MANIPULATION_TRANSFORMRANGETOSPAN_HH
#define HIF_MANIPULATION_TRANSFORMRANGETOSPAN_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Transforms a range into the corresponding span.
///
/// @param r The range to be converted.
/// @param refLang The reference semantics.
/// @param isSigned <tt>true</tt> if the values represented by the span
/// returned are signed, <tt>false</tt> if they are unsigned.
/// @return The corresponding span.
///
HIF_EXPORT
Range * transformRangeToSpan(
        Range * r,
        hif::semantics::ILanguageSemantics * refLang,
        const bool isSigned = true );

}} // end Hif::Manipulation

#endif
