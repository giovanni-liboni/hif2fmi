#ifndef HIF_MANIPULATION_FLATTENDESIGN_HH
#define HIF_MANIPULATION_FLATTENDESIGN_HH

#include "../classes/classes.hh"
#include "../semantics/ILanguageSemantics.hh"

namespace hif { namespace manipulation {

/// @brief flattenDesign() options.
struct HIF_EXPORT FlattenDesignOptions
{
    /// @brief Verbose output flag.
    bool verbose;
    /// @brief The name of the top-level design unit.
    std::string topLevelName;
    /// @brief The set of names of root design units, as hierarchical names.
    std::set<std::string> rootDUs;
    /// @brief The set of names of root instances.
    /// Their format is the hierarchical name.
    std::set<std::string> rootInstances;

    FlattenDesignOptions();
    ~FlattenDesignOptions();

    FlattenDesignOptions(const FlattenDesignOptions&);
    FlattenDesignOptions& operator =(const FlattenDesignOptions&);
};

/// @brief Flattens the given system description (i.e., removes its hierarchy).
/// Selective flattening is available by specifying root design units and/or instances
/// in the dedicated fields of the options structure.
/// @param system The system object to be flattened.
/// @param sem The reference semantics.
/// @param opt The options.
HIF_EXPORT
void flattenDesign(hif::System* system, hif::semantics::ILanguageSemantics* sem,
                   const FlattenDesignOptions& opt = FlattenDesignOptions());

} } // end hif::manipulation

#endif
