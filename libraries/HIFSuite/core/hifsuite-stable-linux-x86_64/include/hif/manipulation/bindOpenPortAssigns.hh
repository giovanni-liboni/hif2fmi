#ifndef HIF_MANIPULATION_BINDOPENPORTASSIGN_HH
#define HIF_MANIPULATION_BINDOPENPORTASSIGN_HH

#include "../classes/classes.hh"
#include "../semantics/HIFSemantics.hh"

namespace hif { namespace manipulation {

/// @brief Adds a dummy signal attached to a port which is not bound.
/// It is useful since in Hif when a design unit is instantiated, ports not
/// bound are not allowed.
///
/// @param object The root object from which to search the instances.
/// @param sem The reference semantics.
///
HIF_EXPORT
void bindOpenPortAssigns ( Object& object,
                          hif::semantics::ILanguageSemantics * sem =
                          hif::semantics::HIFSemantics::getInstance() );

}} // end hif::manipulation

#endif
