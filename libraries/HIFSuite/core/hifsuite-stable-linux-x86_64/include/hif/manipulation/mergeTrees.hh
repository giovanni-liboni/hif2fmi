#ifndef HIF_MANIPULATION_MERGETREES_HH
#define HIF_MANIPULATION_MERGETREES_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {


/// @brief Options for mergeTrees().
struct HIF_EXPORT MergeTreesOptions
{
    MergeTreesOptions();
    ~MergeTreesOptions();

    MergeTreesOptions(const MergeTreesOptions & o);
    MergeTreesOptions & operator = (const MergeTreesOptions & o);

    /// @brief Identify if print addictional infos. Default is false.
    bool printInfos;

    /// @brief Identify if is a description from ipxact. Default is false.
    bool isIpxact;

    /// @brief Forces to merge compultational branches.
    bool mergeBranches;
};

/// @brief This method takes a list of trees and compose them to form a single
/// tree merging the most of information from all of them.
/// Passed trees are deleted by the method.
/// @param partialTrees The list of trees to merge.
/// @param sem The reference semantics.
/// @param opt The options.
/// @return The merged tree
HIF_EXPORT
Object* mergeTrees(std::list<Object*> & partialTrees,
    semantics::ILanguageSemantics* sem , const MergeTreesOptions & opt = MergeTreesOptions());

}} // end hif::manipulation

#endif
