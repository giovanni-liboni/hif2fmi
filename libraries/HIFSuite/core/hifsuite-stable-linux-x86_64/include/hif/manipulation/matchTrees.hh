#ifndef HIF_MANIPULATION_MATCHTREES_HH
#define HIF_MANIPULATION_MATCHTREES_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Visits all reference tree, collecting matches between @p referenceTree
/// and @p matchedTree. Children of unmatched objects are not collected.
/// @param referenceTree The reference tree.
/// @param matchedTree The matched tree.
/// @param matched Map from reference objects to matched objects.
/// @param unmatched Map from reference objects to unmatched objects.
///
HIF_EXPORT
void matchTrees(
    Object * referenceTree,
    Object * matchedTree,
    std::map<Object*, Object*> & matched,
    std::map<Object*, Object*> & unmatched);

}} // end hif::manipulation

#endif
