#ifndef HIF_MANIPULATION_SIMPLIFY_HH
#define HIF_MANIPULATION_SIMPLIFY_HH

#include "../classes/classes.hh"
#include "SimplifyOptions.hh"

namespace hif { namespace manipulation {

/// @name Structs to define the returned type of hif::simplify.
/// @{
template < class T >
struct SimplifiedType
{
    typedef Object type;
};

template <>
struct SimplifiedType < Value >
{
    typedef Value type;
};

template <>
struct SimplifiedType < Cast >
{
    typedef Value type;
};

template <>
struct SimplifiedType < Expression >
{
    typedef Value type;
};

template <>
struct SimplifiedType < Range >
{
    typedef Range type;
};

template <>
struct SimplifiedType < If >
{
    typedef Action type;
};

template <>
struct SimplifiedType < Switch >
{
    typedef Action type;
};

template <>
struct SimplifiedType < For >
{
    typedef Action type;
};

template <>
struct SimplifiedType < IfGenerate >
{
    typedef Declaration type;
};

template <>
struct SimplifiedType < ForGenerate >
{
    typedef Declaration type;
};

template <>
struct SimplifiedType < FieldReference >
{
    typedef Value type;
};

template <>
struct SimplifiedType < Type >
{
    typedef Type type;
};

/// @}

/// @brief Simplifies the given tree.
///
/// @param o The object to be simplified.
/// @param refSem The current semantics.
/// @param opt The simplifying options.
/// @return The simplified object.
///
HIF_EXPORT
Object * simplify(Object * o,
                  hif::semantics::ILanguageSemantics * refSem,
                  const SimplifyOptions & opt = SimplifyOptions());

/// @brief Simplifies the given tree.
///
/// @param o The object to be simplified.
/// @param refSem The current semantics.
/// @param opt The simplifying options.
///
HIF_EXPORT
void simplify(BList<Object> & o,
              hif::semantics::ILanguageSemantics * refSem,
              const SimplifyOptions & opt = SimplifyOptions());

/// @brief Simplifies the given tree.
///
/// @param o The object to be simplified.
/// @param refSem The current semantics.
/// @param opt The simplifying options.
///
template<typename T>
void simplify(BList<T> & o,
              hif::semantics::ILanguageSemantics * refSem,
              const SimplifyOptions & opt = SimplifyOptions());

/// @brief Simplifies the given tree.
///
/// @param o The object to be simplified.
/// @param refSem The current semantics.
/// @param opt The simplifying options.
/// @return The simplified object.
///
template< typename T >
typename SimplifiedType<T>::type * simplify(
        T * o,
        hif::semantics::ILanguageSemantics * refSem,
        const SimplifyOptions & opt = SimplifyOptions());

/// @brief Simplifies and returns a copy of the given tree.
///
/// @param o The object to be simplified.
/// @param refSem The current semantics.
/// @return The fresh simplified object.
///
HIF_EXPORT
Object * getAggressiveSimplified(
        Object * o,
        hif::semantics::ILanguageSemantics * refSem);

/// @brief Simplifies and returns a copy of the given tree.
///
/// @param o The object to be simplified.
/// @param refSem The current semantics.
/// @return The fresh simplified object.
///
template< typename T >
typename SimplifiedType<T>::type * getAggressiveSimplified(
        T * o,
        hif::semantics::ILanguageSemantics * refSem);

/// @}

}} // end hif::manipulation

#endif
