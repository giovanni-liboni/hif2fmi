#ifndef HIF_MANIPULATION_TRANSFORMSPANTORANGE
#define HIF_MANIPULATION_TRANSFORMSPANTORANGE

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Transforms a span into the corresponding range.
///
/// @param r The span to be converted.
/// @param refLang The reference semantics.
/// @param isSigned <tt>true</tt> if the values represented by the span
/// returned are signed, <tt>false</tt> if they are unsigned.
/// @return The corresponding range.
///
HIF_EXPORT
Range * transformSpanToRange(Range * r,
                             hif::semantics::ILanguageSemantics* refLang,
                             const bool isSigned = true);

}} // end Hif::Manipulation

#endif
