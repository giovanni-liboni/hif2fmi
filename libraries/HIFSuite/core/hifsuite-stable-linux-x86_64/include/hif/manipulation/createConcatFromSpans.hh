#ifndef HIF_MANIPULATION_CREATECONCATFROMSPANS_HH
#define HIF_MANIPULATION_CREATECONCATFROMSPANS_HH

#include "../analysis/analyzeSpans.hh"

namespace hif { namespace manipulation {

/// @brief Given a set of indeces, creates a concat expression.
/// @param spanType The type of the span represented by the given indeces.
/// @param indexMap The input indeces and matching values.
/// @param sem The reference semantics.
/// @param others Optional value for indeces not pushed into indexMap.
/// @return The result, or NULL in case of error.
HIF_EXPORT
Value * createConcatFromSpans(Type * spanType,
                              const hif::analysis::IndexMap & indexMap,
                              hif::semantics::ILanguageSemantics * sem,
                              Value * others);

} } // hif::manipulation

#endif

