#ifndef HIF_SEMANTICS_FIXASSIGNMENTS_HH
#define HIF_SEMANTICS_FIXASSIGNMENTS_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

struct HIF_EXPORT FixAssignmentOptions
{
    /// @brief Fix Assign nodes adding cast on right hand side if needed.
    /// Default is false.
    bool fixAssigns;
    /// @brief Fix parameter assign value within parameter type.
    /// Default is false.
    bool fixParameterAssigns;
    /// @brief Fix data declarations initial value within data declaration type.
    /// Default is false.
    bool fixDataDeclarationsValue;

    FixAssignmentOptions();
    ~FixAssignmentOptions();
    FixAssignmentOptions(const FixAssignmentOptions & o);
    FixAssignmentOptions & operator = (FixAssignmentOptions o);
    void swap(FixAssignmentOptions & o);

    bool hasSomethingToFix() const;
};

/// @brief Fix the tree adding potential casts to right hand side of "assignments".
/// @param root The tree root.
/// @param sem The semantics.
/// @param opt The options.
HIF_EXPORT
bool fixAssignments(Object * root,
                    hif::semantics::ILanguageSemantics * sem,
                    const FixAssignmentOptions & opt = FixAssignmentOptions());

}} // end hif::manipulation

#endif

