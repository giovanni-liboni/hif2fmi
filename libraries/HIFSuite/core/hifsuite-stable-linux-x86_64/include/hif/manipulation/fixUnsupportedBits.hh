#ifndef HIF_MANUPULATION_FIXUNSUPPORTEDBITS_HH
#define HIF_MANUPULATION_FIXUNSUPPORTEDBITS_HH

#include "../classes/classes.hh"
#include "../semantics/semantics.hh"

namespace hif { namespace manipulation {

/// @brief Options for fixUnsupportedBits.
struct HIF_EXPORT FixUnsupportedBitsOptions
{
    FixUnsupportedBitsOptions();
    ~FixUnsupportedBitsOptions();
    FixUnsupportedBitsOptions(const FixUnsupportedBitsOptions& other);
    FixUnsupportedBitsOptions & operator = (FixUnsupportedBitsOptions other);
    void swap(FixUnsupportedBitsOptions & other);

    /// @brief Transform all logic values to two-logic values.
    /// Default is false.
    bool onlyBinaryBits;
    /// @brief In case of onlyBinaryBits, the replace value for logic bits.
    /// Default is '0'.
    char xzReplaceValue;
    /// @brief When set, do not fix initial values. Default is false.
    bool skipInitialValues;
};

/// @brief Fix checkSem unsupported bits, by removing them and
/// by replacing them with the most compatible alternative.
///
/// @param root The root of manipulation.
/// @param sem The current tree semantics.
/// @param checkSem The reference semantics for the checks on bits.
/// @param opts The options.
/// @return True when at least one fix has been performed.
///
HIF_EXPORT
bool fixUnsupportedBits(
        Object * root,
        hif::semantics::ILanguageSemantics * sem,
        hif::semantics::ILanguageSemantics * checkSem,
        const FixUnsupportedBitsOptions & opts = FixUnsupportedBitsOptions()
        );

} } // hif::manipulation

#endif // FIXUNSUPPORTEDBISTS_HH
