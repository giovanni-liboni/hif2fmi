#ifndef HIF_MANIPULATION_FIXRANGESDIRECTION_HH
#define HIF_MANIPULATION_FIXRANGESDIRECTION_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Fixes the problem caused by span upto.
/// E.g., in SystemC they don't exists.
/// The original span is transformed into the common [N downto 0].
/// A vhdl example:
/// @code
/// signal s : std_logic_vector(0 to 12);
/// @endcode
///
/// Is changed to:
/// @code
/// signal s : std_logic_vector(12 downto 0);
/// @endcode
///
/// @param o The system object.
/// @param sem The semantics.
///
HIF_EXPORT
void fixRangesDirection(hif::System * o, hif::semantics::ILanguageSemantics * sem);

}} // end hif::manipulation

#endif
