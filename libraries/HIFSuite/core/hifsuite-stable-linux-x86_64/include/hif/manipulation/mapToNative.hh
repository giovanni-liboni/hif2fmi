#ifndef HIF_MANIPULATION_MAPTONATIVE_HH
#define HIF_MANIPULATION_MAPTONATIVE_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {


/// @brief Maps all types of objects in subtree starting by @p v
/// in a native type that can be used for example as template parameter
/// of functions.
///
/// @param v The starting object.
/// @param sem The current tree semantics.
/// @param checkSem The semantics used to check and map native types.
///
HIF_EXPORT
void mapToNative( Object* v, hif::semantics::ILanguageSemantics* sem,
                  hif::semantics::ILanguageSemantics* checkSem );

}} // end hif::manipulation

#endif

