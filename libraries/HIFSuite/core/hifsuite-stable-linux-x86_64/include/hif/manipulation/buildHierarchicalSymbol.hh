#ifndef HIF_MANIPULATION_BUILDHIERARCHICALSYMBOL_HH
#define HIF_MANIPULATION_BUILDHIERARCHICALSYMBOL_HH

#include "../classes/classes.hh"
#include "../semantics/HIFSemantics.hh"

namespace hif { namespace manipulation {


/// @brief The definition style used by method buildHierarchicalSymbol.
struct HIF_EXPORT DefinitionStyle
{
    enum Type { HIF, VHDL, VERILOG, SYSTEMC } ;
};

/// @brief Build a hierarchical symbol against the HIF tree.
///
/// Symbols will be expressed as: top[.sub[(arch)]][.name]
/// e.g.
/// b17.u1.clock (where u1 is instance of b15)
///
/// @param obj The object to be represented as symbol.
/// @param sem The semantics to be used (default: HIF).
/// @param style The style used to build hierarchical names.
/// @return The symbol corresponding to the given object.
///
HIF_EXPORT
std::string buildHierarchicalSymbol(Object* obj,
    hif::semantics::ILanguageSemantics * sem = hif::semantics::HIFSemantics::getInstance(),
    DefinitionStyle::Type style = DefinitionStyle::HIF);

}} // end hif::manipulation

#endif
