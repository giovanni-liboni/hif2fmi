#ifndef HIF_MANIPULATION_RESOLVEHIERARCHICALSYMBOL_HH
#define HIF_MANIPULATION_RESOLVEHIERARCHICALSYMBOL_HH

#include "../classes/classes.hh"
#include "../semantics/HIFSemantics.hh"

namespace hif { namespace manipulation {

/// @brief Resolve a given symbol against the HIF tree.
///
/// Symbols must be expressed as: top[.sub[(arch)]][.name]
/// e.g.
/// b17.u1.clock (where u1 is instance of b15)
///
/// @param symbol The symbol to be resolved.
/// @param system The HIF tree root.
/// @param sem The semantics to be used (default: HIF).
/// @param instancePath Pointer to the list where to store the instances traversed
/// along the hierarchical path, or <tt>NULL</tt> in case this is not needed.
/// @return The object corresponding to the given symbol if resolved,
/// NULL in case of error.
///
HIF_EXPORT
Object* resolveHierarchicalSymbol(const std::string& symbol, hif::System* system,
    hif::semantics::ILanguageSemantics * sem = hif::semantics::HIFSemantics::getInstance(),
    std::list<hif::Instance*> * instancePath = NULL);

}} // end hif::manipulation

#endif
