#ifndef HIF_MANIPULATION_RENAMEINSCOPE_HH
#define HIF_MANIPULATION_RENAMEINSCOPE_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Renames a declaration in the scope containing it.
///
/// @param decl The declaration to be renamed.
/// @param refSem The reference semantics.
/// @return bool <tt>true</tt> if the declaration has been successfully renamed.
///
HIF_EXPORT
bool renameInScope( Declaration * decl, hif::semantics::ILanguageSemantics * refSem );

}} // end hif::manipulation

#endif
