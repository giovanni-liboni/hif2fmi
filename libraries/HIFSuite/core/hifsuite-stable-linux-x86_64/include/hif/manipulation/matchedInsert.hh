#ifndef HIF_MANIPULATION_MATCHEDINSERT_HH
#define HIF_MANIPULATION_MATCHEDINSERT_HH

#include "../classes/classes.hh"
#include "matchedInsertType.hh"

namespace hif { namespace manipulation {

/// @brief Inserts @p newObj inside @p newParent as the same field matched
/// by @p oldObj inside @p oldParent.
///
/// @param newObj The new object to be inserted.
/// @param newParent The parent object where to insert @p newObj.
/// @param oldObj The old object from which to get the position inside @p oldParent.
/// @param oldParent The old parent object to be matched.
/// @param type The insert type. Default is  <tt>TYPE_ERROR</tt>.
/// @return <tt>true</tt> if there are no errors, <tt>false</tt> otherwise.
///
HIF_EXPORT
bool matchedInsert( Object * newObj, Object * newParent,
                    Object * oldObj, Object * oldParent = NULL,
                    const MatchedInsertType::type type = MatchedInsertType::TYPE_ERROR );

}} // end hif::manipulation

#endif
