#ifndef HIF_MANIPULATION_FIXCLOCKEVENT_HH
#define HIF_MANIPULATION_FIXCLOCKEVENT_HH

#include "hifCore.hh"
#include "../classes/classes.hh"
#include "hif/semantics/semantics.hh"

namespace hif { namespace manipulation {


struct HIF_EXPORT RemoveStandardMethodsOptions
{
    /// Consider clock to be always a boolean. Default is false.
    bool boolClock;
    /// Remove use of I/O methods. Default is false.
    bool allowIO;

    RemoveStandardMethodsOptions();
    ~RemoveStandardMethodsOptions();
    RemoveStandardMethodsOptions(const RemoveStandardMethodsOptions & other);
    RemoveStandardMethodsOptions & operator =(RemoveStandardMethodsOptions other);
    void swap(RemoveStandardMethodsOptions & other);
};



/// @brief Remove standard methods calls, assuming that logic values
/// are mapped into two valued logic.
/// - 'event and rising_edge calls.
/// - to_x01
/// @param root The starting object.
/// @param sem Pointer to the reference semantics.
/// @return <tt>true</tt> if at least one fix has been performed.
HIF_EXPORT
bool removeStandardMethods(Object * root,
                   hif::semantics::ILanguageSemantics * sem =
        hif::semantics::HIFSemantics::getInstance(),
                   const RemoveStandardMethodsOptions & opt = RemoveStandardMethodsOptions());


}} // hif::manipulation

#endif // HIF_MANIPULATION_FIXCLOCKEVENT_HH
