#ifndef HIF_MANIPULATION_SORT_HH
#define HIF_MANIPULATION_SORT_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Options related to sort method.
struct HIF_EXPORT SortOptions
{
    SortOptions();
    ~SortOptions();

    /// @brief Indicates whether to recursively proceed with the visit
    /// on children nodes. Default is true.
    bool sortChildren;
    /// @brief <tt>true</tt> if parameters sorting methods must be called to sort
    /// PortAssigns, ParameterAssigns, etc. Default is false.
    bool sortParameters;
};


/// @brief Sorts the given object according with passed options.
///
/// @param obj The object to be sorted.
/// @param sem The reference semantics.
/// @param opt The sort options.
/// @return <tt>True</tt> if at least sort has been performed, <tt>false</tt> otherwise.
HIF_EXPORT
bool sort(Object * obj,
          hif::semantics::ILanguageSemantics * sem,
          const SortOptions& opt = SortOptions());

}} // end hif::manipulation

#endif
