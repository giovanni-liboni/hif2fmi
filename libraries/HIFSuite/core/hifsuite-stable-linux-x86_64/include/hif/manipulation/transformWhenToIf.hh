#ifndef HIF_MANIPULATION_TRANSFORMWHENTOIF_HH
#define HIF_MANIPULATION_TRANSFORMWHENTOIF_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Transforms the given when to an if statement.
/// The given When is not deleted, but its children are moved.
/// The translation works by checking the parent, which must be an assign
/// or a Return.
///
/// @param wo The when object.
/// @param sem The reference semantics.
/// @return The If object or NULL in case of error..
///
HIF_EXPORT
If * transformWhenToIf(When * wo, hif::semantics::ILanguageSemantics* sem);

}} // end hif::manipulation

#endif
