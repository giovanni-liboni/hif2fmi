#ifndef HIF_MANIPULATION_MATCHEDPATTERNINTREE_HH
#define HIF_MANIPULATION_MATCHEDPATTERNINTREE_HH

#include "../classes/classes.hh"
#include "../hifUtils/hifUtils.hh"

namespace hif { namespace manipulation {

/// @brief Searches subtree(s) matching @p pattern inside tree and returns the list
/// of nodes which are roots of pattern occurrences in the tree.
/// Note: the pattern is matched by checking subtrees w.r.t. the given equals options @p opt.
///
/// @param pattern The pattern object to search.
/// @param tree The tree from which to start the search.
/// @param resulList List on which search results are stored.
/// @param opt The equals options used to perform the equality check.
///
HIF_EXPORT
void matchPatternInTree( Object * pattern, Object * tree,
                           std::list< Object * > & resulList, const hif::EqualsOptions &opt );

}} // end hif::manipulation

#endif
