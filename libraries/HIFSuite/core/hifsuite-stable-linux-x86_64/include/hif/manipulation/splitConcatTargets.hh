#ifndef HIF_MANIPULATION_SPLITCONCATTARGETS_HH
#define HIF_MANIPULATION_SPLITCONCATTARGETS_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief options for splitConcatTargets().
struct HIF_EXPORT SplitConcatTargetOptions
{
    SplitConcatTargetOptions();
    ~SplitConcatTargetOptions();
    SplitConcatTargetOptions(const SplitConcatTargetOptions & other);
    SplitConcatTargetOptions & operator =(SplitConcatTargetOptions other);
    void swap(SplitConcatTargetOptions & other);

    /// @brief Create support signal instead of variables. Default is false.
    bool createSignals;
    /// @brief Split also port bindings with concats. Default is false.
    bool splitPortassigns;
    /// @brief Split also targets which are record values. Default is false.
    bool splitRecordValue;
};

/// @brief Splits assignments having concatenations as their left-hand side into multiple
/// corresponding assignments (one for each concatenated item).
/// @param root The object.
/// @param sem The semantics.
/// @param opt The options
HIF_EXPORT
void splitConcatTargets(
        Object * root, hif::semantics::ILanguageSemantics * sem,
        const SplitConcatTargetOptions & opt = SplitConcatTargetOptions());

}} // end hif::manipulation

#endif
