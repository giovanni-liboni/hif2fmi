#ifndef HIF_MANIPULATION_TRANSFORMCONSTANT_HH
#define HIF_MANIPULATION_TRANSFORMCONSTANT_HH

#include "../classes/classes.hh"

namespace hif { namespace manipulation {

/// @brief Transforms a constant value into a desired type.
///
/// @param cvo The current constant.
/// @param to The result constant value.
/// @param sem The reference semantics (default is the HIF one).
/// @param allowTruncation <tt>true</tt> if the transformation is allowed to truncate (potentially causing
///                        loss of information), <tt>false</tt> otherwise.
/// @return A fresh new value, equivalent to @p cvo, but of type @p to.
///
HIF_EXPORT
ConstValue * transformConstant(ConstValue * cvo, Type * to,
                               hif::semantics::ILanguageSemantics * sem = hif::semantics::HIFSemantics::getInstance(),
                               const bool allowTruncation = true );

/// @brief Transforms a value into a desired type.
/// For example, an aggregate can be transformed into a bitvector value.
///
/// @param vo The current value.
/// @param to The result value.
/// @param sem The reference semantics (default is the HIF one).
/// @param allowTruncation <tt>true</tt> if the transformation is allowed to truncate (potentially causing
///                        loss of information), <tt>false</tt> otherwise.
/// @return A fresh new value, equivalent to @p vo, but of type @p to.
///
HIF_EXPORT
Value * transformValue(Value * vo, Type * to,
                       hif::semantics::ILanguageSemantics * sem = hif::semantics::HIFSemantics::getInstance(),
                       const bool allowTruncation = true );


}} // end hif::manipulation

#endif
