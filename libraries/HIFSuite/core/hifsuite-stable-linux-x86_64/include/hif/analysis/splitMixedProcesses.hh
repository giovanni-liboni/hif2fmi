#ifndef HIF_ANALYSIS_SPLITMIXEDPROCESSES_HH
#define HIF_ANALYSIS_SPLITMIXEDPROCESSES_HH

#include "../classes/classes.hh"
#include "analysisTypes.hh"

namespace hif { namespace analysis {

/// @brief Given classified processes, tryes to split mixed processes to make
/// them pure synchronous and pure asynchronous.
///
/// @param map The map where results are stored.
/// @param sem The reference semantics.
/// @param opt The options.
/// @return True on success.
///
HIF_EXPORT
bool splitMixedProcesses(AnalyzeProcessOptions::ProcessMap & map,
                      hif::semantics::ILanguageSemantics * sem,
                      const AnalyzeProcessOptions & opt = AnalyzeProcessOptions());

}} // end hif::analysis

#endif
