#ifndef HIF_ANALYSIS_ANALYSISTYPES
#define HIF_ANALYSIS_ANALYSISTYPES

#include <set>
#include <map>
#include <list>

#include "../applicationUtils/portability.hh"

namespace hif { namespace analysis {

/// @brief Struct helper for template typedefs as C++98.
template <class KEY, class VALUE = KEY>
struct Types
{
    /// @brief Key type.
    typedef KEY Key;
    /// @brief Value type.
    typedef VALUE Value;
    /// @brief Type for storing sets of objects.
    typedef std::set<Value*> Set;
    /// @brief Type for storing maps of objects.
    typedef std::map<Key *, Set> Map;
    /// @brief Type for storing a list of objects.
    typedef std::list<Key*> List;
    /// @brief Type for storing a graph of objects.
    /// First element is considered from child to parent,
    /// second element from parent to child.
    typedef std::pair<Map, Map> Graph;
};

/// @brief Infos extracted for any process.
///
struct HIF_EXPORT ProcessInfos
{
    /// @brief Type of process.
    enum ProcessKind
    {
        ASYNCHRONOUS,
        SYNCHRONOUS,
        DERIVED_SYNCHRONOUS,
        MIXED,
        DERIVED_MIXED
    };

    /// @brief Type of process.
    enum ResetKind
    {
        NO_RESET,
        SYNCHRONOUS_RESET,
        ASYNCHRONOUS_RESET,
        DERIVED_SYNCHRONOUS_RESET
    };

    /// @brief Type of process.
    enum WorkingEdge
    {
        NO_EDGE,
        RISING_EDGE,
        FALLING_EDGE,
        BOTH_EDGES
    };

    /// @name Reset working phase.
    enum ResetPhase
    {
        NO_PHASE,
        HIGH_PHASE,
        LOW_PHASE
    };

    enum ProcessStyle
    {
        /// The body of the process odes not match other templates.
        NO_STYLE,
        /// Process kind:
        /// if (reset == )
        /// else if (clock.event && clock == )
        ///
        STYLE_1,
        /// Process kind:
        /// if (clock == )
        /// {
        ///     // Optional inner if:
        ///     if (reset)
        ///     {}
        ///     else
        ///     {}
        /// }
        ///
        STYLE_2,
        /// Process kind: SYNCH/DERIVED
        /// switch (state)
        /// {
        ///     case  ...
        /// }
        ///
        STYLE_3,
        /// Process kind: MIXED/SYNCH
        /// made by many inner sections, with one matching
        /// process style 1 or 2
        ///
        STYLE_4,
        /// Process kind:
        /// @(pos clock) a <= b;
        ///
        STYLE_5,
        /// Process kind:
        /// @(pos clock)
        ///   if (reset)
        ///   ...
        ///   else
        ///   ...
        ///
        STYLE_6
    };

    /// @brief TYpe for lists of declarations.
    typedef Types<DataDeclaration>::Set ReferredDeclarations;

    /// @name Process behavior.
    /// @{

    ProcessKind processKind;
    ResetKind resetKind;
    WorkingEdge workingEdge;
    ResetPhase resetPhase;
    ProcessStyle processStyle;

    /// @}

    /// @name Sensitivities.
    /// @{

    ReferredDeclarations risingSensitivity;
    ReferredDeclarations fallingSensitivity;
    ReferredDeclarations sensitivity;

    /// @}

    /// @name Process read/write ports, signals and variables.
    /// @{

    ReferredDeclarations inputs;
    ReferredDeclarations outputs;

    ReferredDeclarations inputVariables;
    ReferredDeclarations outputVariables;

    /// @}

    /// @name Signals.
    /// @{

    DataDeclaration * clock;
    DataDeclaration * reset;

    /// @}

    ProcessInfos();
    ~ProcessInfos();
    ProcessInfos(const ProcessInfos & other);
    ProcessInfos & operator = (const ProcessInfos & other);

    ReferredDeclarations::size_type getSensitivitySize() const;
    bool isInSensitivity(ReferredDeclarations::value_type v) const;
};


struct HIF_EXPORT AnalyzeProcessOptions
{
    typedef std::map<Object *, ProcessInfos> ProcessMap;

    /// @brief Specifies the main clock name. Default is NULL.
    Name clock;
    /// @brief Specifies the main reset name. Default is NULL.
    Name reset;

    /// @brief Skips trees rooted by standard declarations. Default: true.
    bool skipStandardDeclarations;

    /// @brief Prints warnings on the screen. Default: false.
    bool printWarnings;

    AnalyzeProcessOptions();
    ~AnalyzeProcessOptions();
    AnalyzeProcessOptions(const AnalyzeProcessOptions & other);
    AnalyzeProcessOptions & operator = (const AnalyzeProcessOptions & other);
};

}} // end hif::analysis

#endif
