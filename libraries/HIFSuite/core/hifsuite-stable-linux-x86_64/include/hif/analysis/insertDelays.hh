#ifndef HIF_ANALYSIS_INSERTDELAYS_HH
#define HIF_ANALYSIS_INSERTDELAYS_HH

#include "../classes/classes.hh"
#include "analysisTypes.hh"

namespace hif { namespace analysis {

struct HIF_EXPORT DelayProperties
{
    typedef std::size_t Size;

    DelayProperties();
    ~DelayProperties();
    DelayProperties(const DelayProperties & other);
    DelayProperties & operator = (const DelayProperties & other);

    /// @brief The port to inject.
    Port * port;
    /// @brief The number of clock cycles of delay.
    Size clockCycles;
    /// @brief The number of delta cycles of delay.
    Size deltas;
    /// @brief If true, means a half clock delay.
    bool halfClock;
};

struct HIF_EXPORT DelayInfos
{
    typedef DelayProperties::Size Size;
    typedef std::map<Port *, DelayProperties> DelayMap;
    typedef ProcessInfos::WorkingEdge WorkingEdge;
    typedef ProcessInfos::ResetPhase ResetPhase;

    DelayInfos();
    ~DelayInfos();
    DelayInfos(const DelayInfos & other);
    DelayInfos & operator = (const DelayInfos & other);

    /// @brief The view to be injected.
    View * view;
    /// @brief The reference clock.
    Port * clock;
    /// @brief The reference reset.
    Port * reset;
    /// @brief The clock working edge.
    WorkingEdge workingEdge;
    /// @brief The reset phase.
    ResetPhase resetPhase;
    /// @brief The delays to be injected.
    DelayMap delayProperties;
};

typedef std::list<DelayInfos> DelayList;


/// @brief Introduces delays on specified design units.
///
/// @param delays The delays.
/// @param sem The reference semantics.
/// @return True on success.
///
HIF_EXPORT
bool insertDelays(DelayList & delays, hif::semantics::ILanguageSemantics * sem);

}} // end hif::analysis

#endif

