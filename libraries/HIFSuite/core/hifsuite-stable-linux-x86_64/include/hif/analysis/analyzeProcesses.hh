#ifndef HIF_ANALYSIS_ANALYZEPROCESSES_HH
#define HIF_ANALYSIS_ANALYZEPROCESSES_HH

#include "../classes/classes.hh"
#include "analysisTypes.hh"

namespace hif { namespace analysis {

/// @brief Classifies all processes inside given root.
///
/// @param root The root of the tree.
/// @param map The map where results are stored.
/// @param sem The reference semantics.
/// @param opt The options.
/// @return True on success.
///
HIF_EXPORT
bool analyzeProcesses(Object * root, AnalyzeProcessOptions::ProcessMap & map,
                      hif::semantics::ILanguageSemantics * sem,
                      const AnalyzeProcessOptions & opt = AnalyzeProcessOptions());

}} // end hif::analysis

#endif
