#ifndef HIF_MANIPULATION_ANALYSIS_HH
#define HIF_MANIPULATION_ANALYSIS_HH

#include "../classes/Object.hh"
#include "../semantics/ILanguageSemantics.hh"

#include "analysisTypes.hh"
#include "sortGraph.hh"
#include "analyzeProcesses.hh"
#include "splitMixedProcesses.hh"
#include "insertDelays.hh"
#include "analyzeSpans.hh"

namespace hif {

/// @brief Wraps methods to analyze and change design characteristics.
namespace analysis {

} } // hif::analysis

#endif
