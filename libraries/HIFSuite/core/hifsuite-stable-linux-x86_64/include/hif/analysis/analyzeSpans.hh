#ifndef HIF_ANALYSIS_ANALYZESPANS_HH
#define HIF_ANALYSIS_ANALYZESPANS_HH

#include <map>
#include "../classes/forwards.hh"
#include "../semantics/semantics.hh"

namespace hif { namespace analysis {


/// @brief Struct used to represent an index, for analyzeSpans().
struct HIF_EXPORT IndexInfo
{
    IndexInfo();
    ~IndexInfo();
    IndexInfo (const IndexInfo &o);
    IndexInfo & operator =(const IndexInfo &o);
    bool operator < (const IndexInfo &o) const;

    /// @brief To be set only for single indeces.
    Value * expression;
    /// @brief To be set to represent a range of indeces, holding the same value.
    Range * range;
    /// @brief To be set for a span whose value is a span. Each index in the span
    /// will get the value at the matching index.
    Range * slice;
};


/// @brief Map from indexes to values, for analyzeSpans():
typedef std::map<IndexInfo, Value*> IndexMap;

/// @brief The result of analyzeSpans().
struct HIF_EXPORT AnalyzeSpansResult
{
    /// @brief The kind of index, as in IndexInfo.
    enum IndexKind
    {
        INDEX_EXPRESSION,
        INDEX_RANGE,
        INDEX_SLICE
    };

    /// @brief Represents a index shifted to zero.
    class HIF_EXPORT ValueIndex
    {
    public:
        ValueIndex();
        ~ValueIndex();
        ValueIndex(const ValueIndex & other);
        ValueIndex & operator = (ValueIndex other);
        void swap(ValueIndex & other);
        ValueIndex(const IndexKind kind,
                   const unsigned long long min,
                   const unsigned long long max = 0);

        bool operator < (const ValueIndex & other) const;
        IndexKind getKind() const;
        unsigned long long getMax() const;
        unsigned long long getMin() const;
        unsigned long long getSize() const;

    private:

        IndexKind _kind;
        unsigned long long _index;
        unsigned long long _minRangeIndex;
        unsigned long long _maxRangeIndex;
        unsigned long long _minSliceIndex;
        unsigned long long _maxSliceIndex;
    };

    /// @brief The map storing indexes and their associated values.
    typedef std::map<ValueIndex, Value *> ValueMap;

    AnalyzeSpansResult();
    ~AnalyzeSpansResult();
    AnalyzeSpansResult(const AnalyzeSpansResult & other);
    AnalyzeSpansResult & operator = (AnalyzeSpansResult other);
    void swap(AnalyzeSpansResult & other);

    /// @brief The result map from indexes to values.
    ValueMap resultMap;
    /// @brief The maximum bound shifted to zero.
    unsigned long long maxBound;
    /// @brief True when the indexes cover completely the original span.
    bool allSpecified;
    /// @brief True when all the indexes values are the same of given other.
    bool allOthers;
};

/// @brief Given a set of indeces, analyze them to unroll and pack their values.
/// @param spanType The type of the span represented by the given indeces.
/// @param indexMap The input indeces and matching values.
/// @param sem The reference semantics.
/// @param others Optional value for indeces not pushed into indexMap.
/// @param result The analysis result.
/// @return False in case of error during analysis.
HIF_EXPORT
bool analyzeSpans(Type * spanType,
                  const IndexMap & indexMap,
                  hif::semantics::ILanguageSemantics * sem,
                  Value * others,
                  AnalyzeSpansResult & result);

} } // hif::anlysis

#endif

