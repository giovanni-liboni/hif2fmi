#ifndef HIF_ANALYSIS_SORTGRAPH_HH
#define HIF_ANALYSIS_SORTGRAPH_HH

#include "../classes/classes.hh"
#include "analysisTypes.hh"

namespace hif { namespace analysis {

/// @brief Sorts a graph of objects.
///
/// @param graph The graph.
/// @param list The output list of sorted keys.
/// @param fromLeaves True if visit must start from leaves.
/// @param stableList (Optional) Eventual list of previous sorting, to have a stable sort.
///
HIF_EXPORT
void sortGraph(
        Types<Object, Object>::Graph & graph,
        Types<Object, Object>::List & list,
        const bool fromLeaves,
        Types<Object, Object>::List * stableList = NULL);

/// @brief Sorts a graph of objects.
///
/// @tparam KEY The key type for the graph.
/// @tparam VALUE The value type for the graph.
/// @param graph The graph.
/// @param list The output list of sorted keys.
/// @param fromLeaves True if visit must start from leaves.
/// @param stableList (Optional) Eventual list of previous sorting, to have a stable sort.
///
template<class KEY, class VALUE>
void sortGraph(
        typename Types<KEY, VALUE>::Graph & graph,
        typename Types<KEY, VALUE>::List & list,
        const bool fromLeaves,
        typename Types<KEY, VALUE>::List * stableList = NULL)
{
    typename Types<Object, Object>::Graph * g =
        reinterpret_cast<typename Types<Object, Object>::Graph*>(&graph);
    typename Types<Object, Object>::List * l =
        reinterpret_cast<typename Types<Object, Object>::List*>(&list);
    typename Types<Object, Object>::List * s =
            reinterpret_cast<typename Types<Object, Object>::List *>(stableList);
    sortGraph(*g, *l, fromLeaves, s);
}

}} // end hif::analysis

#endif
