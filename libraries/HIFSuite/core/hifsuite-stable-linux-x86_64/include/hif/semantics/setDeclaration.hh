#ifndef HIF_SEMANTICS_SETDECLARATION_HH
#define HIF_SEMANTICS_SETDECLARATION_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// @brief Sets the declaration of an object.
/// This function works only with objects that have the SetDeclaration
/// function defined (which is typically private).
///
/// @param o The object of which the declaration has to be set.
/// @param decl The declaration to be set.
///
HIF_EXPORT
void setDeclaration(Object * o, Object * decl);

}} // end hif::semantics

#endif
