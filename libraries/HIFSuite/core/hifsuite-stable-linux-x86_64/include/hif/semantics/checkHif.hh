#ifndef HIF_SEMANTICS_CHECKHIF_HH
#define HIF_SEMANTICS_CHECKHIF_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

struct HIF_EXPORT CheckOptions
{
    /// @brief Check the potential presence of aliases. Useful to find
    /// alias when there is an invalid read.
    /// Default is <tt>false</tt>.
    bool checkAliases;

    /// @brief Works on a copy of given tree.
    /// Default is <tt>false</tt>.
    bool checkOnCopy;

    /// @brief The check is done after call of simplify().
    /// NOTE: simplify modifies passed tree if checksOnCopy option is disabled.
    /// Default is <tt>false</tt>.
    bool checkSimplifiedTree;

    /// @brief Performs a check that the simplified tree and the original ones are equals.
    /// Otherwise, prints differences.
    /// NOTE: this flag is considered only with checkSimplifiedTree enabled.
    /// Default is <tt>false</tt>.
    bool checkMatchOfSimplifiedTree;

    /// @brief The check is done after call of flushInstanceCache and flushTypeCacheEntries.
    /// Default is <tt>false</tt>.
    bool checkFlushingCaches;

    /// @brief The check is done also for standard library defs and standard views.
    /// Default is <tt>false</tt>.
    bool checkStandardLibraryDefs;

    /// @brief Check the instantiation of instantiable objects.
    /// Default is <tt>false</tt>.
    bool checkInstantiate;

    /// @brief Check if the semantics type of typed object contains symbols which
    /// declarations are not in tree.
    /// Default is <tt>false</tt>.
    bool checkSemanticTypeSymbols;

    /// @brief If set, the check is interrupted after the first failure.
    /// Default is <tt>false</tt>.
    bool exitOnErrors;

    CheckOptions();
    ~CheckOptions();

    CheckOptions( const CheckOptions & o );
    CheckOptions & operator =( const CheckOptions & o );
};

/// @brief Check that Hif description is Hif-correct based on Hif Semantics.
///
/// @param o The node from which start to check Hif description.
/// @param sem The semantics on witch works.
/// @param opt The eventual options.
/// @return 0 if no error is found, otherwise 1.
///
HIF_EXPORT
int checkHif( Object * o,
    ILanguageSemantics * sem,
    const CheckOptions & opt = CheckOptions());

/// @brief Check that Hif description is Hif-correct based on Hif Semantics
/// where only native semantics is allowed.
/// e.g. bitvecotrs are not allowed, spans must be powers of 2
/// @param o The node from which start to check Hif description.
/// @param sem The semantics on witch works.
/// @param opt The eventual options.
/// @return 0 if no error is found, otherwise 1.
///
HIF_EXPORT
int checkNativeHif( Object * o,
    ILanguageSemantics * sem,
    const CheckOptions & opt = CheckOptions());

}} // end hif::semantics

#endif
