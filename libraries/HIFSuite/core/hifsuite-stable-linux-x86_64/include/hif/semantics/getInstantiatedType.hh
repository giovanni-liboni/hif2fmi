#ifndef HIF_SEMANTICS_GETINSTANTIATEDTYPE_HH
#define HIF_SEMANTICS_GETINSTANTIATEDTYPE_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {


/// @brief Try to find the instantiated type of the Type t according with
/// semantics passed as parameter. The default semantics is HIF.
///
/// @param t the starting Type
/// @param ref_sem The reference semantics.
///
/// @return a fresh instantiated <tt>t</tt> if it's possible to establish it, NULL otherwise
///
HIF_EXPORT
Type* getInstantiatedType (Type * t,
    ILanguageSemantics * ref_sem = HIFSemantics::getInstance());


}} // end hif::semantics

#endif
