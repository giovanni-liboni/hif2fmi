#ifndef HIF_SEMANTICS_COMPAREVALUES_HH
#define HIF_SEMANTICS_COMPAREVALUES_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// @name Value comparison stuff.
///
///	@{

/// @brief Enumeration for value comparison result.
enum CompareResult
{
    UNKNOWN,
    GREATER,
    EQUAL,
    LESS
};

/// @brief Compares two values.
///
/// @param v1 The first value.
/// @param v2 The second value.
/// @param refSem The reference semantics.
/// @param simplify If <tt>true</tt> simplifies the values before
/// performing comparison.
/// @return The comparison result between the two values.
HIF_EXPORT
CompareResult compareValues(
        Value * v1,
        Value * v2,
        hif::semantics::ILanguageSemantics * refSem,
        const bool simplify );

/// @}

}} // end hif::semantics

#endif
