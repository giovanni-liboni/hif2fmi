#ifndef HIF_SEMANTICS_RANGEGETMAX_HH
#define HIF_SEMANTICS_RANGEGETMAX_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// Returns a new range representing the maximum between the given ranges
/// @p a and @p b.
///
/// @param a The first range to be compared.
/// @param b The second range to be compared
/// @param refLang The reference semantics.
/// @return The maximum between @p a and @p b.
///
HIF_EXPORT
Range* rangeGetMax(Range* a, Range* b, ILanguageSemantics* refLang);

/// Returns a new range representing the minimum between the given ranges
/// @p a and @p b.
///
/// @param a The first range to be compared.
/// @param b The second range to be compared
/// @param refLang The reference semantics.
/// @return The minimum between @p a and @p b.
///
HIF_EXPORT
Range* rangeGetMin(Range* a, Range* b, ILanguageSemantics* refLang);

}} // end hif::semantics

#endif
