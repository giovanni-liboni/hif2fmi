#ifndef HIF_SEMANTICS_RESETDECLARATIONS_HH
#define HIF_SEMANTICS_RESETDECLARATIONS_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// @brief Options for get/update declaration methods.
struct HIF_EXPORT ResetDeclarationsOptions
{
    ResetDeclarationsOptions();
    virtual ~ResetDeclarationsOptions();
    ResetDeclarationsOptions(const ResetDeclarationsOptions& other);
    ResetDeclarationsOptions& operator=(ResetDeclarationsOptions other);
    void swap(ResetDeclarationsOptions & other);

    /// @brief Resets declarations only of current symbol.
    /// Default is false.
    bool onlyCurrent;

    /// @brief If a declaration is visible from the symbol scope, reset it.
    /// Default is false.
    bool onlyVisible;

    /// @brief Resets only symbols related to declaration signatures.
    /// For example, in case of function calls, the declarations of ParameterAssigns
    /// will be reset, but the ParameterAssigns values will be skipped.
    /// Default is false.
    bool onlySignatures;

    /// @brief The semantics used by getDeclaration (if any).
    /// Default is HIF.
    ILanguageSemantics * sem;
};


/// @brief Resets the declaration member of objects that have it
/// starting from @p o.
/// Declaration members will be set to NULL.
///
/// @param o The starting root object.
/// @param opt The options.
///
HIF_EXPORT
void resetDeclarations(
        Object *o,
        const ResetDeclarationsOptions & opt =  ResetDeclarationsOptions());

/// @brief Resets the declaration member of objects that have it
/// starting from @p o.
/// Declaration members will be set to NULL.
///
/// @param o The starting list object.
/// @param opt The options.
///
HIF_EXPORT
void resetDeclarations(
        BList<Object> &o,
        const ResetDeclarationsOptions & opt =  ResetDeclarationsOptions());

/// @brief Resets the declaration member of objects that have it
/// starting from @p o.
/// Declaration members will be set to NULL.
///
/// @param o The starting list object.
/// @param opt The options.
///
template <typename T>
void resetDeclarations(
        BList<T> &o,
        const ResetDeclarationsOptions & opt =  ResetDeclarationsOptions());



}} // end hif::semantics

#endif
