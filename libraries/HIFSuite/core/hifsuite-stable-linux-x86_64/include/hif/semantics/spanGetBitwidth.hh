#ifndef HIF_SEMANTICS_SPANGETBITWIDTH_HH
#define HIF_SEMANTICS_SPANGETBITWIDTH_HH

#include "../classes/classes.hh"
#include "HIFSemantics.hh"

namespace hif { namespace semantics {

/// @brief Computes the bitwidth used to represent values belonging to
/// the given span @p r.
/// It computes SUP - INF + 1.
///
/// @param r The span of which the bitwidth is to be computed.
/// @param sem The reference semantics.
/// @param simplify True if bounds must be simplified.
/// @return The numebr of elements in the span @p r if statically computable,
/// 0 otherwise.
///
HIF_EXPORT
unsigned long long int spanGetBitwidth(
        Range * r,
        ILanguageSemantics * sem = HIFSemantics::getInstance(),
        const bool simplify = true);

/// @brief Function that return the size of <tt>t</tt>,
///	measured in number of bits.
///
/// @param t Type pointer.
/// @param sem The semantics.
/// @param simplify True if bounds must be simplified.
/// @return size of <tt>t</tt> measured in number of bits if it is found, 0 otherwise.
///
HIF_EXPORT
unsigned long long int typeGetSpanBitwidth(
        Type * t,
        ILanguageSemantics * sem,
        const bool simplify = true);

}} // end hif::semantics

#endif
