#ifndef HIF_SEMANTICS_COLLECTSYMBOLS_HH
#define HIF_SEMANTICS_COLLECTSYMBOLS_HH

#include "../classes/classes.hh"
#include "HIFSemantics.hh"

namespace hif { namespace semantics {

typedef std::list<Object*> SymbolList;

/// @brief Collects all symbols (i.e., all objects having a declaration member)
/// starting from @p root.
///
/// @param list The result list where to store all symbols.
/// @param root The starting root object.
/// @param sem The reference semantics.
/// @param skipStandardDeclarations If <tt>true</tt> skip standard declarations
/// and declarations inside standard LibraryDefs. default = false
///
HIF_EXPORT
void collectSymbols(SymbolList & list, Object * root,
                    ILanguageSemantics * sem = hif::semantics::HIFSemantics::getInstance(),
                    const bool skipStandardDeclarations = false);

}} // end hif::semantics

#endif
