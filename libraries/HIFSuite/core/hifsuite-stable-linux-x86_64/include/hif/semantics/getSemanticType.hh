#ifndef HIF_SEMANTICS_GETTYPE_HH
#define HIF_SEMANTICS_GETTYPE_HH

#include "../classes/classes.hh"
#include "HIFSemantics.hh"
#include "../manipulation/prefixTree.hh"

namespace hif { namespace semantics {

/// @brief Try to find the type of the Value v according with
/// semantics passed as parameter. The default semantics is HIF.
///
/// @warning If during manipulation the reference semantics changes
/// its necessary to call resetType() function to calculate the correct
/// type. This function, in fact, calculate the type of <tt>v</tt> only
/// if it's not already set.
///
/// @param v the starting TypedObject
/// @param ref_sem The reference semantics.
/// @param error If true, rise error if can not type a typed object.
///
/// @return type of <tt>v</tt> if it's possible to establish it, NULL otherwise
///
HIF_EXPORT
Type* getSemanticType (TypedObject * v,
    ILanguageSemantics * ref_sem = HIFSemantics::getInstance(),
    const bool error = false);

/// @brief Starting from given <tt>root</tt> node, type all object that has
/// semantics type. If option <tt>error</tt> is true, rise error if can not type
/// a typed object.
///
/// @param root The root node.
/// @param ref_sem The reference semantics.
/// @param error If true, rise error if can not type a typed object.
///
HIF_EXPORT
void typeTree(Object * root,
    ILanguageSemantics * ref_sem = HIFSemantics::getInstance(),
    const bool error = false );

/// @brief Starting from given <tt>root</tt> node, type all object that has
/// semantics type. If option <tt>error</tt> is true, rise error if can not type
/// a typed object.
///
/// @param root The root node.
/// @param ref_sem The reference semantics.
/// @param error If true, rise error if can not type a typed object.
///
HIF_EXPORT
void typeTree(BList<Object> & root,
    ILanguageSemantics * ref_sem = HIFSemantics::getInstance(),
    const bool error = false );

/// @brief Starting from given <tt>root</tt> node, type all object that has
/// semantics type. If option <tt>error</tt> is true, rise error if can not type
/// a typed object.
///
/// @param root The root node.
/// @param ref_sem The reference semantics.
/// @param error If true, rise error if can not type a typed object.
///
template<typename T>
void typeTree(BList<T> & root,
    ILanguageSemantics * ref_sem = HIFSemantics::getInstance(),
    const bool error = false );

/// @brief Flushes the cache of semantic types.
HIF_EXPORT
void flushTypeCacheEntries();

/// @brief Given an object, check if it is in type cache.
/// @param obj The object to be checked.
/// @return True if object is in cache.
HIF_EXPORT
bool isInTypeCache(Object * obj);

/// @brief Given an object, it is added into the type cache.
/// @param obj The object to be inserted.
HIF_EXPORT
void addInTypeCache(Object * obj);

/// @brief Given a type returns the a copy of it with all prefixed symbols.
/// @param t The type to be prefixed.
/// @param sem The semantics.
/// @param opt The optional prefix tree options.
/// @param context The optional context to be used for prefix the symbols.
/// If not passed type symbols is used also how context. When the context is given,
/// internal symbols are simplified w.r.t. it.
/// @return A copy of given type with all prefixed symbols.
HIF_EXPORT
Type * getPrefixedType(
        Type * t,
        ILanguageSemantics * sem,
        const hif::manipulation::PrefixTreeOptions & opt = hif::manipulation::PrefixTreeOptions(),
        Object * context = NULL);

}} // end hif::semantics

#endif
