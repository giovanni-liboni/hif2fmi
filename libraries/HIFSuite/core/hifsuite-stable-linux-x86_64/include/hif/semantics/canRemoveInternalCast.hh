#ifndef HIF_SEMANTICS_CANREMOVEINTERNALCAST_HH
#define HIF_SEMANTICS_CANREMOVEINTERNALCAST_HH

namespace hif {

class Type;

namespace semantics {
class ILanguageSemantics;
} // semantics

} // hif

namespace hif { namespace semantics {

/// @brief Checks whether a cast of type t2 can be removed.
bool canRemoveInternalCast(Type* t1, Type* t2, Type* t3,
                           ILanguageSemantics * sem,
                           const bool conservativeBehavior = false);

} } // hif::semantics

#endif

