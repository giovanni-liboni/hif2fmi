#ifndef HIF_SEMANTICS_DECLARATIONOPTIONS_HH
#define HIF_SEMANTICS_DECLARATIONOPTIONS_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// @brief Options for get/update declaration methods.
struct HIF_EXPORT DeclarationOptions
{
    DeclarationOptions();
    virtual ~DeclarationOptions();
    DeclarationOptions(const DeclarationOptions& other);
    DeclarationOptions& operator=(DeclarationOptions other);
    void swap(DeclarationOptions & other);

    /// @brief The object from which to start the search. If it is
    /// set to NULL, start the search from symbol.
    Object * location;

    /// @brief If <tt>true</tt> and the declaration member of symbol
    /// is already set, it is ignored and the declaration is re-computed.
    /// Default is false.
    bool forceRefresh;

    /// @brief If <tt>true</tt> returns the declaration only if
    /// it is already set in the declaration member of symbol.
    /// Default is false.
    bool dontSearch;

    /// @brief If <tt>true</tt> and a declaration can not found, raise an error.
    /// Default is false.
    bool error;

    /// @brief If a declaration is visible from the current scope, update it.
    /// Other declarations will be kept.
    /// Default is false.
    bool onlyVisible;

    /// @brief If <tt>true</tt> don't raise typing errors.
    /// This is userfull during parsing fixes since the tree could be not typable.
    /// Default is <tt>false</tt>.
    bool looseTypeChecks;
};

}} // end hif::semantics

#endif
