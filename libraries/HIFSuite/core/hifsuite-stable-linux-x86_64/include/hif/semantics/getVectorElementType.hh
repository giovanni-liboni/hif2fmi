#ifndef HIF_SEMANTICS_GETVECTORELEMENTTYPE_HH
#define HIF_SEMANTICS_GETVECTORELEMENTTYPE_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// @brief Returns the type of elements in case of vector or array types.
///
/// @param t The Type to analyze.
/// @param refLang The semantics.
/// @return The type of elements in case of vector or array types, NULL otherwise.
///
HIF_EXPORT
Type * getVectorElementType( Type * t, ILanguageSemantics * refLang );

}} // end hif::semantics

#endif
