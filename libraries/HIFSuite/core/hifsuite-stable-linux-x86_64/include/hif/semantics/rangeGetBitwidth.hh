#ifndef HIF_SEMANTICS_RANGEGETBITWIDTH_HH
#define HIF_SEMANTICS_RANGEGETBITWIDTH_HH

#include "../classes/classes.hh"
#include "HIFSemantics.hh"

namespace hif { namespace semantics {

/// @brief Computes the bitwidth used to represent values belonging to
/// the given range @p r.
/// The bitwidth is obtained by computing the logarithm of
/// max(abs(left), abs(right)) to base 2.
///
/// @param r The range of which the biwidth is to be computed.
/// @param sem The reference semantics.
/// @return Bitwidth to represent values belonging to @p r if statically
/// computable, 0 otherwise.
///
HIF_EXPORT
unsigned int rangeGetBitwidth (
        Range * r,
        ILanguageSemantics * sem = hif::semantics::HIFSemantics::getInstance());

}} // end hif::semantics

#endif
