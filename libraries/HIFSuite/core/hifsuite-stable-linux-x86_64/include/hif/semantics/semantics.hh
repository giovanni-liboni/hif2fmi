#ifndef HIF_SEMANTICS_HH
#define HIF_SEMANTICS_HH

// ///////////////////////////////////////////////////////////////////
// Semantics specific methods.
// ///////////////////////////////////////////////////////////////////
#include "ILanguageSemantics.hh"
#include "HIFSemantics.hh"
#include "SystemCSemantics.hh"
#include "VHDLSemantics.hh"
#include "VerilogSemantics.hh"

// ///////////////////////////////////////////////////////////////////
// Main semantics methods and options.
// ///////////////////////////////////////////////////////////////////
#include "DeclarationOptions.hh"
#include "resetDeclarations.hh"
#include "updateDeclarations.hh"
#include "referencesUtils.hh"
#include "collectSymbols.hh"
#include "callingProcessesUtils.hh"
#include "mapDeclarationsInTree.hh"
#include "DeclarationsStack.hh"
#include "compareValues.hh"
#include "rangeGetBitwidth.hh"
#include "spanGetBitwidth.hh"
#include "spanGetSize.hh"
#include "rangeGetMax.hh"
#include "rangeGetSum.hh"
#include "checkHif.hh"
#include "standardizeDescription.hh"
#include "setDeclaration.hh"
#include "declarationUtils.hh"
#include "typeSemanticUtils.hh"
#include "getVectorElementType.hh"
#include "getSemanticType.hh"
#include "resetTypes.hh"
#include "getBaseType.hh"
#include "getOtherOperandType.hh"
#include "getInstantiatedType.hh"
#include "canRemoveInternalCast.hh"
#include "analyzePrecisionType.hh"

// ///////////////////////////////////////////////////////////////////
// Files grouping many methods.
// ///////////////////////////////////////////////////////////////////

namespace hif {

/// @brief Wraps methods to perform semantic operations.
/// This namespace contains methods concerning visibility, types,
/// libraries, semantic checks etc.
/// This namespace define methods to check semantics of a system description,
///	get value of a specific type, get type from a value, get base type from a type,
///	get cardinality for a type, get size of a range, check and manipulate types,
///	add library definitions, simplify expressions.
namespace semantics { }

} // hif

#endif // HIF_SEMANTICS_HH
