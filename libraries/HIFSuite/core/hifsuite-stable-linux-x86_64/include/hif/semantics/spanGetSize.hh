
#ifndef HIF_SEMANTICS_SPANGETSIZE_HH
#define HIF_SEMANTICS_SPANGETSIZE_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// @brief Extracts the span of a type @p t and it returns the span size
/// by calling the spanGetSize function.
///
/// @param t The type from which the span is to be extracted.
/// @param refLang The reference semantics.
/// @return The span size.
///
HIF_EXPORT
Value * typeGetSpanSize (Type *t, ILanguageSemantics *refLang );


/// @brief Returns the size of span @p r.
/// The size of the span @p r is computed as (r'high - r'low + 1).
/// If the size of the span can not be computed, NULL will be returned.
///
/// @param r The span of which the size is to be computed.
/// @param sem The reference semantics.
/// @param simplify If true simplify given range.
/// @return The size of the span @p r computed as (r'high - r'low + 1).
///
HIF_EXPORT
Value* spanGetSize (Range * r, ILanguageSemantics* sem, const bool simplify = true);


/// @brief Extracts the span of a type @p t.
/// If it's an array extracts the sum of every element's span
///
/// @param t The type from which the span is to be extracted.
/// @param sem The reference semantics.
/// @return The span size.
///
HIF_EXPORT
Value * typeGetTotalSpanSize (Type * t, ILanguageSemantics * sem);

}} // end hif::semantics

#endif
