
#ifndef HIF_SEMANTICS_ILANGUAGESEMANTICS_HH
#define HIF_SEMANTICS_ILANGUAGESEMANTICS_HH

#include "../applicationUtils/portability.hh"
#include "../classes/Object.hh"
#include "../classes/Type.hh"
#include "../classes/Declaration.hh"
#include "../classes/ConstValue.hh"
#include "../classes/Value.hh"
#include "../hifUtils/equals.hh"
#include "../manipulation/sortParameters.hh"
#include <iostream>

#include "../HifFactory.hh"

///
/// ILanguageSemantics
///
/// @brief This class is a virtual class to support semantic of a specific HDL.
/// In consists in a set of methods to retrieve informations about
/// HIF objects according to a specific HDL semantic.
///
///

namespace hif { namespace semantics {

class HIF_EXPORT ILanguageSemantics
{
public:

    /// @brief The supported languages.
    enum SupportedLanguages
    {
        VHDL,
        Verilog,
        SystemC,
        HIF
    };

    /// @brief The possible mapping of standard symbols.
    /// Four cases are possible:
    /// * UNSUPPORTED if the symbol is neither known by the source semantics
    ///     (which does not support it) and the destination semantics.
    ///     Thus, we expect an error.
    /// * SIMPLIFIED if the symbol was known by the source semantics, but
    ///     it is involved in a construct which is simplified (i.e., transformed
    ///     in some other construct ) in the destination semantics.
    /// * MAP_KEEP if the symbol was known by the source semantics, and
    ///     translated into another symbol in the destination semantics.
    ///     The original declaration is kept.
    /// * MAP_DELETE if the symbol was known by the source semantics, and
    ///     translated into another symbol in the destination semantics.
    ///     The original declaration is deleted.
    /// * UNKNOWN if the symbol was known by the source semantics, but
    ///     it is not known by the destination semantics.
    enum MapCases
    {
        UNSUPPORTED,
        SIMPLIFIED,
        MAP_KEEP,
        MAP_DELETE,
        UNKNOWN
    };

    /// @brief Struct representing output for getExprType() function.
	struct HIF_EXPORT ExpressionTypeInfo
    {
        /// @brief Type pointer representing the type returned by the
        /// operation which works with values of given types.
        Type * returnedType;

        /// @brief Type pointer representing the precision adopted on
        /// the operands with the given operation.
        Type * operationPrecision;

        /// @brief Default empty constructor
        ExpressionTypeInfo();

        /// @brief Copy constructor
        ExpressionTypeInfo(const ExpressionTypeInfo& t);

        /// @brief Destructor
        virtual ~ExpressionTypeInfo();

        /// @brief Assignment operator
        ExpressionTypeInfo & operator = ( const ExpressionTypeInfo& t );

    };


    /// @name mapStandardSymbols() related types
    /// @{

    /// Input symbols
    typedef std::pair<Name, Name> KeySymbol;

    /// More than one library could be need to define a symbol (e.g.,
    /// VHDL signed, unsigned libraries)
    typedef std::list<Name> LibraryList;

    /// Output symbols
    struct HIF_EXPORT ValueSymbol
    {
        LibraryList libraries;
        Name mappedSymbol;
        MapCases mapAction;

        ValueSymbol();
        ~ValueSymbol();

        ValueSymbol (const ValueSymbol & other);
        ValueSymbol & operator = (const ValueSymbol & other);
    };

    /// Main map for mapStandardSymbols() method
    typedef std::map<KeySymbol, ValueSymbol> StandardSymbols;

    /// Map for standard library file names.
    typedef std::map<Name, std::string> StandardLibraryFiles;

    /// @}

    /// @name Semantic checks options for check visitor.
    /// @{

    /// @brief Set of options for semantic checks.
    struct HIF_EXPORT SemanticOptions
    {
        SemanticOptions();
        ~SemanticOptions();

        SemanticOptions(const SemanticOptions& t);
        SemanticOptions & operator = ( const SemanticOptions& t );

        /// @brief Enum of type of for conditions.
        enum ForConditionType { RANGE, EXPRESSION, RANGE_AND_EXPRESSION };

        /// @brief Ensures no initial value for input ports.
        bool port_inNoInitialValue;

        /// @brief Ensures initial value for output ports.
        bool port_outInitialValue;

        /// @brief Ensures initial value for fields, variables and signals.
        bool dataDeclaration_initialValue;

        /// @brief Ensures scoped types are inside TypeDefs.
        bool scopedType_insideTypedef;

        /// @brief Native ints are considered as bitfields.
        bool int_bitfields;

        /// @brief Ensure design units has only a view.
        bool designUnit_uniqueView;

        /// @brief Ensure for has a single init declaration.
        bool for_implictIndex;

        /// @brief Specify the allowed type as for condition.
        ForConditionType for_conditionType;

        /// @brief Ensure no generate objects into the tree.
        bool generates_isNoAllowed;

        /// @brief Ensure no after objects into the tree.
        bool after_isNoAllowed;

        /// @brief Ensure no with objects into the tree.
        bool with_isNoAllowed;

        /// @brief Ensure no globact objects into the tree.
        /// Note: the check is done checking number of simpleactions.
        bool globact_isNoAllowed;

        /// @brief Ensure no valueStatement objects into the tree.
        bool valueStatement_isNoAllowed;

        /// @brief Ensure case objects (With or Switch) has only literal semantics.
        bool case_isOnlyLiteral;

        /// @brief Ensure signals and ports are not inside a slices
        /// or members expression.
        bool lang_signPortNoBitAccess;

        /// @brief Language has the dontcare bit value.
        bool lang_hasDontCare;

        /// @brief Language has 9 logic values. False means 4 values.
        bool lang_has9logic;

        /// @brief Flag to be used to sort missing parameters (both templates and method params).
        hif::manipulation::SortMissingKind::type lang_sortKind;
    };

    /// @brief Return the options according with the current semantics.
    /// @return The options.
    const SemanticOptions & getSemanticsOptions() const;

    /// @}

    virtual
    ~ILanguageSemantics() = 0;

    /// @brief Returns the name of the semantics.
    /// @return The name of the semantics.
    virtual std::string getName() = 0;

    /// @name Type management methods.
    ///@{

    ///
    /// @brief Function that given a couple of Type and an operation to do on them
    /// returns a struct containing informations about the returned type of the operation
    /// and the precision applied to the operands. Pure virtual method.
    ///
    /// @param op1Type Type pointer to the first operand involved in the operation.
    ///
    /// @param op2Type Type pointer to the second operand involved in the operation.
    ///
    /// @param operation op enumeration entry representing the operation to do.
    ///
    /// @param sourceObj pointer to starting value from which is starts analysis.
    ///
    /// @return type_info_t ExpressionTypeInfo containing informations about the operation.
    ///
    virtual
    ExpressionTypeInfo getExprType(Type *op1Type, Type *op2Type,
                                 Operator operation, Object * sourceObj ) = 0;


    /// @brief Function to change the semantic checks mode.
    /// Usually, they are more permessive inside frontends,
    /// whilst backends are more strict.
    ///
    /// @param v The mode.
    ///
    void setStrictTypeChecks(const bool v);

    /// @brief Function to get the semantic checks mode.
    ///
    /// @return The mode.
    ///
    bool getStrictTypeChecks() const;

    ///
    /// @brief Function that given a Type returns a Value
    /// pointer to the default value associated to the type.
    /// Pure virtual method.
    ///
    /// @param t Type pointer to the type where the default value
    /// have to be established.
    /// @param d Declaration to which the value refers to.
    ///
    /// @return Type pointer to the correspondent type if exists, NULL otherwise.
    ///
    virtual
    Value * getTypeDefaultValue (Type *t, Declaration* d) = 0;


    ///
    /// @brief Function that given a Type returns a Type pointer
    /// to the correspondent type in the target language (if exists).
    /// Pure virtual method.
    ///
    /// @param t Type pointer to the type to analyze.
    ///
    /// @return Type pointer to the correspondent fresh type.
    ///
    virtual
    Type * getMapForType (Type *t) = 0;


    /// @brief Function that given the operator and the Types of expression
    /// in the source semantics, returns the (eventually changed) operator
    /// in the target language (if exists).
    ///
    /// @param srcOperation The operator.
    /// @param srcT1 The type of the first operand in source semantics.
    /// @param srcT2 The type of the second operand in source semantics.
    /// @param dstT1 The type of the first operand in destination semantics.
    /// @param dstT2 The type of the second operand in destination semantics.
    /// @return Type pointer to the correspondent fresh type.
    ///
    virtual
    Operator getMapForOperator(Operator srcOperation, Type * srcT1, Type * srcT2,
                          Type * dstT1, Type * dstT2 ) = 0;


    /// @brief Function that given a type and an operation to do with
    /// operators of that type returns a pointer to a Type
    /// representing the type that operands have to be cast into to
    /// obtain a valid operation in target SystemC (if possible).
    ///
    /// @param t Type pointer to the operands type.
    /// @param operation op entry enumeration representing operation to do
    /// @param opType The type of operand to be casted.
    /// @param startingObject The startingObject.
    /// @param isOp1 True if it is the type for op1.
    ///
    /// @return Type pointer to the suggested type to obtain
    /// a valid operation in current semantics  (if possible).
    ///
    virtual
    Type * getSuggestedTypeForOp (Type *t, Operator operation, Type * opType,
                                  Object * startingObject, const bool isOp1) = 0;


    /// @brief Function that given a ConstValue returns a Type
    /// pointer representing the type to associate to the constant according
    /// with target language requirements (opportunely setting flags of the type).
    /// Pure virtual method.
    ///
    /// @param c ConstValue pointer to the constant to analyze.
    ///
    /// @return Type pointer to the constant type.
    ///
    virtual
    Type * getTypeForConstant (ConstValue *c) = 0;


    /// @brief Checks if given type is compatible with language requirements
    /// as condition.
    ///
    /// @param t Type of the condition to analyze.
    /// @param o The object containing the condition.
    ///
    /// @return true if the type of the condition is compatible with target
    /// language requirements, false otherwise.
    ///
    virtual
    bool checkCondition (Type *t, Object * o) = 0;


    /// @brief Function that given a Value representing a valid guard
    /// condition in the target language returns an expression representing
    /// the explanation of the guard as boolean condition (with == operator).
    /// Pure virtual method.
    ///
    /// @param c Value pointer to the condition to analyze.
    ///
    /// @return Value pointer to the created expression.
    ///
    virtual
    Value * explicitBoolConversion(Value *c) = 0;


    /// @brief Function that given a Value and a type which the given
    /// object have to be cast into, returns a Value representing the
    /// explicit cast (it may be a nested cast, a function call, a procedure
    /// call or a constant) according with target language semantics.
    ///
    /// @param valueToCast Value pointer to the value to cast.
    /// @param castType Type pointer to the type which the given
    /// Value have to be cast.
    /// @param srcType The type of value in source semantics.
    ///
    /// @return Value pointer to the explicit cast.
    ///
    virtual
    Value * explicitCast(Value *valueToCast, Type *castType, Type * srcType) = 0;


    /// @brief Function that given a real value returns the correspondent
    /// int value according to semantics rules (e.g., VHDL rounds while
    /// SystemC truncates).
    /// @param v The value to convert.
    /// @return The converted value.
    virtual
    long long int transformRealToInt( const double v ) = 0;

    /// @brief Function that given a member returns its semantic type
    /// according to semantics rules (e.g., SystemC with Bitvector prefix type
    /// returns sc_proxy).
    /// @param m The member.
    /// @return The semantic type.
    virtual
    Type * getMemberSemanticType(Member * m);

    /// @brief Function that given a slice returns its semantic type
    /// according to semantics rules (e.g., SystemC with Bitvector prefix type
    /// returns sc_proxy).
    /// @param s The slice.
    /// @return The semantic type.
    virtual
    Type * getSliceSemanticType(Slice * s);

    ///@}


    /// @name Semantic checks methods
    ///@{

    /// @brief Function that given a Type checks whether it is suitable
    /// as bound of a Range
    ///
    /// @param t The type to be checked.
    /// @return NULL if allowed, a suitable fresh type otherwise.
    ///
    virtual
    Type * isTypeAllowedAsBound( Type * t ) = 0;

    /// @brief Function that given the target type and the source type
    /// of a Cast checks whether it is valid a direct conversion with
    /// respect to semantics.
    ///
    /// @param target The target type to be checked.
    /// @param source The source type to be checked.
    /// @return True if allowed, False otherwise.
    ///
    virtual
    bool isCastAllowed( Type * target, Type * source ) = 0;

    /// @brief Returns true if given type is allowed in the semantics.
    ///
    /// @param t The type to be checked.
    /// @return true if is allowed.
    ///
    virtual
    bool isTypeAllowed(Type * t) = 0;

    /// @brief Returns true if given range direction is allowed in the semantics.
    ///
    /// @param r The range to be checked.
    /// @return true if is allowed.
    ///
    virtual
    bool isRangeDirectionAllowed(RangeDirection r) = 0;

    /// @brief Returns true if given type is allowed as switch or with
    /// value in the semantics.
    ///
    /// @param t The type to be checked.
    /// @return true if is allowed.
    ///
    virtual
    bool isTypeAllowedAsCase(Type * t) = 0;

    /// @brief Returns true if given type is allowed as port type.
    ///
    /// @param t The type to be checked.
    /// @return true if is allowed.
    ///
    virtual
    bool isTypeAllowedAsPort(Type * t) = 0;

    /// @brief Returns true if given language id is allowed in the semantics.
    ///
    /// @param id The language id to be checked.
    /// @return true if is allowed.
    ///
    virtual
    bool isLanguageIdAllowed(LanguageID id) = 0;

    /// @brief Returns true if given value is allowed for port binding
    /// in the semantics.
    ///
    /// @param o The value to be checked.
    /// @return true if is allowed.
    ///
    virtual
    bool isValueAllowedInPortBinding(Value * o) = 0;

    /// @brief Returns true if given const value syntactic type is allowed
    /// for the const value in the semantics.
    ///
    /// @param cv The value to be checked.
    /// @param synType The syntactic type to be checked.
    /// @return true if is allowed.
    ///
    virtual
    bool isTypeAllowedForConstValue(ConstValue * cv, Type * synType) = 0;

    /// @brief Checks whether the cast on operands can be removed safetely.
    ///
    /// @param e The original expression.
    /// @param origInfo The original expression Info.
    /// @param simplifiedInfo The info without casts.
    /// @param castT1 The type of first operand cast.
    /// @param castT2 The type of second operand cast.
    /// @param subT1 The type of first operand without cast.
    /// @param subT2 The type of second operand without cast.
    /// @return <tt>true</tt> if cast can be removed.
    ///
    bool canRemoveCastOnOperands(Expression * e,
                                 ExpressionTypeInfo & origInfo,
                                 ExpressionTypeInfo & simplifiedInfo,
                                 Type * castT1, Type * castT2,
                                 Type * subT1, Type * subT2
                                 );

    ///@}


    /// @name Template related stuff
    ///@{

    /// @brief Checks if type is allowed as template type w.r.t.
    ///semantics.
    /// @param t The type to check.
    /// @return True if is allowed.
    ///
    virtual
    bool isTemplateAllowedType( Type * t ) = 0;

    /// @brief Return the fresh mapped type w.r.t. given type that is allowed as type
    /// in template parameter w.r.t. semantics.
    /// @param t The type to map.
    /// @return The allowed type.
    ///
    virtual
    Type * getTemplateAllowedType( Type * t ) = 0;

    ///@}


    /// @name Singleton design pattern.
    ///@{

    ///
    /// @brief Function thats return an instance of the required language semantics class.
    ///
    /// @param lang enumeration entry representing the required language.
    ///
    /// @return ILanguageSemantics object reference to the class that implements
    /// the semantics of the required language.
    ///
    static
    ILanguageSemantics* getInstance (SupportedLanguages lang);


    ///@}

    /// @brief Checks whether a name is forbidden in the current semantics.
    virtual
    bool isForbiddenName( Declaration * decl ) = 0;

    /// @name General support methods.
    /// @{

    Range * getContextPrecision(Object * o);

    /// @brief Returns true when semantics type of slice must be rebased.
    virtual bool isSliceTypeRebased() = 0;

    /// @brief Returns true when syntactic type must be rebased.
    virtual bool isSyntacticTypeRebased() = 0;

    /// @}


    /// @name Standard packages
    /// @{

    /// @brief Get the eventual LibraryDef matching the given name.
    /// @param n The name.
    /// @return The LibraryDef or NULL.
    virtual LibraryDef * getStandardLibrary(Name n) = 0;

    /// @brief Starting from system adds all required standard packages.
    /// @param s The system.
    virtual void addStandardPackages(System * s);

    /// @brief Return True if the given library is native for the semantics.
    virtual bool isNativeLibrary(Name n, const bool hifFormat = false) = 0;

    /// @brief Map an input symbol into the corresponding output one.
    /// @return True if map succeed.
    virtual MapCases mapStandardSymbol(Declaration * decl, KeySymbol& key, ValueSymbol& value,
                                       ILanguageSemantics * srcSem) = 0;

    /// @brief Map a library name in the correspondent header file name.
    std::string mapStandardFilename(Name n);

    /// @brief Returns the mapped symbol w.r.t. the current semantics.
    virtual Object * getSimplifiedSymbol(KeySymbol& key, Object * s) = 0;

    /// @brief Returns true if no namespaces is needed for given library name.
    virtual bool isStandardInclusion(Name n, const bool isLibInclusion);

    /// @brief Creates a copy of the declaration renaming it adding
    /// the given suffix, and return the fresh new declaration.
    template<typename T>
    T* getSuffixedCopy(T* decl, const std::string& suffix);

    /// @brief Returns the actual library filename (which could be different
    /// from the library name set).
    virtual std::string getStandardFilename(Name n);

    /// @brief Returns the event method name w.r.t. current semantics.
    virtual Name getEventMethodName(const bool hifFormat = false) = 0;

    /// @brief Returns <tt>true</tt> if the given call is an event call w.r.t.
    /// the current semantics, <tt>false</tt> otherwise.
    virtual bool isEventCall(FunctionCall * call) = 0;

    /// @}

    bool useNativeSemantics() const;
    void setUseNativeSemantics(const bool b);

protected:

    ILanguageSemantics();

    /// @brief Factory.
    hif::HifFactory _factory;

    /// @brief Factory with semantics HIF (used to build standard hif library)
    hif::HifFactory _hifFactory;

    /// @brief Reference to name table.
    hif::NameTable * _nameT;

    /// @brief The semantics checks mode.
    bool _strictChecking;

    /// @brief Forbidden names.
    std::set< std::string > _forbiddenNames;

    /// @brief The semantic check options.
    SemanticOptions _semanticOptions;

    /// @name Standard packages
    /// @{

    /// @brief Wrapper for a string with possibility to add a prefix "hif_"
    std::string _makeHifName(const std::string& reqName, const bool hifFormat);

    /// @brief Wrapper for Enum creation with possibility to add a prefix "hif_".
    TypeDef * _makeEnum(const char * enumName,
                        const char * values[],
                        size_t size,
                        const bool hifFormat);

    /// @brief Create a SubProgram with at most one parameter.
    SubProgram * _makeAttribute(const std::string& n,
                                Type * retType,
                                Type * paramType,
                                Value * paramValue,
                                const bool unsupported,
                                const bool hifFormat);

    /// @brief Create a SubProgram with two parameters.
    SubProgram * _makeBinaryAttribute(const std::string& n,
                                      Type * retType,
                                      Type * param1Type,
                                      Value * param1Value,
                                      Type * param2Type,
                                      Value * param2Value,
                                      const bool unsupported,
                                      const bool hifFormat);

    /// @brief Create a SubProgram with two parameters.
    SubProgram * _makeTernaryAttribute(const std::string& n,
                                       Type * retType,
                                       Type * param1Type,
                                       Value * param1Value,
                                       Type * param2Type,
                                       Value * param2Value,
                                       Type * param3Type,
                                       Value * param3Value,
                                       const bool unsupported,
                                       const bool hifFormat);

    /// @brief Create a SubProgram parameter.
    void _makeAttributeParameter(SubProgram * scope,
                                 Type * paramType, Value * paramValue, const std::string& paramIndex,
                                 const bool hifFormat);


    void _addMultiparamFunction(LibraryDef * ld,
                                const char * name,
                                hif::HifFactory & factory,
                                const bool hifFormat, Type * ret);

    /// @brief Make an array of type @p t with span <tt>left downto right</tt>.
    /// @param index The suffix for left and right.
    /// @param t The array type.
    /// @return The built array.
    Array * _makeTemplateArray(const std::string & index, Type * t);

    /// @brief Check whether the given name is 'hif_' prefixed.
    bool _isHifPrefixed(Name n, std::string& unprefixed);

    /// @brief Create a StandardSymbols key.
    ILanguageSemantics::KeySymbol _makeKey(const char * library, const char * symbol);

    /// @brief Create a StandardSymbols value.
    ILanguageSemantics::ValueSymbol _makeValue(const char * library[],
                                               const unsigned int size,
                                               const char * symbol,
                                               ILanguageSemantics::MapCases action);

    /// @name Methods used by canRemoveCastOnOperands.
    /// @{

    virtual
    bool _checkConcatCasts(
            Expression * e,
            Type * castT1, Type * castT2,
            Type * subT1, Type * subT2,
            ExpressionTypeInfo & exprInfo,
            ExpressionTypeInfo & info);

    virtual
    bool _checkShiftCasts(
            Expression * e,
            Type * castT1, Type * castT2,
            Type * subT1, Type * subT2,
            ExpressionTypeInfo & exprInfo,
            ExpressionTypeInfo & info);

    virtual
    bool _checkArithmeticCasts(
            Expression * e,
            Type * castT1, Type * castT2,
            Type * subT1, Type * subT2,
            ExpressionTypeInfo & origInfo,
            ExpressionTypeInfo & simplifiedInfo);

    virtual
    bool _checkRelationalCasts(
            Expression * e,
            Type * castT1, Type * castT2,
            Type * subT1, Type * subT2,
            ExpressionTypeInfo & origInfo,
            ExpressionTypeInfo & simplifiedInfo);

    virtual
    bool _checkGenericCasts(
            Expression * e,
            Type * castT1, Type * castT2,
            Type * subT1, Type * subT2,
            ExpressionTypeInfo & origInfo,
            ExpressionTypeInfo & simplifiedInfo,
            const hif::EqualsOptions & precOpt,
            const hif::EqualsOptions & retOpt);

    /// @}

    /// @brief Map of standard symbols for this semantics.
    StandardSymbols _standardSymbols;

    /// @brief Map for standard library file names.
    StandardLibraryFiles _standardFilenames;

    /// @brief Forces to allow only native c++ types
    /// e.g. bitvectors are not allowed
    bool _useNativeSemantics;


private:

    ILanguageSemantics( const ILanguageSemantics & );
    ILanguageSemantics & operator = ( const ILanguageSemantics & );

};


}} // hif::semantics

std::ostream & operator << ( std::ostream &o,
                             const hif::semantics::ILanguageSemantics &	s);


#endif /* LANGUAGESEMANTICS_IF_H_ */
