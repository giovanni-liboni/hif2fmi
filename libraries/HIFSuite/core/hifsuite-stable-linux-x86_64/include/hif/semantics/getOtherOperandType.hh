#ifndef HIF_SEMANTICS_GETOTHEROPERANDTYPE_HH
#define HIF_SEMANTICS_GETOTHEROPERANDTYPE_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// Given a Value returns the type of the other operand involved in the operation.
/// @param o The starting value.
/// @param refSem The reference semantics.
/// @param considerOverloading <tt>true</tt> if type must be retrieved via context
/// checking in case of overloaded methods. Default is <tt>false</tt>.
/// @param looseTypeChecks if <tt>true</tt> don't raise typing errors.
/// This is userfull during parsing fixes since the tree could be not typable.
/// Default is <tt>false</tt>.
/// @return The other operand type, or NULL in case of error.
HIF_EXPORT
Type* getOtherOperandType(Object* o, ILanguageSemantics * refSem,
    const bool considerOverloading = false,
    const bool looseTypeChecks = false);

/// @brief Returns the direction of the other operand involved in the operation.
/// @param o The starting value.
/// @param sem The reference semantics.
/// @return The direction.
///
HIF_EXPORT
hif::RangeDirection getOtherOperandDirection(Object * o, ILanguageSemantics * sem);


}} // end hif::semantics

#endif
