#ifndef HIF_SEMANTICS_STANDARDIZEDESCRIPTION_HH
#define HIF_SEMANTICS_STANDARDIZEDESCRIPTION_HH

#include "../classes/classes.hh"
#include "../applicationUtils/applicationUtils.hh"

namespace hif { namespace semantics {

	/// @brief Function to get standardize an HIF description. This is the entry point
	/// for standardization. This function introduces the necessaries cast and then
	/// simply, where is possible, to obtain a standard HIF description.
	///
	/// @param root Object pointer to the root of the tree which have to be standardize.
	/// @param source_sem The semantics of destination language.
	/// @param dest_sem The semantics of source language.
    /// @param parentFileManager The parent step file manager if any.
	///
	HIF_EXPORT
    void standardizeDescription (
            System * & root,
            ILanguageSemantics * source_sem,
            ILanguageSemantics * dest_sem,
            hif::applicationUtils::StepFileManager * parentFileManager = NULL);
}}

#endif
