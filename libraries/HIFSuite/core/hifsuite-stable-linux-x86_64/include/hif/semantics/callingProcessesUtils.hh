#ifndef HIF_SEMANTICS_CALLINGPROCESSESUTILS_HH
#define HIF_SEMANTICS_CALLINGPROCESSESUTILS_HH

#include "../classes/classes.hh"
#include "referencesUtils.hh"

namespace hif { namespace semantics {

/// @brief The set of calling objects (processes and global actions).
typedef std::set<Object *> CallerSet;
/// @brief The map from sub programs to calling objects.
typedef std::map<SubProgram *, CallerSet> CallerMap;

/// @brief Given a sub program, returns a set of calling objects.
/// @param sub The sub program.
/// @param refMap The references map.
/// @param callerSet The resulting set.
/// @param sem The semantics.
/// @param root The possible sub tree for object search.
HIF_EXPORT
void findCallingProcesses(SubProgram * sub, ReferencesMap & refMap,
                          CallerSet & callerSet, ILanguageSemantics * sem,
                          Object * root = NULL);

/// @brief Given a root, returns a map from sub programs to related calling objects.
/// @param root The root.
/// @param refMap The references map.
/// @param callerMap The resulting map.
/// @param sem The semantics.
HIF_EXPORT
void findAllCallingProcesses(Object * root, ReferencesMap & refMap,
                             CallerMap & callerMap, ILanguageSemantics * sem);

}} // end hif::semantics

#endif
