#ifndef HIF_SEMANTICS_RESETTYPES_HH
#define HIF_SEMANTICS_RESETTYPES_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// @brief Reset the type calculated for every Value of the
/// subtree HIF with root <tt>root</tt>.
///
/// @param root of the subtree
/// @param recursive If true, reset types of sub nodes.
///
HIF_EXPORT
void resetTypes (Object *root, const bool recursive = true);

/// @brief Reset the type calculated for every Value of the
/// subtree HIF with root <tt>root</tt>.
///
/// @param root of the subtree
/// @param recursive If true, reset types of sub nodes.
///
HIF_EXPORT
void resetTypes (BList<Object> & root, const bool recursive = true);

/// @brief Reset the type calculated for every Value of the
/// subtree HIF with root <tt>root</tt>.
///
/// @param root of the subtree
/// @param recursive If true, reset types of sub nodes.
///
template<typename T>
void resetTypes (BList<T> & root, const bool recursive = true);


}} // end hif::semantics

#endif
