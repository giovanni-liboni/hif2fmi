#ifndef HIF_SEMANTICS_RANGEGETSUM_HH
#define HIF_SEMANTICS_RANGEGETSUM_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// Returns a new span that is the sum between the given spans
/// @p a and @p b.
///
/// @param a The first range to be compared.
/// @param b The second range to be compared.
/// @param refLang The reference semantics.
/// @return The span that is the sum between @p a and @p b.
///
HIF_EXPORT
Range* rangeGetSum (Range* a, Range* b, ILanguageSemantics* refLang );

}} // end hif::semantics

#endif
