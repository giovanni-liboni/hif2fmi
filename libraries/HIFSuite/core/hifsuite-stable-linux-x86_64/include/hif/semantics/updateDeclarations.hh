#ifndef HIF_SEMANTICS_UPDATEDECLARATIONS_HH
#define HIF_SEMANTICS_UPDATEDECLARATIONS_HH

#include "../classes/classes.hh"
#include "DeclarationOptions.hh"

namespace hif { namespace semantics {

/// @brief Options related to getCandidates() method.
struct HIF_EXPORT UpdateDeclarationOptions:
        public DeclarationOptions
{
    UpdateDeclarationOptions();
    virtual ~UpdateDeclarationOptions();

    UpdateDeclarationOptions(const UpdateDeclarationOptions& other);
    UpdateDeclarationOptions& operator=(DeclarationOptions other);
    UpdateDeclarationOptions& operator=(UpdateDeclarationOptions other);
    void swap(DeclarationOptions& other);
    void swap(UpdateDeclarationOptions& other);

    /// @brief If a declaration is visible from the current scope, update it.
    /// Other declarations will be kept.
    /// Default is false.
    bool onlyVisible;

    /// @brief If current set declaration is not a subnode of root object,
    /// the declaration will be not updated. Usefull only with onlyVisible option.
    /// Default is <tt>NULL</tt>.
    Object * root;
};

/// @brief Computes the declaration of objects in the subtree starting from
/// @p root according to provided options.
///
/// @param root The node from which to start the computation.
/// @param sem The reference semantics.
/// @param opt The options.
///
HIF_EXPORT
void updateDeclarations(Object  *root, ILanguageSemantics * sem,
                        const UpdateDeclarationOptions & opt = UpdateDeclarationOptions());

}} // end hif::semantics

#endif
