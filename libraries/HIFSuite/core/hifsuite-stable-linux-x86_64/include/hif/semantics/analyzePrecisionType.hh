#ifndef HIF_SEMANTICS_ANALYZEPRECISIONTYPE_HH
#define HIF_SEMANTICS_ANALYZEPRECISIONTYPE_HH

#include "../classes/classes.hh"
#include "analyzePrecisionType.hh"

namespace hif { namespace semantics {


/// @brief Struct representing input for analyzePrecisionType() function.
///
struct HIF_EXPORT AnalyzeParams
{
    typedef std::list<Type *> TypeList;
    AnalyzeParams();
    ~AnalyzeParams();
    AnalyzeParams(const AnalyzeParams& t);
    AnalyzeParams & operator = (const AnalyzeParams& t);

    /// @brief Operation to perform on operands.
    Operator operation;

    /// @brief List of Type pointer containing the type of the operands
    /// involved in the analyzed expression.
    TypeList operandsType;

    /// @brief Pointer to the original object to analyze.
    Object * startingObj;
};


/// @brief Function to get informations about eventual casts to do on
/// operands of an assignment, an aggregate and all the other objects
/// with multiple Value.
/// This function behaves like the analyzeExprType, but it uses the
/// precision instead of the returned type as intermediate value for the
/// computation. The operation considered is the equality.
/// @param params struct of AnalyzeParams type containing analysis
/// parameters. The operation is ignored. In this function is used
/// only the equality.
/// @param sem The semantics.
/// @return The result precision, NULL if cannot be determinated.
///
Type * analyzePrecisionType(const AnalyzeParams & params, ILanguageSemantics * sem);


/// @brief Utility wrapper to general analyzePrecisionType().
/// @param o The object.
/// @param sem The semantics.
/// @return The result precision, NULL if cannot be determinated.
///
Type * analyzePrecisionType(With * o, ILanguageSemantics * sem);

/// @brief Utility wrapper to general analyzePrecisionType().
/// @param o The object.
/// @param sem The semantics.
/// @return The result precision, NULL if cannot be determinated.
///
Type * analyzePrecisionType(Switch * o, ILanguageSemantics * sem);


}} // end hif::semantics

#endif
