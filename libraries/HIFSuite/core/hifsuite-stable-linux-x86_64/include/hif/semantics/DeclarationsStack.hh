#ifndef HIF_SEMANTICS_DECLARATIONSSTACK_HH
#define HIF_SEMANTICS_DECLARATIONSSTACK_HH

#include "../classes/classes.hh"

namespace hif { namespace semantics {

/// @brief This class represents a stack of declarations.
/// It works as a data structure to store the current snapshot
/// of declarations.
class HIF_EXPORT DeclarationsStack
{
public:

    typedef Object Symbol;
    typedef std::map<Symbol *, Declaration * > DeclarationsMap;
    typedef std::list<DeclarationsMap> Stack;

    /// @brief Constructor.
    /// @param root The root subtree from which to retrieve the snapshot of declarations.
    DeclarationsStack(Object * root);
    ~DeclarationsStack();

    /// @brief Inserts a snapshot of declarations.
    void push();

    /// @brief Removes a snapshot of declarations by restoring it.
    void pop();

private:

    Stack _stack;
    Object * _root;

    DeclarationsStack(const DeclarationsStack&);
    DeclarationsStack & operator = (const DeclarationsStack&);
};

}} // end hif::semantics

#endif
