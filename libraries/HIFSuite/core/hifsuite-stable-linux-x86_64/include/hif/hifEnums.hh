#ifndef HIF_LIBRARY_GLOBALS_H
#define HIF_LIBRARY_GLOBALS_H

#include <string>
#include "applicationUtils/portability.hh"

namespace hif {

/// @brief Range directions.
enum RangeDirection
{
    dir_upto,
    dir_downto
};

/// @brief Return string representation of given range direction.
/// @param t The range direction.
/// @return The string representation of given range direction.
HIF_EXPORT
std::string rangeDirectionToString(const RangeDirection t);

/// @brief Return the range direction for given string.
/// @param s The string.
/// @return The range direction for given string.
HIF_EXPORT
RangeDirection rangeDirectionFromString(const std::string & s);


/// @brief Port or parameter directions.
enum PortDirection
{
    dir_none,
    dir_in,
    dir_out,
    dir_inout
};

/// @brief Return string representation of given port direction.
/// @param t The port direction.
/// @return The string representation of given port direction.
HIF_EXPORT
std::string portDirectionToString(const PortDirection t);

/// @brief Return the port direction for given string.
/// @param s The string.
/// @return The port direction for given string.
HIF_EXPORT
PortDirection portDirectionFromString(const std::string & s);


/// @brief Bit values.
enum BitConstant
{
    bit_u,
    bit_x,
    bit_zero,
    bit_one,
    bit_z,
    bit_w,
    bit_l,
    bit_h,
    bit_dontcare
};

/// @brief Return string representation of given bit constant.
/// @param t The bit constant.
/// @return The string representation of given bit constant.
HIF_EXPORT
std::string bitConstantToString(const BitConstant t);

/// @brief Return the bit constant for given string.
/// @param s The string.
/// @return The bit constant for given string.
HIF_EXPORT
BitConstant bitConstantFromString(const std::string & s);


/// @brief All possible operators.
/// All operators starting with 'b' are bitwise operators.
enum Operator
{
    op_none,
    op_plus,
    op_minus,
    op_mult,
    op_div,
    op_or,
    op_bor,
    op_and,
    op_band,
    op_xor,
    op_bxor,
    op_not,
    op_bnot,
    op_eq,
    op_case_eq,
    op_neq,
    op_case_neq,
    op_le,
    op_ge,
    op_lt,
    op_gt,
    op_sll,
    op_srl,
    op_sla,
    op_sra,
    op_rol,
    op_ror,
    op_mod,
    op_rem,
    op_pow,
    op_abs,
    op_concat,
    op_ref,
    op_deref,
    op_andrd,
    op_orrd,
    op_xorrd,
    op_assign,
    op_log,
    op_conv,
    op_bind,
    op_reverse,
    op_size
};

/// @brief Return string representation of given operator.
/// @param t The operator.
/// @return The string representation of given operator.
HIF_EXPORT
std::string operatorToString(const Operator t);

/// @brief Return string representation given operator enum name.
/// E.g. op_and returns "op_and".
/// @param t The operator.
/// @param prefix The optional prefix.
/// @param suffix The optional suffix.
/// @return The string representation of given operator.
HIF_EXPORT
std::string operatorToPlainString(const Operator t,
                                  const std::string & prefix = "",
                                  const std::string & suffix = "");

/// @brief Return the operator for given string.
/// @param s The string.
/// @return The operator for given string.
HIF_EXPORT
Operator operatorFromString(const std::string & s);

/// @brief Return the operator for given string.
/// E.g. "<prefix>op_and" returns op_and.
/// @param s The string.
/// @param prefix The optional prefix.
/// @param suffix The optional suffix.
/// @return The operator for given string.
HIF_EXPORT
Operator operatorFromPlainString(const std::string & s,
                                 const std::string & prefix = "",
                                 const std::string & suffix = "");

/// This enum represent the semantics of a process. It is used in a flag of the
/// StateTable.
/// 
/// pf_hdl is a vhdl like process. Its sensitivity determines when the process
/// is awakened. The values for its local variables are mantained across
/// different awakenings.
///
/// pf_method is a sc_method like process. It is like pt_hdl process, exept for
/// the local variables. They are automatic variables inside a function,
/// therefore their values are not mantained across different awakenings.
///
/// pf_thread is a sc_thread like. The execution of a thread starts at the
/// beginning of the simulation, and when the end of the method is reached, the
/// thread stops forever.
///
/// pf_initial is a verilog initial process. It is executed only once when the
/// simulation starts, typically for initialization purpose.
///
/// pf_analog is a analog process (i.e. verilog AMS process).
///
enum ProcessFlavour
{
    pf_method,  ///@< like sc_method
    pf_thread,  ///@< like sc_thread
    pf_hdl,     ///@< like vhdl and verilog process
    pf_initial, ///@< verilog initial block
    pf_analog   ///@< AMS analog processes
};

/// @brief Return string representation of given process flavour.
/// @param t The process flavour.
/// @return The string representation of given process flavour.
HIF_EXPORT
std::string processFlavourToString(const ProcessFlavour t);

/// @brief Return the process flavour for given string.
/// @param s The string.
/// @return The process flavour for given string.
HIF_EXPORT
ProcessFlavour processFlavourFromString(const std::string & s);


/// @brief The language of the design.
enum LanguageID
{
    rtl,
    tlm,
    cpp,
    c,
    psl,
    ams
};

/// @brief Return string representation of given language ID.
/// @param t The language ID.
/// @return The string representation of given language ID.
HIF_EXPORT
std::string languageIDToString(const LanguageID t);

/// @brief Return the language ID for given string.
/// @param s The string.
/// @return The language ID for given string.
HIF_EXPORT
LanguageID languageIDFromString(const std::string & s);

/// @brief List of pre-defined properties.
enum PropertyId
{
    /// Used to specify whether an Instance is used only as configuration flag.
    PROPERTY_CONFIGURATION_FLAG,
    /// Used to force the print of the arrow operator in C++.
    PROPERTY_TLM_FORCEARROW,
    /// Used to specify a macro (usually, define) required by the object.
    /// Syntax changes with respect to the considered language, e.g.
    /// Verilog: "`define"
    /// SystemC: "#define"
    PROPERTY_REQUIRED_MACRO,
    /// Special case for macros required by the object (header).
    PROPERTY_REQUIRED_MACRO_HH,
    /// Special case for macros required by the object (implementation).
    PROPERTY_REQUIRED_MACRO_CC,
    /// Used to specify an unsupported construct.
    PROPERTY_UNSUPPORTED,
    /// Used to specify an constexpr call.
    PROPERTY_CONSTEXPR,
    /// Used to specify standard calls with internal array access.
    PROPERTY_METHOD_EXPLICIT_PARAMETERS,
    /// Used to specify that object is a temporary.
    PROPERTY_TEMPORARY_OBJECT,
    /// Used to specify the original bitwidth of a type before manipulations
    PROPERTY_ORIGINAL_BITWIDTH
};

HIF_EXPORT
const char * getPropertyName(const PropertyId id);

/// @brief The id of leaf objects
enum ClassId
{
    CLASSID_AGGREGATEALT,
    CLASSID_AGGREGATE,
    CLASSID_ALIAS,
    CLASSID_ARRAY,
    CLASSID_ASSIGN,
    CLASSID_BIT,
    CLASSID_BITVALUE,
    CLASSID_BITVECTOR,
    CLASSID_BITVECTORVALUE,
    CLASSID_BOOL,
    CLASSID_BOOLVALUE,
    CLASSID_BREAK,
    CLASSID_CAST,
    CLASSID_CHAR,
    CLASSID_CHARVALUE,
    CLASSID_CONST,
    CLASSID_CONTENTS,
    CLASSID_CONTINUE,
    CLASSID_DESIGNUNIT,
    CLASSID_ENTITY,
    CLASSID_ENUM,
    CLASSID_ENUMVALUE,
    CLASSID_EVENT,
    CLASSID_EXPRESSION,
    CLASSID_FIELD,
    CLASSID_FIELDREFERENCE,
    CLASSID_FILE,
    CLASSID_FORGENERATE,
    CLASSID_FOR,
    CLASSID_FUNCTIONCALL,
    CLASSID_FUNCTION,
    CLASSID_GLOBALACTION,
    CLASSID_IDENTIFIER,
    CLASSID_IFALT,
    CLASSID_IFGENERATE,
    CLASSID_IF,
    CLASSID_INSTANCE,
    CLASSID_INT,
    CLASSID_INTVALUE,
    CLASSID_LIBRARYDEF,
    CLASSID_LIBRARY,
    CLASSID_MEMBER,
    CLASSID_NULL,
    CLASSID_PARAMETERASSIGN,
    CLASSID_PARAMETER,
    CLASSID_POINTER,
    CLASSID_PORTASSIGN,
    CLASSID_PORT,
    CLASSID_PROCEDURECALL,
    CLASSID_PROCEDURE,
    CLASSID_RANGE,
    CLASSID_REAL,
    CLASSID_REALVALUE,
    CLASSID_RECORD,
    CLASSID_RECORDVALUEALT,
    CLASSID_RECORDVALUE,
    CLASSID_REFERENCE,
    CLASSID_RETURN,
    CLASSID_SIGNAL,
    CLASSID_SIGNED,
    CLASSID_SLICE,
    CLASSID_STATE,
    CLASSID_STATETABLE,
    CLASSID_STRING,
    CLASSID_STRINGVALUE,
    CLASSID_SWITCHALT,
    CLASSID_SWITCH,
    CLASSID_SYSTEM,
    CLASSID_TIME,
    CLASSID_TIMEVALUE,
    CLASSID_TRANSITION,
    CLASSID_TYPEDEF,
    CLASSID_TYPEREFERENCE,
    CLASSID_TYPETPASSIGN,
    CLASSID_TYPETP,
    CLASSID_UNSIGNED,
    CLASSID_VALUESTATEMENT,
    CLASSID_VALUETPASSIGN,
    CLASSID_VALUETP,
    CLASSID_VARIABLE,
    CLASSID_VIEW,
    CLASSID_VIEWREFERENCE,
    CLASSID_WAIT,
    CLASSID_WHENALT,
    CLASSID_WHEN,
    CLASSID_WHILE,
    CLASSID_WITHALT,
    CLASSID_WITH
};

HIF_EXPORT
std::string classIDToString(const ClassId id);


/// @brief The kind of matching inside Switch and With
enum CaseSemantics
{
    /// @brief Strict matching on each bit.
    CASE_LITERAL,
    /// @brief As casez of Verilog.
    CASE_Z,
    /// @brief As casex of Verilog.
    CASE_X
};

HIF_EXPORT
std::string caseSemanticsToString(const CaseSemantics c);

HIF_EXPORT
CaseSemantics caseSemanticsFromString(const std::string & c);

} // namespace hif

#endif
