#ifndef HIF_DECLARATION_VISITOR_HH
#define HIF_DECLARATION_VISITOR_HH

#include "GuideVisitor.hh"
#include "hif/features/ISymbol.hh"
#include "hif/semantics/ILanguageSemantics.hh"
#include "hif/semantics/referencesUtils.hh"

namespace hif {

struct HIF_EXPORT DeclarationVisitorOptions
{
    /// @brief Visit declarations once time. Default is false.
    bool visitDeclarationsOnce;
    /// @brief Visit symbols once time. Default is false.
    bool visitSymbolsOnce;
    /// @brief Visit references after visit of declarations.
    bool visitReferencesAfterDeclaration;
    /// @brief The reference map.
    hif::semantics::ReferencesMap * refsMap;
    /// @brief The semantics
    hif::semantics::ILanguageSemantics * sem;

    DeclarationVisitorOptions();
    ~DeclarationVisitorOptions();
    DeclarationVisitorOptions(const DeclarationVisitorOptions & o);
    DeclarationVisitorOptions & operator = (DeclarationVisitorOptions o);
    void swap(DeclarationVisitorOptions & o);
};

/// @brief This visitor can be used when is necessary to fix the declaration
/// before the relative symbol.
class HIF_EXPORT DeclarationVisitor :
        public GuideVisitor
{
public:
    typedef std::set<Declaration*> DeclarationSet;
    typedef std::set<Object*> SymbolSet;

    DeclarationVisitor(const DeclarationVisitorOptions & opt, System * root = NULL);
    virtual ~DeclarationVisitor();

    /// @name Common virtual methods
    /// @{

    /// @brief Function to visit the declarations.
    /// @param o The declaration.
    /// @return <tt>true</tt> if visited, <tt>false</tt> otherwise.
    virtual int visitDeclaration(Declaration &o);
    /// @brief Function to visit the data declarations.
    /// @param o The data declaration.
    /// @return <tt>true</tt> if visited, <tt>false</tt> otherwise.
    virtual int visitDataDeclaration(DataDeclaration &o);
    /// @brief Function to visit the symbols.
    /// @param o The symbol.
    /// @return rv The return value of the symbol declaration visit.
    virtual int visitSymbol(hif::features::ISymbol &o);
    /// @brief This method is called after declarations and call the visitor
    /// on all declaration references w.r.t. <tt>visitReferencesAfterDeclaration</tt>
    /// option.
    /// @param o The declaration.
    /// @return The <tt>or</tt> of results of visitor called on all references.
    virtual int visitReferences(Declaration &o);

    /// @}

    /// @name Symbols visits
    /// @{

    virtual int visitFieldReference(FieldReference &o);
    virtual int visitFunctionCall(FunctionCall &o);
    virtual int visitIdentifier(Identifier &o);
    virtual int visitInstance(Instance &o);
    virtual int visitLibrary(Library &o);
    virtual int visitParameterAssign(ParameterAssign &o);
    virtual int visitPortAssign(PortAssign &o);
    virtual int visitProcedureCall(ProcedureCall &o);
    virtual int visitTypeReference(TypeReference &o);
    virtual int visitTypeTPAssign(TypeTPAssign &o);
    virtual int visitValueTPAssign(ValueTPAssign &o);
    virtual int visitViewReference(ViewReference &o);

    /// @}

    /// @name Declarations visits
    /// @{

    virtual int visitContents(Contents &o);
    virtual int visitDesignUnit(DesignUnit &o);
    virtual int visitFunction(Function &o);
    virtual int visitEntity(Entity &o);
    virtual int visitForGenerate(ForGenerate &o);
    virtual int visitIfGenerate(IfGenerate &o);
    virtual int visitLibraryDef(LibraryDef &o);
    virtual int visitProcedure(Procedure &o);
    virtual int visitState(State &o);
    virtual int visitStateTable(StateTable &o);
    virtual int visitTypeDef(TypeDef &o);
    virtual int visitTypeTP(TypeTP &o);
    virtual int visitView(View &o);
    virtual int visitSystem(System &o);

    /// @}

    /// @name DataDeclarations visits
    /// @{

    virtual int visitAlias(Alias &o);
    virtual int visitConst(Const &o);
    virtual int visitEnumValue(EnumValue &o);
    virtual int visitField(Field &o);
    virtual int visitParameter(Parameter &o);
    virtual int visitPort(Port &o);
    virtual int visitSignal(Signal &o);
    virtual int visitValueTP(ValueTP &o);
    virtual int visitVariable(Variable &o);

    /// @}


protected:

    /// @brief This method is a wrapper to getDeclaration that can be overridden
    /// by the user.
    virtual Declaration * _getDeclaration(Object * o);

    /// @brief Checks if given symbol is already visited w.r.t.
    /// <tt>visitSymbolsOnce</tt> option.
    /// @param o The symbol.
    /// @return <tt>true</tt> if symbols was not already visited, <tt>false</tt> otherwise.
    virtual bool _checkSymbol(hif::features::ISymbol &o);
    /// @brief Checks if given declaration is already visited w.r.t.
    /// <tt>visitDeclarationsOnce</tt> option.
    /// @param o The declaration.
    /// @return <tt>true</tt> if declaration was not already visited, <tt>false</tt> otherwise.
    virtual bool _checkDeclaration(Declaration &o);

    DeclarationVisitorOptions _opt;
private:
    DeclarationSet _visitedDeclarations;
    SymbolSet _visitedSymbols;

    DeclarationVisitor (const DeclarationVisitor &);
    DeclarationVisitor & operator = (const DeclarationVisitor &);
};

}

#endif // HIF_DECLARATION_VISITOR_HH
