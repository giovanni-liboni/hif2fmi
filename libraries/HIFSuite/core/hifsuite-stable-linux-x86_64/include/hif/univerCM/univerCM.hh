#ifndef HIF_UNIVERCM_HH
#define HIF_UNIVERCM_HH

#include "../applicationUtils/portability.hh"
#include "../classes/LibraryDef.hh"

// ////////////////////////////////////////////
// FORWARD DECLARATIONS
// ////////////////////////////////////////////

namespace hif {

} // Hif


// ////////////////////////////////////////////
// ACTUAL PACKAGE
// ////////////////////////////////////////////

namespace hif {
/// @brief Wraps methods realted to univerCM.
namespace univerCM {

/// @brief Returns the univerCM LibraryDef, according with HIF semantics.
/// @return The LibraryDef.
HIF_EXPORT
LibraryDef * getUniverCMPackage();

} } // hif::univerCM

#endif // HIF_UNIVERCM_HH
