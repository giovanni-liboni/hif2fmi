#ifndef HIF_VHDL_STD_TEXTIO_I_HH
#define HIF_VHDL_STD_TEXTIO_I_HH

#include "../hif_vhdl_std_textio.hh"

namespace hif_vhdl_std_textio {

#ifdef HIF2SCSUPPORT_USE_HDTLIB

template<int W>
void hif_vhdl_read(hif_vhdl_line & l, hdtlib::hl_bv_t<W> & value, bool & good)
{
    sc_dt::sc_bv<W> tmp = value.to_string().c_str();
    hif_vhdl_read(l, tmp, good);
    value = tmp.to_string().c_str();
}

template<int W>
void hif_vhdl_read(hif_vhdl_line & l, hdtlib::hl_bv_t<W> & value)
{
    bool good;
    hif_vhdl_read(l, value, good);
}

template<int W>
void hif_vhdl_write(hif_vhdl_line & l,
                    hdtlib::hl_bv_t<W> value,
                    hif_vhdl_side justified,
                    hif_vhdl_width field)
{
    sc_dt::sc_bv<W> tmp = value.to_string().c_str();
    hif_vhdl_write(l, tmp, justified, field);
}

#endif

} // hif_vhdl_std_textio

#endif
