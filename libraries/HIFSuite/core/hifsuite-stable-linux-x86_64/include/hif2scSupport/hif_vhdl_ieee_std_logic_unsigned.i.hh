#ifndef HIF_VHDL_IEEE_STD_LOGIC_UNSIGNED_I_HH
#define HIF_VHDL_IEEE_STD_LOGIC_UNSIGNED_I_HH

#include "../hif_vhdl_ieee_std_logic_unsigned.hh"
#include "../hif_vhdl_ieee_std_logic_arith.hh"

namespace hif_vhdl_ieee_std_logic_unsigned {

// /////////////////////////////////////////////////////////////////////////////
// Shift operators
// /////////////////////////////////////////////////////////////////////////////

template <int size1, int size2>
sc_dt::sc_lv<size1> hif_vhdl_shl(sc_dt::sc_lv<size1> arg, sc_dt::sc_lv<size2> count)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl_shl(arg, count);
}

template <int size1, int size2>
sc_dt::sc_lv<size1> hif_vhdl_shr(sc_dt::sc_lv<size1> arg, sc_dt::sc_lv<size2> count)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl_shr_unsigned(arg, count);
}

#ifdef HIF2SCSUPPORT_USE_HDTLIB

template <int size1, int size2>
hdtlib::hl_lv_t<size1> hif_vhdl_shl(hdtlib::hl_lv_t<size1> arg, hdtlib::hl_lv_t<size2> count)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl_shl(arg, count);
}

template <int size1, int size2>
hdtlib::hl_lv_t<size1> hif_vhdl_shr(hdtlib::hl_lv_t<size1> arg, hdtlib::hl_lv_t<size2> count)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl_shr_unsigned(arg, count);
}

#endif // HIF2SCSUPPORT_USE_HDTLIB


// /////////////////////////////////////////////////////////////////////////////
// Conversion operators
// /////////////////////////////////////////////////////////////////////////////

template <int size >
long long int hif_vhdl_conv_integer(sc_dt::sc_lv<size> arg)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl_conv_integer_unsigned(arg);
}

#ifdef HIF2SCSUPPORT_USE_HDTLIB

template <int size >
long long int hif_vhdl_conv_integer(hdtlib::hl_lv_t<size> arg)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl_conv_integer_unsigned(arg);
}

#endif // HIF2SCSUPPORT_USE_HDTLIB


// /////////////////////////////////////////////////////////////////////////////
// Relational operators
// /////////////////////////////////////////////////////////////////////////////

template<int size>
bool hif_vhdl__op_eq(const sc_dt::sc_lv<size> & v1, const sc_dt::sc_lv<size> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_eq_unsigned(v1, v2);
}

template<int size>
bool hif_vhdl__op_neq(const sc_dt::sc_lv<size> & v1, const sc_dt::sc_lv<size> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_neq_unsigned(v1, v2);
}

#ifdef HIF2SCSUPPORT_USE_HDTLIB

template<int size>
bool hif_vhdl__op_eq(const hdtlib::hl_lv_t<size> & v1, const hdtlib::hl_lv_t<size> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_eq_unsigned(v1, v2);
}

template<int size>
bool hif_vhdl__op_neq(const hdtlib::hl_lv_t<size> & v1, const hdtlib::hl_lv_t<size> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_neq_unsigned(v1, v2);
}

#endif // HIF2SCSUPPORT_USE_HDTLIB


// /////////////////////////////////////////////////////////////////////////////
// Arithmetic operators
// /////////////////////////////////////////////////////////////////////////////

template<int size>
sc_dt::sc_lv<size> hif_vhdl__op_plus(const sc_dt::sc_lv<size> & v1, const sc_dt::sc_lv<size> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_plus_unsigned(v1, v2);
}

template<int size>
sc_dt::sc_lv<size> hif_vhdl__op_minus(const sc_dt::sc_lv<size> & v1, const sc_dt::sc_lv<size> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_minus_unsigned(v1, v2);
}

template<int size1, int size2>
sc_dt::sc_lv<size1+size2> hif_vhdl__op_mult(const sc_dt::sc_lv<size1> & v1, const sc_dt::sc_lv<size2> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_mult_unsigned(v1, v2);
}

#ifdef HIF2SCSUPPORT_USE_HDTLIB

template<int size>
hdtlib::hl_lv_t<size> hif_vhdl__op_plus(const hdtlib::hl_lv_t<size> & v1, const hdtlib::hl_lv_t<size> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_plus_unsigned(v1, v2);
}

template<int size>
hdtlib::hl_lv_t<size> hif_vhdl__op_minus(const hdtlib::hl_lv_t<size> & v1, const hdtlib::hl_lv_t<size> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_minus_unsigned(v1, v2);
}

template<int size1, int size2>
hdtlib::hl_lv_t<size1+size2> hif_vhdl__op_mult(const hdtlib::hl_lv_t<size1> & v1, const hdtlib::hl_lv_t<size2> & v2)
{
    return hif_vhdl_ieee_std_logic_arith::hif_vhdl__op_mult_unsigned(v1, v2);
}

#endif // HIF2SCSUPPORT_USE_HDTLIB

} // hif_vhdl_ieee_std_logic_unsigned

#endif // HIF_VHDL_IEEE_STD_LOGIC_UNSIGNED__I_HH
