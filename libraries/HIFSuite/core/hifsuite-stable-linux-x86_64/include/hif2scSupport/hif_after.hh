#ifndef HIF_SYSTEMC_EXTENSIONS_HIF_AFTER_HH
#define HIF_SYSTEMC_EXTENSIONS_HIF_AFTER_HH

#include <systemc>

namespace hif_systemc_extensions {

template<class Target, class Value>
class HifAfter
{
public:

    HifAfter(Target & t, const Value v);
    ~HifAfter();
    HifAfter(const HifAfter<Target, Value> & other);
    HifAfter<Target, Value> & operator =(const HifAfter<Target, Value> & other);
    void swap(HifAfter<Target, Value> & other);

    void operator () ();

    sc_core::sc_event * event;

private:

    Target * _target;
    Value _value;

};


template<typename Target, typename Value>
void hif_after(Target & target, const Value v, const sc_core::sc_time & delay);


} // hif_systemc_extensions

#include "hif_after.i.hh"

#endif // HIF_SYSTEMC_EXTENSIONS_HIF_AFTER_HH
