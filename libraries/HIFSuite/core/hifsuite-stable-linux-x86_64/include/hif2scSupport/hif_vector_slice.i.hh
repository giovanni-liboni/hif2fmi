#ifndef HIF_SYSTEMC_EXTENSIONS_HIF_VECTOR_SLICE_I_HH
#define HIF_SYSTEMC_EXTENSIONS_HIF_VECTOR_SLICE_I_HH

#include "hif_assign.hh"
#include <vector>

namespace hif_systemc_extensions {

template< typename T >
std::vector<T>
hif_vector_slice(const sc_core::sc_vector< sc_core::sc_signal<T> > & array,
                 const int left, const int right)
{
    assert(right >= 0);
    assert(right <= left);
    assert(left < array.size());
    std::vector<T> ret;

    for (int i = right; i <= left; ++i)
    {
        ret.push_back(array[i].read());
    }

    return ret;
}

template< typename T >
std::vector<T>
hif_vector_slice(const sc_core::sc_vector< sc_core::sc_in<T> > & array,
                  const int left, const int right)
{
    assert(right >= 0);
    assert(right <= left);
    assert(left < array.size());
    std::vector<T> ret;

    for (int i = right; i <= left; ++i)
    {
        ret.push_back(array[i].read());
    }

    return ret;
}


template< typename T >
std::vector<T>
hif_vector_slice(const sc_core::sc_vector< sc_core::sc_out<T> > & array,
                 const int left, const int right)
{
    assert(right >= 0);
    assert(right <= left);
    assert(left < array.size());
    std::vector<T> ret;

    for (int i = right; i <= left; ++i)
    {
        ret.push_back(array[i].read());
    }

    return ret;
}

template< typename T >
std::vector<T>
hif_vector_slice(const sc_core::sc_vector< sc_core::sc_inout<T> > & array,
                 const int left, const int right)
{
    assert(right >= 0);
    assert(right <= left);
    assert(left < array.size());
    std::vector<T> ret;

    for (int i = right; i <= left; ++i)
    {
        ret.push_back(array[i].read());
    }

    return ret;
}


template<typename T>
std::vector<T>
hif_vector_slice(const std::vector< T > & array,
                    const int left, const int right)
{
    assert(right >= 0);
    assert(right <= left);
    assert(left < array.size());
    std::vector<T> ret;

    for (int i = right; i <= left; ++i)
    {
        ret.push_back(array[i]);
    }

    return ret;
}

template<typename T>
std::vector< std::vector< T > >
hif_vector_slice(const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_signal<T> > > & array,
                    const int left, const int right)

{
    assert(right >= 0);
    assert(right <= left);
    assert(left < array.size());
    std::vector< std::vector< T > > ret;

    for (int i = right; i <= left; ++i)
    {
        ret.push_back(hif_vector_slice(array[i], 0, array[i].size() -1));
    }

    return ret;
}

template<typename T>
std::vector< std::vector< T > >
hif_vector_slice(const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_in<T> > > & array,
                    const int left, const int right)

{
    assert(right >= 0);
    assert(right <= left);
    assert(left < array.size());
    std::vector< std::vector< T > > ret;

    for (int i = right; i <= left; ++i)
    {
        ret.push_back(hif_vector_slice(array[i], 0, array[i].size() -1));
    }

    return ret;
}

template<typename T>
std::vector< std::vector< T > >
hif_vector_slice(const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_out<T> > > & array,
                    const int left, const int right)

{
    assert(right >= 0);
    assert(right <= left);
    assert(left < array.size());
    std::vector< std::vector< T > > ret;

    for (int i = right; i <= left; ++i)
    {
        ret.push_back(hif_vector_slice(array[i], 0, array[i].size() -1));
    }

    return ret;
}

template<typename T>
std::vector< std::vector< T > >
hif_vector_slice(const sc_core::sc_vector< sc_core::sc_vector< sc_core::sc_inout<T> > > & array,
                    const int left, const int right)
{
    assert(right >= 0);
    assert(right <= left);
    assert(left < array.size());
    std::vector< std::vector< T > > ret;

    for (int i = right; i <= left; ++i)
    {
        ret.push_back(hif_vector_slice(array[i], 0, array[i].size() -1));
    }

    return ret;
}


template<typename T>
T *
hif_vector_slice(T* array,
                    const int /*left*/, const int right)
{
    assert(right >= 0);
    return array + right;
}

}

#endif // HIF_SYSTEMC_EXTENSIONS_HIF_VECTOR_SLICE_I_HH
