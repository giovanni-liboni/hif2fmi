#ifndef HIF_VHDL_IEEE_STD_LOGIC_1164_I_HH
#define HIF_VHDL_IEEE_STD_LOGIC_1164_I_HH

#include "../hif_vhdl_ieee_std_logic_1164.hh"

namespace hif_vhdl_ieee_std_logic_1164 {

template <int size>
sc_dt::sc_bv<size> hif_vhdl_to_bitvector(const sc_dt::sc_lv<size> & s, const bool xmap)
{
    std::string sVal = s.to_string();
    for (std::string::size_type i = 0; i < sVal.length(); ++i)
    {
        if (sVal[i] == '1' || sVal[i] == '0') continue;
        sVal[i] = xmap ? '1' : '0';
    }
    return sc_dt::sc_bv<size>(sVal.c_str());
}

template <int size>
sc_dt::sc_lv<size> hif_vhdl_to_x01(const sc_dt::sc_lv<size> & s)
{
    std::string sVal = s.to_string();
    for (std::string::size_type i = 0; i < sVal.length(); ++i)
    {
        if (sVal[i] == '1' || sVal[i] == '0') continue;
        sVal[i] = 'X';
    }
    return sc_dt::sc_lv<size>(sVal.c_str());
}

template <int size>
sc_dt::sc_lv<size> hif_vhdl_to_ux01(const sc_dt::sc_lv<size> & s)
{
    return hif_vhdl_to_x01(s);
}


#ifdef HIF2SCSUPPORT_USE_HDTLIB
template <int size>
hdtlib::hl_logic_t hif_vhdl_resolved(const hdtlib::hl_lv_t<size> & s)
{
    sc_dt::sc_lv<size> tmp = s.to_string().c_str();
    sc_dt::sc_logic tmpRes = hif_vhdl_resolved(tmp);
    hdtlib::hl_logic_t res = tmpRes.to_char();
    return res;
}


template <int size>
hdtlib::hl_bv_t<size> hif_vhdl_to_bitvector(const hdtlib::hl_lv_t<size> & s, const bool /*xmap*/)
{
    sc_dt::sc_lv<size> tmp = s.to_string().c_str();
    sc_dt::sc_bv<size> tmpRes = hif_vhdl_to_bitvector(tmp);
    hdtlib::hl_bv_t<size> res = tmpRes.to_string().c_str();
    return res;
}

template <int size>
hdtlib::hl_lv_t<size> hif_vhdl_to_x01(const hdtlib::hl_lv_t<size> & s)
{
    sc_dt::sc_lv<size> tmp = s.to_string().c_str();
    sc_dt::sc_lv<size> tmpRes = hif_vhdl_to_x01(tmp);
    hdtlib::hl_lv_t<size> res = tmpRes.to_string().c_str();
    return res;
}


template <int size>
hdtlib::hl_lv_t<size> hif_vhdl_to_ux01(const hdtlib::hl_lv_t<size> & s)
{
    sc_dt::sc_lv<size> tmp = s.to_string().c_str();
    sc_dt::sc_lv<size> tmpRes = hif_vhdl_to_ux01(tmp);
    hdtlib::hl_lv_t<size> res = tmpRes.to_string().c_str();
    return res;
}

template <int size>
bool hif_vhdl_is_x(const hdtlib::hl_lv_t<size> s)
{
    return ! s.is_01();
}

#endif


} // hif_vhdl_ieee_std_logic_1164

#endif // HIF_VHDL_IEEE_STD_LOGIC_1164__I_HH
