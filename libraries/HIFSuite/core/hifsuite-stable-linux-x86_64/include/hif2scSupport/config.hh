// /////////////////////////////////////////////////////////////
// Copyright (C) 2013
// EDALab S.r.l.
// All rights reserved.
//
// This file is part of HIF2SCSUPPORT.
//
// HIF2SCSUPPORT is closed non-free software:
// you can use it only with written license permission.
// If you have not a license, please delete this file.
//
// HIF2SCSUPPORT is distributed WITHOUT ANY WARRANTY.
// See the License for more details.
//
// You should have received a copy of the License along with
// HIF2SCSUPPORT, in a file named LICENSE.txt.
// /////////////////////////////////////////////////////////////

#ifndef HIF2SCSUPPORT_CONFIG_HH
#define HIF2SCSUPPORT_CONFIG_HH

#if (defined _MSC_VER)

    #if (defined COMPILE_HIF2SCSUPPORT_LIB)
        // Compiling dynamic
        #define HIF2SCSUPPORT_DEVICE
        #define HIF2SCSUPPORT_EXPORT __declspec(dllexport)
        #define HIF2SCSUPPORT_STL_EXTERN
        #define HIF2SCSUPPORT_STL_EXPORT __declspec(dllexport)
    #elif (defined USE_HIF2SCSUPPORT_LIB)
        // Linking dynamic
        #define HIF2SCSUPPORT_DEVICE
        #define HIF2SCSUPPORT_EXPORT __declspec(dllimport)
        #define HIF2SCSUPPORT_STL_EXTERN extern
        #define HIF2SCSUPPORT_STL_EXPORT __declspec(dllimport)
    #else
        // Compiling/linking static
        #define HIF2SCSUPPORT_DEVICE
        #define HIF2SCSUPPORT_EXPORT
        #define HIF2SCSUPPORT_STL_EXTERN
        #define HIF2SCSUPPORT_STL_EXPORT
    #endif

#else // LINUX
	#define HIF2SCSUPPORT_DEVICE
	#define HIF2SCSUPPORT_EXPORT __attribute__ ((visibility ("default")))
#endif

#if (defined _MSC_VER)
#pragma warning(push)
#pragma warning(disable:4127)
#pragma warning(disable:4512)
#endif


#ifdef HIF2SCSUPPORT_USE_HDTLIB
#include <hdtlib.hh>
#endif
//#ifdef HIF_USE_SYSTEMC
#include <stdint.h>
#include <systemc>
//#endif


#if (defined _MSC_VER)
#pragma warning(pop)
#endif

#if __cplusplus >= 201103L
#define HIF_CONSTEXPR constexpr
#else
#define HIF_CONSTEXPR
#endif

#endif /* HIF2SCSUPPORT_CONFIG_HH */
