#ifndef HIF_SYSTEMC_EXTENSIONS_HIF_AFTER_I_HH
#define HIF_SYSTEMC_EXTENSIONS_HIF_AFTER_I_HH

#include <sysc/kernel/sc_dynamic_processes.h>
#include "hif_after.hh"

namespace hif_systemc_extensions {

// ///////////////////////////////////////////////////////////////////
// HifAfter class
// ///////////////////////////////////////////////////////////////////

template<class Target, class Value>
HifAfter<Target, Value>::HifAfter(Target & t, const Value v):
    event(new sc_core::sc_event()),
    _target(&t),
    _value(v)
{
    // ntd
}


template<class Target, class Value>
HifAfter<Target, Value>::~HifAfter()
{
    delete event;
}

template<class Target, class Value>
HifAfter<Target, Value>::HifAfter(const HifAfter<Target, Value> & other):
    event(other.event),
    _target(other._target),
    _value(other._value)
{
    // move semantics
    HifAfter<Target, Value> * tmp = const_cast<HifAfter<Target, Value>* >(&other);
    tmp->event = NULL;
}

template<class Target, class Value>
HifAfter<Target, Value> &HifAfter<Target, Value>::operator =(const HifAfter<Target, Value> & other)
{
    HifAfter tmp(other);
    swap(tmp);
    return *this;
}

template<class Target, class Value>
void HifAfter<Target, Value>::swap(HifAfter<Target, Value> & other)
{
    std::swap(event, other.event);
    std::swap(_target, other._target);
    std::swap(_value, other._value);
}

template<class Target, class Value>
void HifAfter<Target, Value>::operator ()()
{
    *_target = _value;
}

// ///////////////////////////////////////////////////////////////////
// hif_after()
// ///////////////////////////////////////////////////////////////////

template<typename Target, typename Value>
void hif_after(Target & target, const Value v, const sc_core::sc_time & delay)
{
    HifAfter<Target, Value> * func = new HifAfter<Target, Value>(target, v);
    sc_core::sc_event * event = func->event;
    sc_core::sc_spawn_options opt;
    opt.set_stack_size(0x40000);
    opt.dont_initialize();
    opt.set_sensitivity(event);
    sc_core::sc_spawn(*func, NULL, &opt);
    event->notify(delay);
}

} // hif_systemc_extensions

#endif // HIF_SYSTEMC_EXTENSIONS_HIF_AFTER_I_HH
