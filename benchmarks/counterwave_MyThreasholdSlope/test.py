import subprocess
import decimal
import numpy
import os
import os.path
import time


def drange(x, y, jump):
    while x < y:
        yield float(x)
        x += decimal.Decimal(jump)


working_dir = "/home/deantoni/projects/FMI-dev/benchmarks/counterwave_MyThresholdSlope/FMI-dev/build/bin"
results_dir = "/home/deantoni/projects/FMI-dev/benchmarks/counterwave_MyThresholdSlope/results/07-04-2017/"

start_time = time.time()
tEnd = 10
program_name = "sim_counterwave_conditional_MyThreasholdSlope_"
methods = ["fidel_conditional", "normal", "hybrid_conditional", "fidel"]
delta_ts = [0.001]

os.chdir(working_dir)

for method in methods:
    # Build target program name
    target_program = program_name + method
    # Set delta_t
    for delta_t in delta_ts:
        # Set period
        file_name = results_dir+method+"_tEnd-"+str(tEnd)+"_dT-"+str(delta_t)+".csv"
        if not os.path.isfile(file_name):
            out_file = open(file_name, "w")
            out_file.write("TotalTime,GeneratorTime,ResistorTime,StepPeriod,Steps\n")
        else:
            out_file = open(file_name, "a")

        print file_name
        for c in range(1, 10):
            print "Simulo ", target_program, "con un delta_t di ", delta_t
            res = subprocess.check_output(["./"+target_program, str(tEnd), str(delta_t), str(0), str(1)])
            out_file.write(res)
        out_file.close()



print "Elapsed time :", time.time() - start_time
