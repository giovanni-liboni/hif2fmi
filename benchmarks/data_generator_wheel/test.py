import subprocess
import decimal
import numpy
import os
import os.path
import time


def drange(x, y, jump):
    while x < y:
        yield float(x)
        x += decimal.Decimal(jump)


working_dir = "/home/deantoni/projects/FMI-dev/benchmarks/data_generator_wheel/FMI-dev/build/bin/"
results_dir = "/home/deantoni/projects/FMI-dev/benchmarks/data_generator_wheel/results/"

start_time = time.time()
tEnd = 10
program_name = "sim_data_generator_resistor_"
methods = ["fidel", "normal"]
# cycles = [x for x in range(0, 10, 100)]
cycles = []
cycles.append(2)
cycles.append(10)
cycles.append(100)
cycles.append(1000)

delta_ts = [0.002, 0.005, 0.01, 0.1, 0.009]

os.chdir(working_dir)

for method in methods:
    # Build target program name
    target_program = program_name + method
    # Set delta_t
    for delta_t in delta_ts:
        # Set period
        file_name = results_dir+method+"_tEnd-"+str(tEnd)+"_dT-"+str(delta_t)+".csv"
        if not os.path.isfile(file_name):
            out_file = open(file_name, "w")
            out_file.write("TotalTime,GeneratorTime,ResistorTime,Period,StepPeriod,Steps\n")
        else:
            with open(file_name) as f:
                if len(f.readlines()) < len(cycles):
                    out_file = open(file_name, "a")
                else:
                    break

        print file_name
        for cycle in cycles:
            print "Simulo ", target_program, " per ", cycle, " con un delta_t di ", delta_t
            res = subprocess.check_output(["./"+target_program, str(tEnd), str(delta_t), str(0), str(1), str(cycle)])
            out_file.write(res)
        out_file.close()


    for cycle in cycles:
        # Set period
        file_name = results_dir+method+"_tEnd-"+str(tEnd)+"_cycle-"+str(cycle)+".csv"
        if not os.path.isfile(file_name):
            out_file = open(file_name, "w")
            out_file.write("TotalTime,GeneratorTime,ResistorTime,Period,StepPeriod,Steps\n")
        else:
            with open(file_name) as f:
                if len(f.readlines()) < len(cycles):
                    out_file = open(file_name, "a")
                else:
                    break

        print file_name
        for delta_t in delta_ts:
            print "Simulo ", target_program, " per ", cycle, " con un delta_t di ", delta_t
            res = subprocess.check_output(["./"+target_program, str(tEnd), str(delta_t), str(0), str(1), str(cycle)])
            out_file.write(res)
        out_file.close()

print "Elapsed time :", time.time() - start_time