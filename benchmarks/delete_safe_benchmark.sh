#!/usr/bin/env bash
set -e
set -u

if [ "$#" -ne 1 ]; then
    echo "Use this script to remove ONLY the FMI-dev directory inside the benchmark. This script doesn't remove others directory."
    echo "Usage: $0 <name-of-benchmark>"
    exit 1
fi
git submodule deinit -f "$1/FMI-dev"
git rm "$1/FMI-dev"