import subprocess
import decimal
import numpy
import os
import os.path
import time


def drange(x, y, jump):
    while x < y:
        yield float(x)
        x += decimal.Decimal(jump)


working_dir = "/home/deantoni/projects/FMI-dev/build/bin"
results_dir = "/home/deantoni/projects/FMI-dev/benchmarks/data_generator_resistor/results/24-03-2017/"

start_time = time.time()
tEnd = 500
program_name = "sim_data_generator_resistor_"
methods = ["fidel", "normal", "hybrid", "best_normal"]
cycles = [x for x in range(0, 501, 10)]
cycles.append(2)

delta_ts = [0.001]

os.chdir(working_dir)

for method in methods:
    # Build target program name
    target_program = program_name + method
    # Set delta_t
    for delta_t in delta_ts:
        # Set period
        file_name = results_dir+method+"_tEnd-"+str(tEnd)+"_dT-"+str(delta_t)+".csv"
        if not os.path.isfile(file_name):
            out_file = open(file_name, "w")
            out_file.write("TotalTime,GeneratorTime,ResistorTime,Period,StepPeriod,Steps\n")
        else:
            with open(file_name) as f:
                if len(f.readlines()) < len(cycles):
                    out_file = open(file_name, "a")
                else:
                    break

        print file_name
        for cycle in cycles:
            print "Simulo ", target_program, " per ", cycle, " con un delta_t di ", delta_t
            res = subprocess.check_output(["./"+target_program, str(tEnd), str(delta_t), str(0), str(1), str(cycle)])
            out_file.write(res)
        out_file.close()


    # for delta_t in delta_ts:
    #     # Set period
    #     file_name = results_dir+method+"_"+str(delta_t)+"_fixed_period_20ms_var_tEnd.csv"
    #     if not os.path.isfile(file_name):
    #         out_file = open(file_name, "w")
    #         out_file.write("TotalTime,GeneratorTime,ResistorTime,Period,StepPeriod\n")
    #     else:
    #         with open(file_name) as f:
    #             if len(f.readlines()) < len(cycles):
    #                 out_file = open(file_name, "a")
    #             else:
    #                 break
    #
    #     for tEnd in tEnds:
    #         print "Simulo ", target_program, " per 20 ms con un delta_t di ", delta_t
    #         res = subprocess.check_output(["./"+target_program, str(tEnd), str(delta_t), str(0), str(1), str(2)])
    #         out_file.write(res)
    #
    #     out_file.close()

# tEnds = [200, 400, 600, 800]
# # FIDEL
# for method in methods:
#     target_program = program_name+method
#     file_name = results_dir+method+"_tEnd-var_tGen-10ms_dT-10ms_.csv"
#     out_file = open(file_name, "w")
#     out_file.write("TotalTime,GeneratorTime,ResistorTime,Period,StepPeriod,Steps\n")
#     for tEnd in tEnds:
#         res = subprocess.check_output(["./"+target_program, str(tEnd), "0.01", str(0), str(1), "1"])
#         out_file.write(res)
#     out_file.close()

# NORMAL

print "Elapsed time :", time.time() - start_time
# TODO Create plot
# gnuplot -e "fidel='fidel_1e-05_100'" -e "normal='normal_1e-05_100'" -e "hybrid='hybrid_1e-05_100'" -e "bestnormal='best_normal_1e-05_100'" ../gnuplot/fidel_normal_hybrid_hybridsingle.gnuplot
