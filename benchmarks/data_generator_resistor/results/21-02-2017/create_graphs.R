library(readr)
normal_0_001_100 <- read_csv("./normal_0.001_100.csv")
fidel_0_001_100 <- read_csv("./fidel_0.001_100.csv")
best_normal_0_001_100 <- read_csv("./best_normal_0.001_100.csv")
hybrid_0_001_100 <- read_csv("./hybrid_0.001_100.csv")

op <- par(mfrow = c(1,1),
          oma = c(7,5,0.5,0.5) + 0.1,
          mar = c(1,0,0,0) + 0.1)

plot(1, type='n', xlim=c(0,1000), ylim = c(0,280), ylab='Generator time (ms)', xlab='Period(ms)')
lines(normal_0_001_100$Period, normal_0_001_100$GeneratorTime, type='o', lwd=1, col='green', pch='+')
lines(hybrid_0_001_100$Period, hybrid_0_001_100$GeneratorTime, type='o', lwd=1, col='purple', pch='+')
lines(fidel_0_001_100$Period, fidel_0_001_100$GeneratorTime, type='o', lwd=1, col='red', pch='+')
lines(best_normal_0_001_100$Period, best_normal_0_001_100$GeneratorTime/10, type='o', lwd=1, col='darkgreen', pch='+')

par(xpd=NA) 
legend(-110,-60,c("Fidel", "Normal", "Best normal", "Hybrid"), lty=c(1,1), col=c("red","green", "darkgreen", "purple"), lwd=2)
