library(readr)
normal_0_001_100 <- read_csv("./24-03-2017/normal_tEnd-500_dT-0.001.csv")
fidel_0_001_100 <- read_csv("./24-03-2017/fidel_tEnd-500_dT-0.001.csv")
best_normal_0_001_100 <- read_csv("./24-03-2017/best_normal_tEnd-500_dT-0.001.csv")
hybrid_0_001_100 <- read_csv("./24-03-2017/hybrid_tEnd-500_dT-0.001.csv")

# op <- par(mfrow = c(1,1),
#          oma = c(7,5,0.5,0.5) + 0.1,
#          mar = c(1,0,0,0) + 0.1)

 #plot(1, type='n', xlim=c(0,100), ylim = c(0,7000), ylab='Generator simulation time (us)', xlab='Simulated period time (ms)', yaxp  = c(0, 7000, 7))
#lines(hybrid_0_001_100$Period/1000000, hybrid_0_001_100$GeneratorTime/1000, type='o', lwd=1, col='purple', pch='+')
#lines(fidel_0_001_100$Period/1000000, fidel_0_001_100$GeneratorTime/1000, type='o', lwd=1, col='red', pch='+')
#lines(normal_0_001_100$Period/1000000, normal_0_001_100$GeneratorTime/1000, type='o', lwd=1, col='green', pch='+')
#lines(best_normal_0_001_100$Period/1000000, best_normal_0_001_100$GeneratorTime/1000, type='o', lwd=1, col='darkgreen', pch='+')

par(xpd=NA) 


data <- rbind(fidel_0_001_100$GeneratorTime/1000, normal_0_001_100$GeneratorTime/1000)

#legend("topleft", c("Fidel","Normal"), cex=1.3, bty="n", fill=c("green","red"))

barplot(data, main="Controller Performance benchmark",
        xlab="Simulated period time (ms)", ylab = "Simulation time (ms)", col=c("green","red"),
      beside=TRUE, names.arg = c(1,10,20,30,40,50,60,70,80,90,100))

legend(30,8600,c("Fidel", "Normal"), fill=c("green","red"))