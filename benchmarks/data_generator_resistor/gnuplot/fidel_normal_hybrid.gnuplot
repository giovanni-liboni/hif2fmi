set datafile separator ','
set key autotitle columnhead
set key outside
set key left bottom
set terminal pngcairo size 1024,768


# The .csv is formatted with this pattern:
# TotalTime,GeneratorTime,ResistorTime,Period,StepPeriod

##############################################
# Create period/Total time
##############################################
set xlabel "Period (ns)"
set ylabel "Total time (ns)"

set output '../graphs/'.fidel.'_'.normal.'_xperiod_ytotal.png'

plot fidel.'.csv' u 4:1 w lp t 'FIDEL', normal.'.csv' u 4:1 w lp t 'Normal', hybrid.'.csv' u 4:1 w lp t 'Hybrid'

##############################################
# Create period/generator time
##############################################
set xlabel "Period (ns)"
set ylabel "Generator time (ns)"

set output '../graphs/'.fidel.'_'.normal.'_xperiod_ygenerator.png'

plot fidel.'.csv' u 4:2 w lp t 'FIDEL', normal.'.csv' u 4:2 w lp t 'Normal', hybrid.'.csv' u 4:2 w lp t 'Hybrid'

##############################################
# Create period/Resistor time
##############################################
set xlabel "Period (ns)"
set ylabel "Resistor time (ns)"


set output '../graphs/'.fidel.'_'.normal.'_xperiod_yresistor.png'

plot fidel.'.csv' u 4:3 w lp t 'FIDEL', normal.'.csv' u 4:3 w lp t 'Normal', hybrid.'.csv' u 4:3 w lp t 'Hybrid'
