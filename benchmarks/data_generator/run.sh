#!/usr/bin/env bash
cd FMI-dev/build/bin
./sim_data_generator_normal 0.05 0.007 0 1 3
cp data_generator_normal.csv ../../../results/data_generator_normal_dt_0.007_p_0.003.csv
cp data_generator_normal_result.csv ../../../results/data_generator_normal_result_dt_0.007_p_0.003.csv

./sim_data_generator_fidel 0.05 0.007 0 1 3
cp data_generator_fidel.csv ../../../results/data_generator_fidel_dt_0.007_p_0.003.csv
cp data_generator_fidel_result.csv ../../../results/data_generator_fidel_result_dt_0.007_p_0.003.csv

./sim_data_generator_normal 0.05 0.003 0 1 7
cp data_generator_normal.csv ../../../results/data_generator_normal_dt_0.003_p_0.007.csv
cp data_generator_normal_result.csv ../../../results/data_generator_normal_result_dt_0.003_p_0.007.csv

./sim_data_generator_fidel 0.05 0.003 0 1 7
cp data_generator_fidel.csv ../../../results/data_generator_fidel_dt_0.003_p_0.007.csv
cp data_generator_fidel_result.csv ../../../results/data_generator_fidel_result_dt_0.003_p_0.007.csv