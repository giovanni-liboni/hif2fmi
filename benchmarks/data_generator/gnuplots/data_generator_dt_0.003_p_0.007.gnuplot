set datafile separator ','
set key autotitle columnhead
set key outside
set key left bottom
set terminal pngcairo size 1024,768

# set termoption dash

# The .csv is formatted with this pattern:
# time,gen

##############################################
# Create fidel
##############################################
set xlabel "Time (ms)"
set ylabel "Value"

set output './graphs/data_generator_fidel_dt_0.003_p_0.007.png'

plot [0:50] './results/data_generator_fidel_result_dt_0.003_p_0.007.csv' u ($1 * 1000):2 lw 2 with linespoints t 'Actual', './results/data_generator_fidel_dt_0.003_p_0.007.csv' u ($1 * 1000):2  lw 2 w l t 'Expected'

##############################################
# Create normal
##############################################
set xlabel "Time (ms)"
set ylabel "Value"


set output './graphs/data_generator_normal_dt_0.003_p_0.007.png'

plot [0:50] './results/data_generator_normal_result_dt_0.003_p_0.007.csv' u ($1 * 1000):2 lw 2 w l t 'Actual', './results/data_generator_normal_dt_0.003_p_0.007.csv' u ($1 * 1000):2  lw 2 w l t 'Expected'