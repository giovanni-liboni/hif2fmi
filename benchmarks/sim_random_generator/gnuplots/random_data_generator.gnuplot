set datafile separator ','
set key autotitle columnhead
set key outside
set key left bottom
set terminal pngcairo size 1024,768

set termoption dash

# The .csv is formatted with this pattern:
# TotalTime,GeneratorTime,ResistorTime,Period,StepPeriod

##############################################
# Create fidel
##############################################
set xlabel "Time (ms)"
set ylabel "Value"

set output './graphs/random_data_generator_fidel.png'

plot [0:1000] './results/random_data_generator_fidel_result.csv' u ($1 * 1000):2 lw 2 w l t 'Actual', './results/random_data_generator_fidel.csv' u ($1 * 1000):2  lw 2 w l t 'Expected'

##############################################
# Create normal
##############################################
set xlabel "Time (ms)"
set ylabel "Value"


set output './graphs/random_data_generator_normal.png'

plot [0:1000] './results/random_data_generator_normal_result.csv' u ($1 * 1000):2 lw 2 w l t 'Actual', './results/random_data_generator_normal.csv' u ($1 * 1000):2  lw 2 w l t 'Expected'