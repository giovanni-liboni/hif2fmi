#!/usr/bin/env bash
set -e
set -u

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <name-of-benchmark> <commit-id>"
    exit 1
fi

echo "Creating $1 with $2 commit..."
echo -n "Creating directory..."
if test -d "$1"; then
    echo "Directory already present! Please specify a new directory."
    exit 1
else
    mkdir "$1"
fi
echo "OK"

cd "$1" && git submodule add --force git@gitlab.inria.fr:FIDEL/FMI-dev.git && mkdir graphs results gnuplots

echo "$2" > commit-id

if test -d "FMI-dev"; then
    cd FMI-dev
    git checkout "$2" && mkdir build && cd build && cmake ..
fi