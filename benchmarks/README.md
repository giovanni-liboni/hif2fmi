# Benchmarks
In this directory there are several benchmarks, every directory contains a script to run a benchmark, the results generated, gnuplot scripts to generate graphs and graphs.

# Directories structure
A directory here has the same name as in co-simulator directory. A benchmark runs all different version
of the co-simulator. 

A benchmark directory has this template:
- `gnuplot` : Contains gnuplot script to generate a graph
- `graphs` : Contains all generated graphs
- `results` : Contains all results in .csv format

## Semi-automatic results generator
There is a Python script to run benchmarks, according to the setup, in every directory. 

To generate a graph from the results, run inside the `results` directory:

```
gnuplot -e "fidel='fidel_1e-05_100'" -e "normal='normal_1e-05_100'" -e "hybrid='hybrid_1e-05_100'" -e "bestnormal='best_normal_1e-05_100'" ../gnuplot/fidel_normal_hybrid_hybridsingle.gnuplot
```

## Download all submodules

Run: 

- `git submodule init`
- `git submodule update`

## Create a new benchmark environment

### Before benchmark

- Create a new commit and push it to `gitlab.inria.fr:FIDEL/FMI-dev.git`

### Setup environment

Run this script:

- `./new_benchmark.sh <name-of-benchmark> <commit-id>`

### Run some tests!

Now the benchmark directory contains:
- `FMI-dev` : Source code of this project checking out a particular commit; 
- `gnuplot` : Gnuplot script to generate a graph;
- `graphs`  : All generated graphs;
- `results` : All results in .csv format;
- `commit-id` : Commit id to refer for this benchmark