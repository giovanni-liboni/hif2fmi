# FIDEL project

## Directories structure

- `libraries`
- `src`
    - `models` : Models used by simulators, organized by exported tool;
    - `co-simulator` : Co-simulators source code;
    - `utils` : 
        - `simfmi` : Simulation library
        - `OpenModelicaCompiler` : Scripts to compile OpenModelica model into a FMU;
        - `plotBuilder` : Tool to convert a .csv file in a plot line;
- `archives` : Vanilla version of the sources used by the project;
- `Config.cmake` : Support files to find SystemC and FMILibrary

## Requirements

If you want to use your own version of ddt (HIFSuite library), export:
- `DDTCLIB_HOME` : Path to DDT library;

### Packages

- libgfortran-4.8-dev
- libexpat1-dev
- libxml2-dev

## Installation

```
$ make build
$ cd build
$ cmake ..
$ make
```

## Inside `build` directory
- `bin` : There are all executables (co-simulator,...);
- `fmu` : All FMUs generated are inside this directory;
- `lib` : Output directory for libraries;

## Create a new model

### HIFSuite

If you want to create a new model from a VHDL/Verilog description using HIFSuite tools, load
the environment first:
```
$ source env.sh
```