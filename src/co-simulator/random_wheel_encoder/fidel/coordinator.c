/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include "fmi2.h"
#include "sim_support.h"

#define DEBUG 0

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 1;   // End time
    fmi2Real h = 0.001;       // Step size
    fmi2Real time;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;
    fmi2Real internalTimeFMU = tStart;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);

    // Load Generator
    FMU* generator = loadFMU("../fmu/random_wheel_encoder_fidel.fmu");
    simAddLoggedPortByName(generator, "gen");
    // Instantiate the fmu
    simInstantiate(generator, loggingOn);
    // Initialize fmus
    simInitialize(generator, tStart, tEnd + h, nCategories, categories, fmi2True);

    // enter the simulation loop
    time = tStart;

    fmi2Port* ports = (fmi2Port*) malloc(sizeof(fmi2Port)*1);
    fmi2String portNames[] = {"gen"};
    buildEventPortsByName(generator, ports, 1, portNames);

    clock_t start_gen, diff_gen;
    double total_g = 0;

    while (time < tEnd)
    {

        fmi2Status fmi2Flag = simDoStepUntilDiscontinuity(generator, time, h, ports, 1, &internalTimeFMU);

        if (fmi2Flag == fmi2Discard)
        {
            fmi2Boolean b;
            // check if model requests to end simulation

            if (b == fmi2True)
            {
                return error("the model requested to end the simulation");
            }
            return error("could not complete simulation of the model");
        }
        if (fmi2Flag != fmi2OK)
        {
            return error("could not complete simulation of the model");
        }

        time = internalTimeFMU;
        nSteps++;
    }
    plotFMUResultFile(generator);
    simTerminate(generator);
    free(ports);

    if (benchmark)
    {
        // gen,het,#cycles,step_size,
        printf("%.0f,%.0f,%g,%d\n", generator->t_total , generator->t_total, h * 1000000000, nSteps);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............ %d\n", nSteps);
        printf("  fixed step size .. %g ms\n", h * 1000);
        printf("  simulation whee... %.0f ns\n", generator->t_total);
        printf("CSV file '%s' written\n", RESULT_FILE);
    }
    return EXIT_SUCCESS;
}
