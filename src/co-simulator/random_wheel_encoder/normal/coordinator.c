/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/

#define DEBUG 0

#include <stdlib.h>
#include <stdio.h>
#include <fmi2FunctionTypes.h>
#include <fmi2.h>
#include "fmi2.h"
#include "time.h"
#include "sim_support.h"

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 1;   // End time
    fmi2Real h = 0.0001;       // Step size
    fmi2Real time;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);

    // Load Generator
    FMU* generator = loadFMU("../fmu/random_wheel_encoder_normal.fmu");
    simAddLoggedPortByName(generator, "gen");
    // Instantiate the fmu
    simInstantiate(generator, loggingOn);
    // Initialize fmus
    simInitialize(generator, tStart, tEnd + h, (size_t)nCategories, categories, !benchmark);

    // enter the simulation loop
    time = tStart;

    while (time < tEnd)
    {
        if (fmi2OK != (simDoStep(generator, time, h)))
        {
            return fmi2False;
        }

        time += h;
        nSteps++;
    }

    if (benchmark)
    {
        // gen,het,#cycles,step_size,
        printf("%.0f,%.0f,%g,%d\n", generator->t_total , generator->t_total, h * 1000000000, nSteps);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............ %d\n", nSteps);
        printf("  fixed step size .. %g ms\n", h * 1000);
        printf("  simulation whee... %.0f ns\n", generator->t_total);
        printf("CSV file '%s' written\n", RESULT_FILE);
    }

    simTerminate(generator);
    
    return EXIT_SUCCESS;
}
