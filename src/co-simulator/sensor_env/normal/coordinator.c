/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/

#define DEBUG 0

#include <stdlib.h>
#include <stdio.h>
#include <fmi2FunctionTypes.h>
#include "fmi2.h"
#include "sim_support.h"

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 10;   // End time
    fmi2Real h = 0.05;       // Step size
    fmi2Real time;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);

    // Load OpenModelica Sine generator
    FMU* sinegenerator = loadFMU("../fmu/MySinusGenerator.fmu");
    simAddLoggedPortByName(sinegenerator, "y");
    // Instantiate the fmu
    simInstantiate(sinegenerator, loggingOn);
    // Initialize fmus
    simInitialize(sinegenerator, tStart, tEnd + h, (size_t)nCategories, categories, !benchmark);

    // Load VHDL Sensor
    FMU* sensor = loadFMU("../fmu/sensor_normal.fmu");
    simAddLoggedPortByName(sensor, "y");
    // Instantiate the fmu
    simInstantiate(sensor, loggingOn);
    // Initialize fmus
    simInitialize(sensor, tStart, tEnd + h, (size_t)nCategories, categories, !benchmark);

    fmi2Port y;
    y.name = "y";

    // enter the simulation loop
    time = tStart;

    while (time < tEnd)
    {

        if (fmi2OK != (simDoStep(sensor, time, h)))
        {
            return fmi2False;
        }

        if (fmi2OK != (simDoStep(sinegenerator, time, h)))
        {
            return fmi2False;
        }

        // GetXXX and SetXXX
        y.name = "y";
        simGet(sinegenerator, &y);

        y.name = "u";
        simSet(sensor, &y);

        // Increment time
        time += h;
        nSteps++;
    }

    if (benchmark)
    {
        // gen,het,#cycles,step_size,
        printf("%.0f,%.0f,%.0f,%.0f,%d\n", sensor->t_total + sinegenerator->t_total, sensor->t_total,sinegenerator->t_total, h * 1000000000, nSteps);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............ %d\n", nSteps);
        printf("  fixed step size .. %g ms\n", h * 1000);
        printf("  simulation time... %.0f ns\n", sensor->t_total + sinegenerator->t_total);
        printf("  simulation sens... %.0f ns\n", sensor->t_total);
        printf("  simulation sin.... %.0f ns\n", sinegenerator->t_total);
        printf("CSV file '%s' written\n", RESULT_FILE);
    }

    simTerminate(sensor);
    simTerminate(sinegenerator);

    return EXIT_SUCCESS;
}
