/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/

#define DEBUG 0

#include <stdlib.h>
#include <stdio.h>
#include <fmi2FunctionTypes.h>
#include "fmi2.h"
#include "sim_support.h"

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 10;   // End time
    fmi2Real h = 0.1;       // Step size
    fmi2Real time;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;
    fmi2Real internalTimeFMU = tStart;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);

    // Load OpenModelica Sine generator
    FMU* sinegenerator = loadFMU("../fmu/MySinusGenerator.fmu");
    simAddLoggedPortByName(sinegenerator, "y");
    // Instantiate the fmu
    simInstantiate(sinegenerator, loggingOn);
    // Initialize fmus
    simInitialize(sinegenerator, tStart, tEnd + h, (size_t)nCategories, categories, !benchmark);

    // Load VHDL Sensor
    FMU* sensor = loadFMU("../fmu/sensor_fidel.fmu");
    simAddLoggedPortByName(sensor, "y");
    // Instantiate the fmu
    simInstantiate(sensor, loggingOn);
    // Initialize fmus
    simInitialize(sensor, tStart, tEnd + h, (size_t)nCategories, categories, !benchmark);

    fmi2Port y;
    y.name = "y";

    // enter the simulation loop
    time = tStart;

    fmi2Port* ports = (fmi2Port*) malloc(sizeof(fmi2Port)*1);
    fmi2String portNames[] = {"u"};
    buildEventPortsByName(sensor, ports, 1, portNames);

    while (time < tEnd)
    {
        fmi2Status fmi2Flag = simDoStepUntilRead(sensor, time, h, ports, 1, &internalTimeFMU);

        if (fmi2Flag == fmi2Discard)
        {
            fmi2Boolean b;
            // check if model requests to end simulation
            if (fmi2OK != sensor->getBooleanStatus(sensor->c, fmi2Terminated, &b))
            {
                return error("could not complete simulation of the model. getBooleanStatus return other than fmi2OK");
            }
            if (b == fmi2True)
            {
                return error("the model requested to end the simulation");
            }
            return error("could not complete simulation of the sensor");
        }
        if (fmi2Flag != fmi2OK)
        {
            return error("could not complete simulation of the sensor");
        }

        if (fmi2OK != (simDoStep(sinegenerator, time, internalTimeFMU - time)))
        {
            return fmi2False;
        }

        // GetXXX and SetXXX
        y.name = "y";
        simGet(sinegenerator, &y);

        y.name = "u";
        simSet(sensor, &y);

        // Increment time
        time = internalTimeFMU;
        nSteps++;
    }

    if (benchmark)
    {
        // gen,het,#cycles,step_size,
        printf("%.0f,%.0f,%.0f,%.0f,%d\n", sensor->t_total + sinegenerator->t_total, sensor->t_total,sinegenerator->t_total, h * 1000000000, nSteps);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............ %d\n", nSteps);
        printf("  fixed step size .. %g ms\n", h * 1000);
        printf("  simulation time... %.0f ns\n", sensor->t_total + sinegenerator->t_total);
        printf("  simulation sens... %.0f ns\n", sensor->t_total);
        printf("  simulation sin.... %.0f ns\n", sinegenerator->t_total);
        printf("CSV file '%s' written\n", RESULT_FILE);
    }

    simTerminate(sensor);
    simTerminate(sinegenerator);

    return EXIT_SUCCESS;
}
