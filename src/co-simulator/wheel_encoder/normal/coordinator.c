/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/

#define DEBUG 0

#include <stdlib.h>
#include <stdio.h>
#include <fmi2FunctionTypes.h>
#include <fmi2.h>
#include "fmi2.h"
#include "time.h"
#include "sim_support.h"

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 10;   // End time
    fmi2Real h = 0.05;       // Step size
    fmi2Real time;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;
    fmi2Port cycles;
    cycles.name = "cycles";
    cycles.type = Integer;
    cycles.ivalue = 500;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);
    if (argc > 5)
    {
        if (sscanf(argv[5],"%d", &cycles.ivalue) != 1)
        {
            printf("error: The given number of cycles (%s) is not a number\n", argv[5]);
            exit(EXIT_FAILURE);
        }
    }

    // Load Generator
    FMU* generator = loadFMU("../fmu/wheel_encoder_normal.fmu");
    simAddLoggedPortByName(generator, "gen");
    // Instantiate the fmu
    simInstantiate(generator, loggingOn);
    // Initialize fmus
    simInitialize(generator, tStart, tEnd + h, (size_t)nCategories, categories, !benchmark);
    // Load FMUs
    FMU* fmu = loadFMU("../fmu/speedComputerFromWheelEncoder.fmu");
    simAddLoggedPortByName(fmu, "wheelSpeed");
    // Instantiate the fmu
    simInstantiate(fmu, loggingOn);
    // Initialize fmus
    simInitialize(fmu, tStart, tEnd + h , (size_t)nCategories, categories, !benchmark);
    // enter the simulation loop
    time = tStart;
    fmi2Port openclose;
    openclose.name = "wheelEncoder";
    openclose.type = Boolean;
    openclose.bvalue = fmi2False;

    simSet(generator, &cycles);
    simSet(fmu, &openclose);

    while (time < tEnd)
    {
        if (fmi2OK != (simDoStep(generator, time, h)))
        {
            return fmi2False;
        }

        if (fmi2OK != (simDoStep(fmu, time, h)))
        {
            return fmi2False;
        }

        // GetXXX and SetXXX
        openclose.name = "gen";
        openclose.type = Boolean;
        simGet(generator, &openclose);
        openclose.type = Integer;
        openclose.ivalue = openclose.bvalue? 1:0;
        openclose.name = "wheelEncoder";
        simSet(fmu, &openclose);
        // Increment time
        time += h;
        nSteps++;

    }

    if (benchmark)
    {
        // gen,het,#cycles,step_size,
        printf("%.0f,%.0f,%.0f,%llu,%.0f,%d\n", generator->t_total + fmu->t_total, generator->t_total,fmu->t_total, (long long) cycles.ivalue * 1000000, h * 1000000000, nSteps);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............ %d\n", nSteps);
        printf("  fixed step size .. %g ms\n", h * 1000);
        printf("  simulation time... %.0f ns\n", generator->t_total + fmu->t_total);
        printf("  simulation whee... %.0f ns\n", generator->t_total);
        printf("  simulation spee... %.0f ns\n", fmu->t_total);
        printf("CSV file '%s' written\n", RESULT_FILE);
    }

    simTerminate(fmu);
    simTerminate(generator);

    return EXIT_SUCCESS;
}
