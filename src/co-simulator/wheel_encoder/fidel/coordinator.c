/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <fmi2.h>
#include "fmi2.h"
#include "sim_support.h"
#include "time.h"

#define DEBUG 0

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 10;   // End time
    fmi2Real h = 0.025;       // Step size
    fmi2Real time;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;
    fmi2Real internalTimeFMU = tStart;
    fmi2Port cycles;
    cycles.name = "cycles";
    cycles.type = Integer;
    cycles.ivalue = 500;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);
    if (argc > 5)
    {
        if (sscanf(argv[5],"%d", &cycles.ivalue) != 1)
        {
            printf("error: The given number of cycles (%s) is not a number\n", argv[5]);
            exit(EXIT_FAILURE);
        }
    }

    // Load Generator
    FMU* generator = loadFMU("../fmu/wheel_encoder_fidel.fmu");
    simAddLoggedPortByName(generator, "gen");
    // Instantiate the fmu
    simInstantiate(generator, loggingOn);
    // Initialize fmus
    simInitialize(generator, tStart, tEnd + h, nCategories, categories, !benchmark);
    // Load FMUs
    FMU* fmu = loadFMU("../fmu/speedComputerFromWheelEncoder.fmu");
    simAddLoggedPortByName(fmu, "wheelSpeed");
    // Instantiate the fmu
    simInstantiate(fmu, loggingOn);
    // Initialize fmus
    // TODO: Fix end of simulation
    simInitialize(fmu, tStart, tEnd + 10, nCategories, categories,  !benchmark);
    // enter the simulation loop
    time = tStart;

    fmi2Port openclose;
    openclose.name = "wheelEncoder";
    openclose.type = Boolean;
    openclose.bvalue = fmi2False;
    simSet(generator, &cycles);

    fmi2Port* ports = (fmi2Port*) malloc(sizeof(fmi2Port)*1);
    fmi2String portNames[] = {"gen"};
    buildEventPortsByName(generator, ports, 1, portNames);

    while (time < tEnd)
    {
        /*
         * Execute the wheel encoder driver
         */
        fmi2Status fmi2Flag = simDoStepUntilDiscontinuity(generator, time, h, ports, 1, &internalTimeFMU);
        if (fmi2Flag == fmi2Discard)
        {
            fmi2Boolean b;
            // check if model requests to end simulation
            if (fmi2OK != fmu->getBooleanStatus(generator->c, fmi2Terminated, &b))
            {
                return error("could not complete simulation of the model. getBooleanStatus return other than fmi2OK");
            }
            if (b == fmi2True)
            {
                return error("the model requested to end the simulation");
            }
            return error("could not complete simulation of the model");
        }
        if (fmi2Flag != fmi2OK)
        {
            return error("could not complete simulation of the model");
        }

        /*
         * Execute the Speed Calculator
         */
        if (fmi2OK != (simDoStep(fmu, time, internalTimeFMU - time)))
        {
            return fmi2False;
        }
        /*
         * Increase the global time
         */
        time = internalTimeFMU;

        /*
         * Communication point
         */
        openclose.name = "gen";
        openclose.type = Boolean;
        simGet(generator, &openclose);
        openclose.type = Integer;
        openclose.ivalue = openclose.bvalue;
        openclose.name = "wheelEncoder";
        simSet(fmu, &openclose);

        if (fmi2OK != (simDoStep(fmu, time, 0.0001)))
        {
            return fmi2False;
        }
        time += 0.0001;

        nSteps++;
    }


    free(ports);

    if (benchmark)
    {
        // gen,het,#cycles,step_size,
        printf("%.0f,%.0f,%.0f,%llu,%.0f,%d\n", generator->t_total + fmu->t_total, generator->t_total,fmu->t_total, (long long) cycles.ivalue * 1000000, h * 1000000000, nSteps);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............ %d\n", nSteps);
        printf("  fixed step size .. %g ms\n", h * 1000);
        printf("  simulation time... %.0f ns\n", generator->t_total + fmu->t_total);
        printf("  simulation whee... %.0f ns\n", generator->t_total);
        printf("  simulation spee... %.0f ns\n", fmu->t_total);
        printf("CSV file '%s' written\n", RESULT_FILE);
    }

    simTerminate(generator);
    simTerminate(fmu);

    return EXIT_SUCCESS;
}
