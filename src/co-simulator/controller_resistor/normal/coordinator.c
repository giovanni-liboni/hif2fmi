/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/

#define DEBUG 0

#include <stdlib.h>
#include <stdio.h>
#include <fmi2FunctionTypes.h>
#include "fmi2.h"
#include "time.h"
#include "sim_support.h"

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 400;   // End time
    fmi2Real h = 0.001;       // Step size
    fmi2Real time;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;

    fmi2Port openclose;
    openclose.name = "openclose";
    openclose.type = Boolean;
    openclose.bvalue = fmi2True;

    fmi2Port upper_bound;
    upper_bound.name = "upper_bound";
    upper_bound.rvalue = 70.0;

    fmi2Port value;

    fmi2Port sample;
    sample.name = "sample";
    sample.ivalue = 50000;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);

    // Load Generator
    FMU* controller = loadFMU("../fmu/controller_normal.fmu");
    simAddLoggedPortByName(controller, "gen");
    // Instantiate the fmu
    simInstantiate(controller, loggingOn);
    // Initialize fmus
    simInitialize(controller, tStart, tEnd + h, (size_t)nCategories, categories, fmi2True);
    // Load FMUs
    FMU* fmu = loadFMU("../fmu/MyHeatingResistor.fmu");
    simAddLoggedPortByName(fmu, "CapacitorCPU.v");
    // Instantiate the fmu
    simInstantiate(fmu, loggingOn);
    // Initialize fmus
    simInitialize(fmu, tStart, tEnd + h , (size_t)nCategories, categories, fmi2True);
    // enter the simulation loop
    time = tStart;

    // Init values
    simSet(controller, &upper_bound);
    simSet(controller, &sample);

    // Timing support
    clock_t start = clock(), diff;
    clock_t start_gen, diff_gen;
    clock_t start_h, diff_h;
    double total_g = 0, total_h = 0;

    while (time < tEnd)
    {
        start_gen = clock();
        if (fmi2OK != (simDoStep(controller, time, h)))
        {
            return fmi2False;
        }
        diff_gen = clock() - start_gen;
        total_g += diff_gen;
        // Log controller vars to file
        simLogFMU(controller, time);
        start_h = clock();
        if (fmi2OK != (simDoStep(fmu, time, h)))
        {
            return fmi2False;
        }
        diff_h = clock() - start_h;
        total_h += diff_h;
        // Log resistor vars to file
        simLogFMU(fmu, time);
        // GetXXX and SetXXX
        openclose.name = "gen";
        simGet(controller, &openclose);
        openclose.name = "openclose";
        simSet(fmu, &openclose);

        value.name = "CapacitorCPU.v";
        simGet(fmu, &value);
        value.name = "value";
        simSet(controller, &value);

        // Increment time
        time += h;
        nSteps++;
    }
    plotFMUResultFile(fmu);
    plotFMUResultFile(controller);
    diff = clock() - start;
    double msec = diff ;
    simTerminate(fmu);
    simTerminate(controller);
    if (benchmark)
    {
        // gen,het,#cycles,step_size,
        printf("%.0f,%.0f,%.0f,%g\n", (double)((total_g + total_h)*1000 / CLOCKS_PER_SEC), (double)(total_g*1000 / CLOCKS_PER_SEC),(double)(total_h*1000 / CLOCKS_PER_SEC), h * 1000);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............ %d\n", nSteps);
        printf("  fixed step size .. %g\n", h);
        printf("  simulation time... %.0fms\n", msec);
        printf("  simulation gene... %.0fms\n", (double)(total_g));
        printf("  simulation heat... %.0fms\n", (double)(total_h));
        printf("CSV file '%s' written\n", RESULT_FILE);
    }
    return EXIT_SUCCESS;
}
