set(SIM sim_controller_resistor_fidel)

set(SOURCE_FILES
        coordinator.c
)
include_directories(
        ${SIMFMI_INCLUDE_DIRS}
)
add_executable(${SIM} ${SOURCE_FILES})
target_link_libraries(${SIM}
    simfmi
)
set_target_properties(${SIM}
        PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin
)
add_dependencies(${SIM} controller_fidel MyHeatingResistor)

##############################################
# Test
##############################################
add_test(
        NAME ${SIM}_test
        WORKING_DIRECTORY ${PROJECT_BINARY_DIR}/bin
        COMMAND ${PROJECT_BINARY_DIR}/bin/${SIM}
)