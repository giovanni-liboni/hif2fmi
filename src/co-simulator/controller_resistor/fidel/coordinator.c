/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/

#define DEBUG 0

#include <stdlib.h>
#include <stdio.h>
#include <fmi2FunctionTypes.h>
#include "fmi2.h"
#include "time.h"
#include "sim_support.h"

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 400;   // End time
    fmi2Real h = 1;       // Step size
    fmi2Real time;
    fmi2Real internalTimeFMU;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;
    fmi2Status fmi2Flag;

    fmi2Port openclose;
    openclose.name = "openclose";
    openclose.type = Boolean;
    openclose.bvalue = fmi2True;

    fmi2Port upper_bound;
    upper_bound.name = "upper_bound";
    upper_bound.rvalue = 70.0;

    fmi2Port value;
    value.name = "value";

    fmi2Port sample;
    sample.name = "sample";
    sample.ivalue = 50000;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);
//    if (argc > 5)
//    {
//        if (sscanf(argv[5],"%d", &cycles.ivalue) != 1)
//        {
//            printf("error: The given number of cycles (%s) is not a number\n", argv[5]);
//            exit(EXIT_FAILURE);
//        }
//    }

    // Load Generator
    FMU* controller = loadFMU("../fmu/controller_fidel.fmu");
//    simAddLoggedPortByName(controller, "gen");
    simAddLoggedPortByName(controller, "gen");

    // Instantiate the fmu
    simInstantiate(controller, loggingOn);
    // Initialize fmus
    simInitialize(controller, tStart, tEnd + h, (size_t)nCategories, categories, fmi2True);
    // Load FMUs
    FMU* fmu = loadFMU("../fmu/MyHeatingResistor.fmu");
    simAddLoggedPortByName(fmu, "CapacitorCPU.v");
    // Instantiate the fmu
    simInstantiate(fmu, loggingOn);
    // Initialize fmus
    simInitialize(fmu, tStart, tEnd + h , (size_t)nCategories, categories, fmi2True);
    // enter the simulation loop
    time = tStart;

    // Init values
    simSet(controller, &upper_bound);
    simSet(controller, &sample);

    // Event ports for controller
    fmi2Port* ports = (fmi2Port*) malloc(sizeof(fmi2Port)*1);
    fmi2String portNames[] = {"gen"};
    buildEventPortsByName(controller, ports, 1, portNames);

    // Timing support
    clock_t start = clock(), diff;
    double total_g = 0, total_h = 0;

    while (time < tEnd)
    {
        fmi2Flag = controller->simulateUntilDiscontinuity(controller->c, time, h, fmi2True, ports, 1, &internalTimeFMU);

        // FMI standard checks
        if (fmi2Flag == fmi2Discard)
        {
            fmi2Boolean b;
            // check if model requests to end simulation
            if (fmi2OK != fmu->getBooleanStatus(controller->c, fmi2Terminated, &b))
            {
                return error("could not complete simulation of the model. getBooleanStatus return other than fmi2OK");
            }
            if (b == fmi2True)
            {
                return error("the model requested to end the simulation");
            }
            return error("could not complete simulation of the model");
        }
        if (fmi2Flag == fmi2Event) {
            // fmi2Port p;
            // Handle event, this function fills the port with the correct values
            // fmi2Status eventStatus = fmi2GetEventPort(controller->c, &p);

            // internalTimeFMU = p.nextEventTime;
        }
        if (fmi2Flag != fmi2OK)
        {
            return error("could not complete simulation of the model");
        }

        simLogFMU(controller, internalTimeFMU);
        if (fmi2OK != (simDoStep(fmu, time, internalTimeFMU - time)))
        {
            return fmi2False;
        }

        time = internalTimeFMU;

        // Log FMUs
        simLogFMU(fmu, time);

        // Get all values
        openclose.name = "gen";
        simGet(controller, &openclose);

        value.name = "CapacitorCPU.v";
        simGet(fmu, &value);

        // Set all values
        openclose.name = "openclose";
        simSet(fmu, &openclose);

        value.name = "value";
        simSet(controller, &value);

    }
    plotFMUResultFile(fmu);
    plotFMUResultFile(controller);
    diff = clock() - start;
    double msec = diff ;
    simTerminate(fmu);
    simTerminate(controller);
    if (benchmark)
    {
        // gen,het,#cycles,step_size,
        printf("%.0f,%.0f,%.0f,%g\n", (double)((total_g + total_h)*1000 / CLOCKS_PER_SEC), (double)(total_g*1000 / CLOCKS_PER_SEC),(double)(total_h*1000 / CLOCKS_PER_SEC), h * 1000);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............ %d\n", nSteps);
        printf("  fixed step size .. %g\n", h);
        printf("  simulation time... %.0fus\n", msec);
        printf("  simulation gene... %.0fus\n", (double)(total_g));
        printf("  simulation heat... %.0fus\n", (double)(total_h));
        printf("CSV file '%s' written\n", RESULT_FILE);
    }

    return EXIT_SUCCESS;
}
