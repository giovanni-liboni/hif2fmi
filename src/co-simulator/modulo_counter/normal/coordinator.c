/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/

#define DEBUG 0

#include <stdlib.h>
#include <stdio.h>
#include <fmi2FunctionTypes.h>
#include "fmi2.h"
#include "time.h"
#include "sim_support.h"

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 10;   // End time
    fmi2Real h = 0.05;       // Step size
    fmi2Real time;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);

    // Load counterwave
    FMU* counterwave = loadFMU("../fmu/counterwave_normal.fmu");
    simAddLoggedPortByName(counterwave, "dataout");
    // Instantiate the fmu
    simInstantiate(counterwave, loggingOn);
    // Initialize fmus
    simInitialize(counterwave, tStart, tEnd + h, (size_t)nCategories, categories, !benchmark);
    
    
    // Load thresholdslope
    FMU* thresholdslope = loadFMU("../fmu/MyThresholdSlope.fmu");
    simAddLoggedPortByName(thresholdslope, "y");
    // Instantiate the fmu
    simInstantiate(thresholdslope, loggingOn);
    // Initialize fmus
    simInitialize(thresholdslope, tStart, tEnd + h , (size_t)nCategories, categories, !benchmark);

    fmi2Port dataout;

    // enter the simulation loop
    time = tStart;
    
    dataout.name = "x";
    dataout.ivalue = 0;
    simSet(thresholdslope, &dataout);

    while (time < tEnd)
    {

        if (fmi2OK != (simDoStep(counterwave, time, h)))
        {
            return fmi2False;
        }

        // Log counterwave vars to file

        if (fmi2OK != (simDoStep(thresholdslope, time, h)))
        {
            return fmi2False;
        }

        // GetXXX and SetXXX
        dataout.name = "dataout";
        simGet(counterwave, &dataout);
        dataout.name = "x";
        simSet(thresholdslope, &dataout);
        
        // Increment time
        time += h;
        nSteps++;
    }
    
    if (benchmark)
    {
        // total,counter,slope,timestep,communicationPoints
        printf("%.0f,%.0f,%.0f,%.0f,%d\n", counterwave->t_total + thresholdslope->t_total, counterwave->t_total,thresholdslope->t_total, h * 1000000000, nSteps);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............... %d\n", nSteps);
        printf("  fixed step size ..... %g ms\n", h * 1000);
        printf("  simulation time...... %.0f ns\n", counterwave->t_total + thresholdslope->t_total);
        printf("  simulation counter... %.0f ns\n", counterwave->t_total);
        printf("  simulation slope..... %.0f ns\n", thresholdslope->t_total);
        printf("CSV file '%s' written\n", RESULT_FILE);
    }

    simTerminate(counterwave);
    simTerminate(thresholdslope);
    return EXIT_SUCCESS;
}
