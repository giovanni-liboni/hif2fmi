/* -------------------------------------------------------------------------
 * coordinator.c
 *
 * Author: Giovanni Liboni 2017
 * -------------------------------------------------------------------------*/

#define DEBUG 0

#include <stdlib.h>
#include <stdio.h>
#include <fmi2FunctionTypes.h>
#include <fmi2.h>
#include "fmi2.h"
#include "time.h"
#include "sim_support.h"


/*
 * Given a port, it returns true if the condition is true, false otherwise.
 * The boolean expression must be described if the simulation can go through or not this event.
 * If this function returns true, the simulation stops and the variable will be notify.
 */
fmi2Boolean fCheckInput(fmi2Port p)
{
    static int p_old = 0;

    int res = (( p_old >= 50 && p.ivalue < 50) || (p_old <= 50 && p.ivalue > 50));
    p_old = p.ivalue;
    return res;
}

int main(int argc, char* argv[])
{
    // Default values
    fmi2Real tStart = 0;  // Start time
    fmi2Real tEnd = 10;   // End time
    fmi2Real h = 0.001;       // Step size
    fmi2Real time;
    fmi2Real internalTimeFMU;
    fmi2Integer nSteps = 0;
    fmi2Boolean loggingOn = fmi2False;
    fmi2Boolean benchmark = fmi2False;
    fmi2String* categories = NULL;
    fmi2Integer nCategories = 0;
    fmi2Status fmi2Flag;
    clock_t start_h, diff_h;
    clock_t start_gen, diff_gen;

    parseArguments(argc, argv, &tEnd, &h, &loggingOn, &benchmark);

    // Load Generator
    FMU* counterwave = loadFMU("../fmu/counterwave_fidel.fmu");
    simAddLoggedPortByName(counterwave, "dataout");

    // Instantiate the fmu
    simInstantiate(counterwave, loggingOn);
    // Initialize fmus
    simInitialize(counterwave, tStart, tEnd + h, (size_t)nCategories, categories, !benchmark);

    // Event ports for counterwave
    fmi2Port* ports = (fmi2Port*) malloc(sizeof(fmi2Port)*1);
    fmi2String portNames[] = {"dataout"};
    buildEventPortsByName(counterwave, ports, 1, portNames);
    /*
     * The function can be registered inside the model, in the model description,
     * there is the name of the function linked to the variable.
     */
    ports[0].callbackCondition = fCheckInput;

    // Load OpenModelica Sine generator
    FMU* thresholdslope = loadFMU("../fmu/MyThresholdSlope.fmu");
    simAddLoggedPortByName(thresholdslope, "y");

    // Instantiate the fmu
    simInstantiate(thresholdslope, loggingOn);
    // Initialize fmus
    simInitialize(thresholdslope, tStart, tEnd + 100, (size_t)nCategories, categories, !benchmark);

    /*
     * Ports to exchange data
     */
    fmi2Port dataout;

    // enter the simulation loop
    time = tStart;
    
    dataout.name = "x";
    dataout.ivalue = 0;
    simSet(thresholdslope, &dataout);

    while (time < tEnd)
    {
        fmi2Flag = simDoStepUntilDiscontinuity(counterwave, time, h, ports, 1, &internalTimeFMU);
        
        // FMI standard checks
        if (fmi2Flag == fmi2Discard)
        {
            fmi2Boolean b;
            // check if model requests to end simulation
            if (fmi2OK != counterwave->getBooleanStatus(counterwave->c, fmi2Terminated, &b))
            {
                return error("could not complete simulation of the model. getBooleanStatus return other than fmi2OK");
            }
            if (b == fmi2True)
            {
                return error("the model requested to end the simulation");
            }
            return error("could not complete simulation of the model");
        }
        if (fmi2Flag != fmi2OK)
        {
            return error("could not complete simulation of the model");
        }
        
        if (fmi2OK != (simDoStep(thresholdslope, time, internalTimeFMU - time)))
        {
            return fmi2False;
        }
        
        // GetXXX and SetXXX
        dataout.name = "dataout";
        simGet(counterwave, &dataout);
        dataout.name = "x";
        simSet(thresholdslope, &dataout);

        time = internalTimeFMU;
        nSteps++;
    }

    if (benchmark)
    {
        // total,counter,slope,timestep,communicationPoints
        printf("%.0f,%.0f,%.0f,%.0f,%d\n", counterwave->t_total + thresholdslope->t_total, counterwave->t_total,thresholdslope->t_total, h * 1000000000, nSteps);
    }
    else
    {
        printf("Simulation from %g to %g terminated successful\n", tStart, tEnd);
        printf("  steps ............... %d\n", nSteps);
        printf("  fixed step size ..... %g ms\n", h * 1000);
        printf("  simulation time...... %.0f ns\n", counterwave->t_total + thresholdslope->t_total);
        printf("  simulation counter... %.0f ns\n", counterwave->t_total);
        printf("  simulation slope..... %.0f ns\n", thresholdslope->t_total);
        printf("CSV file '%s' written\n", RESULT_FILE);
    }
    
    simTerminate(counterwave);
    simTerminate(thresholdslope);
    
    return EXIT_SUCCESS;
}
