#include <fmuInterface.hh>
#include "fmuInterface.hh"
#include "generator.hh"

extern "C" {
    // ---------------------------------------------------------------------------
    // Model Specific functions called by fmi2Functions
    // ---------------------------------------------------------------------------
    HIFComponent generator_new(fmi2String fmuResourceLocation)
    {
        return (HIFComponent) new generator();
    }
    void generator_initialize(HIFComponent c, HIFIoStruct io_exchange, int32_t& cycles_number )
    {
        generator* g = (generator*) c;
        generator::generator_iostruct* iostruct = (generator::generator_iostruct*) io_exchange;
        g->initialize();
    }
    void generator_simulate(HIFComponent c, HIFIoStruct io_exchange, int32_t& cycles_number )
    {
        generator* g= (generator*) c;
        generator::generator_iostruct* iostruct = (generator::generator_iostruct*) io_exchange;
        g->simulate(iostruct,cycles_number);
    }
    // Function to simulate until an event happens
    void* generator_simulateUntil(void* data)
    {
        ModelInstance* comp = (ModelInstance*) data;
        generator* g= (generator*) comp->model;
        generator::generator_iostruct* io_exchange = (generator::generator_iostruct*) comp->iostruct;
        comp->threadStarted = fmi2True;
        pthread_mutex_lock(&g->m1);

        while(comp->threadStarted)
        {
            // Step is not completed
            comp->cycleCompleted = fmi2False;

            // Run the real simulation
            g->simulate(io_exchange, comp->cycle_number);

            // Fake clock
            io_exchange->clock = !io_exchange->clock;

            // Tag this step as Completed
            comp->cycleCompleted = fmi2True;
            // One cycle simulation, until we reach the time requested
            if (comp->justOneCycle && ( comp->time > comp->globalTime))
            {
                // Reset to false just one cycle
                comp->justOneCycle = fmi2False;
                // Unlock generator_simulate_and_wait
                pthread_mutex_unlock(&g->m2);
                pthread_mutex_lock(&g->m1);
            }
        }
        pthread_exit(NULL);
    }

    // Simulate until an event happens
    void generator_simulate_and_wait(fmi2Component c, fmi2Boolean oneCycle)
    {
        ModelInstance* comp = (ModelInstance*) c;
        generator* g= (generator*) comp->model;
        comp->justOneCycle = oneCycle;
        if (oneCycle)
        {
            while(!comp->cycleCompleted)
            {
                pthread_mutex_unlock(&g->m1);
                pthread_mutex_lock(&g->m2);
            }
        }
        else
        {
            pthread_mutex_unlock(&g->m1);
            pthread_mutex_lock(&g->m2);
        }
        // Save the current pending variable
        generator_set_pending_variable(comp);
    }

    void generator_set_pending_variable(fmi2Component c)
    {
        ModelInstance* comp = (ModelInstance*) c;
        generator* g= (generator*) comp->model;
        // If the cycle is completed  there isn't a pending variable
        if (!comp->cycleCompleted)
        {
            // Bad way to do this
            if (strcmp(g->variableUnderRead, "gen") == 0)
            {
                //                comp->variablePendingType = fmi2Boolean;
                comp->variablePendingVR = 1;
            }
        }
        if (g->isWriting)
        {
            // TODO
            // The output value is ready, set the result time according to cycles number
            comp->time = comp->cycle_number * GENERATOR_CLOCK_HALF_PERIOD;
        }
        else
        {
            // TODO
            // It's a reading operation
        }
    }

    HIFIoStruct generator_get_iostruct(HIFComponent c)
    {
        generator* g = (generator*) c;
        return (HIFIoStruct)&(g->hif_a2t_data);
    }
    void generator_delete(HIFComponent c)
    {
        generator* g = (generator*) c;
        delete g;
    }
    void iostruct_delete(HIFIoStruct c)
    {
        //ntd
    }

    // macro to be used to log messages. The macro check if current
    // log category is valid and, if true, call the logger provided by simulator.

#define FILTERED_LOG(instance, status, categoryIndex, message, ...) if (isCategoryLogged(instance, categoryIndex)) \
        instance->functions->logger(instance->functions->componentEnvironment, instance->instanceName, status, \
                                    logCategoriesNames[categoryIndex], message, ##__VA_ARGS__);

    static fmi2String logCategoriesNames[] = { "logAll", "logError", "logFmiCall", "logEvent" };

    // ---------------------------------------------------------------------------
    // Private helpers used below to validate function arguments
    // ---------------------------------------------------------------------------

    fmi2Boolean isCategoryLogged(ModelInstance* comp, int categoryIndex);
    /*
    #ifndef FMI_COSIMULATION
    static fmi2Boolean invalidNumber(ModelInstance *comp, const char *f,
    const char *arg, int n, int nExpected) {

    if (n != nExpected) {
    comp->state = modelError;
    FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"%s: Invalid argument %s = %d. Expected %d.", f, arg, n,nExpected)
    return fmi2True;
    }
    return fmi2False;
    }
    #endif
    */

    // ---------------------------------------------------------------------------
    // Private helpers logger
    // ---------------------------------------------------------------------------

    // array of value references of states
#if NUMBER_OF_REALS>0
    fmi2ValueReference vrStates[NUMBER_OF_STATES] = STATES;
#endif

#ifndef max
#define max(a,b) ((a)>(b) ? (a) : (b))
#endif

    //    int stepToexecute = 0;
    //    float currentModelTime = 0.0;
    //    fmi2ValueReference allRefs[] = {  1    ,2    ,3    ,4    ,5    ,6    ,7    ,8    ,9    ,10    ,11    ,12    ,13    ,14  };

    static fmi2Boolean invalidState(ModelInstance* comp, const char* f, int statesExpected)
    {
        if (!comp)	return fmi2True;
        return fmi2False;
    }

    fmi2Status fmi2EnterContinuousTimeMode(fmi2Component c)
    {
        return fmi2OK;
    }

    fmi2Status fmi2CompletedIntegratorStep(fmi2Component c,fmi2Boolean noSetFMUStatePriorToCurrentPoint,fmi2Boolean* enterEventMode, fmi2Boolean* terminateSimulation)
    {
        return fmi2OK;
    }

    /* Providing independent variables and re-initialization of caching */

    // decide how to set the time into the SystemC model

    fmi2Status fmi2SetTime(fmi2Component c, fmi2Real time)
    {
        ModelInstance* comp = (ModelInstance*) c;
        if (invalidState(comp, "fmi2SetTime", MASK_fmi2SetTime)) return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetTime: time=%.16g", time)
        comp->time = time;
        return fmi2OK;
    }

    fmi2Status fmi2SetContinuousStates(fmi2Component c, const fmi2Real x[], size_t nx)
    {
        return fmi2OK;
    }

    /* Evaluation of the model equations */
    fmi2Status fmi2GetDerivatives(fmi2Component c, fmi2Real derivatives[], size_t nx)
    {
        return fmi2OK;
    }

    fmi2Status fmi2GetEventIndicators(fmi2Component c, fmi2Real eventIndicators[], size_t ni)
    {
        return fmi2OK;
    }

    fmi2Status fmi2GetContinuousStates(fmi2Component c, fmi2Real states[], size_t nx)
    {
        return fmi2OK;
    }

    fmi2Status fmi2GetNominalsOfContinuousStates(fmi2Component c,fmi2Real x_nominal[], size_t nx)
    {
        return fmi2OK;
    }

    static fmi2Boolean nullPointer(ModelInstance* comp, const char* f,const char* arg, const void* p)
    {
        if (!p)
        {
            comp->state = modelError;
            FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"%s: Invalid argument %s = NULL.", f, arg)
            return fmi2True;
        }
        return fmi2False;
    }

    static fmi2Boolean vrOutOfRange(ModelInstance* comp, const char* f,fmi2ValueReference vr, int end)
    {
        if (vr >= end)
        {
            FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"%s: Illegal value reference %u.", f, vr)
            comp->state = modelError;
            return fmi2True;
        }
        return fmi2False;
    }

    static fmi2Status unsupportedFunction(fmi2Component c, const char* fName,int statesExpected)
    {
        ModelInstance* comp = (ModelInstance*) c;
        if (invalidState(comp, fName, statesExpected))	return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, fName);
        FILTERED_LOG(comp, fmi2Error, LOG_ERROR, "%s: Function not implemented.",fName)
        return fmi2Error;
    }

    // FROM THE MODEL

    //  float i=0;
    // called by fmi2Instantiate
    // Set values for all variables that define a start value
    // Settings used unless changed by fmi2SetX before fmi2EnterInitializationMode
    void setStartValues(ModelInstance* comp)
    {
        //TO BE IMPLEMENTED
    }

    void calculateValues(ModelInstance* comp)
    {
        //ntd
    }

    fmi2Real getEventIndicator(ModelInstance* comp, int z)
    {
        return 0;
    }

    // used to set the next time event, if any.
    /*void eventUpdate(ModelInstance *comp, fmi2EventInfo *eventInfo, int isTimeEvent) {
      eventInfo->valuesOfContinuousStatesChanged   = fmi2True;
      eventInfo->nominalsOfContinuousStatesChanged = fmi2False;
      eventInfo->terminateSimulation   = fmi2False;
      eventInfo->nextEventTimeDefined  = fmi2False;
      }*/

    void getLogicalSteps(ModelInstance* comp)
    {
        // printf("getLogicalSteps::enter\n");
        // call the method to get all the possible logical steps
        // jobject result = (*env)->CallObjectMethod(env, main_class, ls_method, "");
        // const char* str = (*env)->GetStringUTFChars(env, (jstring) result, 0);
        // put the logical steps in the string variable
        // s(string_)= str;
    }

    // return fmi2True if logging category is on. Else return fmi2False.
    fmi2Boolean isCategoryLogged(ModelInstance* comp, int categoryIndex)
    {
        if (categoryIndex < NUMBER_OF_CATEGORIES && (comp->logCategories[categoryIndex] || comp->logCategories[LOG_ALL]))
        {
            return fmi2True;
        }
        return fmi2False;
    }

    // ---------------------------------------------------------------------------
    // FMI functions
    // ---------------------------------------------------------------------------

    fmi2Component fmi2Instantiate(fmi2String instanceName, fmi2Type fmuType,fmi2String fmuGUID, fmi2String fmuResourceLocation,const fmi2CallbackFunctions* functions, fmi2Boolean visible,fmi2Boolean loggingOn)
    {
        ModelInstance* comp;
        generator::generator_iostruct* iostruct;
        if (!functions->logger)
        {
            return NULL;
        }
        if (!functions->allocateMemory || !functions->freeMemory)
        {
            functions->logger(functions->componentEnvironment, instanceName,fmi2Error, "error","fmi2Instantiate: Missing callback function.");
            return NULL;
        }
        if (!instanceName || strlen(instanceName) == 0)
        {
            functions->logger(functions->componentEnvironment, "?", fmi2Error,"error", "fmi2Instantiate: Missing instance name.");
            return NULL;
        }
        if (!fmuGUID || strlen(fmuGUID) == 0)
        {
            functions->logger(functions->componentEnvironment, instanceName,fmi2Error, "error", "fmi2Instantiate: Missing GUID.");
            return NULL;
        }
        if (strcmp(fmuGUID, MODEL_GUID))
        {
            functions->logger(functions->componentEnvironment, instanceName,fmi2Error, "error","fmi2Instantiate: Wrong GUID %s. Expected %s.", fmuGUID,MODEL_GUID);
            return NULL;
        }
        comp = (ModelInstance*) functions->allocateMemory(1,sizeof(ModelInstance));
        if (comp)
        {
            int i;
            comp->r = (fmi2Real*) functions->allocateMemory(NUMBER_OF_REALS,sizeof(fmi2Real));
            comp->i = (fmi2Integer*) functions->allocateMemory(NUMBER_OF_INTEGERS,sizeof(fmi2Integer));
            comp->b = (fmi2Boolean*) functions->allocateMemory(NUMBER_OF_BOOLEANS,sizeof(fmi2Boolean));
            comp->s = (fmi2String*) functions->allocateMemory(NUMBER_OF_STRINGS,sizeof(fmi2String));
            comp->isPositive = (fmi2Boolean*) functions->allocateMemory(NUMBER_OF_EVENT_INDICATORS, sizeof(fmi2Boolean));
            comp->instanceName = (char* )functions->allocateMemory(1 + strlen(instanceName),sizeof(char));
            comp->GUID = (char* )functions->allocateMemory(1 + strlen(fmuGUID),sizeof(char));
            // allocate the space for the model using the functions provided by the wrapper. it call the constructor of the object
            comp->model = (HIFComponent*) generator_new(fmuResourceLocation);
            //set the structor
            comp->iostruct = (HIFIoStruct) new generator::generator_iostruct();
            comp->cycle_number=0;
            generator* g = (generator*) comp->model;
            g->ports = comp->ports;
            g->nPorts = comp->nPorts;
            // set all categories to on or off. fmi2SetDebugLogging should be called to choose specific categories.
            for (i = 0; i < NUMBER_OF_CATEGORIES; i++)
            {
                comp->logCategories[i] = loggingOn;
            }
        }
        if (!comp || !comp->r || !comp->i || !comp->b || !comp->s|| !comp->isPositive || !comp->instanceName || !comp->GUID)
        {
            functions->logger(functions->componentEnvironment, instanceName,fmi2Error, "error", "fmi2Instantiate: Out of memory.");
            return NULL;
        }
        // Set internal and global time at 0
        comp->globalTime = 0;
        comp->time = 0;
        comp->threadStarted = fmi2False;
        comp->justOneCycle = fmi2False;
        strcpy((char*) comp->instanceName, (char*) instanceName);
        comp->type = fmuType;
        strcpy((char*) comp->GUID, (char*) fmuGUID);
        comp->functions = functions;
        comp->componentEnvironment = functions->componentEnvironment;
        comp->loggingOn = loggingOn;
        comp->state = modelInstantiated;
        comp->eventInfo.newDiscreteStatesNeeded = fmi2False;
        comp->eventInfo.terminateSimulation = fmi2False;
        comp->eventInfo.nominalsOfContinuousStatesChanged = fmi2False;
        comp->eventInfo.valuesOfContinuousStatesChanged = fmi2False;
        comp->eventInfo.nextEventTimeDefined = fmi2True;
        comp->eventInfo.nextEventTime = 0;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2Instantiate: GUID=%s",fmuGUID)
        //	comp->isDirtyValues = 1; // because we just called setStartValues
        return comp;
    }

    /* ********************************************************************************************************************	*/
    /* ********************************************************************************************************************	*/

    fmi2Status fmi2SetupExperiment(fmi2Component c, fmi2Boolean toleranceDefined,fmi2Real tolerance, fmi2Real startTime, fmi2Boolean stopTimeDefined,fmi2Real stopTime)
    {
        ModelInstance* comp = (ModelInstance*) c;
        generator* model = (generator* ) comp->model;
        if (invalidState(comp, "fmi2SetupExperiment", MASK_fmi2SetupExperiment)) return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL,"fmi2SetupExperiment: toleranceDefined=%d tolerance=%g",toleranceDefined, tolerance)
        // Setup time
        comp->time = startTime;
        comp->globalTime = startTime;
        // Start the thread simulation if not started
        if (!comp->threadStarted)
        {
            FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "Starting thread");
            pthread_attr_init(&comp->attr);
            pthread_attr_setdetachstate(&comp->attr, PTHREAD_CREATE_JOINABLE);
            pthread_create(&comp->sim, &comp->attr, generator_simulateUntil, (void*) comp);
            pthread_detach(comp->sim);
        }
        // initialize the model
        generator_initialize(comp->model,comp->iostruct,comp->cycle_number);
        return fmi2OK;
    }

    /* ********************************************************************************************************************	*/
    /* ********************************************************************************************************************	*/
    /* ********************************************************************************************************************	*/


    fmi2Status fmi2EnterInitializationMode(fmi2Component c)
    {
        ModelInstance* comp = (ModelInstance*)c;
        comp->state = modelInitializationMode;
        ((ModelInstance*)c)->eventInfo.nextEventTimeDefined = fmi2True;
        // nothing to do ...
        return fmi2OK;
    }

    fmi2Status fmi2ExitInitializationMode(fmi2Component c)
    {
        return fmi2OK;
    }

    fmi2Status fmi2Terminate(fmi2Component c)
    {
        fflush(stdout); // really needed?
        ModelInstance* comp = (ModelInstance*) c;
        comp->threadStarted = fmi2False;
        generator_simulate_and_wait(comp, fmi2True);
        if (invalidState(comp, "fmi2Terminate", MASK_fmi2Terminate))	return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2Terminate")
        //model->finalize(); // create the function
        comp->state = modelTerminated;
        return fmi2OK;
    }

    fmi2Status fmi2Reset(fmi2Component c)
    {
        ModelInstance* comp = (ModelInstance*) c;
        if (invalidState(comp, "fmi2Reset", MASK_fmi2Reset))	return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2Reset")
        comp->state = modelInstantiated;
        // nothing to do ...
        comp->isDirtyValues = 1; // because we just called setStartValues
        return fmi2OK;
    }

    void fmi2FreeInstance(fmi2Component c)
    {
        fflush(stdout);
        fmi2Status status;
        ModelInstance* comp = (ModelInstance*) c;
        if (!comp)	return;
        if (invalidState(comp, "fmi2FreeInstance", MASK_fmi2FreeInstance))	return;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2FreeInstance")
        if (comp->r)	comp->functions->freeMemory(comp->r);
        if (comp->i)	comp->functions->freeMemory(comp->i);
        if (comp->b)	comp->functions->freeMemory(comp->b);
        if (comp->s)
        {
            int i;
            for (i = 0; i < NUMBER_OF_STRINGS; i++)
            {
                if (comp->s[i])	comp->functions->freeMemory((void*) comp->s[i]);
            }
            comp->functions->freeMemory((void*) comp->s);
        }
        if (comp->isPositive) comp->functions->freeMemory(comp->isPositive);
        if (comp->instanceName) comp->functions->freeMemory((void*) comp->instanceName);
        if (comp->GUID)	comp->functions->freeMemory((void*) comp->GUID);
        //free(generator_iostruct);
        pthread_attr_destroy(&comp->attr);
        generator_delete(comp->model);
        comp->functions->freeMemory(comp);
    }

    // ---------------------------------------------------------------------------
    // FMI functions: class methods not depending of a specific model instance
    // ---------------------------------------------------------------------------

    const char* fmi2GetVersion()
    {
#ifdef PRINT
        printf("\n[ FMU ] fmi2GetVersion\n");
#endif
        fflush(stdout);
        return fmi2Version;
    }

    const char* fmi2GetTypesPlatform()
    {
        return fmi2TypesPlatform;
    }

    // ---------------------------------------------------------------------------
    // FMI functions: logging control, setters and getters for Real, Integer,
    // Boolean, String
    // ---------------------------------------------------------------------------

    fmi2Status fmi2SetDebugLogging(fmi2Component c, fmi2Boolean loggingOn,size_t nCategories, const fmi2String categories[])
    {
        // ignore arguments: nCategories, categories
        int i, j;
        ModelInstance* comp = (ModelInstance*) c;
        if (invalidState(comp, "fmi2SetDebugLogging", MASK_fmi2SetDebugLogging))return fmi2Error;
        comp->loggingOn = loggingOn;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetDebugLogging")
        // reset all categories
        for (j = 0; j < NUMBER_OF_CATEGORIES; j++)
        {
            comp->logCategories[j] = fmi2False;
        }
        if (nCategories == 0)
        {
            // no category specified, set all categories to have loggingOn value
            for (j = 0; j < NUMBER_OF_CATEGORIES; j++)
            {
                comp->logCategories[j] = loggingOn;
            }
        }
        else
        {
            // set specific categories on
            for (i = 0; i < nCategories; i++)
            {
                fmi2Boolean categoryFound = fmi2False;
                for (j = 0; j < NUMBER_OF_CATEGORIES; j++)
                {
                    if (strcmp(logCategoriesNames[j], categories[i]) == 0)
                    {
                        comp->logCategories[j] = loggingOn;
                        categoryFound = fmi2True;
                        break;
                    }
                }
                if (!categoryFound)
                {
                    comp->functions->logger(comp->componentEnvironment,comp->instanceName, fmi2Warning,logCategoriesNames[LOG_ERROR],"logging category '%s' is not supported by model",categories[i]);
                } // end if
            }//end for
        }//end else
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetDebugLogging")
        return fmi2OK;
    }

    fmi2Status fmi2GetReal(fmi2Component c, const fmi2ValueReference vr[],size_t nvr, fmi2Real value[])
    {
        ModelInstance* comp = (ModelInstance*) c;
        if (invalidState(comp, "fmi2GetReal", MASK_fmi2GetReal)) return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2GetReal", "vr[]", vr)) return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2GetReal", "value[]", value)) return fmi2Error;
        if (nvr > 0 && comp->isDirtyValues)
        {
            calculateValues(comp);
            comp->isDirtyValues = 0;
        }
#if NUMBER_OF_REALS > 0
        int i;
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
        for (i = 0; i < nvr; i++)
        {
            if (vrOutOfRange(comp, "fmi2GetReal", vr[i], NUMBER_OF_REALS))return fmi2Error;
            /*switch(vr[i]){
              case 0: //i've only one Real port ...

              value[i] = iostruct->threshold;

              break;
              }*/
            FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2GetReal: #r%u# = %.16g", vr[i], value[i])
        } // end for
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
#endif
        return fmi2OK;
    }

    fmi2Status fmi2GetInteger(fmi2Component c, const fmi2ValueReference vr[],size_t nvr, fmi2Integer value[])
    {
        int i;
        ModelInstance* comp = (ModelInstance*) c;
        generator::generator_iostruct* iostruct = (generator::generator_iostruct*) comp->iostruct;
        if (invalidState(comp, "fmi2GetInteger", MASK_fmi2GetInteger)) return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2GetInteger", "vr[]", vr)) return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2GetInteger", "value[]", value)) return fmi2Error;
        if (nvr > 0 && comp->isDirtyValues)
        {
            calculateValues(comp);
            comp->isDirtyValues = 0;
        }
        return fmi2OK;
    }

    fmi2Status fmi2GetBoolean(fmi2Component c, const fmi2ValueReference vr[],size_t nvr, fmi2Boolean value[])
    {
        int i;
        ModelInstance* comp = (ModelInstance*) c;
        generator::generator_iostruct* iostruct = (generator::generator_iostruct*) comp->iostruct;
        if (invalidState(comp, "fmi2GetBoolean", MASK_fmi2GetBoolean))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2GetBoolean", "vr[]", vr))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2GetBoolean", "value[]", value))return fmi2Error;
        if (nvr > 0 && comp->isDirtyValues)
        {
            calculateValues(comp);
            comp->isDirtyValues = 0;
        }//end if
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
        for (i = 0; i < nvr; i++)
        {
            if (vrOutOfRange(comp, "fmi2GetBoolean", vr[i], NUMBER_OF_BOOLEANS))return fmi2Error;
            switch(vr[i])
            {
                case 0:
                    value[i] = iostruct->gen ;
                    break;
            } // end switch
            FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL,"fmi2GetBoolean: #b%u# = %s", vr[i],value[i] ? "true" : "false")
        } // end for
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
        return fmi2OK;
    } // end function

    fmi2Status fmi2GetString(fmi2Component c, const fmi2ValueReference vr[],size_t nvr, fmi2String value[])
    {
        int i;
        ModelInstance* comp = (ModelInstance*) c;
        if (invalidState(comp, "fmi2GetString", MASK_fmi2GetString))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2GetString", "vr[]", vr))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2GetString", "value[]", value))return fmi2Error;
        if (nvr > 0 && comp->isDirtyValues)
        {
            calculateValues(comp);
            comp->isDirtyValues = 0;
        }
        for (i = 0; i < nvr; i++)
        {
            if (vrOutOfRange(comp, "fmi2GetString", vr[i], NUMBER_OF_STRINGS))return fmi2Error;
            //value[i]= NULL;   TO DO
            FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL,"fmi2GetString: #s%u# = '%s'", vr[i], value[i])
        }
        return fmi2OK;
    }

    //TO BE FIXED
    fmi2Status fmi2SetReal(fmi2Component c, const fmi2ValueReference vr[],size_t nvr, const fmi2Real value[])
    {
        int i;
        fmi2Status status;
        ModelInstance* comp = (ModelInstance*) c;
        generator::generator_iostruct* iostruct = (generator::generator_iostruct*) comp->iostruct;
        if (invalidState(comp, "fmi2SetReal", MASK_fmi2SetReal))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2SetReal", "vr[]", vr))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2SetReal", "value[]", value))return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetReal: nvr = %d", nvr)
        // no check whether setting the value is allowed in the current state
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
        for (i = 0; i < nvr; i++)
        {
            if (vrOutOfRange(comp, "fmi2SetReal", vr[i], NUMBER_OF_REALS))return fmi2Error;
            FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetReal: #r%d# = %.16g",vr[i], value[i])
            // SET METHOD PUT HERE! TO DOOOOO
            /*switch(vr[i]){

              case 0:



              break;

              } // end switch*/
        }  // end for
        if (nvr > 0) comp->isDirtyValues = 1;
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
        return fmi2OK;
    }

    fmi2Status fmi2SetInteger(fmi2Component c, const fmi2ValueReference vr[],size_t nvr, const fmi2Integer value[])
    {
        int i;
        fmi2Status status;
        ModelInstance* comp = (ModelInstance*) c;
        generator::generator_iostruct* iostruct = (generator::generator_iostruct*)comp->iostruct;
#ifdef PRINT
        printf("\n[ FMU ] fmi2SetInteger\n");
#endif
        if (invalidState(comp, "fmi2SetInteger", MASK_fmi2SetInteger))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2SetInteger", "vr[]", vr))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2SetInteger", "value[]", value))return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetInteger: nvr = %d", nvr)
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
        for (i = 0; i < nvr; i++)
        {
            if (vrOutOfRange(comp, "fmi2SetInteger", vr[i], NUMBER_OF_INTEGERS))return fmi2Error;
            FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetInteger: #i%d# = %d",vr[i], value[i]);
            switch(vr[i])
            {
                case 0:
                    iostruct->cycles = value[i];
                    break;
            }  // end switch
        }// end for
        if (nvr > 0)comp->isDirtyValues = 1;
        //--------------------------------------------------
        //--------------------------------------------------
        //--------------------------------------------------
        return fmi2OK;
    }

    fmi2Status fmi2SetBoolean(fmi2Component c, const fmi2ValueReference vr[],size_t nvr, const fmi2Boolean value[])
    {
        int i;
        ModelInstance* comp = (ModelInstance*) c;
        generator::generator_iostruct* iostruct= (generator::generator_iostruct*)comp->iostruct;
        if (invalidState(comp, "fmi2SetBoolean", MASK_fmi2SetBoolean))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2SetBoolean", "vr[]", vr))return fmi2Error;
        if (nvr > 0 && nullPointer(comp, "fmi2SetBoolean", "value[]", value))return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetBoolean: nvr = %d", nvr)
        return fmi2OK;
    }

    //TO BE FIXED
    fmi2Status fmi2SetString(fmi2Component c, const fmi2ValueReference vr[],size_t nvr, const fmi2String value[])
    {
        int i;
#ifdef PRINT
        printf("\n[ FMU ] fmi2SetString\n");
#endif
        /*
           ModelInstance *comp = (ModelInstance *) c;

           if (invalidState(comp, "fmi2SetString", MASK_fmi2SetString))return fmi2Error;

           if (nvr > 0 && nullPointer(comp, "fmi2SetString", "vr[]", vr))return fmi2Error;

           if (nvr > 0 && nullPointer(comp, "fmi2SetString", "value[]", value))return fmi2Error;

           FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetString: nvr = %d", nvr)

           for (i = 0; i < nvr; i++) {

           char *string = (char *) comp->s[vr[i]];

           if (vrOutOfRange(comp, "fmi2SetString", vr[i], NUMBER_OF_STRINGS))return fmi2Error;

           FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetString: #s%d# = '%s'",vr[i], value[i])


           if (value[i] == NULL) {

           if (string)comp->functions->freeMemory(string);

           comp->s[vr[i]] = NULL;

           FILTERED_LOG(comp, fmi2Warning, LOG_ERROR,"fmi2SetString: string argument value[%d] = NULL.", i);

           } else {

           if (string == NULL || strlen(string) < strlen(value[i])) {

           if (string)comp->functions->freeMemory(string);

           comp->s[vr[i]] = comp->functions->allocateMemory(1 + strlen(value[i]), sizeof(char));

           if (!comp->s[vr[i]]) {

           comp->state = modelError;
           FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"fmi2SetString: Out of memory.")

           return fmi2Error;

           }

           }


        	//cycle on the strings and set them

        	//strcpy((char *) comp->s[vr[i]], (char *) value[i]);

        	}
        	}
        	if (nvr > 0)comp->isDirtyValues = 1;

        */
        return fmi2OK;
    }

    fmi2Status fmi2GetFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
    {
        return unsupportedFunction(c, "fmi2GetFMUstate", MASK_fmi2GetFMUstate);
    }

    fmi2Status fmi2SetFMUstate(fmi2Component c, fmi2FMUstate FMUstate)
    {
        return unsupportedFunction(c, "fmi2SetFMUstate", MASK_fmi2SetFMUstate);
    }

    fmi2Status fmi2FreeFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
    {
        return unsupportedFunction(c, "fmi2FreeFMUstate", MASK_fmi2FreeFMUstate);
    }

    fmi2Status fmi2SerializedFMUstateSize(fmi2Component c, fmi2FMUstate FMUstate,size_t* size)
    {
        return unsupportedFunction(c, "fmi2SerializedFMUstateSize",MASK_fmi2SerializedFMUstateSize);
    }

    fmi2Status fmi2SerializeFMUstate(fmi2Component c, fmi2FMUstate FMUstate,fmi2Byte serializedState[], size_t size)
    {
        return unsupportedFunction(c, "fmi2SerializeFMUstate",MASK_fmi2SerializeFMUstate);
    }

    fmi2Status fmi2DeSerializeFMUstate(fmi2Component c,const fmi2Byte serializedState[], size_t size, fmi2FMUstate* FMUstate)
    {
        return unsupportedFunction(c, "fmi2DeSerializeFMUstate",MASK_fmi2DeSerializeFMUstate);
    }

    fmi2Status fmi2GetDirectionalDerivative(fmi2Component c,const fmi2ValueReference vUnknown_ref[], size_t nUnknown,const fmi2ValueReference vKnown_ref[], size_t nKnown,const fmi2Real dvKnown[], fmi2Real dvUnknown[])
    {
        return unsupportedFunction(c, "fmi2GetDirectionalDerivative",MASK_fmi2GetDirectionalDerivative);
    }

    // ---------------------------------------------------------------------------
    // Functions for FMI for Co-Simulation
    // ---------------------------------------------------------------------------

    /* Simulating the slave */

    fmi2Status fmi2SetRealInputDerivatives(fmi2Component c, const fmi2ValueReference vr[], size_t nvr,const fmi2Integer order[], const fmi2Real value[])
    {
        ModelInstance* comp = (ModelInstance*)c;
        if (invalidState(comp, "fmi2SetRealInputDerivatives", MASK_fmi2SetRealInputDerivatives)) return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2SetRealInputDerivatives: nvr= %d", nvr)
        FILTERED_LOG(comp, fmi2Error, LOG_ERROR, "fmi2SetRealInputDerivatives: ignoring function call.This model cannot interpolate inputs: canInterpolateInputs=\"fmi2False\"")
        return fmi2Error;
    }

    fmi2Status fmi2GetRealOutputDerivatives(fmi2Component c, const fmi2ValueReference vr[], size_t nvr,const fmi2Integer order[], fmi2Real value[])
    {
        int i;
        ModelInstance* comp = (ModelInstance*)c;
        if (invalidState(comp, "fmi2GetRealOutputDerivatives", MASK_fmi2GetRealOutputDerivatives))	return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2GetRealOutputDerivatives: nvr= %d", nvr)
        FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"fmi2GetRealOutputDerivatives: ignoring function call. This model cannot compute derivatives of outputs: MaxOutputDerivativeOrder=\"0\"")
        for (i = 0; i < nvr; i++) value[i] = 0;
        return fmi2Error;
    }

    fmi2Status fmi2CancelStep(fmi2Component c)
    {
        ModelInstance* comp = (ModelInstance*)c;
        if (invalidState(comp, "fmi2CancelStep", MASK_fmi2CancelStep))
        {
            // always fmi2CancelStep is invalid, because model is never in modelStepInProgress state.
            return fmi2Error;
        }
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2CancelStep")
        FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"fmi2CancelStep: Can be called when fmi2DoStep returned fmi2Pending. This is not the case.");
        // comp->state = modelStepCanceled;
        return fmi2Error;
    }

    fmi2Status fmi2DoStep(fmi2Component c, fmi2Real currentCommunicationPoint,fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPoint)
    {
        ModelInstance* comp = (ModelInstance*)c;
        //        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "DoStep called");
#ifdef PRINT
        printf("\n[ FMU ] fmi2DoStep\n");
#endif
        // Update knowledge of global time
        comp->globalTime = currentCommunicationPoint;
        // Update current step size
        comp->currentStepSize = communicationStepSize;
        // If the current global time is smaller than internal time, do nothing
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "Current global time: %g", comp->globalTime);
        if (!comp->cycleCompleted)
        {
            generator_simulate_and_wait(comp, fmi2True);
        }
        if (currentCommunicationPoint+communicationStepSize > comp->time)
        {
            return fmi2OK;
        }
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "fmi2DoStep: currentCommunicationPoint = %.16g communicationStepSize=%.16g, noSetFMUStatePriorToCurrentPoint =%i, internal time=%.16g",currentCommunicationPoint,communicationStepSize,noSetFMUStatePriorToCurrentPoint,comp->time)
        return fmi2OK;
    }

    fmi2Status fmi2SimulateUntilDiscontinuity(fmi2Component c, fmi2Real currentCommunicationPoint,fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPoint, fmi2Port* ports,
                                    size_t nPorts, fmi2Real* internalTime)
    {
        ModelInstance* comp = (ModelInstance*)c;
        generator* g = (generator*) comp->model;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "Called fmi2SimulateUntilDiscontinuity");
        // Update global time (current communiaction point)
        comp->globalTime = currentCommunicationPoint;
        comp->currentStepSize = communicationStepSize;
        // Update ports under eventMode
        g->ports = ports;
        g->nPorts = nPorts;
        // Run the simulation until an event occur
        // Wait for the event
        generator_simulate_and_wait(comp, fmi2False);
        *internalTime = comp->time;
        return fmi2OK;
    }

    fmi2Status fmi2SimulateUntilRead(fmi2Component c, fmi2Real currentCommunicationPoint,fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPoint, fmi2Port* ports,
                                              size_t nPorts, fmi2Real* nextEventTime)
    {
        ModelInstance* comp = (ModelInstance*)c;
        generator* g = (generator*) comp->model;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "Called fmi2SimulateUntilRead");
        // Update global time (current communiaction point)
        comp->globalTime = currentCommunicationPoint;
        comp->currentStepSize = communicationStepSize;
        // Update ports under eventMode
        g->ports = ports;
        g->nPorts = nPorts;
        // Run the simulation until an event occur
        // Wait for the event
        generator_simulate_and_wait(comp, fmi2False);
        *nextEventTime = comp->time;
        return fmi2OK;
    }

    fmi2Status fmi2SimulateUntilCondition(fmi2Component c, fmi2Real currentCommunicationPoint,fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPoint, fmi2Port* ports,
                                     size_t nPorts, fmi2Real* nextEventTime)
    {
        ModelInstance* comp = (ModelInstance*)c;
        generator* g = (generator*) comp->model;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "Called fmi2SimulateUntilCondition");
        // Update global time (current communiaction point)
        comp->globalTime = currentCommunicationPoint;
        comp->currentStepSize = communicationStepSize;
        // Update ports under eventMode
        g->ports = ports;
        g->nPorts = nPorts;
        // Run the simulation until an event occur
        // Wait for the event
        generator_simulate_and_wait(comp, fmi2False);
        *nextEventTime = comp->time;
        return fmi2OK;
    }
    /* Inquire slave status */

    static fmi2Status getStatus(char* fname, fmi2Component c, const fmi2StatusKind s)
    {
        const char* statusKind[3] = {"fmi2DoStepStatus","fmi2PendingStatus","fmi2LastSuccessfulTime"};
        ModelInstance* comp = (ModelInstance*)c;
        if (invalidState(comp, fname, MASK_fmi2GetStatus)) // all get status have the same MASK_fmi2GetStatus
            return fmi2Error;
        FILTERED_LOG(comp, fmi2OK, LOG_FMI_CALL, "$s: fmi2StatusKind = %s", fname, statusKind[s])
        switch(s)
        {
            case fmi2DoStepStatus:
                FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"%s: Can be called with fmi2DoStepStatus when fmi2DoStep returned fmi2Pending.This is not the case.", fname)
                break;
            case fmi2PendingStatus:
                FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"%s: Can be called with fmi2PendingStatus when fmi2DoStep returned fmi2Pending. This is not the case.", fname)
                break;
            case fmi2LastSuccessfulTime:
                FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"%s: Can be called with fmi2LastSuccessfulTime when fmi2DoStep returned fmi2Discard.This is not the case.", fname)
                break;
            case fmi2Terminated:
                FILTERED_LOG(comp, fmi2Error, LOG_ERROR,"%s: Can be called with fmi2Terminated when fmi2DoStep returned fmi2Discard. This is not the case.", fname)
                break;
        }
        // TODO Protezione di questa sezione con i mutex
        // Retrive current internal state
        if (comp->state == modelStepInProgress)
        {
            return fmi2Pending;
        }
        else if (comp->state == modelStepComplete)
        {
            return fmi2OK;
        }
        return fmi2Discard;
    }

    fmi2Status fmi2GetStatus(fmi2Component c, const fmi2StatusKind s, fmi2Status* value)
    {
        return getStatus("fmi2GetStatus", c, s);
    }

    fmi2Status fmi2GetRealStatus(fmi2Component c, const fmi2StatusKind s, fmi2Real* value)
    {
        if (s == fmi2LastSuccessfulTime)
        {
            ModelInstance* comp = (ModelInstance*)c;
            if (invalidState(comp, "fmi2GetRealStatus", MASK_fmi2GetRealStatus))return fmi2Error;
            *value = comp->time;
            return fmi2OK;
        }
        return getStatus("fmi2GetRealStatus", c, s);
    }

    fmi2Status fmi2GetIntegerStatus(fmi2Component c, const fmi2StatusKind s, fmi2Integer* value)
    {
        return getStatus("fmi2GetIntegerStatus", c, s);
    }

    fmi2Status fmi2GetBooleanStatus(fmi2Component c, const fmi2StatusKind s, fmi2Boolean* value)
    {
        if (s == fmi2Terminated)
        {
            ModelInstance* comp = (ModelInstance*)c;
            if (invalidState(comp, "fmi2GetBooleanStatus", MASK_fmi2GetBooleanStatus))	return fmi2Error;
            *value = comp->eventInfo.terminateSimulation;
            return fmi2OK;
        }
        return getStatus("fmi2GetBooleanStatus", c, s);
    }

    fmi2Status fmi2GetStringStatus(fmi2Component c, const fmi2StatusKind s, fmi2String* value)
    {
        return getStatus("fmi2GetStringStatus", c, s);
    }
}
