PACKAGE generator_pack IS
  CONSTANT ST_0 : INTEGER := 0;
  CONSTANT ST_1 : INTEGER := 1;
END generator_pack;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE WORK.generator_pack.ALL;
USE IEEE.NUMERIC_BIT.ALL;

entity generator is
	port (
    	clock	: in bit;
        cycles	: in integer;
        gen		: out bit
	);
end generator;

architecture generator of generator is
	subtype status_t is integer range 0 to 1;
	signal STATUS: status_t := ST_0;
	signal NEXT_STATUS: status_t := ST_0;
	signal Counter: integer := 0;
begin

process(STATUS, Counter)
	begin
		case STATUS is
		  when ST_0 =>
			NEXT_STATUS<=ST_1;
		  when ST_1   =>
			if Counter < cycles - 1 then
			  NEXT_STATUS<=ST_1;
			else
			  NEXT_STATUS<=ST_0;
			end if;
		end case;
	end process;

process(clock)
	begin
		if clock='1' then
			STATUS<=NEXT_STATUS;
            case NEXT_STATUS is
              when ST_0 =>
                Counter <= 0;
                gen <= not gen;
              when ST_1 =>
                Counter <= Counter + 1;
            end case;
         end if;
end process;
end generator;