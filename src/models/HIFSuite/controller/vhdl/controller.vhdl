PACKAGE controller_pack IS
  CONSTANT ST_0 : INTEGER := 0;
  CONSTANT ST_1 : INTEGER := 1;
  CONSTANT ST_2 : INTEGER := 2;
  CONSTANT ST_Reset: INTEGER := 3;
END controller_pack;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE WORK.controller_pack.ALL;
USE IEEE.NUMERIC_BIT.ALL;
use IEEE.MATH_REAL.ALL;

entity controller is
	port (	
    	clock       : in bit;
        sample		: in integer;
        upper_bound : in real;
		value 		: in real;
        gen 		: out bit
	);
end controller;

architecture controller of controller is
	subtype status_t is integer range 0 to 3;
	signal STATUS: status_t := ST_Reset;
	signal NEXT_STATUS: status_t := ST_Reset;
	signal Counter: integer := 0;
begin

	process(STATUS, Counter)
	begin
		case STATUS is
        when ST_Reset =>
      		NEXT_STATUS<=ST_0;
		when ST_0 =>
        	if Counter < sample then
				NEXT_STATUS <= ST_0;
            else
            	NEXT_STATUS <= ST_1;
            end if;
		when ST_1   =>
			if value < (upper_bound - 2.0) then
			  NEXT_STATUS <= ST_2;
			else
			  NEXT_STATUS <= ST_0;
			end if;
        when ST_2 =>
        	if Counter < sample then
				NEXT_STATUS<=ST_2;
            else
            	NEXT_STATUS<=ST_1;
            end if;
		end case;
	end process;

	process(clock)
	begin
	if clock'event and clock='1' then
      	STATUS<=NEXT_STATUS;
      	case NEXT_STATUS is
        	when ST_Reset =>
        		Counter <= 0;
                gen <= '0';
      		when ST_0 => 
      			Counter <= Counter + 1;
          		gen <= '0';
        	when ST_1 =>
      			Counter <= 0;
        	when ST_2 =>
        		gen <= '1';
      			Counter <= Counter + 1;
		end case;
	  end if;
	end process;
    
end controller;