// /////////////////////////////////////////////////////////////////////////
// C++ code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#ifndef CONTROLLER_HH
#define CONTROLLER_HH

#define CONTROLLER_CLOCK_HALF_PERIOD 0.0005
#define CONTROLLER_LOG 0

#include <iostream>
#include <fstream>

#include <stdint.h>
#include <systemc>
#include "controller_pack/controller_pack_dataTypes.hh"

#include <pthread.h>
#include "fmi2FunctionTypes.h"
#include "fmi2.h"

class controller
{

public:

    double value_old;
    double upper_bound_old;
    int32_t sample_old;
    bool clock_old;
    bool gen_old;

    struct controller_iostruct{
        bool clock;
        int32_t sample;
        double upper_bound;
        double value;
        bool gen;

        controller_iostruct():
            clock(false),
            sample(0L),
            upper_bound(0.0),
            value(0.0),
            gen(false)
        {}

        controller_iostruct( const bool clock_0, const int32_t sample_0, const double
             upper_bound_0, const double value_0, const bool gen_0 ):
            clock(clock_0),
            sample(sample_0),
            upper_bound(upper_bound_0),
            value(value_0),
            gen(gen_0)
        {}

        ~controller_iostruct()
        {}

        bool operator == (const controller_iostruct & other) const
        {
            if (clock != other.clock) return false;
            if (sample != other.sample) return false;
            if (upper_bound != other.upper_bound) return false;
            if (value != other.value) return false;
            if (gen != other.gen) return false;
            return true;
        }
    };
    controller_iostruct hif_a2t_data;
    typedef int32_t status_t;
    status_t status;
    status_t next_status;
    int32_t counter;
    bool clkSuphif_0;
    controller();
    ~controller();

    std::ofstream logfile;

    status_t status_new;
    status_t next_status_new;
    int32_t counter_new;
    bool clkSuphif_0_new;
    bool process_in_queue;
    bool flag_vhdl_process_executed;
    bool flag_status;
    bool flag_counter;
    bool flag_vhdl_process_0_UpdclkSuphif_0_executed;
    bool flag_clock;

    fmi2Port* ports;
    size_t nPorts;
    pthread_mutex_t m1;
    pthread_mutex_t m2;
    char*  variableUnderRead;
    bool isReading;
    bool isWriting;
    void preReadInputPort(char*);
    void postWriteOutputPort(char*);
    void update_outputs();

    void vhdl_process();


    void vhdl_process_0_UpdclkSuphif_0();


    void update_input_queue( bool synch_phase = true );


    void update_event_queue();


    void flag_elaboration();


    void synch_elaboration();


    void simulate( controller_iostruct * io_exchange, int32_t & cycles_number );


    void start_of_simulation();


    void initialize();


    void finalize();


private:

    controller( const controller & );
    const controller& operator= ( const controller & );


};


#endif

