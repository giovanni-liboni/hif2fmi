set(FMU "${MODEL}_fidel")
file(GLOB_RECURSE SRC_FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cc
)
include_directories(
        ${CMAKE_CURRENT_SOURCE_DIR}/cpp/inc
)
add_library(${FMU}
        SHARED ${SRC_FILES}
)
target_link_libraries(${FMU}
        systemc
        pthread
)
set_target_properties(${FMU}
        PROPERTIES
        LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/fmu/${FMU}/binaries/linux64
        LIBRARY_OUTPUT_NAME ${FMU}
        PREFIX ""
)
set_property(DIRECTORY
        APPEND
        PROPERTY
        ADDITIONAL_MAKE_CLEAN_FILES "${FMU}.fmu"
)
add_custom_command(TARGET ${FMU}
        POST_BUILD
        WORKING_DIRECTORY ${PROJECT_BINARY_DIR}/fmu/${FMU}
        COMMAND ln -s ${CMAKE_CURRENT_SOURCE_DIR}/modelDescription.xml .
        COMMAND zip -r ${FMU}.fmu binaries modelDescription.xml
        COMMAND mv ${FMU}.fmu ..
        COMMAND rm modelDescription.xml
)
add_dependencies(${FMU} SystemC)