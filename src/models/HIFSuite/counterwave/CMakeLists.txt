set(MODEL counterwave)

add_definitions(
	-Wno-write-strings
	-fPIC
)
include_directories(
		${SIMFMI_INCLUDE_DIRS}
)

add_subdirectory(normal)
add_subdirectory(fidel)