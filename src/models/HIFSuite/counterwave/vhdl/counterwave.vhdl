LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.NUMERIC_BIT.ALL;
use IEEE.MATH_REAL.ALL;

entity counterwave is
	port (
    	clock	: in bit;
        dataout : out integer := 0
	);
end counterwave;

architecture counterwave of counterwave is
begin
process(clock)
variable Counter: integer := 0;
begin
	if clock'event AND clock = '1' then
      if Counter < 100 then
          Counter := Counter + 1;
      else
          Counter := 0;
      end if;
    end if;
    dataout <= Counter;
end process;

end counterwave;