#!/usr/bin/env bash
set -e
set -u

MODEL=$1
if test -d $MODEL
then
    if test -e $MODEL/vhdl/$MODEL.vhdl
    then
        # Variables
        HIFXML_DIR=`echo $MODEL/hif_descriptions`
        HIF_MODEL=`echo $HIFXML_DIR/$MODEL.hif.xml`
        HIF_MODEL_DDT=`echo $HIFXML_DIR/"$MODEL"_ddt.hif.xml`
        HIF_MODEL_A2T=`echo $HIFXML_DIR/"$MODEL"_ddt_a2t.hif.xml`
        HIF_MODEL_CPP_NORMAL_DIR=`echo $MODEL/normal/cpp`
        HIF_MODEL_CPP_FIDEL_DIR=`echo $MODEL/fidel/cpp`
        HIFVHDL_SRC=`echo $MODEL/vhdl/$MODEL.vhdl`

        if test -e $HIF_MODEL_CPP_FIDEL_DIR
        then
            cd $MODEL
            mv fidel fidel.tmp
            cd ..
        fi

        if test -e $HIF_MODEL_CPP_NORMAL_DIR
        then
            cd $MODEL
            mv normal normal.tmp
            cd ..
        fi

        vhdl2hif -o $HIF_MODEL $HIFVHDL_SRC
        ddt -o $HIF_MODEL_DDT $HIF_MODEL
        a2tool -p CPP -o $HIF_MODEL_A2T $HIF_MODEL_DDT
        hif2sc -D $HIF_MODEL_CPP_NORMAL_DIR $HIF_MODEL_A2T
        hif2sc -D $HIF_MODEL_CPP_FIDEL_DIR $HIF_MODEL_A2T

        rm VHDL2HIF_0*
        rm HIF2SC_11_STD_0*


        if test -e $HIF_MODEL_CPP_FIDEL_DIR
        then
            mv $MODEL/fidel.tmp/CMakeLists.txt $MODEL/fidel
            mv $MODEL/fidel.tmp/modelDescription.xml $MODEL/fidel
            mv $MODEL/fidel.tmp/cpp/inc/fmuInterface.hh $MODEL/fidel/cpp/inc
            mv $MODEL/fidel.tmp/cpp/src/fmuInterface.cc $MODEL/fidel/cpp/src
            rm -fr $MODEL/fidel.tmp
        fi

        if test -e $HIF_MODEL_CPP_NORMAL_DIR
        then
            mv $MODEL/normal.tmp/CMakeLists.txt $MODEL/normal
            mv $MODEL/normal.tmp/modelDescription.xml $MODEL/normal
            mv $MODEL/normal.tmp/cpp/inc/fmuInterface.hh $MODEL/normal/cpp/inc
            mv $MODEL/normal.tmp/cpp/src/fmuInterface.cc $MODEL/normal/cpp/src
            rm -fr $MODEL/normal.tmp
        fi
    fi
else
    echo "Please specify a valid directory!";
fi
