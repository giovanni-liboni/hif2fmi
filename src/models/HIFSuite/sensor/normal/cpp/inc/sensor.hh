// /////////////////////////////////////////////////////////////////////////
// C++ code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#ifndef SENSOR_HH
#define SENSOR_HH

#define SENSOR_CLOCK_HALF_PERIOD 0.0005
#define SENSOR_LOG 1

#include <iostream>
#include <fstream>

#include <stdint.h>
#include <systemc>
#include "sensor_pack/sensor_pack_dataTypes.hh"

using namespace std;

class sensor
{

public:

    double u_old;
    int32_t sample_old;
    bool clock_old;
    struct sensor_iostruct{
        bool clock;
        int32_t sample;
        double u;
        double y;

        sensor_iostruct():
            clock(false),
            sample(0L),
            u(0.0),
            y(0.0)
        {}

        sensor_iostruct( const bool clock_0, const int32_t sample_0, const double
             u_0, const double y_0 ):
            clock(clock_0),
            sample(sample_0),
            u(u_0),
            y(y_0)
        {}

        ~sensor_iostruct()
        {}

        bool operator == (const sensor_iostruct & other) const
        {
            if (clock != other.clock) return false;
            if (sample != other.sample) return false;
            if (u != other.u) return false;
            if (y != other.y) return false;
            return true;
        }
    };
    sensor_iostruct hif_a2t_data;
    typedef int32_t status_t;
    status_t status;
    status_t next_status;
    int32_t counter;
    bool clkSuphif_0;
    sensor();
    ~sensor();

    std::ofstream logfile;
    int32_t cycles;

    status_t status_new;
    status_t next_status_new;
    int32_t counter_new;
    bool clkSuphif_0_new;
    bool process_in_queue;
    bool flag_vhdl_process_executed;
    bool flag_status;
    bool flag_counter;
    bool flag_vhdl_process_0_UpdclkSuphif_0_executed;
    bool flag_clock;

    void vhdl_process();


    void vhdl_process_0_UpdclkSuphif_0();


    void update_input_queue( bool synch_phase = true );


    void update_event_queue();


    void flag_elaboration();


    void synch_elaboration();


    void simulate( sensor_iostruct * io_exchange, int32_t & cycles_number );


    void start_of_simulation();


    void initialize();


    void finalize();


private:

    sensor( const sensor & );
    const sensor& operator= ( const sensor & );


};


#endif

