PACKAGE sensor_pack IS
  CONSTANT ST_0 : INTEGER := 0;
  CONSTANT ST_1 : INTEGER := 1;
  CONSTANT ST_Reset: INTEGER := 2;
END sensor_pack;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE WORK.sensor_pack.ALL;
USE IEEE.NUMERIC_BIT.ALL;
use IEEE.MATH_REAL.ALL;

entity sensor is
	port (
    	clock	: in bit;
        sample	: in integer;
		u 		: in real;
        y 		: out real
	);
end sensor;

architecture sensor of sensor is
	subtype status_t is integer range 0 to 2;
	signal STATUS: status_t := ST_Reset;
	signal NEXT_STATUS: status_t := ST_Reset;
	signal Counter: integer := 0;
begin

	process(STATUS, Counter)
	begin
		case STATUS is
        when ST_Reset =>
      		NEXT_STATUS <= ST_0;
		when ST_0 =>
        	if Counter < sample - 1 then
				NEXT_STATUS <= ST_0;
            else
            	NEXT_STATUS <= ST_1;
            end if;
		when ST_1   =>
			NEXT_STATUS <= ST_0;
		end case;
	end process;

	process(clock)
	begin
	if clock'event and clock='1' then
      	STATUS<=NEXT_STATUS;
      	case NEXT_STATUS is
        	when ST_Reset =>
        		Counter <= 0;
                y <= 0.0;
      		when ST_0 =>
      			Counter <= Counter + 1;
          		y <= y;
        	when ST_1 =>
      			y <= u;
                Counter <= 0;
		end case;
	  end if;
	end process;

end sensor;