# HIFSuite models sources

## `wheel_encoder`
This model generates a boolean value, the interval between two signal it's specified by the `cycles` variable. The waiting period it's defined by `cycles * clock period`. 

### Input ports
- `cycles` : Cycles to wait to switch the new signal value to the opposite of the old one;
- `clock` : Clock signal;

### Output ports
- `gen` : Boolean signal;

### Model configuration
To define a clock period, change the default value in `generator.hh`, expressed in second. Default value 10ms.

```c
#define GENERATOR_CLOCK_HALF_PERIOD 0.005
```

## `controller`
Contains a controller model

## `generator`
Contains an event generator

## `root`
Contains a root model, normal implementation provided by Stefano Centomo.

## `root_testbench`
Contains the testbench for the `root` model


# **!! WARNING: hif2sc target directory will be erase !!**

## Generate CPP code from a VHDL description

```bash
$ vhdl2hif -o hif/root vhdl/root.vhdl
$ ddt -o hif/root_ddt hif/root.hif.xml
$ a2tool -p CPP -o hif/root_ddt_a2t hif/root_ddt.hif.xml
$ hif2sc -D fmu/cpp/hif hif/root_ddt_a2t.hif.xml
```

## Second method to generate CPP code
If the directory has the same name as the model, you can launch `vhdl2cpp.sh` script:

```bash
$ ./vhdl2cpp.sh <name-of-direcotry>
```

It generates the CPP code from VHDL description using HIFSuite.