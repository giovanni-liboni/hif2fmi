model MyHeatingResistor "MyTest"
  extends Modelica.Icons.Example;
  import L = Modelica.Electrical.Digital.Interfaces.Logic;
  Modelica.Electrical.Analog.Basic.Ground G annotation(Placement(visible = true, transformation(origin = {-20, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Capacitor CapacitorCPU(C = 0.005) annotation(Placement(visible = true, transformation(origin = {80, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 90)));
  Modelica.Electrical.Analog.Ideal.IdealCommutingSwitch switch annotation(Placement(visible = true, transformation(origin = {0, 20}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor ResistorCPU(R = 2000) annotation(Placement(visible = true, transformation(origin = {40, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  // Modelica.Electrical.Digital.Converters.LogicToBoolean L2B(n = 1) "convert a logic signal into a boolean" annotation(Placement(visible = true, transformation(origin = {-20, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage runningMAxTemp(V = 95) annotation(Placement(visible = true, transformation(origin = {-40, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage roomTemperature(V = 25) annotation(Placement(visible = true, transformation(origin = {-20, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  discrete input Modelica.Blocks.Interfaces.BooleanInput openclose annotation(Placement(visible = true, transformation(origin = {-76, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-80, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  //replaceable Modelica.Electrical.Digital.Converters.RealToLogic R2L(n = 1, upper_limit = 0.5, lower_limit = 0.5, upper_value = L.'1', lower_value = L.'0', middle_value = L.'0') annotation(Placement(visible = true, transformation(origin = {-50, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(openclose, switch.control) annotation(
    Line(points = {{-76, 40}, {0, 40}, {0, 28}, {0, 28}}, color = {0, 0, 127}));
 /* if openclose then
    //L2B.x[1] = L.'1';
    switch.control = true;
  else
    //L2B.x[1] = L.'0';
    switch.control = false;
  end if;*/
  //connect(L2B.x[1], R2L.y[1]) annotation(Line(points = {{-25, 40}, {-46, 40}, {-46, 40}, {-46, 40}, {-46, 40}}, color = {127, 0, 127}));
  //connect(R2L.x[1], openclose) annotation(Line(points = {{-55, 40}, {-70, 40}, {-70, 40}, {-70, 40}}, color = {0, 0, 127}));
  connect(roomTemperature.p, switch.n2) annotation(Line(points = {{-20, 10}, {-20.3137, 20.3922}, {-10.6666, 20.3922}, {-10.3529, 20.1569}}, color = {0, 0, 255}));
  connect(roomTemperature.n, G.p) annotation(Line(points = {{-20, -10}, {-20.3137, -29.1765}, {-20.7059, -29.1765}, {-20.3922, -29.4118}}, color = {0, 0, 255}));
  connect(runningMAxTemp.p, switch.n1) annotation(Line(points = {{-40, 10}, {-40.3137, 24.9412}, {-10.3529, 24.9412}, {-10.0392, 24.7059}}, color = {0, 0, 255}));
  connect(runningMAxTemp.n, G.p) annotation(Line(points = {{-40, -10}, {-40.3137, -29.4902}, {-19.9215, -29.4902}, {-19.6078, -29.7255}}, color = {0, 0, 255}));
  // connect(L2B.y[1], switch.control) annotation(Line(points = {{-15, 40}, {0.156875, 40.2353}, {0.156875, 28.2353}, {0, 28}}, color = {255, 0, 255}));
  connect(ResistorCPU.p, switch.p) annotation(Line(points = {{30, 20}, {10.0392, 20.2353}, {10.0392, 20.2353}, {10, 20}}, color = {0, 0, 255}));
  connect(ResistorCPU.n, CapacitorCPU.p) annotation(Line(points = {{50, 20}, {79.8432, 20.2353}, {79.8432, 11.2941}, {80.1569, 11.0588}}, color = {0, 0, 255}));
  connect(CapacitorCPU.n, G.p) annotation(Line(points = {{80, -10}, {79.6863, -29.6471}, {-20.3921, -29.6471}, {-20.0784, -29.8823}}, color = {0, 0, 255}));
  annotation(Diagram(coordinateSystem(preserveAspectRatio = true, extent = {{-100, -100}, {100, 100}}), graphics = {Text(extent = {{-94, 102}, {0, 74}}, lineColor = {0, 0, 255}, textString = "HeatingResistor")}), Documentation(info = "<html>
<p>This is a very simple circuit consisting of a voltage source and a resistor. The loss power in the resistor is transported to the environment via its heatPort.</p>
</html>"), experiment(StopTime = 5));
end MyHeatingResistor;