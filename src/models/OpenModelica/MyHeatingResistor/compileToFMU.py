# Import the compiler function
from pymodelica import compile_fmu
import sys



# Specify Modelica model and model file (.mo or .mop)
model_name = sys.argv[1]
mo_file = sys.argv[1]+'.mo'
# Compile the model and save the return argument, which is the file name of the FMU
my_fmu = compile_fmu(model_name, mo_file, target='cs', version='2.0')
