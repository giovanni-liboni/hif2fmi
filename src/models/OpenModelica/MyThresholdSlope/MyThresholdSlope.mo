block MyThresholdSlope "Generate sine signal"
  parameter Real a = 0.3 "Slope";
  parameter Real c = 1 "Slope2";
  parameter Real b = 2 "Initial value";
  parameter Modelica.SIunits.Time startTime = 0 "Output = offset for time < startTime";
  input Integer x;
  output Real y;
equation
  y = if time < startTime then 0 else if x < 50 then a * time + b else (c * time) + b;
end MyThresholdSlope;