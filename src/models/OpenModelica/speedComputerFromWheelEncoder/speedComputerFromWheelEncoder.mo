model speedComputerFromWheelEncoder "compute a speed according to wheel encoder input"
  parameter Integer wheelDiameter = 1 "in meter, by default its a by truck :)";
  parameter Integer wheelEncoderSensorNumber = 32 "by default change value when 1/32 revilution is done";
  output Real wheelSpeed "in meter by seconds";
  input Integer wheelEncoder "switch from 0 to 1 when reacch 1/wheelEncoderSensorNumber revolution";
  constant Real pi = Modelica.Constants.pi;
  Real lastChange[2] (start={0.0,0.0});
  Integer nextValue (start=1);
algorithm 
   when (wheelEncoder == nextValue)
  then 
    nextValue := if (wheelEncoder==0) then 1 else 0;
    lastChange := {time, lastChange[1]};
    wheelSpeed := ((wheelDiameter * pi)/wheelEncoderSensorNumber) /(lastChange[1] -lastChange[2]);
  end when;
equation


end speedComputerFromWheelEncoder;