block MySinusGenerator "Generate sine signal"
  parameter Real amplitude = 1 "Amplitude of sine wave";
  parameter Modelica.SIunits.Frequency freqHz(start = 1) "Frequency of sine wave";
  parameter Modelica.SIunits.Angle phase = 0 "Phase of sine wave";
  parameter Real offset = 0 "Offset of output signal";
  parameter Modelica.SIunits.Time startTime = 0 "Output = offset for time < startTime";
  output Real y;
protected
  constant Real pi = Modelica.Constants.pi;
equation
  y = offset + (if time < startTime then 0 else amplitude * Modelica.Math.sin(2 * pi * freqHz * (time - startTime) + phase));
end MySinusGenerator;
