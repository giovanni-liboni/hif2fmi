# FMI APIs 

Contains header files for FMI APIs. There are two different directories:

- `fidel` : Contains modified headers, temporary version 2.2
- `normal` : Contains vanilla headers, version 2.0

## Development `FIDEL` API

1. Define and export the new method in `fmi2Functions.h` , es:

```c
#define fmi2SimulateUntilDiscontinuity            fmi2FullName(fmi2SimulateUntilDiscontinuity)

...

FMI2_Export fmi2SimulateUntilDiscontinuityTYPE fmi2SimulateUntilDiscontinuity;
``` 

2. Add to `fmi2FunctionTypes.h`, es:

```c
typedef fmi2Status fmi2SimulateUntilDiscontinuityTYPE (fmi2Component, fmi2Real, fmi2Real, fmi2Boolean, fmi2Port*, size_t, fmi2Real*);
```

3. If you want to include the new API inside the simulator `simfmi`, please refer to the `README` inside `simfmi`.

## New APIs

#### `fmi2SimulateUntilDiscontinuity` 

```c
/**
 * Simulate a FMU until an event or a change occurs on ports.
 * fmi2Component c
 * fmi2Real currentCommunicationPoint Same as in fmi2DoStep
 * fmi2Real communicationStepSize Same as in fmi2DoStep
 * fmi2Boolean noSetFMUStatePriorToCurrentPoint Same as in fmi2DoStep
 * fmi2Port* ports Ports to monitor, only these ports are monitored;
 * size_t nPorts Size of the ports array;
 * fmi2Real* internalTime Internal time of the FMU, if an event occured it's the time of that event;
 */
fmi2Status 
fmi2SimulateUntilDiscontinuity (
            fmi2Component c, 
            fmi2Real currentCommunicationPoint, 
			fmi2Real communicationStepSize, 
			fmi2Boolean noSetFMUStatePriorToCurrentPoint, 
			fmi2Port* ports, size_t nPorts, 
			fmi2Real* internalTime );
```

#### `fmi2Port` struct
A `fmi2Port` represents a input or output port. 

```c
typedef struct
{
    simType             type;
    fmi2ValueReference  vref;
    fmi2Real            rvalue;
    fmi2Boolean         bvalue;
    fmi2Integer         ivalue;
    fmi2String          svalue;
    fmi2Char            cvalue;
    char*               name;
    fmi2Boolean         eventPort;
    fmi2Real            nextEventTime;
} fmi2Port;
```

#### `Type` enum
Defined the type of a value

```c
typedef enum
{
    Boolean,
    Integer,
    String,
    Real,
    Byte
} simType;
```

Last Update: 22-02-2017