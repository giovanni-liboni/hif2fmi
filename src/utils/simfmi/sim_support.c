/* -------------------------------------------------------------------------
 * sim_support.c
 * Functions used by both FMU simulators fmu20sim_me and fmu20sim_cs
 * to parse command-line arguments, to unzip and load an fmu,
 * to write CSV file, and more.
 *
 * Revision history
 *  07.03.2014 initial version released in FMU SDK 2.0.0
 *  10.04.2014 use FMI 2.0 headers that prefix function and type names with 'fmi2'.
 *             When 'fmi2' functions are not found in loaded DLL, look also for
 *             FMI 2.0 RC1 function names.
 *
 * Author: Adrian Tirea
 * Original work Copyright QTronic GmbH. All rights reserved.
 * Modified work Giovanni Liboni <giovanni.liboni@inria.fr>
 * -------------------------------------------------------------------------*/

#include <fmi2FunctionTypes.h>
#include "sim_support.h"
#include "fmi2.h"


#if WINDOWS
int unzip(const char* zipPath, const char* outPath)
{
    int code;
    char cwd[BUFSIZE];
    char binPath[BUFSIZE];
    int n = strlen(UNZIP_CMD) + strlen(outPath) + 3 +  strlen(zipPath) + 9;
    char* cmd = (char*)calloc(sizeof(char), n);
    // remember current directory
    if (!GetCurrentDirectory(BUFSIZE, cwd))
    {
        printf ("error: Could not get current directory\n");
        return 0; // error
    }
    // change to %FMUSDK_HOME%\bin to find 7z.dll and 7z.exe
    if (!GetEnvironmentVariable("FMUSDK_HOME", binPath, BUFSIZE))
    {
        if (GetLastError() == ERROR_ENVVAR_NOT_FOUND)
        {
            printf ("error: Environment variable FMUSDK_HOME not defined\n");
        }
        else
        {
            printf ("error: Could not get value of FMUSDK_HOME\n");
        }
        return 0; // error
    }
    strcat(binPath, "\\bin");
    if (!SetCurrentDirectory(binPath))
    {
        printf ("error: could not change to directory '%s'\n", binPath);
        return 0; // error
    }
    // run the unzip command
    // remove "> NUL" to see the unzip protocol
    sprintf(cmd, "%s\"%s\" \"%s\" > NUL", UNZIP_CMD, outPath, zipPath);
    // printf("cmd='%s'\n", cmd);
    code = system(cmd);
    free(cmd);
    if (code != SEVEN_ZIP_NO_ERROR)
    {
        printf("7z: ");
        switch (code)
        {
            case SEVEN_ZIP_WARNING:
                printf("warning\n");
                break;
            case SEVEN_ZIP_ERROR:
                printf("error\n");
                break;
            case SEVEN_ZIP_COMMAND_LINE_ERROR:
                printf("command line error\n");
                break;
            case SEVEN_ZIP_OUT_OF_MEMORY:
                printf("out of memory\n");
                break;
            case SEVEN_ZIP_STOPPED_BY_USER:
                printf("stopped by user\n");
                break;
            default:
                printf("unknown problem\n");
        }
    }
    // restore current directory
    SetCurrentDirectory(cwd);
    return (code == SEVEN_ZIP_NO_ERROR || code == SEVEN_ZIP_WARNING) ? 1 : 0;
}

#else /* WINDOWS */

int unzip(const char* zipPath, const char* outPath)
{
    int code;
    char cwd[BUFSIZE];
    int n;
    char* cmd;
    // remember current directory
    if (!getcwd(cwd, BUFSIZE))
    {
        printf ("error: Could not get current directory\n");
        return 0; // error
    }
    // run the unzip command
    n = strlen(UNZIP_CMD) + strlen(outPath) + 1 +  strlen(zipPath) + 16;
    cmd = (char*)calloc(sizeof(char), n);
    sprintf(cmd, "%s%s \"%s\" > /dev/null", UNZIP_CMD, outPath, zipPath);
#if DEBUG
    printf("cmd='%s'\n", cmd);
#endif
    code = system(cmd);
    free(cmd);
    if (code!=SEVEN_ZIP_NO_ERROR)
    {
        printf("%s: ", UNZIP_CMD);
        switch (code)
        {
            case 1:
                printf("warning\n");
                break;
            case 2:
                printf("error\n");
                break;
            case 3:
                printf("severe error\n");
                break;
            case 4:
            case 5:
            case 6:
            case 7:
                printf("out of memory\n");
                break;
            case 10:
                printf("command line error\n");
                break;
            default:
                printf("unknown problem %d\n", code);
        }
    }
    // restore current directory
    chdir(cwd);
    return (code==SEVEN_ZIP_NO_ERROR || code==SEVEN_ZIP_WARNING) ? 1 : 0;
}
#endif /* WINDOWS */

#ifdef _MSC_VER
// fileName is an absolute path, e.g. C:\test\a.fmu
// or relative to the current dir, e.g. ..\test\a.fmu
// Does not check for existence of the file
static char* getFmuPath(const char* fileName)
{
    char pathName[MAX_PATH];
    int n = GetFullPathName(fileName, MAX_PATH, pathName, NULL);
    return n ? strdup(pathName) : NULL;
}

static char* getTmpPath()
{
    char tmpPath[BUFSIZE];
    if(! GetTempPath(BUFSIZE, tmpPath))
    {
        printf ("error: Could not find temporary disk space\n");
        return NULL;
    }
    strcat(tmpPath, "fmu\\");
    return strdup(tmpPath);
}

#else
// fmuFileName is an absolute path, e.g. "C:\test\a.fmu"
// or relative to the current dir, e.g. "..\test\a.fmu"
static char* getFmuPath(const char* fmuFileName)
{
    /* Not sure why this is useful.  Just returning the filename. */
    return strdup(fmuFileName);
}
static char* getTmpPath()
{
    char* results;
    char template[13];  // Lenght of "fmuTmpXXXXXX" + null
    sprintf(template, "%s", "fmuTmpXXXXXX");
    //char *tmp = mkdtemp(strdup("fmuTmpXXXXXX"));
    char* tmp = mkdtemp(template);
    if (tmp==NULL)
    {
        fprintf(stderr, "Couldn't create temporary directory\n");
        exit(1);
    }
    results = calloc(sizeof(char), strlen(tmp) + 2048);
    if (getcwd(results, 2048) == NULL)
    {
        fprintf(stderr, "getcwd() error\n");
        exit(1);
    }
    strcat(results, "/");
    strncat(results, tmp, strlen(tmp));
    return strcat(results, "/");
}
#endif

char* getTempResourcesLocation(char* path)
{
    char* resourcesLocation = (char*)calloc(sizeof(char), 9 + strlen(RESOURCES_DIR) + strlen(path));
    strcpy(resourcesLocation, "file:///");
    strcat(resourcesLocation, path);
    strcat(resourcesLocation, RESOURCES_DIR);
    return resourcesLocation;
}

static void* getAdr(int* success, HMODULE dllHandle, const char* functionName)
{
    void* fp;
#ifdef _MSC_VER
    fp = GetProcAddress(dllHandle, functionName);
#else
    fp = dlsym(dllHandle, functionName);
#endif
    if (!fp)
    {
#ifdef _MSC_VER
#else
        printf ("Error was: %s\n", dlerror());
#endif
        printf("warning: Function %s not found in dll\n", functionName);
        *success = 0;
    }
    return fp;
}

// Load the given dll and set function pointers in fmu
// Return 0 to indicate failure
static int loadDll(const char* dllPath, FMU* fmu)
{
    int s = 1;
#ifdef _MSC_VER
    HMODULE h = LoadLibrary(dllPath);
#else
#if DEBUG
    printf("dllPath = %s\n", dllPath);
#endif
    HMODULE h = dlopen(dllPath, RTLD_LAZY);
#endif
    if (!h)
    {
#ifdef _MSC_VER
#else
        printf("The error was: %s\n", dlerror());
#endif
        printf("error: Could not load %s\n", dllPath);
        return 0; // failure
    }
    fmu->dllHandle = h;
    fmu->getTypesPlatform          = (fmi2GetTypesPlatformTYPE*)      getAdr(&s, h, "fmi2GetTypesPlatform");
    fmu->getVersion                = (fmi2GetVersionTYPE*)            getAdr(&s, h, "fmi2GetVersion");
    fmu->setDebugLogging           = (fmi2SetDebugLoggingTYPE*)       getAdr(&s, h, "fmi2SetDebugLogging");
    fmu->instantiate               = (fmi2InstantiateTYPE*)           getAdr(&s, h, "fmi2Instantiate");
    fmu->freeInstance              = (fmi2FreeInstanceTYPE*)          getAdr(&s, h, "fmi2FreeInstance");
    fmu->setupExperiment           = (fmi2SetupExperimentTYPE*)       getAdr(&s, h, "fmi2SetupExperiment");
    fmu->enterInitializationMode   = (fmi2EnterInitializationModeTYPE*) getAdr(&s, h, "fmi2EnterInitializationMode");
    fmu->exitInitializationMode    = (fmi2ExitInitializationModeTYPE*) getAdr(&s, h, "fmi2ExitInitializationMode");
    fmu->terminate                 = (fmi2TerminateTYPE*)             getAdr(&s, h, "fmi2Terminate");
    fmu->reset                     = (fmi2ResetTYPE*)                 getAdr(&s, h, "fmi2Reset");
    fmu->getReal                   = (fmi2GetRealTYPE*)               getAdr(&s, h, "fmi2GetReal");
    fmu->getInteger                = (fmi2GetIntegerTYPE*)            getAdr(&s, h, "fmi2GetInteger");
    fmu->getBoolean                = (fmi2GetBooleanTYPE*)            getAdr(&s, h, "fmi2GetBoolean");
    fmu->getString                 = (fmi2GetStringTYPE*)             getAdr(&s, h, "fmi2GetString");
    fmu->setReal                   = (fmi2SetRealTYPE*)               getAdr(&s, h, "fmi2SetReal");
    fmu->setInteger                = (fmi2SetIntegerTYPE*)            getAdr(&s, h, "fmi2SetInteger");
    fmu->setBoolean                = (fmi2SetBooleanTYPE*)            getAdr(&s, h, "fmi2SetBoolean");
    fmu->setString                 = (fmi2SetStringTYPE*)             getAdr(&s, h, "fmi2SetString");
    fmu->getFMUstate               = (fmi2GetFMUstateTYPE*)           getAdr(&s, h, "fmi2GetFMUstate");
    fmu->setFMUstate               = (fmi2SetFMUstateTYPE*)           getAdr(&s, h, "fmi2SetFMUstate");
    fmu->freeFMUstate              = (fmi2FreeFMUstateTYPE*)          getAdr(&s, h, "fmi2FreeFMUstate");
    fmu->serializedFMUstateSize    = (fmi2SerializedFMUstateSizeTYPE*) getAdr(&s, h, "fmi2SerializedFMUstateSize");
    fmu->serializeFMUstate         = (fmi2SerializeFMUstateTYPE*)     getAdr(&s, h, "fmi2SerializeFMUstate");
    fmu->deSerializeFMUstate       = (fmi2DeSerializeFMUstateTYPE*)   getAdr(&s, h, "fmi2DeSerializeFMUstate");
    fmu->getDirectionalDerivative  = (fmi2GetDirectionalDerivativeTYPE*) getAdr(&s, h, "fmi2GetDirectionalDerivative");
#ifdef FMI_COSIMULATION
    fmu->setRealInputDerivatives   = (fmi2SetRealInputDerivativesTYPE*) getAdr(&s, h, "fmi2SetRealInputDerivatives");
    fmu->getRealOutputDerivatives  = (fmi2GetRealOutputDerivativesTYPE*) getAdr(&s, h, "fmi2GetRealOutputDerivatives");
    fmu->doStep                    = (fmi2DoStepTYPE*)                getAdr(&s, h, "fmi2DoStep");
    fmu->cancelStep                = (fmi2CancelStepTYPE*)            getAdr(&s, h, "fmi2CancelStep");
    fmu->getStatus                 = (fmi2GetStatusTYPE*)             getAdr(&s, h, "fmi2GetStatus");
    fmu->getRealStatus             = (fmi2GetRealStatusTYPE*)         getAdr(&s, h, "fmi2GetRealStatus");
    fmu->getIntegerStatus          = (fmi2GetIntegerStatusTYPE*)      getAdr(&s, h, "fmi2GetIntegerStatus");
    fmu->getBooleanStatus          = (fmi2GetBooleanStatusTYPE*)      getAdr(&s, h, "fmi2GetBooleanStatus");
    fmu->getStringStatus           = (fmi2GetStringStatusTYPE*)       getAdr(&s, h, "fmi2GetStringStatus");
#else // FMI2 for Model Exchange
    fmu->enterEventMode            = (fmi2EnterEventModeTYPE*)        getAdr(&s, h, "fmi2EnterEventMode");
    fmu->newDiscreteStates         = (fmi2NewDiscreteStatesTYPE*)     getAdr(&s, h, "fmi2NewDiscreteStates");
    fmu->enterContinuousTimeMode   = (fmi2EnterContinuousTimeModeTYPE*) getAdr(&s, h, "fmi2EnterContinuousTimeMode");
    fmu->completedIntegratorStep   = (fmi2CompletedIntegratorStepTYPE*) getAdr(&s, h, "fmi2CompletedIntegratorStep");
    fmu->setTime                   = (fmi2SetTimeTYPE*)               getAdr(&s, h, "fmi2SetTime");
    fmu->setContinuousStates       = (fmi2SetContinuousStatesTYPE*)   getAdr(&s, h, "fmi2SetContinuousStates");
    fmu->getDerivatives            = (fmi2GetDerivativesTYPE*)        getAdr(&s, h, "fmi2GetDerivatives");
    fmu->getEventIndicators        = (fmi2GetEventIndicatorsTYPE*)    getAdr(&s, h, "fmi2GetEventIndicators");
    fmu->getContinuousStates       = (fmi2GetContinuousStatesTYPE*)   getAdr(&s, h, "fmi2GetContinuousStates");
    fmu->getNominalsOfContinuousStates = (fmi2GetNominalsOfContinuousStatesTYPE*) getAdr(&s, h, "fmi2GetNominalsOfContinuousStates");
#endif
    const char* fmiVersion = getAttributeValue((Element*)fmu->modelDescription, att_fmiVersion);
    if (fmiVersion!= NULL && strcmp(fmiVersion, "2.2") == 0)
    {
        fmu->simulateUntilDiscontinuity = (fmi2SimulateUntilDiscontinuityTYPE*)      getAdr(&s, h, "fmi2SimulateUntilDiscontinuity");
        fmu->simulateUntilCondition     = (fmi2SimulateUntilConditionTYPE*) getAdr(&s, h, "fmi2SimulateUntilCondition");
        fmu->simulateUntilRead          = (fmi2SimulateUntilReadTYPE*) getAdr(&s, h, "fmi2SimulateUntilRead");
    }
    if (fmu->getVersion == NULL && fmu->instantiate == NULL)
    {
        printf("warning: Functions from FMI 2.0 could not be found in %s\n", dllPath);
        printf("warning: Simulator will look for FMI 2.0 RC1 functions names...\n");
        fmu->getTypesPlatform          = (fmi2GetTypesPlatformTYPE*)      getAdr(&s, h, "fmiGetTypesPlatform");
        fmu->getVersion                = (fmi2GetVersionTYPE*)            getAdr(&s, h, "fmiGetVersion");
        fmu->setDebugLogging           = (fmi2SetDebugLoggingTYPE*)       getAdr(&s, h, "fmiSetDebugLogging");
        fmu->instantiate               = (fmi2InstantiateTYPE*)           getAdr(&s, h, "fmiInstantiate");
        fmu->freeInstance              = (fmi2FreeInstanceTYPE*)          getAdr(&s, h, "fmiFreeInstance");
        fmu->setupExperiment           = (fmi2SetupExperimentTYPE*)       getAdr(&s, h, "fmiSetupExperiment");
        fmu->enterInitializationMode   = (fmi2EnterInitializationModeTYPE*) getAdr(&s, h, "fmiEnterInitializationMode");
        fmu->exitInitializationMode    = (fmi2ExitInitializationModeTYPE*) getAdr(&s, h, "fmiExitInitializationMode");
        fmu->terminate                 = (fmi2TerminateTYPE*)             getAdr(&s, h, "fmiTerminate");
        fmu->reset                     = (fmi2ResetTYPE*)                 getAdr(&s, h, "fmiReset");
        fmu->getReal                   = (fmi2GetRealTYPE*)               getAdr(&s, h, "fmiGetReal");
        fmu->getInteger                = (fmi2GetIntegerTYPE*)            getAdr(&s, h, "fmiGetInteger");
        fmu->getBoolean                = (fmi2GetBooleanTYPE*)            getAdr(&s, h, "fmiGetBoolean");
        fmu->getString                 = (fmi2GetStringTYPE*)             getAdr(&s, h, "fmiGetString");
        fmu->setReal                   = (fmi2SetRealTYPE*)               getAdr(&s, h, "fmiSetReal");
        fmu->setInteger                = (fmi2SetIntegerTYPE*)            getAdr(&s, h, "fmiSetInteger");
        fmu->setBoolean                = (fmi2SetBooleanTYPE*)            getAdr(&s, h, "fmiSetBoolean");
        fmu->setString                 = (fmi2SetStringTYPE*)             getAdr(&s, h, "fmiSetString");
        fmu->getFMUstate               = (fmi2GetFMUstateTYPE*)           getAdr(&s, h, "fmiGetFMUstate");
        fmu->setFMUstate               = (fmi2SetFMUstateTYPE*)           getAdr(&s, h, "fmiSetFMUstate");
        fmu->freeFMUstate              = (fmi2FreeFMUstateTYPE*)          getAdr(&s, h, "fmiFreeFMUstate");
        fmu->serializedFMUstateSize    = (fmi2SerializedFMUstateSizeTYPE*) getAdr(&s, h, "fmiSerializedFMUstateSize");
        fmu->serializeFMUstate         = (fmi2SerializeFMUstateTYPE*)     getAdr(&s, h, "fmiSerializeFMUstate");
        fmu->deSerializeFMUstate       = (fmi2DeSerializeFMUstateTYPE*)   getAdr(&s, h, "fmiDeSerializeFMUstate");
        fmu->getDirectionalDerivative  = (fmi2GetDirectionalDerivativeTYPE*) getAdr(&s, h, "fmiGetDirectionalDerivative");
#ifdef FMI_COSIMULATION
        fmu->setRealInputDerivatives   = (fmi2SetRealInputDerivativesTYPE*) getAdr(&s, h, "fmiSetRealInputDerivatives");
        fmu->getRealOutputDerivatives  = (fmi2GetRealOutputDerivativesTYPE*) getAdr(&s, h, "fmiGetRealOutputDerivatives");
        fmu->doStep                    = (fmi2DoStepTYPE*)                getAdr(&s, h, "fmiDoStep");
        fmu->cancelStep                = (fmi2CancelStepTYPE*)            getAdr(&s, h, "fmiCancelStep");
        fmu->getStatus                 = (fmi2GetStatusTYPE*)             getAdr(&s, h, "fmiGetStatus");
        fmu->getRealStatus             = (fmi2GetRealStatusTYPE*)         getAdr(&s, h, "fmiGetRealStatus");
        fmu->getIntegerStatus          = (fmi2GetIntegerStatusTYPE*)      getAdr(&s, h, "fmiGetIntegerStatus");
        fmu->getBooleanStatus          = (fmi2GetBooleanStatusTYPE*)      getAdr(&s, h, "fmiGetBooleanStatus");
        fmu->getStringStatus           = (fmi2GetStringStatusTYPE*)       getAdr(&s, h, "fmiGetStringStatus");
#else // FMI2 for Model Exchange
        fmu->enterEventMode            = (fmi2EnterEventModeTYPE*)        getAdr(&s, h, "fmiEnterEventMode");
        fmu->newDiscreteStates         = (fmi2NewDiscreteStatesTYPE*)     getAdr(&s, h, "fmiNewDiscreteStates");
        fmu->enterContinuousTimeMode   = (fmi2EnterContinuousTimeModeTYPE*) getAdr(&s, h, "fmiEnterContinuousTimeMode");
        fmu->completedIntegratorStep   = (fmi2CompletedIntegratorStepTYPE*) getAdr(&s, h, "fmiCompletedIntegratorStep");
        fmu->setTime                   = (fmi2SetTimeTYPE*)               getAdr(&s, h, "fmiSetTime");
        fmu->setContinuousStates       = (fmi2SetContinuousStatesTYPE*)   getAdr(&s, h, "fmiSetContinuousStates");
        fmu->getDerivatives            = (fmi2GetDerivativesTYPE*)        getAdr(&s, h, "fmiGetDerivatives");
        fmu->getEventIndicators        = (fmi2GetEventIndicatorsTYPE*)    getAdr(&s, h, "fmiGetEventIndicators");
        fmu->getContinuousStates       = (fmi2GetContinuousStatesTYPE*)   getAdr(&s, h, "fmiGetContinuousStates");
        fmu->getNominalsOfContinuousStates = (fmi2GetNominalsOfContinuousStatesTYPE*) getAdr(&s, h, "fmiGetNominalsOfContinuousStates");
#endif
    }
    return s;
}

static void printModelDescription(ModelDescription* md)
{
    Element* e = (Element*)md;
    int i;
    int n; // number of attributes
    const char** attributes = getAttributesAsArray(e, &n);
    Component* component;
    if (!attributes)
    {
        printf("ModelDescription printing aborted.");
        return;
    }
    printf("%s\n", getElementTypeName(e));
    for (i = 0; i < n; i += 2)
    {
        printf("  %s=%s\n", attributes[i], attributes[i+1]);
    }
    free((void*)attributes);
#ifdef FMI_COSIMULATION
    component = getCoSimulation(md);
    if (!component)
    {
        printf("error: No CoSimulation element found in model description. This FMU is not for Co-Simulation.\n");
        exit(EXIT_FAILURE);
    }
#else // FMI_MODEL_EXCHANGE
    component = getModelExchange(md);
    if (!component)
    {
        printf("error: No ModelExchange element found in model description. This FMU is not for Model Exchange.\n");
        exit(EXIT_FAILURE);
    }
#endif
    printf("%s\n", getElementTypeName((Element*)component));
    attributes = getAttributesAsArray((Element*)component, &n);
    if (!attributes)
    {
        printf("ModelDescription printing aborted.");
        return;
    }
    for (i = 0; i < n; i += 2)
    {
        printf("  %s=%s\n", attributes[i], attributes[i+1]);
    }
    free((void*)attributes);
}

FMU* loadFMU(const char* fmuFileName)
{
    char* fmuPath;
    char* xmlPath;
    char* dllPath;
    const char* modelId;
    FMU* fmu = (FMU*) malloc(sizeof(FMU));
    // Initialize FMU struct
    fmu->lenLoggedPorts = 0;
    fmu->loggedPorts = NULL;
    // get absolute path to FMU, NULL if not found
    fmuPath = getFmuPath(fmuFileName);
    if (!fmuPath) exit(EXIT_FAILURE);
    // unzip the FMU to the tmpPath directory
    fmu->tmpPath = getTmpPath();
    if (!unzip(fmuPath, fmu->tmpPath)) exit(EXIT_FAILURE);
    // parse tmpPath\modelDescription.xml
    xmlPath = calloc(sizeof(char), strlen(fmu->tmpPath) + strlen(XML_FILE) + 1);
    sprintf(xmlPath, "%s%s", fmu->tmpPath, XML_FILE);
    fmu->modelDescription = parse(xmlPath);
    free(xmlPath);
    if (!fmu->modelDescription) exit(EXIT_FAILURE);
#if DEBUG
    printModelDescription(fmu->modelDescription);
#endif
#ifdef FMI_COSIMULATION
    modelId = getAttributeValue((Element*)getCoSimulation(fmu->modelDescription), att_modelIdentifier);
#else // FMI_MODEL_EXCHANGE
    modelId = getAttributeValue((Element*)getModelExchange(fmu.modelDescription), att_modelIdentifier);
#endif
    // load the FMU dll
    dllPath = calloc(sizeof(char), strlen(fmu->tmpPath) + strlen(DLL_DIR)
                     + strlen(modelId) +  strlen(DLL_SUFFIX) + 1);
    sprintf(dllPath,"%s%s%s%s", fmu->tmpPath, DLL_DIR, modelId, DLL_SUFFIX);
    if (!loadDll(dllPath, fmu))
    {
        free(dllPath);
        // try the alternative directory and suffix
        dllPath = calloc(sizeof(char), strlen(fmu->tmpPath) + strlen(DLL_DIR2)
                         + strlen(modelId) +  strlen(DLL_SUFFIX2) + 1);
        sprintf(dllPath,"%s%s%s%s", fmu->tmpPath, DLL_DIR2, modelId, DLL_SUFFIX2);
        if (!loadDll(dllPath, fmu)) exit(EXIT_FAILURE);
    }
#if DEBUG
    printf("-----------------------------------------------\n");
#endif
    free(dllPath);
    free(fmuPath);
    return fmu;
}

void unloadFMU(FMU* fmu)
{
    free(fmu);
}
void deleteUnzippedFiles(char* fmuTempPath)
{
    char* cmd = (char*)calloc(15 + strlen(fmuTempPath), sizeof(char));
#if WINDOWS
    sprintf(cmd, "rmdir /S /Q %s", fmuTempPath);
#else
    sprintf(cmd, "rm -rf %s", fmuTempPath);
#endif
    system(cmd);
    free(fmuTempPath);
    free(cmd);
}

static void doubleToCommaString(char* buffer, double r)
{
    char* comma;
    sprintf(buffer, "%.16g", r);
    comma = strchr(buffer, '.');
    if (comma) *comma = ',';
}

// output time and all variables in CSV format
// if separator is ',', columns are separated by ',' and '.' is used for floating-point numbers.
// otherwise, the given separator (e.g. ';' or '\t') is to separate columns, and ',' is used
// as decimal dot in floating-point numbers.
void outputRow(FMU* fmu, fmi2Component c, double time, FILE* file, char separator, fmi2Boolean header)
{
    int k;
    fmi2Real r;
    fmi2Integer i;
    fmi2Boolean b;
    fmi2String s;
    fmi2ValueReference vr;
    int n;
    if (fmu->lenLoggedPorts > 0 )
    {
        n = fmu->lenLoggedPorts;
    }
    else
    {
        n = getScalarVariableSize(fmu->modelDescription);
    }
    char buffer[32];
    // print first column
    if (header)
    {
        fprintf(file, "time");
    }
    else
    {
        if (separator==',')
            fprintf(file, "%.16g", time);
        else
        {
            // separator is e.g. ';' or '\t'
            doubleToCommaString(buffer, time);
            fprintf(file, "%s", buffer);
        }
    }
    ScalarVariable* sv;
    // print all other columns
    for (k = 0; k < n; k++)
    {
        if (fmu->lenLoggedPorts > 0)
        {
            sv = getVariable(fmu->modelDescription, fmu->loggedPorts[k].name);
        }
        else
        {
            sv = getScalarVariable(fmu->modelDescription, k);
        }
        if (header)
        {
            // output names only
            if (separator == ',')
            {
                // treat array element, e.g. print a[1, 2] as a[1.2]
                const char* s = getAttributeValue((Element*)sv, att_name);
                fprintf(file, "%c", separator);
                while (*s)
                {
                    if (*s != ' ')
                    {
                        fprintf(file, "%c", *s == ',' ? '.' : *s);
                    }
                    s++;
                }
            }
            else
            {
                fprintf(file, "%c%s", separator, getAttributeValue((Element*)sv, att_name));
            }
        }
        else
        {
            // output values
            vr = getValueReference(sv);
            switch (getElementType(getTypeSpec(sv)))
            {
                case elm_Real:
                    fmu->getReal(c, &vr, 1, &r);
                    if (separator == ',')
                    {
                        fprintf(file, ",%.16g", r);
                    }
                    else
                    {
                        // separator is e.g. ';' or '\t'
                        doubleToCommaString(buffer, r);
                        fprintf(file, "%c%s", separator, buffer);
                    }
                    break;
                case elm_Integer:
                case elm_Enumeration:
                    fmu->getInteger(c, &vr, 1, &i);
                    fprintf(file, "%c%d", separator, i);
                    break;
                case elm_Boolean:
                    fmu->getBoolean(c, &vr, 1, &b);
                    fprintf(file, "%c%d", separator, b);
                    break;
                case elm_String:
                    fmu->getString(c, &vr, 1, &s);
                    fprintf(file, "%c%s", separator, s);
                    break;
                default:
                    fprintf(file, "%cNoValueForType=%d", separator, getElementType(getTypeSpec(sv)));
            }
        }
    } // for
    // terminate this row
    fprintf(file, "\n");
}

static const char* fmi2StatusToString(fmi2Status status)
{
    switch (status)
    {
        case fmi2OK:
            return "ok";
        case fmi2Warning:
            return "warning";
        case fmi2Discard:
            return "discard";
        case fmi2Error:
            return "error";
        case fmi2Fatal:
            return "fatal";
#ifdef FMI_COSIMULATION
        case fmi2Pending:
            return "fmi2Pending";
#endif
        default:
            return "?";
    }
}

// search a fmu for the given variable, matching the type specified.
// return NULL if not found
static ScalarVariable* getSV(FMU* fmu, char type, fmi2ValueReference vr)
{
    int i;
    int n = getScalarVariableSize(fmu->modelDescription);
    Elm tp;
    switch (type)
    {
        case 'r':
            tp = elm_Real;
            break;
        case 'i':
            tp = elm_Integer;
            break;
        case 'b':
            tp = elm_Boolean;
            break;
        case 's':
            tp = elm_String;
            break;
        default :
            tp = elm_BAD_DEFINED;
    }
    for (i = 0; i < n; i++)
    {
        ScalarVariable* sv = getScalarVariable(fmu->modelDescription ,i);
        if (vr == getValueReference(sv) && tp == getElementType(getTypeSpec(sv)))
        {
            return sv;
        }
    }
    return NULL;
}

// replace e.g. #r1365# by variable name and ## by # in message
// copies the result to buffer
static void replaceRefsInMessage(const char* msg, char* buffer, int nBuffer, FMU* fmu)
{
    int i = 0; // position in msg
    int k = 0; // position in buffer
    int n;
    char c = msg[i];
    while (c != '\0' && k < nBuffer)
    {
        if (c != '#')
        {
            buffer[k++] = c;
            i++;
            c = msg[i];
        }
        else
        {
            char* end = strchr(msg + i + 1, '#');
            if (!end)
            {
                printf("unmatched '#' in '%s'\n", msg);
                buffer[k++] = '#';
                break;
            }
            n = end - (msg + i);
            if (n == 1)
            {
                // ## detected, output #
                buffer[k++] = '#';
                i += 2;
                c = msg[i];
            }
            else
            {
                char type = msg[i + 1]; // one of ribs
                fmi2ValueReference vr;
                int nvr = sscanf(msg + i + 2, "%u", &vr);
                if (nvr == 1)
                {
                    // vr of type detected, e.g. #r12#
                    ScalarVariable* sv = getSV(fmu, type, vr);
                    const char* name = sv ? getAttributeValue((Element*)sv, att_name) : "?";
                    sprintf(buffer + k, "%s", name);
                    k += strlen(name);
                    i += (n+1);
                    c = msg[i];
                }
                else
                {
                    // could not parse the number
                    printf("illegal value reference at position %d in '%s'\n", i + 2, msg);
                    buffer[k++] = '#';
                    break;
                }
            }
        }
    } // while
    buffer[k] = '\0';
}

#define MAX_MSG_SIZE 2048
void fmuLogger(void* componentEnvironment, fmi2String instanceName, fmi2Status status,
               fmi2String category, fmi2String message, ...)
{
    FMU* fmu = (FMU*)componentEnvironment;
    char msg[MAX_MSG_SIZE];
    char* copy;
    va_list argp;
    // replace C format strings
    va_start(argp, message);
    vsprintf(msg,
             message,
             argp);
    va_end(argp);
    // replace e.g. ## and #r12#
    copy = strdup(msg);
    replaceRefsInMessage(copy, msg, MAX_MSG_SIZE, fmu);
    free(copy);
    // print the final message
    if (!instanceName) instanceName = "?";
    if (!category) category = "?";
    printf("%s %s (%s): %s\n", fmi2StatusToString(status), instanceName, category, msg);
}

int error(const char* message)
{
    printf("%s\n", message);
    return -1;
}

void parseArguments(int argc, char* argv[], double* tEnd, double* h,
                    int* loggingOn, int* benchmarkOn)
{
    if (argc > 1)
    {
        if (sscanf(argv[1],"%lf", tEnd) != 1)
        {
            printf("error: The given end time (%s) is not a number\n", argv[1]);
            exit(EXIT_FAILURE);
        }
    }
    if (argc > 2)
    {
        if (sscanf(argv[2],"%lf", h) != 1)
        {
            printf("error: The given stepsize (%s) is not a number\n", argv[2]);
            exit(EXIT_FAILURE);
        }
    }
    if (argc > 3)
    {
        if (sscanf(argv[3],"%d", loggingOn) != 1 || *loggingOn < 0 || *loggingOn > 1)
        {
            printf("error: The given logging flag (%s) is not boolean\n", argv[3]);
            exit(EXIT_FAILURE);
        }
    }
    if (argc > 4)
    {
        if (sscanf(argv[4],"%d", benchmarkOn) != 1 || *benchmarkOn < 0 || *benchmarkOn > 1)
        {
            printf("error: The given benchmark flag (%s) is not boolean\n", argv[4]);
            exit(EXIT_FAILURE);
        }
    }
}

void printHelp()
{
    printf("command syntax: <tEnd> <h> <loggingOn> <benchmarkOn>\n");
    printf("   <tEnd> ......... end  time of simulation,     optional, defaults to 1.0 sec\n");
    printf("   <h> ............ step size of simulation,     optional, defaults to 0.1 sec\n");
    printf("   <loggingOn> .... 1 to activate logging,       optional, defaults to 0\n");
    printf("   <benchmarkOn> .. 1 to activate benchmarking,  optional, defaults to 0\n");
}

int simInstantiate(FMU* fmu, fmi2Boolean loggingOn)
{
    const char* guid;                       // global unique id of the fmu
    const char* instanceName;               // instance name
    char* fmuResourceLocation = getTempResourcesLocation(fmu->tmpPath); // path to the fmu resources as URL, "file://C:\QTronic\sales"
    fmi2CallbackFunctions callbacksGen = {fmuLogger, calloc, free, NULL, fmu};
    memcpy(&fmu->callbacksGen, &callbacksGen, sizeof(fmi2CallbackFunctions));
    // , calloc, free, NULL, fmu}
    fmi2Boolean visible = fmi2False;        // no simulator user interface
    guid = getAttributeValue((Element*)fmu->modelDescription, att_guid);
    instanceName = getAttributeValue((Element*)getCoSimulation(fmu->modelDescription), att_modelIdentifier);
    fmu->c = fmu->instantiate(instanceName, fmi2CoSimulation, guid, fmuResourceLocation,
                              &fmu->callbacksGen, visible, loggingOn);
    free(fmuResourceLocation);
    if (!fmu->c) return error("could not instantiate model");
}

int simInitialize(FMU* fmu, fmi2Real tStart, fmi2Real tEnd, size_t nCategories, const fmi2String categories[], fmi2Boolean resultOnFile)
{
    Element* defaultExp;
    fmi2Boolean toleranceDefined = fmi2False;  // true if model description define tolerance
    fmi2Real tolerance = 0;                    // used in setting up the experiment
    fmi2Status fmi2Flag;                    // return code of the fmu functions
    ValueStatus vs = valueMissing;
    if (nCategories > 0)
    {
        fmi2Flag = fmu->setDebugLogging(fmu->c, fmi2True, nCategories, categories);
        if (fmi2Flag > fmi2Warning)
        {
            return error("could not initialize model; failed FMI set debug logging");
        }
    }
    defaultExp = getDefaultExperiment(fmu->modelDescription);
    if (defaultExp) tolerance = getAttributeDouble(defaultExp, att_tolerance, &vs);
    if (vs == valueDefined)
    {
        toleranceDefined = fmi2True;
    }
    fmi2Flag = fmu->setupExperiment(fmu->c, toleranceDefined, tolerance, tStart, fmi2True, tEnd);
    if (fmi2Flag > fmi2Warning)
    {
        return error("could not initialize model; failed FMI setup experiment");
    }
    fmi2Flag = fmu->enterInitializationMode(fmu->c);
    if (fmi2Flag > fmi2Warning)
    {
        return error("could not initialize model; failed FMI enter initialization mode");
    }
    fmi2Flag = fmu->exitInitializationMode(fmu->c);
    if (fmi2Flag > fmi2Warning)
    {
        return error("could not initialize model; failed FMI exit initialization mode");
    }
    if (resultOnFile)
    {
        fmu->resultOnFile = fmi2True;
        const char* modelId = getAttributeValue((Element*)getCoSimulation(fmu->modelDescription), att_modelIdentifier);
        char* results = calloc(sizeof(char), 2048);
        strcat(results, modelId);
        strcat(results, "_");
        strcat(results, RESULT_FILE);
        // open result file
        if (!(fmu->file = fopen(results, "w")))
        {
            printf("could not write %s because:\n", RESULT_FILE);
            printf("    %s\n", strerror(errno));
            free(results);
            return -1; // failure
        }
        free(results);
        // output solution for time t0
        outputRow(fmu, fmu->c, tStart, fmu->file, SEPARATOR, fmi2True);  // output column names
        outputRow(fmu, fmu->c, tStart, fmu->file, SEPARATOR, fmi2False); // output values
    }
    else
    {
        fmu->resultOnFile = fmi2False;
    }
}

// Helper function: Build an array of ports by providing an array of port names
void buildEventPortsByName(FMU* fmu, fmi2Port* ports, int nPorts, const fmi2String nameOfPorts[])
{
    int i = 0;
    for (i = 0; i < nPorts; ++i)
    {
        ScalarVariable* sv = getVariable(fmu->modelDescription, nameOfPorts[i]);
        ports[i].vref = getValueReference(sv);
        switch (getElementType(getTypeSpec(sv)))
        {
            case elm_Real:
                ports[0].type = Real;
                break;
            case elm_Integer:
            case elm_Enumeration:
                ports[0].type = Integer;
                break;
            case elm_Boolean:
                ports[0].type = Boolean;
                break;
            case elm_String:
                ports[0].type = String;
                break;
            default:
                break;
        }
        ports[i].callbackCondition = NULL;
    }
}

// Please free the result after use.
fmi2Port* buildPortByName(FMU* fmu, const fmi2String name)
{
    ScalarVariable* sv = getVariable(fmu->modelDescription, name);
    if (sv == NULL)
    {
        printf("fmi2Port %s not found!\n", name);
        return NULL;
    }
    fmi2Port* port = (fmi2Port*) malloc(sizeof(fmi2Port));
    port->vref = getValueReference(sv);
    port->name = (char*) malloc(sizeof(char)*strlen(name));
    port->callbackCondition = NULL;
    strcpy(port->name, name);
    switch (getElementType(getTypeSpec(sv)))
    {
        case elm_Real:
            port->type = Real;
            break;
        case elm_Integer:
        case elm_Enumeration:
            port->type = Integer;
            break;
        case elm_Boolean:
            port->type = Boolean;
            break;
        case elm_String:
            port->type = String;
            break;
        default:
            break;
    }
    return port;
}
/**
 * Destroy a port
 * @param port Pointer to the port to free
 */
void destroyPort(fmi2Port* port)
{
    if (port)
        free(port->name);
    free(port);
}
/**
 * Retrives a value from the FMU and saves it inside the port specified.
 * @param fmu FMU to get the value
 * @param port fmi2Port pointer, it must contains a valid name
 */
void simGet(FMU* fmu, fmi2Port* port)
{
    // TODO: If the informations about vr and type are already available don't look up in the XML
    ScalarVariable* sv = getVariable(fmu->modelDescription, port->name);
    if (sv == NULL)
    {
        printf("fmi2Port %s not found!\n", port->name);
        return;
    }
    fmi2ValueReference vr = getValueReference(sv);
    switch (getElementType(getTypeSpec(sv)))
    {
        case elm_Real:
            fmu->getReal(fmu->c, &vr, 1, &port->rvalue);
            break;
        case elm_Integer:
        case elm_Enumeration:
            fmu->getInteger(fmu->c, &vr, 1, &port->ivalue);
            break;
        case elm_Boolean:
            fmu->getBoolean(fmu->c, &vr, 1, &port->bvalue);
            break;
        case elm_String:
            fmu->getString(fmu->c, &vr, 1, &port->svalue);
            break;
        default:
            break;
    }
}
/**
 * Set the port passed inside the FMU
 * @param fmu FMU pointer
 * @param port fmi2Port pointer, it must contains a valid name
 */
void simSet(FMU* fmu, fmi2Port* port)
{
    ScalarVariable* sv = getVariable(fmu->modelDescription, port->name);
    if (sv == NULL)
    {
        printf("fmi2Port %s not found!\n", port->name);
        return;
    }
    fmi2ValueReference vr = getValueReference(sv);
    switch (getElementType(getTypeSpec(sv)))
    {
        case elm_Real:
            fmu->setReal(fmu->c, &vr, 1, &port->rvalue);
            break;
        case elm_Integer:
        case elm_Enumeration:
            fmu->setInteger(fmu->c, &vr, 1, &port->ivalue);
            break;
        case elm_Boolean:
            fmu->setBoolean(fmu->c, &vr, 1, &port->bvalue);
            break;
        case elm_String:
            fmu->setString(fmu->c, &vr, 1, &port->svalue);
            break;
        default:
            break;
    }
}

fmi2Status simDoStepUntilDiscontinuity(FMU* fmu, fmi2Real time, fmi2Real step, fmi2Port* ports, size_t len_ports, fmi2Real* nextEventTime)
{
    fmu->t_start = clock();
    fmi2Status fmi2Flag = fmu->simulateUntilDiscontinuity(fmu->c, time, step, fmi2True, ports, len_ports, nextEventTime);
    fmu->t_total += clock() - fmu->t_start;

    simLogFMU(fmu, *nextEventTime);

    return fmi2Flag;
}

fmi2Status simDoStepUntilRead(FMU* fmu, fmi2Real time, fmi2Real step, fmi2Port* ports, size_t len_ports, fmi2Real* nextEventTime)
{
    fmu->t_start = clock();
    fmi2Status fmi2Flag = fmu->simulateUntilDiscontinuity(fmu->c, time, step, fmi2True, ports, len_ports, nextEventTime);
    fmu->t_total += clock() - fmu->t_start;
    simLogFMU(fmu, *nextEventTime);

    return fmi2Flag;
}

int simDoStep(FMU* fmu, fmi2Real time, fmi2Real step)
{
    fmu->t_start = clock();
    fmi2Status fmi2Flag = fmu->doStep(fmu->c, time, step, fmi2True);
    fmu->t_total += clock() - fmu->t_start;

    if (fmi2Flag == fmi2Discard)
    {
        fmi2Boolean b;
        // check if model requests to end simulation
        if (fmi2OK != fmu->getBooleanStatus(fmu->c, fmi2Terminated, &b))
        {
            return error("could not complete simulation of the model. getBooleanStatus return other than fmi2OK");
        }
        if (b == fmi2True)
        {
            return error("the model requested to end the simulation");
        }
        return error("could not complete simulation of the model");
    }
    if (fmi2Flag != fmi2OK)
    {
        return error("could not complete simulation of the model");
    }
    simLogFMU(fmu, time+step);
}

void simLogFMU(FMU* fmu, fmi2Real time)
{
    if (fmu->resultOnFile)
    {
        outputRow(fmu, fmu->c, floor(time / 0.0001 + 0.0005 ) * 0.0001, fmu->file, SEPARATOR, fmi2False); // output values for this step
    }
}

void simTerminate(FMU* fmu)
{
    // Write log file
    plotFMUResultFile(fmu);

    // end simulation
    fmu->terminate(fmu->c);
    fmu->freeInstance(fmu->c);
    if (fmu->resultOnFile)
    {
        fclose(fmu->file);
    }
    // release FMU
#ifdef _MSC_VER
    FreeLibrary(fmu.dllHandle);
#else
    dlclose(fmu->dllHandle);
#endif
    freeModelDescription(fmu->modelDescription);
    //if (categories) free(categories);
    // delete temp files obtained by unzipping the FMU
    int i = 0;
    deleteUnzippedFiles(fmu->tmpPath);
    if (fmu->lenLoggedPorts)
    {
        for (i = 0; i < fmu->lenLoggedPorts; ++i)
        {
            destroyPort(&fmu->loggedPorts[i]);
        }
    }
    unloadFMU(fmu);
}
/**
 * Add a port to log, it creates a .csv file to store all the intermediate results
 * @param fmu FMU to log, the port must be exist inside this FMU
 * @param name Name of the port to log
 */
void simAddLoggedPortByName(FMU* fmu, char* name)
{
    // TODO: Create a .csv file only if necessary
    // Initialized loggedfmi2Ports
    fmi2Port* p = buildPortByName(fmu, name);
    if (p == NULL)
    {
        printf("No port found with name %s\n", name);
        return;
    }
    // New list
    fmi2Port* new = (fmi2Port*) malloc((fmu->lenLoggedPorts + 1) * sizeof(fmi2Port));
    memcpy(new, fmu->loggedPorts, fmu->lenLoggedPorts);
    memcpy(new + fmu->lenLoggedPorts, p, sizeof(fmi2Port));
    if (fmu->loggedPorts != NULL)
    {
        free(fmu->loggedPorts);
    }
    fmu->loggedPorts = new;
    fmu->lenLoggedPorts++;
}
/**
 * Plots the first 2 columns of data to a .png file. It retrives from the .csv file of the FMU passed the values.
 * @param fmu FMU to plot, it must contains a valid FILE pointer.
 */
void plotFMUResultFile(FMU* fmu)
{
    if (fmu->resultOnFile)
    {
        const char* modelId = getAttributeValue((Element*)getCoSimulation(fmu->modelDescription), att_modelIdentifier);
        char* results = calloc(sizeof(char), 2048);
        strcat(results, modelId);
        strcat(results, "_");
        strcat(results, RESULT_FILE);
        fflush(fmu->file);
        gnuplot_ctrl* h ;
        h = gnuplot_init() ;
        gnuplot_set_xlabel(h, "Time");
        gnuplot_set_ylabel(h, "Vars");
        gnuplot_cmd(h, "set datafile separator \",\"");
        gnuplot_cmd(h, "set key autotitle columnhead");
        gnuplot_cmd(h, "set terminal pngcairo");
        gnuplot_cmd(h, "set output \"%s.png\"", modelId);
        gnuplot_cmd(h, "plot '%s' u 1:2 w steps lw 2", results);
        gnuplot_close(h);
        free(results);

        // Prevent a rewrite on the same file
        fmu->resultOnFile = fmi2False;
    }
}
// Return a new system, user have to call simTerminateSystem before exit.
System* simBuildSystem()
{
    System* s = (System*) malloc(sizeof(System));
    s->len = 0;
    return s;
}

void simTerminateSystem(System* s)
{
    // Terminate every FMU inside the system
    int i;
    for (i = 0; i < s->len; i++)
    {
        simTerminate(s->fmus+i);
    }
    free(s);
}

void simAddToSystem(System* s, FMU* fmu)
{
    // New list
    FMU* new_fmus = (FMU*) malloc((s->len + 1) * sizeof(FMU));
    memcpy(new_fmus, s->fmus, s->len);
    memcpy(new_fmus + s->len, fmu, sizeof(FMU));
    if (s->fmus != NULL)
    {
        free(s->fmus);
    }
    s->fmus = new_fmus;
    s->len++;
}
void simLogSystem(System* s, fmi2Real time)
{
    // For every FMUs inside the system
    int i;
    for(i=0; i<s->len; ++i)
    {
        simLogFMU(s->fmus+1, time);
    }
}