/* -------------------------------------------------------------------------
 * sim_support.h
 * Functions used by the FMU simulations fmusim_me and fmusim_cs.
 * Original work Copyright QTronic GmbH. All rights reserved.
 * Modified work Giovanni Liboni <giovanni.liboni@inria.fr>
 * -------------------------------------------------------------------------*/
#ifndef SIM_SUPPORT_H
#define SIM_SUPPORT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "fmi2.h"
#include "gnuplot_i.h"
#include "time.h"
#include <math.h>

#ifndef _MSC_VER
#define MAX_PATH 1024
#include <unistd.h>  // mkdtemp()
#include <dlfcn.h> //dlsym()
#include "fmi2FunctionTypes.h"
#endif

extern FMU fmu;

#if WINDOWS
// Used 7z options, version 4.57:
// -x   Extracts files from an archive with their full paths in the current dir, or in an output dir if specified
// -aoa Overwrite All existing files without prompt
// -o   Specifies a destination directory where files are to be extracted
#define UNZIP_CMD "7z x -aoa -o"
#else
// -o   Overwrite existing files without prompting
// -d   The directory in which to write files.
#define UNZIP_CMD "unzip -o -d "
#endif
#define XML_FILE  "modelDescription.xml"
#ifndef RESULT_FILE
#define RESULT_FILE "result.csv"
#endif
#define BUFSIZE 4096
#if WINDOWS
#ifdef _WIN64
#define DLL_DIR   "binaries\\win64\\"
#define DLL_DIR2   "binaries\\win64\\"
#else
#define DLL_DIR   "binaries\\win32\\"
#define DLL_DIR2   "binaries\\win32\\"
#endif

#define DLL_SUFFIX ".dll"
#define DLL_SUFFIX2 ".dll"

#else
#if __APPLE__

// Use these for platforms other than OpenModelica
#define DLL_DIR   "binaries/darwin64/"
#define DLL_SUFFIX ".dylib"

// Use these for OpenModelica 1.8.1
#define DLL_DIR2   "binaries/darwin-x86_64/"
#define DLL_SUFFIX2 ".so"


#else /*__APPLE__*/
// Linux
#ifdef __x86_64
#define DLL_DIR   "binaries/linux64/"
#define DLL_DIR2   "binaries/linux32/"
#else
// It may be necessary to compile with -m32, see ../Makefile
#define DLL_DIR   "binaries/linux32/"
#define DLL_DIR2   "binaries/linux64/"
#endif /*__x86_64*/
#define DLL_SUFFIX ".so"
#define DLL_SUFFIX2 ".so"
#endif /*__APPLE__*/
#endif /*WINDOWS*/

#define RESOURCES_DIR "resources\\"

// return codes of the 7z command line tool
#define SEVEN_ZIP_NO_ERROR 0 // success
#define SEVEN_ZIP_WARNING 1  // e.g., one or more files were locked during zip
#define SEVEN_ZIP_ERROR 2
#define SEVEN_ZIP_COMMAND_LINE_ERROR 7
#define SEVEN_ZIP_OUT_OF_MEMORY 8
#define SEVEN_ZIP_STOPPED_BY_USER 255

/*
 * Callbacks
 */
void fmuLogger(fmi2Component c, fmi2String instanceName, fmi2Status status, fmi2String category, fmi2String message, ...);

/*
 * Logger
 */
void simLogFMU(FMU* fmu, fmi2Real time);
void outputRow(FMU* fmu, fmi2Component c, double time, FILE* file, char separator, fmi2Boolean header);
void simLogSystem(System* s, fmi2Real time);
void simAddLoggedPortByName(FMU* fmu, char* name);

void plotFMUResultFile(FMU* fmu);

/*
 * Utils
 */
int unzip(const char* zipPath, const char* outPath);
void parseArguments(int argc, char* argv[], double* tEnd, double* h,
                    int* loggingOn, int* benchmarkOn);
void deleteUnzippedFiles(char* fmuTempPath);
int error(const char* message);
void printHelp();
char* getTempResourcesLocation(char* path); // caller has to free the result
FMU* loadFMU(const char* fmuFileName);
void unloadFMU(FMU* fmu);

/*
 * Simulator API
 */
// FMU specific
void buildEventPortsByName(FMU* fmu, fmi2Port* ports, int nPorts, const fmi2String nameOfPorts[]);
void simSet(FMU* fmu, fmi2Port* port);
void simGet(FMU* fmu, fmi2Port* port);
int simInstantiate(FMU* fmu, fmi2Boolean loggingOn);
int simInitialize(FMU* fmu, fmi2Real tStart, fmi2Real tEnd, size_t nCategories, const fmi2String categories[], fmi2Boolean resultOnFile);
int simDoStep(FMU* fmu, fmi2Real time, fmi2Real step);
void simTerminate(FMU* fmu);

// System specific
System* simBuildSystem();
void simAddToSystem(System* s, FMU* fmu);
void simTerminateSystem(System* s);

fmi2Status simDoStepUntilDiscontinuity(FMU* fmu, fmi2Real time, fmi2Real step, fmi2Port* ports, size_t len_ports, fmi2Real* nextEventTime);
fmi2Status simDoStepUntilRead(FMU* fmu, fmi2Real time, fmi2Real step, fmi2Port* ports, size_t len_ports, fmi2Real* nextEventTime);

#endif