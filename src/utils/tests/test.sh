#!/usr/bin/env bash
# <exec> tEnd #cycles stepSize loggingOn benchMarkOutput
TEND=200
for ((i=1;i<=500;i++)) do
    ./sim_data_generator_resistor_fidel $TEND $i 0.01 0 1 >> fidel_001.csv
done
for ((i=1;i<=500;i++)) do
    ./sim_data_generator_resistor_normal $TEND $i 0.01 0 1 >> normal_001.csv
done

for ((i=1;i<=500;i++)) do
    ./sim_data_generator_resistor_fidel $TEND $i 0.001 0 1 >> fidel_0001.csv
done
for ((i=198;i<=500;i++)) do
    ./sim_data_generator_resistor_normal $TEND $i 0.001 0 1 >> normal_0001.csv
done

for ((i=1;i<=500;i++)) do
    ./sim_data_generator_resistor_fidel $TEND $i 0.0001 0 1 >> fidel_00001.csv
done
for ((i=1;i<=500;i++)) do
    ./sim_data_generator_resistor_normal $TEND $i 0.0001 0 1 >> normal_00001.csv
done